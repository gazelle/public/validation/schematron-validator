 --
 -- Copyright 2008 IHE International (http://www.ihe.net)
 --
 -- Licensed under the Apache License, Version 2.0 (the "License");
 -- you may not use this file except in compliance with the License.
 -- You may obtain a copy of the License at
 --
 -- http://www.apache.org/licenses/LICENSE-2.0
 --
 -- Unless required by applicable law or agreed to in writing, software
 -- distributed under the License is distributed on an "AS IS" BASIS,
 -- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 -- See the License for the specific language governing permissions and
 -- limitations under the License.
 --

 
--
--  SQL Functions for Gazelle databases
--
DROP FUNCTION IF EXISTS comma_cat(text, text) CASCADE ;
CREATE FUNCTION comma_cat(text, text) RETURNS text AS $_$select case WHEN $2 is null or $2 = '' THEN $1 WHEN $1 is null or $1 = '' THEN $2 ELSE $1 || ', ' || $2 END$_$ LANGUAGE sql; 

ALTER FUNCTION public.comma_cat(text, text) OWNER TO gazelle;

--
-- Name: list(text); Type: AGGREGATE; Schema: public; Owner: gazelle
--

CREATE AGGREGATE list ( BASETYPE = text, SFUNC = comma_cat, STYPE = text, INITCOND = '');


ALTER AGGREGATE public.list(text) OWNER TO gazelle;

 --
 -- Copyright 2008 IHE International (http://www.ihe.net)
 --
 -- Licensed under the Apache License, Version 2.0 (the "License");
 -- you may not use this file except in compliance with the License.
 -- You may obtain a copy of the License at
 --
 -- http://www.apache.org/licenses/LICENSE-2.0
 --
 -- Unless required by applicable law or agreed to in writing, software
 -- distributed under the License is distributed on an "AS IS" BASIS,
 -- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 -- See the License for the specific language governing permissions and
 -- limitations under the License.
 --

 
--
-- Data for Name: system; Type: TABLE DATA; Schema: public; Owner: gazelle
--



-- *****************************************************************************************************************************
--
-- INFORMATION : This file is a generated/template SQL file. This file is generated using values from the 'build-dev/prod.properties'
--
-- *****************************************************************************************************************************


-- General application properties
INSERT INTO "cmn_application_preference" (id, preference_name, preference_value, description, class_name) VALUES (1, 'application_url', 'http://localhost:8081/SchematronValidator', 'URL used to reach the application', 'java.lang.String');
INSERT INTO "cmn_application_preference" (id, preference_name, preference_value, description, class_name) VALUES (2, 'application_name', 'SchematronValidator', 'Application name', 'java.lang.String');
INSERT INTO "cmn_application_preference" (id, preference_name, preference_value, description, class_name) VALUES (3, 'application_admin_name', 'Eric Poiseau', 'Admin (name) used to manage the application', 'java.lang.String');
INSERT INTO "cmn_application_preference" (id, preference_name, preference_value, description, class_name) VALUES (4, 'application_admin_email', 'aberge@irisa.fr', 'Admin (email) used to manage the application', 'java.lang.String');
INSERT INTO "cmn_application_preference" (id, preference_name, preference_value, description, class_name) VALUES (5, 'application_email_account_for_history', 'net.gazelle.dev@gmail.com', 'Account (email address) where are sent all emails as BCC (for emails history/checking in)', 'java.lang.String');
INSERT INTO "cmn_application_preference" (id, preference_name, preference_value, description, class_name) VALUES (6, 'time_zone', 'UTC+01', 'Default time zone used by the application', 'java.lang.String');
INSERT INTO "cmn_application_preference" (id, preference_name, preference_value, description, class_name) VALUES (7, 'application_url_basename', 'SchematronValidator', 'URL basename used to reach the application', 'java.lang.String');
INSERT INTO "cmn_application_preference" (id, preference_name, preference_value, description, class_name) VALUES (8, 'google_analytics_code', '', 'Container for google analytics code (eg: "UA-2810172-2" for Gazelle) ', 'java.lang.String');
INSERT INTO "cmn_application_preference" (id, preference_name, preference_value, description, class_name) VALUES (9, 'application_issue_tracker_url', 'http://gazelle.ihe.net/jira', 'Issue Tracker Website URL - Used by some links in the application to report a problem (eg. of value = http://sumo.irisa.fr:8080/jira/ )', 'java.lang.String');
INSERT INTO "cmn_application_preference" (id, preference_name, preference_value, description, class_name) VALUES (10, 'application_gazelle_release_notes_url', 'releaseNotesUrl', 'Gazelle Release Notes URL - Used by some links in the application to detail each Gazelle Release : improvements, bug fixed (eg. of value = http://sumo.irisa.fr/europe2009/gazelle_release_notes.html )', 'java.lang.String');
INSERT INTO "cmn_application_preference" (id, preference_name, preference_value, description, class_name) VALUES (11, 'application_zone', 'EUROPE', 'Application zone (possible values :  "EUROPE", "NA" , "JAPAN") - Rendered pages depend on the zone (homepage, contacts, informations... )', 'java.lang.String');
INSERT INTO "cmn_application_preference" (id, preference_name, preference_value, description, class_name) VALUES (12, 'application_profile', 'dev', 'Application profile (possible values :  "dev", "prod") - Rendered pages/components depend on the profile value ', 'java.lang.String');
INSERT INTO "cmn_application_preference" (id, preference_name, preference_value, description, class_name) VALUES (13, 'application_build_time', 'December 20, 2010 - 02:46 PM', 'Application build time (Ant)', 'java.lang.String');
INSERT INTO "cmn_application_preference" (id, preference_name, preference_value, description, class_name) VALUES (14, 'application_db_drop_and_import', 'create-drop', 'This value does NOT change. It indicates if DB has been dropped and SQL imported during deployment ? ', 'java.lang.String');
INSERT INTO "cmn_application_preference" (id, preference_name, preference_value, description, class_name) VALUES (15, 'application_drools_loaded_at_startup', 'true', 'This value does NOT change. It indicates if Drools has beeen loaded at startup ? (to improve speed deployment)', 'java.lang.String');


-- Gazelle paths (on local server)  - Used to store files
INSERT INTO "cmn_application_preference" (id, preference_name, preference_value, description, class_name) VALUES (21, 'gazelle_home_path', '/opt/SchematronValidator_dev', 'ABSOLUTE PATH TO Gazelle home path. Here is where are stored generated datas, reports and other required binaries for application', 'java.lang.String');
INSERT INTO "cmn_application_preference" (id, preference_name, preference_value, description, class_name) VALUES (22, 'data_path', 'data', 'RELATIVE PATH TO gazelle_home_path. This value normally does NOT change. It corresponds to Gazelle data path inside "gazelle_home_path". Here is where data (invoices, integration statements... will be stored).', 'java.lang.String');
INSERT INTO "cmn_application_preference" (id, preference_name, preference_value, description, class_name) VALUES (23, 'reports_path', 'reports', 'RELATIVE PATH TO gazelle_home_path. This value normally does NOT change. It corresponds to Gazelle reports path inside "gazelle_home_path". Here is where are stored required reports', 'java.lang.String');
INSERT INTO "cmn_application_preference" (id, preference_name, preference_value, description, class_name) VALUES (24, 'bin_path', 'bin', 'RELATIVE PATH TO gazelle_home_path. This value normally does NOT change. It corresponds to Gazelle bin path inside "gazelle_home_path". Here is where are stored required binaries', 'java.lang.String');
INSERT INTO "cmn_application_preference" (id, preference_name, preference_value, description, class_name) VALUES (25, 'mif_root_directory', '/opt/SchematronValidator_dev', 'absolute path to root directory for Mif and Vocab folders', 'java.lang.String');



-- *****************************************************************************************************************************
--
-- INFORMATION : This file is a generated/template SQL file. This file is generated using values from the 'build-dev/prod.properties'
--
-- *****************************************************************************************************************************




--
-- PostgreSQL database dump complete
--

 --
 -- Copyright 2010 IHE International (http://www.ihe.net)
 --
 -- Licensed under the Apache License, Version 2.0 (the "License");
 -- you may not use this file except in compliance with the License.
 -- You may obtain a copy of the License at
 --
 -- http://www.apache.org/licenses/LICENSE-2.0
 --
 -- Unless required by applicable law or agreed to in writing, software
 -- distributed under the License is distributed on an "AS IS" BASIS,
 -- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 -- See the License for the specific language governing permissions and
 -- limitations under the License.
 --


INSERT INTO cmn_number_of_results_per_page ( id , number_of_results_per_page ) VALUES (5, 5);
INSERT INTO cmn_number_of_results_per_page ( id , number_of_results_per_page ) VALUES (10, 10);
INSERT INTO cmn_number_of_results_per_page ( id , number_of_results_per_page ) VALUES (20, 20);
INSERT INTO cmn_number_of_results_per_page ( id , number_of_results_per_page ) VALUES (50, 50);
INSERT INTO cmn_number_of_results_per_page ( id , number_of_results_per_page ) VALUES (100, 100);
INSERT INTO cmn_number_of_results_per_page ( id , number_of_results_per_page ) VALUES (200, 200);
INSERT INTO cmn_number_of_results_per_page ( id , number_of_results_per_page ) VALUES (500, 500);
INSERT INTO cmn_number_of_results_per_page ( id , number_of_results_per_page ) VALUES (1000, 1000);


SELECT pg_catalog.setval('cmn_number_of_results_per_page_id_seq', 1000 , true);


--
-- PostgreSQL database dump complete
--

--
 -- Copyright 2010 IHE International (http://www.ihe.net)
 --
 -- Licensed under the Apache License, Version 2.0 (the "License");
 -- you may not use this file except in compliance with the License.
 -- You may obtain a copy of the License at
 --
 -- http://www.apache.org/licenses/LICENSE-2.0
 --
 -- Unless required by applicable law or agreed to in writing, software
 -- distributed under the License is distributed on an "AS IS" BASIS,
 -- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 -- See the License for the specific language governing permissions and
 -- limitations under the License.
 --




--
-- Data for Name: sch_validator_object_type; Type: TABLE DATA; Schema: public; Owner: gazelle
--
INSERT INTO sch_validator_object_type (id, keyword, label_to_display, description) VALUES (1, 'HL7v3', 'HL7v3', 'messages HL7v3');
INSERT INTO sch_validator_object_type (id, keyword, label_to_display, description) VALUES (2, 'CDA', 'CDA', 'CDA documents');
INSERT INTO sch_validator_object_type (id, keyword, label_to_display, description) VALUES (3, 'AM', 'Audit Message', 'Audit Messages for ATNA profile');
INSERT INTO sch_validator_object_type (id, keyword, label_to_display, description) VALUES (4, 'SAML', 'Assertion (SAML)', 'SAML');


SELECT pg_catalog.setval('sch_validator_object_type_id_seq', 4 , true);


--
-- PostgreSQL database dump complete
-- --
 -- Copyright 2008 IHE International (http://www.ihe.net)
 --
 -- Licensed under the Apache License, Version 2.0 (the "License");
 -- you may not use this file except in compliance with the License.
 -- You may obtain a copy of the License at
 --
 -- http://www.apache.org/licenses/LICENSE-2.0
 --
 -- Unless required by applicable law or agreed to in writing, software
 -- distributed under the License is distributed on an "AS IS" BASIS,
 -- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 -- See the License for the specific language governing permissions and
 -- limitations under the License.
 --

--
-- Data for Name: instance_status; Type: TABLE DATA; Schema: public; Owner: gazelle
--

INSERT INTO revinfo(rev,revtstmp) VALUES (1,null);

SELECT pg_catalog.setval('hibernate_sequence', 1, true);

INSERT INTO "cmn_application_preference" (id, preference_name, preference_value, description, class_name) VALUES (30, 'cda_xsd_path', '/opt/SchematronValidator_dev/xsd/cda/CDA.xsd', 'path of the XSD for validating CDA documents', 'java.lang.String');
INSERT INTO "cmn_application_preference" (id, preference_name, preference_value, description, class_name) VALUES (31, 'epsos_cda_xsd_path', '/opt/SchematronValidator_dev/xsd/cda/CDA_extended.xsd', 'path of the XSD for validating epSOS CDA documents', 'java.lang.String');
INSERT INTO "cmn_application_preference" (id, preference_name, preference_value, description, class_name) VALUES (32, 'monitor_email', 'anne-gaelle.berge@inria.fr', 'Monitor s email', 'java.lang.String');
INSERT INTO "cmn_application_preference" (id, preference_name, preference_value, description, class_name) VALUES (33, 'monitor_name', 'Anne-Gaelle Berge', 'Monitor s email', 'java.lang.String');
INSERT INTO "user" (id, username, password_hash, enabled) VALUES (1, 'epoiseau', 'jdkxMNWCYdXNVlKhedMZsfhTntc=', true);
INSERT INTO "user" (id, username, password_hash, enabled) VALUES (2, 'aberge', 'jdkxMNWCYdXNVlKhedMZsfhTntc=', true);

SELECT pg_catalog.setval('user_id_seq', 2 , true);insert into "user_role" (id, name, conditional) values (1, 'admin', false);
insert into "user_role" (id, name, conditional) values (2, 'member', false);
insert into "user_role" (id, name, conditional) values (3, 'guest', true);

insert into "user_role_group" (role_id, member_of_role) values (1, 2);

SELECT pg_catalog.setval('user_role_id_seq', 3 , true);--
 -- Copyright 2010 IHE International (http://www.ihe.net)
 --
 -- Licensed under the Apache License, Version 2.0 (the "License");
 -- you may not use this file except in compliance with the License.
 -- You may obtain a copy of the License at
 --
 -- http://www.apache.org/licenses/LICENSE-2.0
 --
 -- Unless required by applicable law or agreed to in writing, software
 -- distributed under the License is distributed on an "AS IS" BASIS,
 -- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 -- See the License for the specific language governing permissions and
 -- limitations under the License.
 --




--
-- Data for Name: sch_validator_schematron; Type: TABLE DATA; Schema: public; Owner: gazelle
--
INSERT INTO sch_validator_schematron (id, name, version, label, last_changed, last_modifier_id, description, path, object_type_id, xsd_path, available) VALUES (1, 'XCPD ITI-55 request', '1.0', 'XCPD-request', null, null,'schematron to validate HL7v3 messages for XCPD profile (request for transaction ITI-55)', 'xcpd/PRPA_IN201305UV02.sch', 1, 'hl7v3/multicacheschemas/PRPA_IN201305UV02.xsd', true);
INSERT INTO sch_validator_schematron (id, name, version, label, last_changed, last_modifier_id, description, path, object_type_id, xsd_path, available) VALUES (2, 'APS', '1.0', 'APS.1.0', null, null,'schematron to validate APS Documents', 'ihe_pcc_all/aps.sch', 2,  NULL, false);
INSERT INTO sch_validator_schematron (id, name, version, label, last_changed, last_modifier_id, description, path, object_type_id, xsd_path, available) VALUES (4, 'APS', '2.0', 'APS.2.0', null, null,'schematron to validate APS Documents', 'ihe_pcc_all/aps2.sch', 2,  NULL, true);
INSERT INTO sch_validator_schematron (id, name, version, label, last_changed, last_modifier_id, description, path, object_type_id, xsd_path, available) VALUES (3, 'APE', '1.0', 'APE.1.0', null, null,'schematron to validate APE Documents', 'ihe_pcc_all/ape.sch', 2,  NULL, true);
INSERT INTO sch_validator_schematron (id, name, version, label, last_changed, last_modifier_id, description, path, object_type_id, xsd_path, available) VALUES (5, 'XCPD ITI-55 response', '1.0', 'XCPD-response', null, null,'schematron to validate HL7v3 messages for XCPD profile (response for transaction ITI-55)', 'xcpd/PRPA_IN201306UV02.sch', 1,  'hl7v3/multicacheschemas/PRPA_IN201306UV02.xsd', true);
INSERT INTO sch_validator_schematron (id, name, version, label, last_changed, last_modifier_id, description, path, object_type_id, xsd_path, available) VALUES (6, 'epSOS -APS', '2.0', 'epSOS - APS', null, null,'schematron to validate APS Documents', 'ihe_pcc_all/aps.sch', 2,  NULL, true);
INSERT INTO sch_validator_schematron (id, name, version, label, last_changed, last_modifier_id, description, path, object_type_id, xsd_path, available) VALUES (7, 'epSOS - ATNA Test', '2.0', 'epSOS - ATNA Test', null, null,'schematron to validate Audit Messages', 'ihe_pcc_all/aps.sch', 3,  'atna/RFC3881.xsd', true);
INSERT INTO sch_validator_schematron (id, name, version, label, last_changed, last_modifier_id, description, path, object_type_id, xsd_path, available) VALUES (8, 'epSOS - SAML Test', '2.0', 'epSOS - SAML Test', null, null,'schematron to validate SAML', 'ihe_pcc_all/aps.sch', 4,  'saml/saml-schema-assertion-2.0.xsd', true);

SELECT pg_catalog.setval('sch_validator_schematron_id_seq', 8 , true);


--
-- PostgreSQL database dump complete
--INSERT INTO "user_user_role" (role_id, user_id) VALUES (1, 1);
INSERT INTO "user_user_role" (role_id, user_id) VALUES (1, 2); --
 -- Copyright 2008 IHE International (http://www.ihe.net)
 --
 -- Licensed under the Apache License, Version 2.0 (the "License");
 -- you may not use this file except in compliance with the License.
 -- You may obtain a copy of the License at
 --
 -- http://www.apache.org/licenses/LICENSE-2.0
 --
 -- Unless required by applicable law or agreed to in writing, software
 -- distributed under the License is distributed on an "AS IS" BASIS,
 -- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 -- See the License for the specific language governing permissions and
 -- limitations under the License.
 --

 
--
-- Data for Name: system; Type: TABLE DATA; Schema: public; Owner: gazelle
--





-- Database populating - Checking Flag : if this value is inserted in the database, it means that database has been successfully populated during deployment
INSERT INTO "cmn_application_preference" (id, preference_name, preference_value, description, class_name) VALUES (9999, 'application_database_initialization_flag', 'database_successfully_initialized', 'Checking Flag : if this value is inserted in the database, it means that database has been successfully populated during deployment', 'java.lang.String');





--
-- PostgreSQL database dump complete
--

