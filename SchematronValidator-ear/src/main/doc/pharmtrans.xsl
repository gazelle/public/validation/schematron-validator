<xsl:stylesheet version="1.0" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:iso="http://purl.oclc.org/dsdl/schematron"
    xmlns:xhtml="http://www.w3.org/1999/xhtml">
    <xsl:output 
        encoding="ISO-8859-15"
        method="xml"
        indent="yes" />
    
    <xsl:template match="node() | @*">
        <xsl:copy>
            <xsl:apply-templates select="@* | node()"/>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="iso:assert">
        <iso:assert>
            <xsl:attribute name="id"><xsl:value-of select="@id"/></xsl:attribute>
            <xsl:attribute name="role"><xsl:value-of select="@role"/></xsl:attribute>
            <xsl:attribute name="test"><xsl:value-of select="xhtml:p[@class='debug' and @lang='en' and @var='test']"/></xsl:attribute>
            <xsl:value-of select="@role"/> : 
            <xsl:value-of select="xhtml:p[@id and @lang='en']"/> 
            <xsl:if test="xhtml:p[@class='specification' and @lang='en']">(<xsl:value-of select="xhtml:p[@class='specification' and @lang='en']"/>)</xsl:if>
        </iso:assert>
    </xsl:template>
    
    <xsl:template match="iso:report">
        <iso:report>
            <xsl:attribute name="id"><xsl:value-of select="@id"/></xsl:attribute>
            <xsl:attribute name="test"><xsl:value-of select="xhtml:p[@class='debug' and @lang='en' and @var='test']"/></xsl:attribute>
            <xsl:value-of select="xhtml:p[@id and @lang='en']"/> 
            (<xsl:value-of select="xhtml:p[@class='specification' and @lang='en']"/>)
        </iso:report>
    </xsl:template>
</xsl:stylesheet>