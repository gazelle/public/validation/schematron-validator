<?xml version="1.0" encoding="UTF-8"?>

<sch:schema xmlns:sch="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2"
    xmlns:sqf="http://www.schematron-quickfix.com/validator/process">

    <sch:ns uri="urn:ihe:iti:xdw:2011" prefix="xdw"/>
    <sch:ns uri="urn:hl7-org:v3" prefix="hl7"/>
    <sch:ns uri="http://www.w3.org/2001/XMLSchema-instance" prefix="xsi"/>
    <sch:ns uri="http://docs.oasis-open.org/ns/bpel4people/ws-humantask/types/200803" prefix="ws-ht"/>
    <sch:ns uri="urn:rve:2018:xdw:extension" prefix="ext2018"/>
    <sch:ns uri="urn:rve:2017:xdw:extension" prefix="ext2017"/>

    <sch:ns uri="http://www.w3.org/2003/05/soap-envelope" prefix="soap"/>
    <sch:ns uri="http://schemas.xmlsoap.org/soap/envelope/" prefix="soapAssertion"/>
    <sch:ns uri="urn:oasis:names:tc:SAML:2.0:assertion" prefix="saml"/>
    <sch:ns uri="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" prefix="wsse"/>
    <sch:ns uri="urn:ihe:iti:xds-b:2007" prefix="ihe"/>

    <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rim:3.0" prefix="rim"/>
    <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rs:3.0" prefix="rs"/>
    <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:lcm:3.0" prefix="lcm"/>
    <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:query:3.0" prefix="query"/>

    <sch:pattern>

        <sch:rule context="union/xdw:XDW.WorkflowDocument/xdw:author/xdw:assignedAuthor/hl7:id">
            <sch:let name="author_tot" value="//union/soap:Envelope/soap:Body/query:AdhocQueryResponse/rim:RegistryObjectList/rim:ExtrinsicObject/rim:Classification[@classificationScheme = 'urn:uuid:93606bcf-9494-43ec-9b4e-a7748d1a838d']/rim:Slot[@name = 'authorPerson']/rim:ValueList/rim:Value"/>
            <sch:let name="author" value="tokenize($author_tot,'\^')[1]"/>

        </sch:rule>

        <sch:rule context="union/xdw:XDW.WorkflowDocument/xdw:workflowStatusHistory/xdw:documentEvent[xdw:actualStatus='OPEN']/xdw:eventTime">
            <sch:let name="serviceStartTime" value="//union/soap:Envelope/soap:Body/query:AdhocQueryResponse/rim:RegistryObjectList/rim:ExtrinsicObject/rim:Slot[@name = 'serviceStartTime']/rim:ValueList/rim:Value"/>
            <sch:let name="eventTime_transl" value="translate(translate(translate(translate(text(),':',''),'T',''),'-',''),'Z','')"/>

            <sch:assert test="(string-length($serviceStartTime)&gt;0 and $serviceStartTime=$eventTime_transl) or (string-length($serviceStartTime)=0 and string-length($eventTime_transl)&gt;0)">
                Error3
            </sch:assert>
        </sch:rule>

    </sch:pattern>
</sch:schema>