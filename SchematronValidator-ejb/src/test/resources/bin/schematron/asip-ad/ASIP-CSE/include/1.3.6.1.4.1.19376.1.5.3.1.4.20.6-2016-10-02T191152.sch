<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.3.6.1.4.1.19376.1.5.3.1.4.20.6
Name: IHE Occupation Observation Entry
Description: An Occupation Observation Entry is a clinical statement about the type of occupation  which the subject currently holds or has held in the past.
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron" id="template-1.3.6.1.4.1.19376.1.5.3.1.4.20.6-2016-10-02T191152">
    <title>IHE Occupation Observation Entry</title>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.4.20.6
Context: *[hl7:observation[@classCode='OBS'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']]]
Item: (IHEOccupationObservationEntry)
-->

<!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.4.20.6
Context: *[hl7:observation[@classCode='OBS'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']]]/hl7:observation[@classCode='OBS'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']]
Item: (IHEOccupationObservationEntry)
-->
    <rule context="*[hl7:observation[@classCode='OBS'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']]]/hl7:observation[@classCode='OBS'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']]" id="d506897e4402-false-d670987e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.6" test="string(@classCode)=('OBS')">(IHEOccupationObservationEntry): The value for @classCode SHALL be 'OBS'.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.6" test="string(@moodCode)=('EVN')">(IHEOccupationObservationEntry): The value for @moodCode SHALL be 'EVN'.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.6" test="count(hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6'])&gt;=1">(IHEOccupationObservationEntry): element hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6'] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.6" test="count(hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6'])&lt;=1">(IHEOccupationObservationEntry): element hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6'] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.6" test="count(hl7:id)&gt;=1">(IHEOccupationObservationEntry): element hl7:id is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.6" test="count(hl7:code[(@code='LOINC-6' and @codeSystem='2.16.840.1.113883.6.1')])&gt;=1">(IHEOccupationObservationEntry): element hl7:code[(@code='LOINC-6' and @codeSystem='2.16.840.1.113883.6.1')] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.6" test="count(hl7:code[(@code='LOINC-6' and @codeSystem='2.16.840.1.113883.6.1')])&lt;=1">(IHEOccupationObservationEntry): element hl7:code[(@code='LOINC-6' and @codeSystem='2.16.840.1.113883.6.1')] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.6" test="count(hl7:statusCode[@code='completed' or @nullFlavor])&gt;=1">(IHEOccupationObservationEntry): element hl7:statusCode[@code='completed' or @nullFlavor] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.6" test="count(hl7:statusCode[@code='completed' or @nullFlavor])&lt;=1">(IHEOccupationObservationEntry): element hl7:statusCode[@code='completed' or @nullFlavor] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.6" test="count(hl7:effectiveTime)&gt;=1">(IHEOccupationObservationEntry): element hl7:effectiveTime is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.6" test="count(hl7:effectiveTime)&lt;=1">(IHEOccupationObservationEntry): element hl7:effectiveTime appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.6" test="count(hl7:value[not(@nullFlavor)])&gt;=1">(IHEOccupationObservationEntry): element hl7:value[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.6" test="count(hl7:value[not(@nullFlavor)])&lt;=1">(IHEOccupationObservationEntry): element hl7:value[not(@nullFlavor)] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.6" test="count(hl7:participant[@typeCode='IND'][not(@nullFlavor)])&gt;=1">(IHEOccupationObservationEntry): element hl7:participant[@typeCode='IND'][not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.6" test="count(hl7:participant[@typeCode='IND'][not(@nullFlavor)])&lt;=1">(IHEOccupationObservationEntry): element hl7:participant[@typeCode='IND'][not(@nullFlavor)] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.6" test="count(hl7:entryRelationship[@typeCode='REFR'][hl7:observation[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.7']]])&lt;=1">(IHEOccupationObservationEntry): element hl7:entryRelationship[@typeCode='REFR'][hl7:observation[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.7']]] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.6" test="count(hl7:entryRelationship[@typeCode='REFR'][hl7:observation[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.8']]])&lt;=1">(IHEOccupationObservationEntry): element hl7:entryRelationship[@typeCode='REFR'][hl7:observation[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.8']]] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.4.20.6
Context: *[hl7:observation[@classCode='OBS'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']]]/hl7:observation[@classCode='OBS'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']]/hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']
Item: (IHEOccupationObservationEntry)
-->
    <rule context="*[hl7:observation[@classCode='OBS'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']]]/hl7:observation[@classCode='OBS'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']]/hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']" id="d506897e4405-false-d671092e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.6" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHEOccupationObservationEntry): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.6" test="string(@root)=('1.3.6.1.4.1.19376.1.5.3.1.4.20.6')">(IHEOccupationObservationEntry): The value for @root SHALL be '1.3.6.1.4.1.19376.1.5.3.1.4.20.6'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.4.20.6
Context: *[hl7:observation[@classCode='OBS'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']]]/hl7:observation[@classCode='OBS'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']]/hl7:id
Item: (IHEOccupationObservationEntry)
-->
    <rule context="*[hl7:observation[@classCode='OBS'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']]]/hl7:observation[@classCode='OBS'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']]/hl7:id" id="d506897e4407-false-d671106e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.6" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHEOccupationObservationEntry): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.4.20.6
Context: *[hl7:observation[@classCode='OBS'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']]]/hl7:observation[@classCode='OBS'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']]/hl7:code[(@code='LOINC-6' and @codeSystem='2.16.840.1.113883.6.1')]
Item: (IHEOccupationObservationEntry)
-->
    <rule context="*[hl7:observation[@classCode='OBS'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']]]/hl7:observation[@classCode='OBS'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']]/hl7:code[(@code='LOINC-6' and @codeSystem='2.16.840.1.113883.6.1')]" id="d506897e4408-false-d671117e0">
        <extends rule="CD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.6" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHEOccupationObservationEntry): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.6" test="@nullFlavor or (@code='LOINC-6' and @codeSystem='2.16.840.1.113883.6.1' and @displayName='History of Occupation' and @codeSystemName='LOINC')">(IHEOccupationObservationEntry): The element value SHALL be one of 'code 'LOINC-6' codeSystem '2.16.840.1.113883.6.1' displayName='History of Occupation' codeSystemName='LOINC''.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.4.20.6
Context: *[hl7:observation[@classCode='OBS'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']]]/hl7:observation[@classCode='OBS'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']]/hl7:statusCode[@code='completed' or @nullFlavor]
Item: (IHEOccupationObservationEntry)
-->
    <rule context="*[hl7:observation[@classCode='OBS'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']]]/hl7:observation[@classCode='OBS'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']]/hl7:statusCode[@code='completed' or @nullFlavor]" id="d506897e4410-false-d671134e0">
        <extends rule="CS"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.6" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CS' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHEOccupationObservationEntry): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CS", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.6" test="@nullFlavor or (@code='completed')">(IHEOccupationObservationEntry): The element value SHALL be one of 'code 'completed''.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.4.20.6
Context: *[hl7:observation[@classCode='OBS'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']]]/hl7:observation[@classCode='OBS'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']]/hl7:effectiveTime
Item: (IHEOccupationObservationEntry)
-->
    <rule context="*[hl7:observation[@classCode='OBS'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']]]/hl7:observation[@classCode='OBS'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']]/hl7:effectiveTime" id="d506897e4412-false-d671150e0">
        <extends rule="IVL_TS"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.6" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='IVL_TS' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHEOccupationObservationEntry): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:IVL_TS", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.6" test="count(hl7:low)&lt;=1">(IHEOccupationObservationEntry): element hl7:low appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.6" test="count(hl7:high)&gt;=1">(IHEOccupationObservationEntry): element hl7:high is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.6" test="count(hl7:high)&lt;=1">(IHEOccupationObservationEntry): element hl7:high appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.4.20.6
Context: *[hl7:observation[@classCode='OBS'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']]]/hl7:observation[@classCode='OBS'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']]/hl7:effectiveTime/hl7:low
Item: (IHEOccupationObservationEntry)
-->
    <rule context="*[hl7:observation[@classCode='OBS'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']]]/hl7:observation[@classCode='OBS'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']]/hl7:effectiveTime/hl7:low" id="d506897e4413-false-d671177e0">
        <extends rule="IVXB_TS"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.6" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='IVXB_TS' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHEOccupationObservationEntry): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:IVXB_TS", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.6" test="string(@nullFlavor)=('UNK') or not(@nullFlavor)">(IHEOccupationObservationEntry): The value for @nullFlavor SHALL be 'UNK'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.4.20.6
Context: *[hl7:observation[@classCode='OBS'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']]]/hl7:observation[@classCode='OBS'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']]/hl7:effectiveTime/hl7:high
Item: (IHEOccupationObservationEntry)
-->
    <rule context="*[hl7:observation[@classCode='OBS'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']]]/hl7:observation[@classCode='OBS'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']]/hl7:effectiveTime/hl7:high" id="d506897e4415-false-d671191e0">
        <extends rule="IVXB_TS"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.6" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='IVXB_TS' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHEOccupationObservationEntry): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:IVXB_TS", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.4.20.6
Context: *[hl7:observation[@classCode='OBS'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']]]/hl7:observation[@classCode='OBS'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']]/hl7:value[not(@nullFlavor)]
Item: (IHEOccupationObservationEntry)
-->
    <rule context="*[hl7:observation[@classCode='OBS'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']]]/hl7:observation[@classCode='OBS'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']]/hl7:value[not(@nullFlavor)]" id="d506897e4416-false-d671201e0">
        <extends rule="CD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.6" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHEOccupationObservationEntry): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.4.20.6
Context: *[hl7:observation[@classCode='OBS'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']]]/hl7:observation[@classCode='OBS'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']]/hl7:participant[@typeCode='IND'][not(@nullFlavor)]
Item: (IHEOccupationObservationEntry)
-->
    <rule context="*[hl7:observation[@classCode='OBS'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']]]/hl7:observation[@classCode='OBS'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']]/hl7:participant[@typeCode='IND'][not(@nullFlavor)]" id="d506897e4417-false-d671211e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.6" test="string(@typeCode)=('IND')">(IHEOccupationObservationEntry): The value for @typeCode SHALL be 'IND'.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.6" test="count(hl7:participantRole[@classCode='ROL'][not(@nullFlavor)])&gt;=1">(IHEOccupationObservationEntry): element hl7:participantRole[@classCode='ROL'][not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.6" test="count(hl7:participantRole[@classCode='ROL'][not(@nullFlavor)])&lt;=1">(IHEOccupationObservationEntry): element hl7:participantRole[@classCode='ROL'][not(@nullFlavor)] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.4.20.6
Context: *[hl7:observation[@classCode='OBS'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']]]/hl7:observation[@classCode='OBS'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']]/hl7:participant[@typeCode='IND'][not(@nullFlavor)]/hl7:participantRole[@classCode='ROL'][not(@nullFlavor)]
Item: (IHEOccupationObservationEntry)
-->
    <rule context="*[hl7:observation[@classCode='OBS'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']]]/hl7:observation[@classCode='OBS'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']]/hl7:participant[@typeCode='IND'][not(@nullFlavor)]/hl7:participantRole[@classCode='ROL'][not(@nullFlavor)]" id="d506897e4419-false-d671231e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.6" test="string(@classCode)=('ROL')">(IHEOccupationObservationEntry): The value for @classCode SHALL be 'ROL'.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.6" test="count(hl7:id[not(@nullFlavor)])&gt;=1">(IHEOccupationObservationEntry): element hl7:id[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.6" test="count(hl7:id[not(@nullFlavor)])&lt;=1">(IHEOccupationObservationEntry): element hl7:id[not(@nullFlavor)] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.4.20.6
Context: *[hl7:observation[@classCode='OBS'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']]]/hl7:observation[@classCode='OBS'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']]/hl7:participant[@typeCode='IND'][not(@nullFlavor)]/hl7:participantRole[@classCode='ROL'][not(@nullFlavor)]/hl7:id[not(@nullFlavor)]
Item: (IHEOccupationObservationEntry)
-->
    <rule context="*[hl7:observation[@classCode='OBS'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']]]/hl7:observation[@classCode='OBS'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']]/hl7:participant[@typeCode='IND'][not(@nullFlavor)]/hl7:participantRole[@classCode='ROL'][not(@nullFlavor)]/hl7:id[not(@nullFlavor)]" id="d506897e4421-false-d671251e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.6" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHEOccupationObservationEntry): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.4.20.6
Context: *[hl7:observation[@classCode='OBS'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']]]/hl7:observation[@classCode='OBS'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']]/hl7:entryRelationship[@typeCode='REFR'][hl7:observation[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.7']]]
Item: (IHEOccupationObservationEntry)
-->
    <rule context="*[hl7:observation[@classCode='OBS'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']]]/hl7:observation[@classCode='OBS'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']]/hl7:entryRelationship[@typeCode='REFR'][hl7:observation[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.7']]]">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.6" test="string(@typeCode)=('REFR')">(IHEOccupationObservationEntry): The value for @typeCode SHALL be 'REFR'.</assert>
    </rule>

   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.4.20.6
Context: *[hl7:observation[@classCode='OBS'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']]]/hl7:observation[@classCode='OBS'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']]/hl7:entryRelationship[@typeCode='REFR'][hl7:observation[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.8']]]
Item: (IHEOccupationObservationEntry)
-->
    <rule context="*[hl7:observation[@classCode='OBS'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']]]/hl7:observation[@classCode='OBS'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.6']]/hl7:entryRelationship[@typeCode='REFR'][hl7:observation[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.8']]]">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.6" test="string(@typeCode)=('REFR')">(IHEOccupationObservationEntry): The value for @typeCode SHALL be 'REFR'.</assert>
    </rule>
</pattern>