<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.250.1.213.1.1.1.5.3
Name: Certificat du 24e mois
Description: 
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron" id="template-1.2.250.1.213.1.1.1.5.3-2017-12-11T152315">
    <title>Certificat du 24e mois</title>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.5.3
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]
Item: (CertificatDu24eMois)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]" id="d506897e2337-false-d608299e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.3" test="count(hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3'])&gt;=1">(CertificatDu24eMois): element hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3'] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.3" test="count(hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3'])&lt;=1">(CertificatDu24eMois): element hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3'] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.3" test="count(hl7:id)&gt;=1">(CertificatDu24eMois): element hl7:id is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.3" test="count(hl7:id)&lt;=1">(CertificatDu24eMois): element hl7:id appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.3" test="count(hl7:code[@codeSystem='2.16.840.1.113883.6.1' or @nullFlavor])&gt;=1">(CertificatDu24eMois): element hl7:code[@codeSystem='2.16.840.1.113883.6.1' or @nullFlavor] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.3" test="count(hl7:code[@codeSystem='2.16.840.1.113883.6.1' or @nullFlavor])&lt;=1">(CertificatDu24eMois): element hl7:code[@codeSystem='2.16.840.1.113883.6.1' or @nullFlavor] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.3" test="count(hl7:title)&lt;=1">(CertificatDu24eMois): element hl7:title appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.3" test="count(hl7:effectiveTime)&gt;=1">(CertificatDu24eMois): element hl7:effectiveTime is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.3" test="count(hl7:effectiveTime)&lt;=1">(CertificatDu24eMois): element hl7:effectiveTime appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.3" test="count(hl7:component)&gt;=1">(CertificatDu24eMois): element hl7:component is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.3" test="count(hl7:component)&lt;=1">(CertificatDu24eMois): element hl7:component appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.3" test="count(hl7:componentOf[not(@nullFlavor)])&gt;=1">(CertificatDu24eMois): element hl7:componentOf[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.3" test="count(hl7:componentOf[not(@nullFlavor)])&lt;=1">(CertificatDu24eMois): element hl7:componentOf[not(@nullFlavor)] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.3" test="count(hl7:relatedDocument[@typeCode='RPLC'])&gt;=1">(CertificatDu24eMois): element hl7:relatedDocument[@typeCode='RPLC'] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.3" test="count(hl7:relatedDocument[@typeCode='RPLC'])&lt;=1">(CertificatDu24eMois): element hl7:relatedDocument[@typeCode='RPLC'] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.3" test="count(hl7:documentationOf[not(@nullFlavor)])&gt;=1">(CertificatDu24eMois): element hl7:documentationOf[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.3" test="count(hl7:documentationOf[not(@nullFlavor)])&lt;=1">(CertificatDu24eMois): element hl7:documentationOf[not(@nullFlavor)] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.3" test="count(hl7:custodian[not(@nullFlavor)])&gt;=1">(CertificatDu24eMois): element hl7:custodian[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.3" test="count(hl7:custodian[not(@nullFlavor)])&lt;=1">(CertificatDu24eMois): element hl7:custodian[not(@nullFlavor)] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.3" test="count(hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]])&gt;=1">(CertificatDu24eMois): element hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.3" test="count(hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]])&lt;=1">(CertificatDu24eMois): element hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.3" test="count(hl7:author[not(@nullFlavor)])&gt;=1">(CertificatDu24eMois): element hl7:author[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.3" test="count(hl7:author[not(@nullFlavor)])&lt;=1">(CertificatDu24eMois): element hl7:author[not(@nullFlavor)] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.3" test="count(hl7:informant)&gt;=1">(CertificatDu24eMois): element hl7:informant is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.3" test="count(hl7:informant)&lt;=1">(CertificatDu24eMois): element hl7:informant appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.3" test="count(hl7:participant)&gt;=1">(CertificatDu24eMois): element hl7:participant is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.3" test="count(hl7:participant)&lt;=1">(CertificatDu24eMois): element hl7:participant appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.3" test="count(hl7:recordTarget)&gt;=1">(CertificatDu24eMois): element hl7:recordTarget is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.3" test="count(hl7:recordTarget)&lt;=1">(CertificatDu24eMois): element hl7:recordTarget appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.5.3
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']
Item: (CertificatDu24eMois)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']" id="d506897e2338-false-d608769e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.3" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CertificatDu24eMois): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.3" test="string(@root)=('1.2.250.1.213.1.1.1.5.3')">(CertificatDu24eMois): The value for @root SHALL be '1.2.250.1.213.1.1.1.5.3'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.5.3
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:id
Item: (CertificatDu24eMois)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:id" id="d506897e2340-false-d608783e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.3" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CertificatDu24eMois): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.5.3
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:code[@codeSystem='2.16.840.1.113883.6.1' or @nullFlavor]
Item: (CertificatDu24eMois)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:code[@codeSystem='2.16.840.1.113883.6.1' or @nullFlavor]" id="d506897e2341-false-d608794e0">
        <extends rule="CE"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.3" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CE' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CertificatDu24eMois): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CE", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.3" test="@nullFlavor or (@codeSystem='2.16.840.1.113883.6.1')">(CertificatDu24eMois): The element value SHALL be one of 'codeSystem '2.16.840.1.113883.6.1''.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.5.3
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:title
Item: (CertificatDu24eMois)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:title" id="d506897e2343-false-d608810e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.3" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CertificatDu24eMois): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.5.3
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:effectiveTime
Item: (CertificatDu24eMois)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:effectiveTime" id="d506897e2344-false-d608820e0">
        <extends rule="TS"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.3" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TS' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CertificatDu24eMois): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TS", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.3" test="not(*)">(CertificatDu24eMois): <value-of select="local-name()"/> with datatype TS, SHOULD NOT have child elements.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.5.3
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:component
Item: (CertificatDu24eMois)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:component" id="d506897e2345-false-d608984e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.3" test="count(hl7:structuredBody)&gt;=1">(CertificatDu24eMois): element hl7:structuredBody is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.3" test="count(hl7:structuredBody)&lt;=1">(CertificatDu24eMois): element hl7:structuredBody appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.5.3
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:component/hl7:structuredBody
Item: (CertificatDu24eMois)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:component/hl7:structuredBody" id="d506897e2346-false-d609301e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.3" test="count(hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.15']]])&gt;=1">(CertificatDu24eMois): element hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.15']]] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.3" test="count(hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.15']]])&lt;=1">(CertificatDu24eMois): element hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.15']]] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.3" test="count(hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.8']]])&gt;=1">(CertificatDu24eMois): element hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.8']]] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.3" test="count(hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.8']]])&lt;=1">(CertificatDu24eMois): element hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.8']]] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.3" test="count(hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.15.1'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.15']]])&gt;=1">(CertificatDu24eMois): element hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.15.1'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.15']]] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.3" test="count(hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.15.1'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.15']]])&lt;=1">(CertificatDu24eMois): element hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.15.1'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.15']]] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.3" test="count(hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.3']]])&gt;=1">(CertificatDu24eMois): element hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.3']]] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.3" test="count(hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.3']]])&lt;=1">(CertificatDu24eMois): element hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.3']]] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.3" test="count(hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.13.2.5']]])&gt;=1">(CertificatDu24eMois): element hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.13.2.5']]] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.3" test="count(hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.13.2.5']]])&lt;=1">(CertificatDu24eMois): element hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.13.2.5']]] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.3" test="count(hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.23'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.6']]])&gt;=1">(CertificatDu24eMois): element hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.23'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.6']]] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.3" test="count(hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.23'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.6']]])&lt;=1">(CertificatDu24eMois): element hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.23'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.6']]] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.3" test="count(hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.31'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.10']]])&gt;=1">(CertificatDu24eMois): element hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.31'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.10']]] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.3" test="count(hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.31'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.10']]])&lt;=1">(CertificatDu24eMois): element hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.31'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.10']]] appears too often [max 1x].</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.5.3
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:component/hl7:structuredBody/hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.15']]]
Item: (CertificatDu24eMois)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.5.3
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:component/hl7:structuredBody/hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.8']]]
Item: (CertificatDu24eMois)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.5.3
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:component/hl7:structuredBody/hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.15.1'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.15']]]
Item: (CertificatDu24eMois)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.5.3
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:component/hl7:structuredBody/hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.3']]]
Item: (CertificatDu24eMois)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.5.3
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:component/hl7:structuredBody/hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.13.2.5']]]
Item: (CertificatDu24eMois)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.5.3
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:component/hl7:structuredBody/hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.23'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.6']]]
Item: (CertificatDu24eMois)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.5.3
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:component/hl7:structuredBody/hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.31'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.10']]]
Item: (CertificatDu24eMois)
-->
<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]" id="d609791e12-false-d609823e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:encompassingEncounter[not(@nullFlavor)])&gt;=1">(CI-SISComponentOf): element hl7:encompassingEncounter[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:encompassingEncounter[not(@nullFlavor)])&lt;=1">(CI-SISComponentOf): element hl7:encompassingEncounter[not(@nullFlavor)] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]" id="d609791e13-false-d609885e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:code[(@code='IMP' and @codeSystem='2.16.840.1.113883.5.4') or (@code='EMER' and @codeSystem='2.16.840.1.113883.5.4') or (@code='AMB' and @codeSystem='2.16.840.1.113883.5.4') or (@code='FLD' and @codeSystem='2.16.840.1.113883.5.4') or (@code='HH' and @codeSystem='2.16.840.1.113883.5.4') or (@code='VR' and @codeSystem='2.16.840.1.113883.5.4') or (@code='EXTERNE' and @codeSystem='1.2.250.1.213.1.1.4.2.291') or (@code='SEANCE' and @codeSystem='1.2.250.1.213.1.1.4.2.291')])&lt;=1">(CI-SISComponentOf): element hl7:code[(@code='IMP' and @codeSystem='2.16.840.1.113883.5.4') or (@code='EMER' and @codeSystem='2.16.840.1.113883.5.4') or (@code='AMB' and @codeSystem='2.16.840.1.113883.5.4') or (@code='FLD' and @codeSystem='2.16.840.1.113883.5.4') or (@code='HH' and @codeSystem='2.16.840.1.113883.5.4') or (@code='VR' and @codeSystem='2.16.840.1.113883.5.4') or (@code='EXTERNE' and @codeSystem='1.2.250.1.213.1.1.4.2.291') or (@code='SEANCE' and @codeSystem='1.2.250.1.213.1.1.4.2.291')] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:effectiveTime)&gt;=1">(CI-SISComponentOf): element hl7:effectiveTime is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:effectiveTime)&lt;=1">(CI-SISComponentOf): element hl7:effectiveTime appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:dischargeDispositionCode)&lt;=1">(CI-SISComponentOf): element hl7:dischargeDispositionCode appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:responsibleParty)&lt;=1">(CI-SISComponentOf): element hl7:responsibleParty appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]])&gt;=1">(CI-SISComponentOf): element hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]])&lt;=1">(CI-SISComponentOf): element hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:id
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:id" id="d609791e19-false-d609967e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISComponentOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="not(@extension) or string-length(@extension)&gt;0">(CI-SISComponentOf): Attribute @extension SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="@root">(CI-SISComponentOf): attribute @root SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="not(@root) or matches(@root,'^[0-2](\.(0|[1-9]\d*))*$') or matches(@root,'^[A-Fa-f\d]{8}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{12}$') or matches(@root,'^[A-Za-z][A-Za-z\d\-]*$')">(CI-SISComponentOf): Attribute @root SHALL be of data type 'uid'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:code[(@code='IMP' and @codeSystem='2.16.840.1.113883.5.4') or (@code='EMER' and @codeSystem='2.16.840.1.113883.5.4') or (@code='AMB' and @codeSystem='2.16.840.1.113883.5.4') or (@code='FLD' and @codeSystem='2.16.840.1.113883.5.4') or (@code='HH' and @codeSystem='2.16.840.1.113883.5.4') or (@code='VR' and @codeSystem='2.16.840.1.113883.5.4') or (@code='EXTERNE' and @codeSystem='1.2.250.1.213.1.1.4.2.291') or (@code='SEANCE' and @codeSystem='1.2.250.1.213.1.1.4.2.291')]
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:code[(@code='IMP' and @codeSystem='2.16.840.1.113883.5.4') or (@code='EMER' and @codeSystem='2.16.840.1.113883.5.4') or (@code='AMB' and @codeSystem='2.16.840.1.113883.5.4') or (@code='FLD' and @codeSystem='2.16.840.1.113883.5.4') or (@code='HH' and @codeSystem='2.16.840.1.113883.5.4') or (@code='VR' and @codeSystem='2.16.840.1.113883.5.4') or (@code='EXTERNE' and @codeSystem='1.2.250.1.213.1.1.4.2.291') or (@code='SEANCE' and @codeSystem='1.2.250.1.213.1.1.4.2.291')]" id="d609791e27-false-d609989e0">
        <extends rule="CE"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CE' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISComponentOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CE", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="@nullFlavor or (@code='IMP' and @codeSystem='2.16.840.1.113883.5.4') or (@code='EMER' and @codeSystem='2.16.840.1.113883.5.4') or (@code='AMB' and @codeSystem='2.16.840.1.113883.5.4') or (@code='FLD' and @codeSystem='2.16.840.1.113883.5.4') or (@code='HH' and @codeSystem='2.16.840.1.113883.5.4') or (@code='VR' and @codeSystem='2.16.840.1.113883.5.4') or (@code='EXTERNE' and @codeSystem='1.2.250.1.213.1.1.4.2.291') or (@code='SEANCE' and @codeSystem='1.2.250.1.213.1.1.4.2.291')">(CI-SISComponentOf): The element value SHALL be one of 'code 'IMP' codeSystem '2.16.840.1.113883.5.4' or code 'EMER' codeSystem '2.16.840.1.113883.5.4' or code 'AMB' codeSystem '2.16.840.1.113883.5.4' or code 'FLD' codeSystem '2.16.840.1.113883.5.4' or code 'HH' codeSystem '2.16.840.1.113883.5.4' or code 'VR' codeSystem '2.16.840.1.113883.5.4' or code 'EXTERNE' codeSystem '1.2.250.1.213.1.1.4.2.291' or code 'SEANCE' codeSystem '1.2.250.1.213.1.1.4.2.291''.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="@displayName">(CI-SISComponentOf): attribute @displayName SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="not(@displayName) or string-length(@displayName)&gt;0">(CI-SISComponentOf): Attribute @displayName SHALL be of data type 'st'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:effectiveTime
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:effectiveTime" id="d609791e70-false-d610033e0">
        <extends rule="IVL_TS"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='IVL_TS' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISComponentOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:IVL_TS", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:low)&lt;=1">(CI-SISComponentOf): element hl7:low appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:high)&lt;=1">(CI-SISComponentOf): element hl7:high appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:effectiveTime/hl7:low
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:effectiveTime/hl7:low" id="d609791e76-false-d610057e0">
        <extends rule="IVXB_TS"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='IVXB_TS' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISComponentOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:IVXB_TS", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="@value">(CI-SISComponentOf): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="not(@value) or matches(string(@value), '^[0-9]{4,14}')">(CI-SISComponentOf): Attribute @value SHALL be of data type 'ts'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:effectiveTime/hl7:high
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:effectiveTime/hl7:high" id="d609791e84-false-d610074e0">
        <extends rule="IVXB_TS"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='IVXB_TS' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISComponentOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:IVXB_TS", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="@value">(CI-SISComponentOf): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="not(@value) or matches(string(@value), '^[0-9]{4,14}')">(CI-SISComponentOf): Attribute @value SHALL be of data type 'ts'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:dischargeDispositionCode
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:dischargeDispositionCode" id="d609791e92-false-d610091e0">
        <extends rule="CE"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CE' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISComponentOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CE", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="@displayName">(CI-SISComponentOf): attribute @displayName SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="not(@displayName) or string-length(@displayName)&gt;0">(CI-SISComponentOf): Attribute @displayName SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="string(@codeSystem)=('1.2.250.1.213.2.14') or not(@codeSystem)">(CI-SISComponentOf): The value for @codeSystem SHALL be '1.2.250.1.213.2.14'.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="@code">(CI-SISComponentOf): attribute @code SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="not(@code) or (string-length(@code)&gt;0 and not(matches(@code,'\s')))">(CI-SISComponentOf): Attribute @code SHALL be of data type 'cs'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty" id="d609791e107-false-d610130e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']])&gt;=1">(CI-SISComponentOf): element hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']])&lt;=1">(CI-SISComponentOf): element hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]" id="d609791e113-false-d610169e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:id[@root='1.2.250.1.71.4.2.1'])&gt;=1">(CI-SISComponentOf): element hl7:id[@root='1.2.250.1.71.4.2.1'] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:id[@root='1.2.250.1.71.4.2.1'])&lt;=1">(CI-SISComponentOf): element hl7:id[@root='1.2.250.1.71.4.2.1'] appears too often [max 1x].</assert>
        <let name="elmcount" value="count(hl7:addr|hl7:addr)"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:assignedPerson)&lt;=1">(CI-SISComponentOf): element hl7:assignedPerson appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:representedOrganization)&lt;=1">(CI-SISComponentOf): element hl7:representedOrganization appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:id[@root='1.2.250.1.71.4.2.1']
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:id[@root='1.2.250.1.71.4.2.1']" id="d609791e121-false-d610235e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISComponentOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="@extension">(CI-SISComponentOf): attribute @extension SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="not(@extension) or string-length(@extension)&gt;0">(CI-SISComponentOf): Attribute @extension SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="string(@root)=('1.2.250.1.71.4.2.1')">(CI-SISComponentOf): The value for @root SHALL be '1.2.250.1.71.4.2.1'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]
Item: (CI-SISAddr)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr" id="d610236e76-false-d610258e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@hl7:use) or (string-length(@hl7:use)&gt;0 and not(matches(@hl7:use,'\s')))">(CI-SISAddr): Attribute @hl7:use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:country)&lt;=1">(CI-SISAddr): element hl7:country appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:state)&lt;=1">(CI-SISAddr): element hl7:state appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:city)&lt;=1">(CI-SISAddr): element hl7:city appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postalCode)&lt;=1">(CI-SISAddr): element hl7:postalCode appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumber)&lt;=1">(CI-SISAddr): element hl7:houseNumber appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumberNumeric)&lt;=1">(CI-SISAddr): element hl7:houseNumberNumeric appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetName)&lt;=1">(CI-SISAddr): element hl7:streetName appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetNameType)&lt;=1">(CI-SISAddr): element hl7:streetNameType appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:additionalLocator)&lt;=1">(CI-SISAddr): element hl7:additionalLocator appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:unitID)&lt;=1">(CI-SISAddr): element hl7:unitID appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postBox)&lt;=1">(CI-SISAddr): element hl7:postBox appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:precinct)&lt;=1">(CI-SISAddr): element hl7:precinct appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:country
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:country" id="d610236e94-false-d610356e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:state
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:state" id="d610236e98-false-d610366e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:city
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:city" id="d610236e102-false-d610376e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:postalCode
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:postalCode" id="d610236e110-false-d610386e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:houseNumber
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:houseNumber" id="d610236e118-false-d610396e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:houseNumberNumeric
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:houseNumberNumeric" id="d610236e122-false-d610406e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetName
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetName" id="d610236e126-false-d610416e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetNameType
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetNameType" id="d610236e130-false-d610426e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:additionalLocator
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:additionalLocator" id="d610236e175-false-d610436e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:unitID
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:unitID" id="d610236e190-false-d610446e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:postBox
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:postBox" id="d610236e200-false-d610456e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:precinct
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:precinct" id="d610236e204-false-d610466e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr" id="d610236e208-false-d610476e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISAddr): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine" id="d610236e226-false-d610532e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine" id="d610236e232-false-d610542e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine" id="d610236e236-false-d610552e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine" id="d610236e240-false-d610562e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine" id="d610236e244-false-d610572e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine" id="d610236e248-false-d610582e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.19
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:telecom
Item: (CI-SISTelecom)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:telecom" id="d610583e27-false-d610593e0">
        <extends rule="TEL"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TEL' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISTelecom): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TEL", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISTelecom): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@value">(CI-SISTelecom): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@value) or string(@value castable as xs:anyURI)">(CI-SISTelecom): Attribute @value SHALL be of data type 'url'<value-of select="@value"/>
        </assert>
        <let name="prefix" value="substring-before(@value, ':')"/>
        <let name="suffix" value="substring-after(@value, ':')"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(             (count(@*) = 1 and name(@*) = 'nullFlavor' and             (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or             ($suffix and (             $prefix = 'tel' or              $prefix = 'fax' or              $prefix = 'mailto' or              $prefix = 'http' or              $prefix = 'ftp' or              $prefix = 'mllp'))             )">(CI-SISTelecom): Erreur de conformité CI-SIS : <name/> n'est pas conforme à une adresse de télécommunication préfixe:chaîne 
            (avec préfixe = tel, fax, mailto, http, ftp ou mllp) 
            ou est vide et sans nullFlavor, ou contient un nullFlavor non admis.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@use = 'H'                      or @use = 'HP'                      or @use = 'HV'                      or @use = 'WP'                     or @use = 'DIR'                      or @use = 'PUB'                      or @use = 'EC'                      or @use = 'MC'                      or @use = 'PG'                      or not(@use)">(CI-SISTelecom): Erreur de conformité CI-SIS : L'attribut use de l'élément telecom n'est pas conforme. 
            Il est facultatif et les valeurs permises sont 'H','HP', 'HV','WP','DIR','PUB','EC','MC','PG'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:assignedPerson
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:assignedPerson" id="d609791e126-false-d610614e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:name)&gt;=1">(CI-SISComponentOf): element hl7:name is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:name)&lt;=1">(CI-SISComponentOf): element hl7:name appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:assignedPerson/hl7:name
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:assignedPerson/hl7:name" id="d609791e127-false-d610630e0">
        <extends rule="PN"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='PN' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISComponentOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:PN", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:family)&gt;=1">(CI-SISComponentOf): element hl7:family is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:family)&lt;=1">(CI-SISComponentOf): element hl7:family appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:given)&lt;=1">(CI-SISComponentOf): element hl7:given appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:prefix)&lt;=1">(CI-SISComponentOf): element hl7:prefix appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:assignedPerson/hl7:name/hl7:family
Item: (CI-SISComponentOf)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:assignedPerson/hl7:name/hl7:given
Item: (CI-SISComponentOf)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:assignedPerson/hl7:name/hl7:prefix
Item: (CI-SISComponentOf)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization" id="d609791e140-false-d610691e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:id[@root='1.2.250.1.71.4.2.2'])&lt;=1">(CI-SISComponentOf): element hl7:id[@root='1.2.250.1.71.4.2.2'] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:name)&lt;=1">(CI-SISComponentOf): element hl7:name appears too often [max 1x].</assert>
        <let name="elmcount" value="count(hl7:addr|hl7:addr)"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:standardIndustryClassCode)&lt;=1">(CI-SISComponentOf): element hl7:standardIndustryClassCode appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:id[@root='1.2.250.1.71.4.2.2']
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:id[@root='1.2.250.1.71.4.2.2']" id="d609791e141-false-d610748e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISComponentOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="@extension">(CI-SISComponentOf): attribute @extension SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="not(@extension) or string-length(@extension)&gt;0">(CI-SISComponentOf): Attribute @extension SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="string(@root)=('1.2.250.1.71.4.2.2')">(CI-SISComponentOf): The value for @root SHALL be '1.2.250.1.71.4.2.2'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:name
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:name" id="d609791e149-false-d610769e0">
        <extends rule="ON"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ON' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISComponentOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ON", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization
Item: (CI-SISAddr)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr" id="d610770e70-false-d610781e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@hl7:use) or (string-length(@hl7:use)&gt;0 and not(matches(@hl7:use,'\s')))">(CI-SISAddr): Attribute @hl7:use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:country)&lt;=1">(CI-SISAddr): element hl7:country appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:state)&lt;=1">(CI-SISAddr): element hl7:state appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:city)&lt;=1">(CI-SISAddr): element hl7:city appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postalCode)&lt;=1">(CI-SISAddr): element hl7:postalCode appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumber)&lt;=1">(CI-SISAddr): element hl7:houseNumber appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumberNumeric)&lt;=1">(CI-SISAddr): element hl7:houseNumberNumeric appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetName)&lt;=1">(CI-SISAddr): element hl7:streetName appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetNameType)&lt;=1">(CI-SISAddr): element hl7:streetNameType appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:additionalLocator)&lt;=1">(CI-SISAddr): element hl7:additionalLocator appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:unitID)&lt;=1">(CI-SISAddr): element hl7:unitID appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postBox)&lt;=1">(CI-SISAddr): element hl7:postBox appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:precinct)&lt;=1">(CI-SISAddr): element hl7:precinct appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:country
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:country" id="d610770e88-false-d610879e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:state
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:state" id="d610770e92-false-d610889e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:city
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:city" id="d610770e96-false-d610899e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:postalCode
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:postalCode" id="d610770e104-false-d610909e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:houseNumber
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:houseNumber" id="d610770e112-false-d610919e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:houseNumberNumeric
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:houseNumberNumeric" id="d610770e116-false-d610929e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetName
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetName" id="d610770e120-false-d610939e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetNameType
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetNameType" id="d610770e124-false-d610949e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:additionalLocator
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:additionalLocator" id="d610770e169-false-d610959e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:unitID
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:unitID" id="d610770e184-false-d610969e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:postBox
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:postBox" id="d610770e194-false-d610979e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:precinct
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:precinct" id="d610770e198-false-d610989e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr" id="d610770e202-false-d610999e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISAddr): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine" id="d610770e220-false-d611055e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine" id="d610770e226-false-d611065e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine" id="d610770e230-false-d611075e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine" id="d610770e234-false-d611085e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine" id="d610770e238-false-d611095e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine" id="d610770e242-false-d611105e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.19
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:telecom
Item: (CI-SISTelecom)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:telecom" id="d611106e27-false-d611116e0">
        <extends rule="TEL"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TEL' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISTelecom): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TEL", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISTelecom): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@value">(CI-SISTelecom): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@value) or string(@value castable as xs:anyURI)">(CI-SISTelecom): Attribute @value SHALL be of data type 'url'<value-of select="@value"/>
        </assert>
        <let name="prefix" value="substring-before(@value, ':')"/>
        <let name="suffix" value="substring-after(@value, ':')"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(             (count(@*) = 1 and name(@*) = 'nullFlavor' and             (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or             ($suffix and (             $prefix = 'tel' or              $prefix = 'fax' or              $prefix = 'mailto' or              $prefix = 'http' or              $prefix = 'ftp' or              $prefix = 'mllp'))             )">(CI-SISTelecom): Erreur de conformité CI-SIS : <name/> n'est pas conforme à une adresse de télécommunication préfixe:chaîne 
            (avec préfixe = tel, fax, mailto, http, ftp ou mllp) 
            ou est vide et sans nullFlavor, ou contient un nullFlavor non admis.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@use = 'H'                      or @use = 'HP'                      or @use = 'HV'                      or @use = 'WP'                     or @use = 'DIR'                      or @use = 'PUB'                      or @use = 'EC'                      or @use = 'MC'                      or @use = 'PG'                      or not(@use)">(CI-SISTelecom): Erreur de conformité CI-SIS : L'attribut use de l'élément telecom n'est pas conforme. 
            Il est facultatif et les valeurs permises sont 'H','HP', 'HV','WP','DIR','PUB','EC','MC','PG'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:standardIndustryClassCode
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:standardIndustryClassCode" id="d609791e152-false-d611137e0">
        <extends rule="CE"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CE' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISComponentOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CE", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="@displayName">(CI-SISComponentOf): attribute @displayName SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="not(@displayName) or string-length(@displayName)&gt;0">(CI-SISComponentOf): Attribute @displayName SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="@codeSystem">(CI-SISComponentOf): attribute @codeSystem SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="not(@codeSystem) or matches(@codeSystem,'^[0-2](\.(0|[1-9]\d*))*$')">(CI-SISComponentOf): Attribute @codeSystem SHALL be of data type 'oid'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="@code">(CI-SISComponentOf): attribute @code SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="not(@code) or (string-length(@code)&gt;0 and not(matches(@code,'\s')))">(CI-SISComponentOf): Attribute @code SHALL be of data type 'cs'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant" id="d609791e165-false-d611179e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']])&gt;=1">(CI-SISComponentOf): element hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']])&lt;=1">(CI-SISComponentOf): element hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]" id="d609791e171-false-d611218e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:id[@root='1.2.250.1.71.4.2.1'])&gt;=1">(CI-SISComponentOf): element hl7:id[@root='1.2.250.1.71.4.2.1'] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:id[@root='1.2.250.1.71.4.2.1'])&lt;=1">(CI-SISComponentOf): element hl7:id[@root='1.2.250.1.71.4.2.1'] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:assignedPerson)&lt;=1">(CI-SISComponentOf): element hl7:assignedPerson appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:representedOrganization)&lt;=1">(CI-SISComponentOf): element hl7:representedOrganization appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:id[@root='1.2.250.1.71.4.2.1']
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:id[@root='1.2.250.1.71.4.2.1']" id="d609791e179-false-d611274e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISComponentOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="@extension">(CI-SISComponentOf): attribute @extension SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="not(@extension) or string-length(@extension)&gt;0">(CI-SISComponentOf): Attribute @extension SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="string(@root)=('1.2.250.1.71.4.2.1')">(CI-SISComponentOf): The value for @root SHALL be '1.2.250.1.71.4.2.1'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]
Item: (CI-SISAddr)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr" id="d611275e76-false-d611297e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@hl7:use) or (string-length(@hl7:use)&gt;0 and not(matches(@hl7:use,'\s')))">(CI-SISAddr): Attribute @hl7:use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:country)&lt;=1">(CI-SISAddr): element hl7:country appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:state)&lt;=1">(CI-SISAddr): element hl7:state appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:city)&lt;=1">(CI-SISAddr): element hl7:city appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postalCode)&lt;=1">(CI-SISAddr): element hl7:postalCode appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumber)&lt;=1">(CI-SISAddr): element hl7:houseNumber appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumberNumeric)&lt;=1">(CI-SISAddr): element hl7:houseNumberNumeric appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetName)&lt;=1">(CI-SISAddr): element hl7:streetName appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetNameType)&lt;=1">(CI-SISAddr): element hl7:streetNameType appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:additionalLocator)&lt;=1">(CI-SISAddr): element hl7:additionalLocator appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:unitID)&lt;=1">(CI-SISAddr): element hl7:unitID appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postBox)&lt;=1">(CI-SISAddr): element hl7:postBox appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:precinct)&lt;=1">(CI-SISAddr): element hl7:precinct appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:country
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:country" id="d611275e94-false-d611395e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:state
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:state" id="d611275e98-false-d611405e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:city
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:city" id="d611275e102-false-d611415e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:postalCode
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:postalCode" id="d611275e110-false-d611425e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:houseNumber
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:houseNumber" id="d611275e118-false-d611435e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:houseNumberNumeric
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:houseNumberNumeric" id="d611275e122-false-d611445e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetName
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetName" id="d611275e126-false-d611455e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetNameType
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetNameType" id="d611275e130-false-d611465e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:additionalLocator
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:additionalLocator" id="d611275e175-false-d611475e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:unitID
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:unitID" id="d611275e190-false-d611485e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:postBox
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:postBox" id="d611275e200-false-d611495e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:precinct
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:precinct" id="d611275e204-false-d611505e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr" id="d611275e208-false-d611515e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISAddr): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine" id="d611275e226-false-d611571e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine" id="d611275e232-false-d611581e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine" id="d611275e236-false-d611591e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine" id="d611275e240-false-d611601e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine" id="d611275e244-false-d611611e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine" id="d611275e248-false-d611621e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.19
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:telecom
Item: (CI-SISTelecom)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:telecom" id="d611622e27-false-d611632e0">
        <extends rule="TEL"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TEL' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISTelecom): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TEL", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISTelecom): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@value">(CI-SISTelecom): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@value) or string(@value castable as xs:anyURI)">(CI-SISTelecom): Attribute @value SHALL be of data type 'url'<value-of select="@value"/>
        </assert>
        <let name="prefix" value="substring-before(@value, ':')"/>
        <let name="suffix" value="substring-after(@value, ':')"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(             (count(@*) = 1 and name(@*) = 'nullFlavor' and             (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or             ($suffix and (             $prefix = 'tel' or              $prefix = 'fax' or              $prefix = 'mailto' or              $prefix = 'http' or              $prefix = 'ftp' or              $prefix = 'mllp'))             )">(CI-SISTelecom): Erreur de conformité CI-SIS : <name/> n'est pas conforme à une adresse de télécommunication préfixe:chaîne 
            (avec préfixe = tel, fax, mailto, http, ftp ou mllp) 
            ou est vide et sans nullFlavor, ou contient un nullFlavor non admis.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@use = 'H'                      or @use = 'HP'                      or @use = 'HV'                      or @use = 'WP'                     or @use = 'DIR'                      or @use = 'PUB'                      or @use = 'EC'                      or @use = 'MC'                      or @use = 'PG'                      or not(@use)">(CI-SISTelecom): Erreur de conformité CI-SIS : L'attribut use de l'élément telecom n'est pas conforme. 
            Il est facultatif et les valeurs permises sont 'H','HP', 'HV','WP','DIR','PUB','EC','MC','PG'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:assignedPerson
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:assignedPerson" id="d609791e184-false-d611653e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:name)&gt;=1">(CI-SISComponentOf): element hl7:name is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:name)&lt;=1">(CI-SISComponentOf): element hl7:name appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:assignedPerson/hl7:name
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:assignedPerson/hl7:name" id="d609791e185-false-d611669e0">
        <extends rule="PN"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='PN' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISComponentOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:PN", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:family)&gt;=1">(CI-SISComponentOf): element hl7:family is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:family)&lt;=1">(CI-SISComponentOf): element hl7:family appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:given)&lt;=1">(CI-SISComponentOf): element hl7:given appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:prefix)&lt;=1">(CI-SISComponentOf): element hl7:prefix appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:assignedPerson/hl7:name/hl7:family
Item: (CI-SISComponentOf)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:assignedPerson/hl7:name/hl7:given
Item: (CI-SISComponentOf)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:assignedPerson/hl7:name/hl7:prefix
Item: (CI-SISComponentOf)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization" id="d609791e189-false-d611730e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:id[@root='1.2.250.1.71.4.2.2'])&lt;=1">(CI-SISComponentOf): element hl7:id[@root='1.2.250.1.71.4.2.2'] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:name)&lt;=1">(CI-SISComponentOf): element hl7:name appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:standardIndustryClassCode)&lt;=1">(CI-SISComponentOf): element hl7:standardIndustryClassCode appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:id[@root='1.2.250.1.71.4.2.2']
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:id[@root='1.2.250.1.71.4.2.2']" id="d609791e190-false-d611777e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISComponentOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="@extension">(CI-SISComponentOf): attribute @extension SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="not(@extension) or string-length(@extension)&gt;0">(CI-SISComponentOf): Attribute @extension SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="string(@root)=('1.2.250.1.71.4.2.2')">(CI-SISComponentOf): The value for @root SHALL be '1.2.250.1.71.4.2.2'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:name
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:name" id="d609791e198-false-d611798e0">
        <extends rule="ON"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ON' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISComponentOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ON", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization
Item: (CI-SISAddr)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr" id="d611799e70-false-d611810e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@hl7:use) or (string-length(@hl7:use)&gt;0 and not(matches(@hl7:use,'\s')))">(CI-SISAddr): Attribute @hl7:use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:country)&lt;=1">(CI-SISAddr): element hl7:country appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:state)&lt;=1">(CI-SISAddr): element hl7:state appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:city)&lt;=1">(CI-SISAddr): element hl7:city appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postalCode)&lt;=1">(CI-SISAddr): element hl7:postalCode appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumber)&lt;=1">(CI-SISAddr): element hl7:houseNumber appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumberNumeric)&lt;=1">(CI-SISAddr): element hl7:houseNumberNumeric appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetName)&lt;=1">(CI-SISAddr): element hl7:streetName appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetNameType)&lt;=1">(CI-SISAddr): element hl7:streetNameType appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:additionalLocator)&lt;=1">(CI-SISAddr): element hl7:additionalLocator appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:unitID)&lt;=1">(CI-SISAddr): element hl7:unitID appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postBox)&lt;=1">(CI-SISAddr): element hl7:postBox appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:precinct)&lt;=1">(CI-SISAddr): element hl7:precinct appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:country
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:country" id="d611799e88-false-d611908e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:state
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:state" id="d611799e92-false-d611918e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:city
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:city" id="d611799e96-false-d611928e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:postalCode
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:postalCode" id="d611799e104-false-d611938e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:houseNumber
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:houseNumber" id="d611799e112-false-d611948e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:houseNumberNumeric
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:houseNumberNumeric" id="d611799e116-false-d611958e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetName
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetName" id="d611799e120-false-d611968e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetNameType
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetNameType" id="d611799e124-false-d611978e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:additionalLocator
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:additionalLocator" id="d611799e169-false-d611988e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:unitID
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:unitID" id="d611799e184-false-d611998e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:postBox
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:postBox" id="d611799e194-false-d612008e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:precinct
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:precinct" id="d611799e198-false-d612018e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr" id="d611799e202-false-d612028e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISAddr): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine" id="d611799e220-false-d612084e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine" id="d611799e226-false-d612094e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine" id="d611799e230-false-d612104e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine" id="d611799e234-false-d612114e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine" id="d611799e238-false-d612124e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine" id="d611799e242-false-d612134e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.19
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:telecom
Item: (CI-SISTelecom)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:telecom" id="d612135e27-false-d612145e0">
        <extends rule="TEL"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TEL' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISTelecom): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TEL", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISTelecom): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@value">(CI-SISTelecom): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@value) or string(@value castable as xs:anyURI)">(CI-SISTelecom): Attribute @value SHALL be of data type 'url'<value-of select="@value"/>
        </assert>
        <let name="prefix" value="substring-before(@value, ':')"/>
        <let name="suffix" value="substring-after(@value, ':')"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(             (count(@*) = 1 and name(@*) = 'nullFlavor' and             (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or             ($suffix and (             $prefix = 'tel' or              $prefix = 'fax' or              $prefix = 'mailto' or              $prefix = 'http' or              $prefix = 'ftp' or              $prefix = 'mllp'))             )">(CI-SISTelecom): Erreur de conformité CI-SIS : <name/> n'est pas conforme à une adresse de télécommunication préfixe:chaîne 
            (avec préfixe = tel, fax, mailto, http, ftp ou mllp) 
            ou est vide et sans nullFlavor, ou contient un nullFlavor non admis.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@use = 'H'                      or @use = 'HP'                      or @use = 'HV'                      or @use = 'WP'                     or @use = 'DIR'                      or @use = 'PUB'                      or @use = 'EC'                      or @use = 'MC'                      or @use = 'PG'                      or not(@use)">(CI-SISTelecom): Erreur de conformité CI-SIS : L'attribut use de l'élément telecom n'est pas conforme. 
            Il est facultatif et les valeurs permises sont 'H','HP', 'HV','WP','DIR','PUB','EC','MC','PG'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:standardIndustryClassCode
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:standardIndustryClassCode" id="d609791e201-false-d612166e0">
        <extends rule="CE"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CE' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISComponentOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CE", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="@displayName">(CI-SISComponentOf): attribute @displayName SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="not(@displayName) or string-length(@displayName)&gt;0">(CI-SISComponentOf): Attribute @displayName SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="@codeSystem">(CI-SISComponentOf): attribute @codeSystem SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="not(@codeSystem) or matches(@codeSystem,'^[0-2](\.(0|[1-9]\d*))*$')">(CI-SISComponentOf): Attribute @codeSystem SHALL be of data type 'oid'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="@code">(CI-SISComponentOf): attribute @code SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="not(@code) or (string-length(@code)&gt;0 and not(matches(@code,'\s')))">(CI-SISComponentOf): Attribute @code SHALL be of data type 'cs'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]" id="d609791e214-false-d612203e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]])&gt;=1">(CI-SISComponentOf): element hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]])&lt;=1">(CI-SISComponentOf): element hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]" id="d609791e220-false-d612231e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)])&gt;=1">(CI-SISComponentOf): element hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)])&lt;=1">(CI-SISComponentOf): element hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:location)&gt;=1">(CI-SISComponentOf): element hl7:location is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:location)&lt;=1">(CI-SISComponentOf): element hl7:location appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]" id="d609791e226-false-d612266e0">
        <extends rule="CE"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CE' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISComponentOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CE", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <let name="theCode" value="@code"/>
        <let name="theCodeSystem" value="@codeSystem"/>
        <let name="theCodeSystemVersion" value="@codeSystemVersion"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="@nullFlavor or exists(doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1][conceptList/concept[@code = $theCode][@codeSystem = $theCodeSystem][not(@codeSystemVersion) or @codeSystemVersion=$theCodeSystemVersion] or completeCodeSystem[@codeSystem=$theCodeSystem][not(@codeSystemVersion) or @codeSystemVersion=$theCodeSystemVersion]])">(CI-SISComponentOf): The element value SHALL be one of '1.2.250.1.213.1.1.5.3 J02-HealthcareFacilityTypeCode (2018-01-05T00:00:00)'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location" id="d609791e236-false-d612287e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:name)&lt;=1">(CI-SISComponentOf): element hl7:name appears too often [max 1x].</assert>
        <let name="elmcount" value="count(hl7:addr|hl7:addr)"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="$elmcount&lt;=1">(CI-SISComponentOf): choice (hl7:addr or hl7:addr) contains too many elements [max 1x]</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:name
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:name" id="d609791e242-false-d612321e0">
        <extends rule="EN"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='EN' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISComponentOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:EN", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location
Item: (CI-SISAddr)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr" id="d612322e70-false-d612333e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@hl7:use) or (string-length(@hl7:use)&gt;0 and not(matches(@hl7:use,'\s')))">(CI-SISAddr): Attribute @hl7:use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:country)&lt;=1">(CI-SISAddr): element hl7:country appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:state)&lt;=1">(CI-SISAddr): element hl7:state appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:city)&lt;=1">(CI-SISAddr): element hl7:city appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postalCode)&lt;=1">(CI-SISAddr): element hl7:postalCode appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumber)&lt;=1">(CI-SISAddr): element hl7:houseNumber appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumberNumeric)&lt;=1">(CI-SISAddr): element hl7:houseNumberNumeric appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetName)&lt;=1">(CI-SISAddr): element hl7:streetName appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetNameType)&lt;=1">(CI-SISAddr): element hl7:streetNameType appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:additionalLocator)&lt;=1">(CI-SISAddr): element hl7:additionalLocator appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:unitID)&lt;=1">(CI-SISAddr): element hl7:unitID appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postBox)&lt;=1">(CI-SISAddr): element hl7:postBox appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:precinct)&lt;=1">(CI-SISAddr): element hl7:precinct appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:country
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:country" id="d612322e88-false-d612431e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:state
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:state" id="d612322e92-false-d612441e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:city
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:city" id="d612322e96-false-d612451e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:postalCode
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:postalCode" id="d612322e104-false-d612461e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:houseNumber
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:houseNumber" id="d612322e112-false-d612471e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:houseNumberNumeric
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:houseNumberNumeric" id="d612322e116-false-d612481e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:streetName
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:streetName" id="d612322e120-false-d612491e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:streetNameType
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:streetNameType" id="d612322e124-false-d612501e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:additionalLocator
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:additionalLocator" id="d612322e169-false-d612511e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:unitID
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:unitID" id="d612322e184-false-d612521e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:postBox
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:postBox" id="d612322e194-false-d612531e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:precinct
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:precinct" id="d612322e198-false-d612541e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr" id="d612322e202-false-d612551e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISAddr): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:streetAddressLine" id="d612322e220-false-d612607e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:streetAddressLine" id="d612322e226-false-d612617e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:streetAddressLine" id="d612322e230-false-d612627e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:streetAddressLine" id="d612322e234-false-d612637e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:streetAddressLine" id="d612322e238-false-d612647e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:streetAddressLine" id="d612322e242-false-d612657e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.3
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:relatedDocument[@typeCode='RPLC']
Item: (CI-SISRelatedDocument)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:relatedDocument[@typeCode='RPLC']" id="d612658e48-false-d612668e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.3" test="string(@typeCode)=('RPLC')">(CI-SISRelatedDocument): The value for @typeCode SHALL be 'RPLC'.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.3" test="count(hl7:parentDocument[not(@nullFlavor)])&gt;=1">(CI-SISRelatedDocument): element hl7:parentDocument[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.3" test="count(hl7:parentDocument[not(@nullFlavor)])&lt;=1">(CI-SISRelatedDocument): element hl7:parentDocument[not(@nullFlavor)] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.3
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:relatedDocument[@typeCode='RPLC']/hl7:parentDocument[not(@nullFlavor)]
Item: (CI-SISRelatedDocument)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:relatedDocument[@typeCode='RPLC']/hl7:parentDocument[not(@nullFlavor)]" id="d612658e50-false-d612688e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.3" test="count(hl7:id[not(@nullFlavor)])&gt;=1">(CI-SISRelatedDocument): element hl7:id[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.3" test="count(hl7:id[not(@nullFlavor)])&lt;=1">(CI-SISRelatedDocument): element hl7:id[not(@nullFlavor)] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.3
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:relatedDocument[@typeCode='RPLC']/hl7:parentDocument[not(@nullFlavor)]/hl7:id[not(@nullFlavor)]
Item: (CI-SISRelatedDocument)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:relatedDocument[@typeCode='RPLC']/hl7:parentDocument[not(@nullFlavor)]/hl7:id[not(@nullFlavor)]" id="d612658e51-false-d612704e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.3" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISRelatedDocument): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.3" test="not(@extension) or string-length(@extension)&gt;0">(CI-SISRelatedDocument): Attribute @extension SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.3" test="@root">(CI-SISRelatedDocument): attribute @root SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.3" test="not(@root) or matches(@root,'^[0-2](\.(0|[1-9]\d*))*$') or matches(@root,'^[A-Fa-f\d]{8}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{12}$') or matches(@root,'^[A-Za-z][A-Za-z\d\-]*$')">(CI-SISRelatedDocument): Attribute @root SHALL be of data type 'uid'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.4
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]
Item: (CI-SISDocumentationOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]" id="d612705e25-false-d612736e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:serviceEvent[not(@nullFlavor)])&gt;=1">(CI-SISDocumentationOf): element hl7:serviceEvent[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:serviceEvent[not(@nullFlavor)])&lt;=1">(CI-SISDocumentationOf): element hl7:serviceEvent[not(@nullFlavor)] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.4
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]
Item: (CI-SISDocumentationOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]" id="d612705e26-false-d612772e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:code)&lt;=1">(CI-SISDocumentationOf): element hl7:code appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:effectiveTime)&lt;=1">(CI-SISDocumentationOf): element hl7:effectiveTime appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:performer[@typeCode='PRF'][not(@nullFlavor)])&lt;=1">(CI-SISDocumentationOf): element hl7:performer[@typeCode='PRF'][not(@nullFlavor)] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.4
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:code
Item: (CI-SISDocumentationOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:code" id="d612705e34-false-d612809e0">
        <extends rule="CE"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CE' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISDocumentationOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CE", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="not(@displayName) or string-length(@displayName)&gt;0">(CI-SISDocumentationOf): Attribute @displayName SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="@codeSystem">(CI-SISDocumentationOf): attribute @codeSystem SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="not(@codeSystem) or matches(@codeSystem,'^[0-2](\.(0|[1-9]\d*))*$')">(CI-SISDocumentationOf): Attribute @codeSystem SHALL be of data type 'oid'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="@code">(CI-SISDocumentationOf): attribute @code SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="not(@code) or (string-length(@code)&gt;0 and not(matches(@code,'\s')))">(CI-SISDocumentationOf): Attribute @code SHALL be of data type 'cs'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.4
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:effectiveTime
Item: (CI-SISDocumentationOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:effectiveTime" id="d612705e58-false-d612837e0">
        <extends rule="IVL_TS"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='IVL_TS' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISDocumentationOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:IVL_TS", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:low)&gt;=1">(CI-SISDocumentationOf): element hl7:low is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:low)&lt;=1">(CI-SISDocumentationOf): element hl7:low appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:high)&lt;=1">(CI-SISDocumentationOf): element hl7:high appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.4
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:effectiveTime/hl7:low
Item: (CI-SISDocumentationOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:effectiveTime/hl7:low" id="d612705e62-false-d612864e0">
        <extends rule="IVXB_TS"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='IVXB_TS' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISDocumentationOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:IVXB_TS", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="@value">(CI-SISDocumentationOf): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="not(@value) or matches(string(@value), '^[0-9]{4,14}')">(CI-SISDocumentationOf): Attribute @value SHALL be of data type 'ts'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.4
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:effectiveTime/hl7:high
Item: (CI-SISDocumentationOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:effectiveTime/hl7:high" id="d612705e70-false-d612881e0">
        <extends rule="IVXB_TS"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='IVXB_TS' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISDocumentationOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:IVXB_TS", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="@value">(CI-SISDocumentationOf): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="not(@value) or matches(string(@value), '^[0-9]{4,14}')">(CI-SISDocumentationOf): Attribute @value SHALL be of data type 'ts'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.4
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]
Item: (CI-SISDocumentationOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]" id="d612705e78-false-d612908e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="string(@typeCode)=('PRF')">(CI-SISDocumentationOf): The value for @typeCode SHALL be 'PRF'.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:assignedEntity)&gt;=1">(CI-SISDocumentationOf): element hl7:assignedEntity is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:assignedEntity)&lt;=1">(CI-SISDocumentationOf): element hl7:assignedEntity appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.4
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity
Item: (CI-SISDocumentationOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity" id="d612705e117-false-d612948e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:id)&gt;=1">(CI-SISDocumentationOf): element hl7:id is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:id)&lt;=1">(CI-SISDocumentationOf): element hl7:id appears too often [max 1x].</assert>
        <let name="elmcount" value="count(hl7:addr|hl7:addr)"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:assignedPerson)&lt;=1">(CI-SISDocumentationOf): element hl7:assignedPerson appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:representedOrganization)&gt;=1">(CI-SISDocumentationOf): element hl7:representedOrganization is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:representedOrganization)&lt;=1">(CI-SISDocumentationOf): element hl7:representedOrganization appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.4
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:id
Item: (CI-SISDocumentationOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:id" id="d612705e130-false-d613015e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISDocumentationOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="@extension">(CI-SISDocumentationOf): attribute @extension SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="not(@extension) or string-length(@extension)&gt;0">(CI-SISDocumentationOf): Attribute @extension SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="@root">(CI-SISDocumentationOf): attribute @root SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="not(@root) or matches(@root,'^[0-2](\.(0|[1-9]\d*))*$') or matches(@root,'^[A-Fa-f\d]{8}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{12}$') or matches(@root,'^[A-Za-z][A-Za-z\d\-]*$')">(CI-SISDocumentationOf): Attribute @root SHALL be of data type 'uid'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.19
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:telecom
Item: (CI-SISTelecom)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:telecom" id="d613016e35-false-d613040e0">
        <extends rule="TEL"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TEL' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISTelecom): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TEL", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISTelecom): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@value">(CI-SISTelecom): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@value) or string(@value castable as xs:anyURI)">(CI-SISTelecom): Attribute @value SHALL be of data type 'url'<value-of select="@value"/>
        </assert>
        <let name="prefix" value="substring-before(@value, ':')"/>
        <let name="suffix" value="substring-after(@value, ':')"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(             (count(@*) = 1 and name(@*) = 'nullFlavor' and             (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or             ($suffix and (             $prefix = 'tel' or              $prefix = 'fax' or              $prefix = 'mailto' or              $prefix = 'http' or              $prefix = 'ftp' or              $prefix = 'mllp'))             )">(CI-SISTelecom): Erreur de conformité CI-SIS : <name/> n'est pas conforme à une adresse de télécommunication préfixe:chaîne 
            (avec préfixe = tel, fax, mailto, http, ftp ou mllp) 
            ou est vide et sans nullFlavor, ou contient un nullFlavor non admis.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@use = 'H'                      or @use = 'HP'                      or @use = 'HV'                      or @use = 'WP'                     or @use = 'DIR'                      or @use = 'PUB'                      or @use = 'EC'                      or @use = 'MC'                      or @use = 'PG'                      or not(@use)">(CI-SISTelecom): Erreur de conformité CI-SIS : L'attribut use de l'élément telecom n'est pas conforme. 
            Il est facultatif et les valeurs permises sont 'H','HP', 'HV','WP','DIR','PUB','EC','MC','PG'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity
Item: (CI-SISAddr)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr" id="d613041e85-false-d613063e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@hl7:use) or (string-length(@hl7:use)&gt;0 and not(matches(@hl7:use,'\s')))">(CI-SISAddr): Attribute @hl7:use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:country)&lt;=1">(CI-SISAddr): element hl7:country appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:state)&lt;=1">(CI-SISAddr): element hl7:state appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:city)&lt;=1">(CI-SISAddr): element hl7:city appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postalCode)&lt;=1">(CI-SISAddr): element hl7:postalCode appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumber)&lt;=1">(CI-SISAddr): element hl7:houseNumber appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumberNumeric)&lt;=1">(CI-SISAddr): element hl7:houseNumberNumeric appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetName)&lt;=1">(CI-SISAddr): element hl7:streetName appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetNameType)&lt;=1">(CI-SISAddr): element hl7:streetNameType appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:additionalLocator)&lt;=1">(CI-SISAddr): element hl7:additionalLocator appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:unitID)&lt;=1">(CI-SISAddr): element hl7:unitID appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postBox)&lt;=1">(CI-SISAddr): element hl7:postBox appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:precinct)&lt;=1">(CI-SISAddr): element hl7:precinct appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:country
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:country" id="d613041e103-false-d613161e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:state
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:state" id="d613041e107-false-d613171e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:city
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:city" id="d613041e111-false-d613181e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:postalCode
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:postalCode" id="d613041e119-false-d613191e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:houseNumber
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:houseNumber" id="d613041e127-false-d613201e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:houseNumberNumeric
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:houseNumberNumeric" id="d613041e131-false-d613211e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:streetName
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:streetName" id="d613041e135-false-d613221e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:streetNameType
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:streetNameType" id="d613041e139-false-d613231e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:additionalLocator
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:additionalLocator" id="d613041e184-false-d613241e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:unitID
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:unitID" id="d613041e199-false-d613251e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:postBox
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:postBox" id="d613041e209-false-d613261e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:precinct
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:precinct" id="d613041e213-false-d613271e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr" id="d613041e217-false-d613281e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISAddr): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine" id="d613041e235-false-d613337e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine" id="d613041e241-false-d613347e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine" id="d613041e245-false-d613357e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine" id="d613041e249-false-d613367e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine" id="d613041e253-false-d613377e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine" id="d613041e257-false-d613387e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.4
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:assignedPerson
Item: (CI-SISDocumentationOf)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.4
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:assignedPerson/hl7:name
Item: (CI-SISDocumentationOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:assignedPerson/hl7:name" id="d612705e136-false-d613407e0">
        <extends rule="PN"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='PN' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISDocumentationOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:PN", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:family)&gt;=1">(CI-SISDocumentationOf): element hl7:family is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:family)&lt;=1">(CI-SISDocumentationOf): element hl7:family appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:given)&lt;=1">(CI-SISDocumentationOf): element hl7:given appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:prefix)&lt;=1">(CI-SISDocumentationOf): element hl7:prefix appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.4
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:assignedPerson/hl7:name/hl7:family
Item: (CI-SISDocumentationOf)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.4
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:assignedPerson/hl7:name/hl7:given
Item: (CI-SISDocumentationOf)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.4
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:assignedPerson/hl7:name/hl7:prefix
Item: (CI-SISDocumentationOf)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.4
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization
Item: (CI-SISDocumentationOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization" id="d612705e143-false-d613467e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:id)&lt;=1">(CI-SISDocumentationOf): element hl7:id appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:name)&lt;=1">(CI-SISDocumentationOf): element hl7:name appears too often [max 1x].</assert>
        <let name="elmcount" value="count(hl7:addr|hl7:addr)"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:standardIndustryClassCode[not(@nullFlavor)])&gt;=1">(CI-SISDocumentationOf): element hl7:standardIndustryClassCode[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:standardIndustryClassCode[not(@nullFlavor)])&lt;=1">(CI-SISDocumentationOf): element hl7:standardIndustryClassCode[not(@nullFlavor)] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.4
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:id
Item: (CI-SISDocumentationOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:id" id="d612705e147-false-d613526e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISDocumentationOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.4
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:name
Item: (CI-SISDocumentationOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:name" id="d612705e148-false-d613536e0">
        <extends rule="ON"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ON' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISDocumentationOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ON", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.19
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:telecom
Item: (CI-SISTelecom)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:telecom" id="d613537e27-false-d613547e0">
        <extends rule="TEL"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TEL' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISTelecom): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TEL", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISTelecom): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@value">(CI-SISTelecom): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@value) or string(@value castable as xs:anyURI)">(CI-SISTelecom): Attribute @value SHALL be of data type 'url'<value-of select="@value"/>
        </assert>
        <let name="prefix" value="substring-before(@value, ':')"/>
        <let name="suffix" value="substring-after(@value, ':')"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(             (count(@*) = 1 and name(@*) = 'nullFlavor' and             (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or             ($suffix and (             $prefix = 'tel' or              $prefix = 'fax' or              $prefix = 'mailto' or              $prefix = 'http' or              $prefix = 'ftp' or              $prefix = 'mllp'))             )">(CI-SISTelecom): Erreur de conformité CI-SIS : <name/> n'est pas conforme à une adresse de télécommunication préfixe:chaîne 
            (avec préfixe = tel, fax, mailto, http, ftp ou mllp) 
            ou est vide et sans nullFlavor, ou contient un nullFlavor non admis.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@use = 'H'                      or @use = 'HP'                      or @use = 'HV'                      or @use = 'WP'                     or @use = 'DIR'                      or @use = 'PUB'                      or @use = 'EC'                      or @use = 'MC'                      or @use = 'PG'                      or not(@use)">(CI-SISTelecom): Erreur de conformité CI-SIS : L'attribut use de l'élément telecom n'est pas conforme. 
            Il est facultatif et les valeurs permises sont 'H','HP', 'HV','WP','DIR','PUB','EC','MC','PG'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization
Item: (CI-SISAddr)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr" id="d613548e85-false-d613570e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@hl7:use) or (string-length(@hl7:use)&gt;0 and not(matches(@hl7:use,'\s')))">(CI-SISAddr): Attribute @hl7:use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:country)&lt;=1">(CI-SISAddr): element hl7:country appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:state)&lt;=1">(CI-SISAddr): element hl7:state appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:city)&lt;=1">(CI-SISAddr): element hl7:city appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postalCode)&lt;=1">(CI-SISAddr): element hl7:postalCode appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumber)&lt;=1">(CI-SISAddr): element hl7:houseNumber appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumberNumeric)&lt;=1">(CI-SISAddr): element hl7:houseNumberNumeric appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetName)&lt;=1">(CI-SISAddr): element hl7:streetName appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetNameType)&lt;=1">(CI-SISAddr): element hl7:streetNameType appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:additionalLocator)&lt;=1">(CI-SISAddr): element hl7:additionalLocator appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:unitID)&lt;=1">(CI-SISAddr): element hl7:unitID appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postBox)&lt;=1">(CI-SISAddr): element hl7:postBox appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:precinct)&lt;=1">(CI-SISAddr): element hl7:precinct appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:country
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:country" id="d613548e103-false-d613668e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:state
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:state" id="d613548e107-false-d613678e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:city
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:city" id="d613548e111-false-d613688e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:postalCode
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:postalCode" id="d613548e119-false-d613698e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:houseNumber
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:houseNumber" id="d613548e127-false-d613708e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:houseNumberNumeric
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:houseNumberNumeric" id="d613548e131-false-d613718e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetName
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetName" id="d613548e135-false-d613728e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetNameType
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetNameType" id="d613548e139-false-d613738e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:additionalLocator
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:additionalLocator" id="d613548e184-false-d613748e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:unitID
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:unitID" id="d613548e199-false-d613758e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:postBox
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:postBox" id="d613548e209-false-d613768e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:precinct
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:precinct" id="d613548e213-false-d613778e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr" id="d613548e217-false-d613788e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISAddr): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine" id="d613548e235-false-d613844e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine" id="d613548e241-false-d613854e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine" id="d613548e245-false-d613864e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine" id="d613548e249-false-d613874e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine" id="d613548e253-false-d613884e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine" id="d613548e257-false-d613894e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.4
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:standardIndustryClassCode[not(@nullFlavor)]
Item: (CI-SISDocumentationOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:standardIndustryClassCode[not(@nullFlavor)]" id="d612705e151-false-d613904e0">
        <extends rule="CE"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CE' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISDocumentationOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CE", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="@displayName">(CI-SISDocumentationOf): attribute @displayName SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="not(@displayName) or string-length(@displayName)&gt;0">(CI-SISDocumentationOf): Attribute @displayName SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="@codeSystem">(CI-SISDocumentationOf): attribute @codeSystem SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="not(@codeSystem) or matches(@codeSystem,'^[0-2](\.(0|[1-9]\d*))*$')">(CI-SISDocumentationOf): Attribute @codeSystem SHALL be of data type 'oid'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="@code">(CI-SISDocumentationOf): attribute @code SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="not(@code) or (string-length(@code)&gt;0 and not(matches(@code,'\s')))">(CI-SISDocumentationOf): Attribute @code SHALL be of data type 'cs'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.5
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:custodian[not(@nullFlavor)]
Item: (CI-SISCustodian)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:custodian[not(@nullFlavor)]" id="d613905e29-false-d613941e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.5" test="count(hl7:assignedCustodian)&gt;=1">(CI-SISCustodian): element hl7:assignedCustodian is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.5" test="count(hl7:assignedCustodian)&lt;=1">(CI-SISCustodian): element hl7:assignedCustodian appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.5
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian
Item: (CI-SISCustodian)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian" id="d613905e35-false-d613967e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.5" test="count(hl7:representedCustodianOrganization)&gt;=1">(CI-SISCustodian): element hl7:representedCustodianOrganization is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.5" test="count(hl7:representedCustodianOrganization)&lt;=1">(CI-SISCustodian): element hl7:representedCustodianOrganization appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.5
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization
Item: (CI-SISCustodian)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization" id="d613905e41-false-d613993e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.5" test="count(hl7:id[not(@nullFlavor)])&gt;=1">(CI-SISCustodian): element hl7:id[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.5" test="count(hl7:id[not(@nullFlavor)])&lt;=1">(CI-SISCustodian): element hl7:id[not(@nullFlavor)] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.5" test="count(hl7:name)&lt;=1">(CI-SISCustodian): element hl7:name appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.5" test="count(hl7:telecom)&lt;=1">(CI-SISCustodian): element hl7:telecom appears too often [max 1x].</assert>
        <let name="elmcount" value="count(hl7:addr|hl7:addr)"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.5" test="$elmcount&lt;=1">(CI-SISCustodian): choice (hl7:addr or hl7:addr) contains too many elements [max 1x]</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.5
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:id[not(@nullFlavor)]
Item: (CI-SISCustodian)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:id[not(@nullFlavor)]" id="d613905e92-false-d614051e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.5" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISCustodian): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.5" test="not(@extension) or string-length(@extension)&gt;0">(CI-SISCustodian): Attribute @extension SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.5" test="@root">(CI-SISCustodian): attribute @root SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.5" test="not(@root) or matches(@root,'^[0-2](\.(0|[1-9]\d*))*$') or matches(@root,'^[A-Fa-f\d]{8}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{12}$') or matches(@root,'^[A-Za-z][A-Za-z\d\-]*$')">(CI-SISCustodian): Attribute @root SHALL be of data type 'uid'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.5" test="hl7:custodian/hl7:representedCustodianOrganization/hl7:id/@root='1.2.250.1.71.4.2.2' or hl7:custodian/hl7:representedCustodianOrganization/hl7:id/@root='1.2.250.1.213.4.1'">(CI-SISCustodian): L'attribut @root doit avoir la valeur :
- soit "1.2.250.1.71.4.2.2" pour l'OID des structures de santé
- soit "1.2.250.1.213.4.1" pour l'OID de l'organisation hébergeant les documents produits par le patient</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.5
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:name
Item: (CI-SISCustodian)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:name" id="d613905e110-false-d614072e0">
        <extends rule="ON"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.5" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ON' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISCustodian): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ON", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.19
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:telecom
Item: (CI-SISTelecom)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:telecom" id="d614073e27-false-d614083e0">
        <extends rule="TEL"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TEL' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISTelecom): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TEL", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISTelecom): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@value">(CI-SISTelecom): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@value) or string(@value castable as xs:anyURI)">(CI-SISTelecom): Attribute @value SHALL be of data type 'url'<value-of select="@value"/>
        </assert>
        <let name="prefix" value="substring-before(@value, ':')"/>
        <let name="suffix" value="substring-after(@value, ':')"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(             (count(@*) = 1 and name(@*) = 'nullFlavor' and             (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or             ($suffix and (             $prefix = 'tel' or              $prefix = 'fax' or              $prefix = 'mailto' or              $prefix = 'http' or              $prefix = 'ftp' or              $prefix = 'mllp'))             )">(CI-SISTelecom): Erreur de conformité CI-SIS : <name/> n'est pas conforme à une adresse de télécommunication préfixe:chaîne 
            (avec préfixe = tel, fax, mailto, http, ftp ou mllp) 
            ou est vide et sans nullFlavor, ou contient un nullFlavor non admis.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@use = 'H'                      or @use = 'HP'                      or @use = 'HV'                      or @use = 'WP'                     or @use = 'DIR'                      or @use = 'PUB'                      or @use = 'EC'                      or @use = 'MC'                      or @use = 'PG'                      or not(@use)">(CI-SISTelecom): Erreur de conformité CI-SIS : L'attribut use de l'élément telecom n'est pas conforme. 
            Il est facultatif et les valeurs permises sont 'H','HP', 'HV','WP','DIR','PUB','EC','MC','PG'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization
Item: (CI-SISAddr)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr" id="d614084e85-false-d614106e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@hl7:use) or (string-length(@hl7:use)&gt;0 and not(matches(@hl7:use,'\s')))">(CI-SISAddr): Attribute @hl7:use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:country)&lt;=1">(CI-SISAddr): element hl7:country appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:state)&lt;=1">(CI-SISAddr): element hl7:state appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:city)&lt;=1">(CI-SISAddr): element hl7:city appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postalCode)&lt;=1">(CI-SISAddr): element hl7:postalCode appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumber)&lt;=1">(CI-SISAddr): element hl7:houseNumber appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumberNumeric)&lt;=1">(CI-SISAddr): element hl7:houseNumberNumeric appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetName)&lt;=1">(CI-SISAddr): element hl7:streetName appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetNameType)&lt;=1">(CI-SISAddr): element hl7:streetNameType appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:additionalLocator)&lt;=1">(CI-SISAddr): element hl7:additionalLocator appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:unitID)&lt;=1">(CI-SISAddr): element hl7:unitID appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postBox)&lt;=1">(CI-SISAddr): element hl7:postBox appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:precinct)&lt;=1">(CI-SISAddr): element hl7:precinct appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:country
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:country" id="d614084e103-false-d614204e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:state
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:state" id="d614084e107-false-d614214e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:city
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:city" id="d614084e111-false-d614224e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:postalCode
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:postalCode" id="d614084e119-false-d614234e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:houseNumber
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:houseNumber" id="d614084e127-false-d614244e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:houseNumberNumeric
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:houseNumberNumeric" id="d614084e131-false-d614254e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:streetName
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:streetName" id="d614084e135-false-d614264e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:streetNameType
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:streetNameType" id="d614084e139-false-d614274e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:additionalLocator
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:additionalLocator" id="d614084e184-false-d614284e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:unitID
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:unitID" id="d614084e199-false-d614294e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:postBox
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:postBox" id="d614084e209-false-d614304e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:precinct
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:precinct" id="d614084e213-false-d614314e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr" id="d614084e217-false-d614324e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISAddr): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:streetAddressLine" id="d614084e235-false-d614380e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:streetAddressLine" id="d614084e241-false-d614390e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:streetAddressLine" id="d614084e245-false-d614400e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:streetAddressLine" id="d614084e249-false-d614410e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:streetAddressLine" id="d614084e253-false-d614420e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:streetAddressLine" id="d614084e257-false-d614430e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.6
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]
Item: (CI-SISLegalAuthenticator)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]" id="d614431e25-false-d614452e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:time)&gt;=1">(CI-SISLegalAuthenticator): element hl7:time is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:time)&lt;=1">(CI-SISLegalAuthenticator): element hl7:time appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:signatureCode[@code='S' or @nullFlavor])&gt;=1">(CI-SISLegalAuthenticator): element hl7:signatureCode[@code='S' or @nullFlavor] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:signatureCode[@code='S' or @nullFlavor])&lt;=1">(CI-SISLegalAuthenticator): element hl7:signatureCode[@code='S' or @nullFlavor] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:assignedEntity)&gt;=1">(CI-SISLegalAuthenticator): element hl7:assignedEntity is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:assignedEntity)&lt;=1">(CI-SISLegalAuthenticator): element hl7:assignedEntity appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.6
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:time
Item: (CI-SISLegalAuthenticator)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:time" id="d614431e26-false-d614499e0">
        <extends rule="TS"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TS' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISLegalAuthenticator): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TS", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="not(*)">(CI-SISLegalAuthenticator): <value-of select="local-name()"/> with datatype TS, SHOULD NOT have child elements.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="@value">(CI-SISLegalAuthenticator): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="not(@value) or matches(string(@value), '^[0-9]{4,14}')">(CI-SISLegalAuthenticator): Attribute @value SHALL be of data type 'ts'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.6
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:signatureCode[@code='S' or @nullFlavor]
Item: (CI-SISLegalAuthenticator)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:signatureCode[@code='S' or @nullFlavor]" id="d614431e34-false-d614520e0">
        <extends rule="CS"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CS' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISLegalAuthenticator): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CS", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="@nullFlavor or (@code='S')">(CI-SISLegalAuthenticator): The element value SHALL be one of 'code 'S''.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.6
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity
Item: (CI-SISLegalAuthenticator)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity" id="d614431e41-false-d614546e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:id[not(@nullFlavor)])&gt;=1">(CI-SISLegalAuthenticator): element hl7:id[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:id[not(@nullFlavor)])&lt;=1">(CI-SISLegalAuthenticator): element hl7:id[not(@nullFlavor)] appears too often [max 1x].</assert>
        <let name="elmcount" value="count(hl7:addr|hl7:addr)"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:assignedPerson[not(@nullFlavor)])&gt;=1">(CI-SISLegalAuthenticator): element hl7:assignedPerson[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:assignedPerson[not(@nullFlavor)])&lt;=1">(CI-SISLegalAuthenticator): element hl7:assignedPerson[not(@nullFlavor)] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']])&lt;=1">(CI-SISLegalAuthenticator): element hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.6
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:id[not(@nullFlavor)]
Item: (CI-SISLegalAuthenticator)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:id[not(@nullFlavor)]" id="d614431e51-false-d614614e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISLegalAuthenticator): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="@root">(CI-SISLegalAuthenticator): attribute @root SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="not(@root) or matches(@root,'^[0-2](\.(0|[1-9]\d*))*$') or matches(@root,'^[A-Fa-f\d]{8}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{12}$') or matches(@root,'^[A-Za-z][A-Za-z\d\-]*$')">(CI-SISLegalAuthenticator): Attribute @root SHALL be of data type 'uid'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="@extension">(CI-SISLegalAuthenticator): attribute @extension SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="not(@extension) or string-length(@extension)&gt;0">(CI-SISLegalAuthenticator): Attribute @extension SHALL be of data type 'st'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity
Item: (CI-SISAddr)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr" id="d614615e78-false-d614640e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@hl7:use) or (string-length(@hl7:use)&gt;0 and not(matches(@hl7:use,'\s')))">(CI-SISAddr): Attribute @hl7:use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:country)&lt;=1">(CI-SISAddr): element hl7:country appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:state)&lt;=1">(CI-SISAddr): element hl7:state appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:city)&lt;=1">(CI-SISAddr): element hl7:city appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postalCode)&lt;=1">(CI-SISAddr): element hl7:postalCode appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumber)&lt;=1">(CI-SISAddr): element hl7:houseNumber appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumberNumeric)&lt;=1">(CI-SISAddr): element hl7:houseNumberNumeric appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetName)&lt;=1">(CI-SISAddr): element hl7:streetName appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetNameType)&lt;=1">(CI-SISAddr): element hl7:streetNameType appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:additionalLocator)&lt;=1">(CI-SISAddr): element hl7:additionalLocator appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:unitID)&lt;=1">(CI-SISAddr): element hl7:unitID appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postBox)&lt;=1">(CI-SISAddr): element hl7:postBox appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:precinct)&lt;=1">(CI-SISAddr): element hl7:precinct appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:country
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:country" id="d614615e96-false-d614738e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:state
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:state" id="d614615e100-false-d614748e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:city
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:city" id="d614615e104-false-d614758e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:postalCode
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:postalCode" id="d614615e112-false-d614768e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:houseNumber
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:houseNumber" id="d614615e120-false-d614778e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:houseNumberNumeric
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:houseNumberNumeric" id="d614615e124-false-d614788e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:streetName
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:streetName" id="d614615e128-false-d614798e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:streetNameType
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:streetNameType" id="d614615e132-false-d614808e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:additionalLocator
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:additionalLocator" id="d614615e177-false-d614818e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:unitID
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:unitID" id="d614615e192-false-d614828e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:postBox
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:postBox" id="d614615e202-false-d614838e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:precinct
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:precinct" id="d614615e206-false-d614848e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr" id="d614615e210-false-d614858e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISAddr): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine" id="d614615e228-false-d614914e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine" id="d614615e234-false-d614924e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine" id="d614615e238-false-d614934e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine" id="d614615e242-false-d614944e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine" id="d614615e246-false-d614954e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine" id="d614615e250-false-d614964e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.19
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:telecom
Item: (CI-SISTelecom)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:telecom" id="d614965e27-false-d614975e0">
        <extends rule="TEL"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TEL' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISTelecom): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TEL", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISTelecom): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@value">(CI-SISTelecom): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@value) or string(@value castable as xs:anyURI)">(CI-SISTelecom): Attribute @value SHALL be of data type 'url'<value-of select="@value"/>
        </assert>
        <let name="prefix" value="substring-before(@value, ':')"/>
        <let name="suffix" value="substring-after(@value, ':')"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(             (count(@*) = 1 and name(@*) = 'nullFlavor' and             (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or             ($suffix and (             $prefix = 'tel' or              $prefix = 'fax' or              $prefix = 'mailto' or              $prefix = 'http' or              $prefix = 'ftp' or              $prefix = 'mllp'))             )">(CI-SISTelecom): Erreur de conformité CI-SIS : <name/> n'est pas conforme à une adresse de télécommunication préfixe:chaîne 
            (avec préfixe = tel, fax, mailto, http, ftp ou mllp) 
            ou est vide et sans nullFlavor, ou contient un nullFlavor non admis.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@use = 'H'                      or @use = 'HP'                      or @use = 'HV'                      or @use = 'WP'                     or @use = 'DIR'                      or @use = 'PUB'                      or @use = 'EC'                      or @use = 'MC'                      or @use = 'PG'                      or not(@use)">(CI-SISTelecom): Erreur de conformité CI-SIS : L'attribut use de l'élément telecom n'est pas conforme. 
            Il est facultatif et les valeurs permises sont 'H','HP', 'HV','WP','DIR','PUB','EC','MC','PG'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.6
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:assignedPerson[not(@nullFlavor)]
Item: (CI-SISLegalAuthenticator)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:assignedPerson[not(@nullFlavor)]" id="d614431e73-false-d614996e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:name[not(@nullFlavor)])&gt;=1">(CI-SISLegalAuthenticator): element hl7:name[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:name[not(@nullFlavor)])&lt;=1">(CI-SISLegalAuthenticator): element hl7:name[not(@nullFlavor)] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.6
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:assignedPerson[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]
Item: (CI-SISLegalAuthenticator)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:assignedPerson[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]" id="d614431e80-false-d615012e0">
        <extends rule="PN"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='PN' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISLegalAuthenticator): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:PN", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:family)&gt;=1">(CI-SISLegalAuthenticator): element hl7:family is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:family)&lt;=1">(CI-SISLegalAuthenticator): element hl7:family appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:given)&lt;=1">(CI-SISLegalAuthenticator): element hl7:given appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:prefix)&lt;=1">(CI-SISLegalAuthenticator): element hl7:prefix appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.6
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:assignedPerson[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]/hl7:family
Item: (CI-SISLegalAuthenticator)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.6
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:assignedPerson[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]/hl7:given
Item: (CI-SISLegalAuthenticator)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.6
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:assignedPerson[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]/hl7:prefix
Item: (CI-SISLegalAuthenticator)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.6
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]
Item: (CI-SISLegalAuthenticator)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]" id="d614431e90-false-d615073e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:id[@root='1.2.250.1.71.4.2.2'])&gt;=1">(CI-SISLegalAuthenticator): element hl7:id[@root='1.2.250.1.71.4.2.2'] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:id[@root='1.2.250.1.71.4.2.2'])&lt;=1">(CI-SISLegalAuthenticator): element hl7:id[@root='1.2.250.1.71.4.2.2'] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:name)&lt;=1">(CI-SISLegalAuthenticator): element hl7:name appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:standardIndustryClassCode)&lt;=1">(CI-SISLegalAuthenticator): element hl7:standardIndustryClassCode appears too often [max 1x].</assert>
        <let name="elmcount" value="count(hl7:addr|hl7:addr)"/>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.6
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:id[@root='1.2.250.1.71.4.2.2']
Item: (CI-SISLegalAuthenticator)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:id[@root='1.2.250.1.71.4.2.2']" id="d614431e91-false-d615134e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISLegalAuthenticator): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="@extension">(CI-SISLegalAuthenticator): attribute @extension SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="not(@extension) or string-length(@extension)&gt;0">(CI-SISLegalAuthenticator): Attribute @extension SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="string(@root)=('1.2.250.1.71.4.2.2')">(CI-SISLegalAuthenticator): The value for @root SHALL be '1.2.250.1.71.4.2.2'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.6
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:name
Item: (CI-SISLegalAuthenticator)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:name" id="d614431e94-false-d615155e0">
        <extends rule="ON"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ON' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISLegalAuthenticator): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ON", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.6
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:standardIndustryClassCode
Item: (CI-SISLegalAuthenticator)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:standardIndustryClassCode" id="d614431e95-false-d615165e0">
        <extends rule="CE"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CE' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISLegalAuthenticator): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CE", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="@displayName">(CI-SISLegalAuthenticator): attribute @displayName SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="not(@displayName) or string-length(@displayName)&gt;0">(CI-SISLegalAuthenticator): Attribute @displayName SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="@codeSystem">(CI-SISLegalAuthenticator): attribute @codeSystem SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="not(@codeSystem) or matches(@codeSystem,'^[0-2](\.(0|[1-9]\d*))*$')">(CI-SISLegalAuthenticator): Attribute @codeSystem SHALL be of data type 'oid'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="@code">(CI-SISLegalAuthenticator): attribute @code SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="not(@code) or (string-length(@code)&gt;0 and not(matches(@code,'\s')))">(CI-SISLegalAuthenticator): Attribute @code SHALL be of data type 'cs'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.19
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:telecom
Item: (CI-SISTelecom)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:telecom" id="d615166e39-false-d615197e0">
        <extends rule="TEL"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TEL' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISTelecom): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TEL", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISTelecom): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@value">(CI-SISTelecom): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@value) or string(@value castable as xs:anyURI)">(CI-SISTelecom): Attribute @value SHALL be of data type 'url'<value-of select="@value"/>
        </assert>
        <let name="prefix" value="substring-before(@value, ':')"/>
        <let name="suffix" value="substring-after(@value, ':')"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(             (count(@*) = 1 and name(@*) = 'nullFlavor' and             (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or             ($suffix and (             $prefix = 'tel' or              $prefix = 'fax' or              $prefix = 'mailto' or              $prefix = 'http' or              $prefix = 'ftp' or              $prefix = 'mllp'))             )">(CI-SISTelecom): Erreur de conformité CI-SIS : <name/> n'est pas conforme à une adresse de télécommunication préfixe:chaîne 
            (avec préfixe = tel, fax, mailto, http, ftp ou mllp) 
            ou est vide et sans nullFlavor, ou contient un nullFlavor non admis.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@use = 'H'                      or @use = 'HP'                      or @use = 'HV'                      or @use = 'WP'                     or @use = 'DIR'                      or @use = 'PUB'                      or @use = 'EC'                      or @use = 'MC'                      or @use = 'PG'                      or not(@use)">(CI-SISTelecom): Erreur de conformité CI-SIS : L'attribut use de l'élément telecom n'est pas conforme. 
            Il est facultatif et les valeurs permises sont 'H','HP', 'HV','WP','DIR','PUB','EC','MC','PG'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]
Item: (CI-SISAddr)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr" id="d615198e85-false-d615220e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@hl7:use) or (string-length(@hl7:use)&gt;0 and not(matches(@hl7:use,'\s')))">(CI-SISAddr): Attribute @hl7:use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:country)&lt;=1">(CI-SISAddr): element hl7:country appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:state)&lt;=1">(CI-SISAddr): element hl7:state appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:city)&lt;=1">(CI-SISAddr): element hl7:city appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postalCode)&lt;=1">(CI-SISAddr): element hl7:postalCode appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumber)&lt;=1">(CI-SISAddr): element hl7:houseNumber appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumberNumeric)&lt;=1">(CI-SISAddr): element hl7:houseNumberNumeric appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetName)&lt;=1">(CI-SISAddr): element hl7:streetName appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetNameType)&lt;=1">(CI-SISAddr): element hl7:streetNameType appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:additionalLocator)&lt;=1">(CI-SISAddr): element hl7:additionalLocator appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:unitID)&lt;=1">(CI-SISAddr): element hl7:unitID appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postBox)&lt;=1">(CI-SISAddr): element hl7:postBox appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:precinct)&lt;=1">(CI-SISAddr): element hl7:precinct appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:country
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:country" id="d615198e103-false-d615318e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:state
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:state" id="d615198e107-false-d615328e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:city
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:city" id="d615198e111-false-d615338e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:postalCode
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:postalCode" id="d615198e119-false-d615348e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:houseNumber
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:houseNumber" id="d615198e127-false-d615358e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:houseNumberNumeric
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:houseNumberNumeric" id="d615198e131-false-d615368e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:streetName
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:streetName" id="d615198e135-false-d615378e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:streetNameType
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:streetNameType" id="d615198e139-false-d615388e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:additionalLocator
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:additionalLocator" id="d615198e184-false-d615398e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:unitID
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:unitID" id="d615198e199-false-d615408e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:postBox
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:postBox" id="d615198e209-false-d615418e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:precinct
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:precinct" id="d615198e213-false-d615428e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr" id="d615198e217-false-d615438e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISAddr): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:streetAddressLine" id="d615198e235-false-d615494e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:streetAddressLine" id="d615198e241-false-d615504e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:streetAddressLine" id="d615198e245-false-d615514e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:streetAddressLine" id="d615198e249-false-d615524e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:streetAddressLine" id="d615198e253-false-d615534e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:streetAddressLine" id="d615198e257-false-d615544e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.7
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]
Item: (CI-SISAuthor)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]" id="d615545e99-false-d615561e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:functionCode[(@code='ADMPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ATTPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='DISPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PRISURG' and @codeSystem='2.16.840.1.113883.5.88') or (@code='FASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='SASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='NASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANEST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANRS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='MDWF' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PCP' and @codeSystem='2.16.840.1.113883.5.88') or (@code='CORRE' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='REMPL' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='GYNEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='OPHTA' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PSYCH' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='CARDT' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PRELV' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='ASPEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='APSIN' and @codeSystem='1.2.250.1.213.1.1.4.2.280')])&lt;=1">(CI-SISAuthor): element hl7:functionCode[(@code='ADMPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ATTPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='DISPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PRISURG' and @codeSystem='2.16.840.1.113883.5.88') or (@code='FASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='SASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='NASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANEST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANRS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='MDWF' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PCP' and @codeSystem='2.16.840.1.113883.5.88') or (@code='CORRE' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='REMPL' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='GYNEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='OPHTA' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PSYCH' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='CARDT' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PRELV' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='ASPEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='APSIN' and @codeSystem='1.2.250.1.213.1.1.4.2.280')] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:time)&gt;=1">(CI-SISAuthor): element hl7:time is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:time)&lt;=1">(CI-SISAuthor): element hl7:time appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:assignedAuthor[not(@nullFlavor)])&gt;=1">(CI-SISAuthor): element hl7:assignedAuthor[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:assignedAuthor[not(@nullFlavor)])&lt;=1">(CI-SISAuthor): element hl7:assignedAuthor[not(@nullFlavor)] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.7
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:functionCode[(@code='ADMPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ATTPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='DISPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PRISURG' and @codeSystem='2.16.840.1.113883.5.88') or (@code='FASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='SASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='NASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANEST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANRS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='MDWF' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PCP' and @codeSystem='2.16.840.1.113883.5.88') or (@code='CORRE' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='REMPL' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='GYNEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='OPHTA' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PSYCH' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='CARDT' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PRELV' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='ASPEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='APSIN' and @codeSystem='1.2.250.1.213.1.1.4.2.280')]
Item: (CI-SISAuthor)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:functionCode[(@code='ADMPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ATTPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='DISPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PRISURG' and @codeSystem='2.16.840.1.113883.5.88') or (@code='FASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='SASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='NASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANEST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANRS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='MDWF' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PCP' and @codeSystem='2.16.840.1.113883.5.88') or (@code='CORRE' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='REMPL' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='GYNEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='OPHTA' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PSYCH' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='CARDT' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PRELV' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='ASPEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='APSIN' and @codeSystem='1.2.250.1.213.1.1.4.2.280')]" id="d615545e100-false-d615601e0">
        <extends rule="CE"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CE' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAuthor): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CE", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="@nullFlavor or (@code='ADMPHYS' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='médecin ayant admis le patient dans la structure de soins') or (@code='ATTPHYS' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='médecin responsable du patient dans la structure de soins') or (@code='DISPHYS' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='médecin ayant décidé la sortie du patient de la structure de soins') or (@code='PRISURG' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='médecin intervenant principal') or (@code='FASST' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='premier assistant lors de l’intervention') or (@code='SASST' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='second assistant lors de l’intervention') or (@code='NASST' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='infirmier(e)') or (@code='ANEST' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='médecin anesthésiste') or (@code='ANRS' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='infirmier(e) anesthésiste') or (@code='MDWF' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='sage-femme') or (@code='PCP' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='médecin traitant') or (@code='CORRE' and @codeSystem='1.2.250.1.213.1.1.4.2.280' and @displayName='médecin correspondant') or (@code='REMPL' and @codeSystem='1.2.250.1.213.1.1.4.2.280' and @displayName='médecin remplaçant du médecin traitant') or (@code='GYNEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280' and @displayName='gynécologue') or (@code='OPHTA' and @codeSystem='1.2.250.1.213.1.1.4.2.280' and @displayName='ophtalmologue') or (@code='PSYCH' and @codeSystem='1.2.250.1.213.1.1.4.2.280' and @displayName='psychiatre ou neuropsychiatre') or (@code='CARDT' and @codeSystem='1.2.250.1.213.1.1.4.2.280' and @displayName='cardiologue traitant') or (@code='PRELV' and @codeSystem='1.2.250.1.213.1.1.4.2.280' and @displayName='préleveur d’échantillons biologiques pour l’exécution d’une prescription d’examens biologiques') or (@code='ASPEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280' and @displayName='autre spécialiste') or (@code='APSIN' and @codeSystem='1.2.250.1.213.1.1.4.2.280' and @displayName='autre PS informateur')">(CI-SISAuthor): The element value SHALL be one of 'code 'ADMPHYS' codeSystem '2.16.840.1.113883.5.88' displayName='médecin ayant admis le patient dans la structure de soins' or code 'ATTPHYS' codeSystem '2.16.840.1.113883.5.88' displayName='médecin responsable du patient dans la structure de soins' or code 'DISPHYS' codeSystem '2.16.840.1.113883.5.88' displayName='médecin ayant décidé la sortie du patient de la structure de soins' or code 'PRISURG' codeSystem '2.16.840.1.113883.5.88' displayName='médecin intervenant principal' or code 'FASST' codeSystem '2.16.840.1.113883.5.88' displayName='premier assistant lors de l’intervention' or code 'SASST' codeSystem '2.16.840.1.113883.5.88' displayName='second assistant lors de l’intervention' or code 'NASST' codeSystem '2.16.840.1.113883.5.88' displayName='infirmier(e)' or code 'ANEST' codeSystem '2.16.840.1.113883.5.88' displayName='médecin anesthésiste' or code 'ANRS' codeSystem '2.16.840.1.113883.5.88' displayName='infirmier(e) anesthésiste' or code 'MDWF' codeSystem '2.16.840.1.113883.5.88' displayName='sage-femme' or code 'PCP' codeSystem '2.16.840.1.113883.5.88' displayName='médecin traitant' or code 'CORRE' codeSystem '1.2.250.1.213.1.1.4.2.280' displayName='médecin correspondant' or code 'REMPL' codeSystem '1.2.250.1.213.1.1.4.2.280' displayName='médecin remplaçant du médecin traitant' or code 'GYNEC' codeSystem '1.2.250.1.213.1.1.4.2.280' displayName='gynécologue' or code 'OPHTA' codeSystem '1.2.250.1.213.1.1.4.2.280' displayName='ophtalmologue' or code 'PSYCH' codeSystem '1.2.250.1.213.1.1.4.2.280' displayName='psychiatre ou neuropsychiatre' or code 'CARDT' codeSystem '1.2.250.1.213.1.1.4.2.280' displayName='cardiologue traitant' or code 'PRELV' codeSystem '1.2.250.1.213.1.1.4.2.280' displayName='préleveur d’échantillons biologiques pour l’exécution d’une prescription d’examens biologiques' or code 'ASPEC' codeSystem '1.2.250.1.213.1.1.4.2.280' displayName='autre spécialiste' or code 'APSIN' codeSystem '1.2.250.1.213.1.1.4.2.280' displayName='autre PS informateur''.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="not(@displayName) or string-length(@displayName)&gt;0">(CI-SISAuthor): Attribute @displayName SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="@codeSystem">(CI-SISAuthor): attribute @codeSystem SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="not(@codeSystem) or matches(@codeSystem,'^[0-2](\.(0|[1-9]\d*))*$')">(CI-SISAuthor): Attribute @codeSystem SHALL be of data type 'oid'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="@code">(CI-SISAuthor): attribute @code SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="not(@code) or (string-length(@code)&gt;0 and not(matches(@code,'\s')))">(CI-SISAuthor): Attribute @code SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:originalText)&lt;=1">(CI-SISAuthor): element hl7:originalText appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.7
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:functionCode[(@code='ADMPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ATTPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='DISPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PRISURG' and @codeSystem='2.16.840.1.113883.5.88') or (@code='FASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='SASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='NASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANEST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANRS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='MDWF' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PCP' and @codeSystem='2.16.840.1.113883.5.88') or (@code='CORRE' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='REMPL' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='GYNEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='OPHTA' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PSYCH' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='CARDT' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PRELV' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='ASPEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='APSIN' and @codeSystem='1.2.250.1.213.1.1.4.2.280')]/hl7:originalText
Item: (CI-SISAuthor)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:functionCode[(@code='ADMPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ATTPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='DISPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PRISURG' and @codeSystem='2.16.840.1.113883.5.88') or (@code='FASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='SASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='NASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANEST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANRS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='MDWF' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PCP' and @codeSystem='2.16.840.1.113883.5.88') or (@code='CORRE' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='REMPL' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='GYNEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='OPHTA' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PSYCH' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='CARDT' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PRELV' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='ASPEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='APSIN' and @codeSystem='1.2.250.1.213.1.1.4.2.280')]/hl7:originalText" id="d615545e180-false-d615699e0">
        <extends rule="ED"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ED' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAuthor): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ED", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.7
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:time
Item: (CI-SISAuthor)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:time" id="d615545e186-false-d615709e0">
        <extends rule="TS"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TS' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAuthor): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TS", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="not(*)">(CI-SISAuthor): <value-of select="local-name()"/> with datatype TS, SHOULD NOT have child elements.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="@value">(CI-SISAuthor): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="not(@value) or matches(string(@value), '^[0-9]{4,14}')">(CI-SISAuthor): Attribute @value SHALL be of data type 'ts'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.7
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]
Item: (CI-SISAuthor)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]" id="d615545e194-false-d615735e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:id)&gt;=1">(CI-SISAuthor): element hl7:id is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:id)&lt;=1">(CI-SISAuthor): element hl7:id appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.1-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem) or @nullFlavor])&lt;=1">(CI-SISAuthor): element hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.1-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem) or @nullFlavor] appears too often [max 1x].</assert>
        <let name="elmcount" value="count(hl7:addr|hl7:addr)"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:assignedPerson)&lt;=1">(CI-SISAuthor): element hl7:assignedPerson appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:assignedAuthoringDevice)&lt;=1">(CI-SISAuthor): element hl7:assignedAuthoringDevice appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:representedOrganization)&lt;=1">(CI-SISAuthor): element hl7:representedOrganization appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.7
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:id
Item: (CI-SISAuthor)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:id" id="d615545e205-false-d615809e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAuthor): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="@root">(CI-SISAuthor): attribute @root SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="not(@root) or matches(@root,'^[0-2](\.(0|[1-9]\d*))*$') or matches(@root,'^[A-Fa-f\d]{8}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{12}$') or matches(@root,'^[A-Za-z][A-Za-z\d\-]*$')">(CI-SISAuthor): Attribute @root SHALL be of data type 'uid'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="@extension">(CI-SISAuthor): attribute @extension SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="not(@extension) or string-length(@extension)&gt;0">(CI-SISAuthor): Attribute @extension SHALL be of data type 'st'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.7
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.1-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem) or @nullFlavor]
Item: (CI-SISAuthor)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.1-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem) or @nullFlavor]" id="d615545e223-false-d615836e0">
        <extends rule="CE"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CE' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAuthor): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CE", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <let name="theCode" value="@code"/>
        <let name="theCodeSystem" value="@codeSystem"/>
        <let name="theCodeSystemVersion" value="@codeSystemVersion"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="@nullFlavor or exists(doc('include/voc-1.2.250.1.213.1.1.5.1-DYNAMIC.xml')//valueSet[1][conceptList/concept[@code = $theCode][@codeSystem = $theCodeSystem][not(@codeSystemVersion) or @codeSystemVersion=$theCodeSystemVersion] or completeCodeSystem[@codeSystem=$theCodeSystem][not(@codeSystemVersion) or @codeSystemVersion=$theCodeSystemVersion]])">(CI-SISAuthor): The element value SHALL be one of '1.2.250.1.213.1.1.5.1 JDV_J01-XdsAuthorSpecialty-CISIS (DYNAMIC)'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]
Item: (CI-SISAddr)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr" id="d615837e75-false-d615859e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@hl7:use) or (string-length(@hl7:use)&gt;0 and not(matches(@hl7:use,'\s')))">(CI-SISAddr): Attribute @hl7:use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:country)&lt;=1">(CI-SISAddr): element hl7:country appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:state)&lt;=1">(CI-SISAddr): element hl7:state appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:city)&lt;=1">(CI-SISAddr): element hl7:city appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postalCode)&lt;=1">(CI-SISAddr): element hl7:postalCode appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumber)&lt;=1">(CI-SISAddr): element hl7:houseNumber appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumberNumeric)&lt;=1">(CI-SISAddr): element hl7:houseNumberNumeric appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetName)&lt;=1">(CI-SISAddr): element hl7:streetName appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetNameType)&lt;=1">(CI-SISAddr): element hl7:streetNameType appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:additionalLocator)&lt;=1">(CI-SISAddr): element hl7:additionalLocator appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:unitID)&lt;=1">(CI-SISAddr): element hl7:unitID appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postBox)&lt;=1">(CI-SISAddr): element hl7:postBox appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:precinct)&lt;=1">(CI-SISAddr): element hl7:precinct appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:country
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:country" id="d615837e93-false-d615957e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:state
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:state" id="d615837e97-false-d615967e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:city
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:city" id="d615837e101-false-d615977e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:postalCode
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:postalCode" id="d615837e109-false-d615987e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:houseNumber
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:houseNumber" id="d615837e117-false-d615997e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:houseNumberNumeric
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:houseNumberNumeric" id="d615837e121-false-d616007e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:streetName
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:streetName" id="d615837e125-false-d616017e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:streetNameType
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:streetNameType" id="d615837e129-false-d616027e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:additionalLocator
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:additionalLocator" id="d615837e174-false-d616037e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:unitID
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:unitID" id="d615837e189-false-d616047e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:postBox
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:postBox" id="d615837e199-false-d616057e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:precinct
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:precinct" id="d615837e203-false-d616067e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr" id="d615837e207-false-d616077e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISAddr): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:streetAddressLine" id="d615837e225-false-d616133e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:streetAddressLine" id="d615837e231-false-d616143e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:streetAddressLine" id="d615837e235-false-d616153e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:streetAddressLine" id="d615837e239-false-d616163e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:streetAddressLine" id="d615837e243-false-d616173e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:streetAddressLine" id="d615837e247-false-d616183e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.19
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:telecom
Item: (CI-SISTelecom)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:telecom" id="d616184e27-false-d616194e0">
        <extends rule="TEL"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TEL' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISTelecom): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TEL", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISTelecom): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@value">(CI-SISTelecom): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@value) or string(@value castable as xs:anyURI)">(CI-SISTelecom): Attribute @value SHALL be of data type 'url'<value-of select="@value"/>
        </assert>
        <let name="prefix" value="substring-before(@value, ':')"/>
        <let name="suffix" value="substring-after(@value, ':')"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(             (count(@*) = 1 and name(@*) = 'nullFlavor' and             (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or             ($suffix and (             $prefix = 'tel' or              $prefix = 'fax' or              $prefix = 'mailto' or              $prefix = 'http' or              $prefix = 'ftp' or              $prefix = 'mllp'))             )">(CI-SISTelecom): Erreur de conformité CI-SIS : <name/> n'est pas conforme à une adresse de télécommunication préfixe:chaîne 
            (avec préfixe = tel, fax, mailto, http, ftp ou mllp) 
            ou est vide et sans nullFlavor, ou contient un nullFlavor non admis.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@use = 'H'                      or @use = 'HP'                      or @use = 'HV'                      or @use = 'WP'                     or @use = 'DIR'                      or @use = 'PUB'                      or @use = 'EC'                      or @use = 'MC'                      or @use = 'PG'                      or not(@use)">(CI-SISTelecom): Erreur de conformité CI-SIS : L'attribut use de l'élément telecom n'est pas conforme. 
            Il est facultatif et les valeurs permises sont 'H','HP', 'HV','WP','DIR','PUB','EC','MC','PG'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.7
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:assignedPerson
Item: (CI-SISAuthor)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:assignedPerson" id="d615545e259-false-d616215e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:name)&gt;=1">(CI-SISAuthor): element hl7:name is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:name)&lt;=1">(CI-SISAuthor): element hl7:name appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.7
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:assignedPerson/hl7:name
Item: (CI-SISAuthor)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:assignedPerson/hl7:name" id="d615545e265-false-d616231e0">
        <extends rule="PN"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='PN' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAuthor): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:PN", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:family)&gt;=1">(CI-SISAuthor): element hl7:family is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:family)&lt;=1">(CI-SISAuthor): element hl7:family appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:given)&lt;=1">(CI-SISAuthor): element hl7:given appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:prefix[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.24-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem) or @nullFlavor])&lt;=1">(CI-SISAuthor): element hl7:prefix[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.24-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem) or @nullFlavor] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.7
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:assignedPerson/hl7:name/hl7:family
Item: (CI-SISAuthor)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.7
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:assignedPerson/hl7:name/hl7:given
Item: (CI-SISAuthor)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.7
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:assignedPerson/hl7:name/hl7:prefix[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.24-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem) or @nullFlavor]
Item: (CI-SISAuthor)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:assignedPerson/hl7:name/hl7:prefix[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.24-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem) or @nullFlavor]" id="d615545e295-false-d616285e0">
        <let name="theCode" value="@code"/>
        <let name="theCodeSystem" value="@codeSystem"/>
        <let name="theCodeSystemVersion" value="@codeSystemVersion"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="@nullFlavor or exists(doc('include/voc-1.2.250.1.213.1.1.5.24-2018-01-05T000000.xml')//valueSet[1][conceptList/concept[@code = $theCode][@codeSystem = $theCodeSystem][not(@codeSystemVersion) or @codeSystemVersion=$theCodeSystemVersion] or completeCodeSystem[@codeSystem=$theCodeSystem][not(@codeSystemVersion) or @codeSystemVersion=$theCodeSystemVersion]])">(CI-SISAuthor): The element value SHALL be one of '1.2.250.1.213.1.1.5.24 JDV_J12-CiviliteTitre (2018-01-05T00:00:00)'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.7
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:assignedAuthoringDevice
Item: (CI-SISAuthor)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:assignedAuthoringDevice" id="d615545e302-false-d616299e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:manufacturerModelName)&lt;=1">(CI-SISAuthor): element hl7:manufacturerModelName appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:softwareName)&lt;=1">(CI-SISAuthor): element hl7:softwareName appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.7
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:assignedAuthoringDevice/hl7:manufacturerModelName
Item: (CI-SISAuthor)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:assignedAuthoringDevice/hl7:manufacturerModelName" id="d615545e308-false-d616319e0">
        <extends rule="SC"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='SC' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAuthor): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:SC", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.7
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:assignedAuthoringDevice/hl7:softwareName
Item: (CI-SISAuthor)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:assignedAuthoringDevice/hl7:softwareName" id="d615545e314-false-d616329e0">
        <extends rule="SC"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='SC' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAuthor): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:SC", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.7
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:representedOrganization
Item: (CI-SISAuthor)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:representedOrganization" id="d615545e320-false-d616339e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:id[@root='1.2.250.1.71.4.2.2'])&lt;=1">(CI-SISAuthor): element hl7:id[@root='1.2.250.1.71.4.2.2'] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:name)&lt;=1">(CI-SISAuthor): element hl7:name appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.7
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:representedOrganization/hl7:id[@root='1.2.250.1.71.4.2.2']
Item: (CI-SISAuthor)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:representedOrganization/hl7:id[@root='1.2.250.1.71.4.2.2']" id="d615545e326-false-d616361e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAuthor): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="@extension">(CI-SISAuthor): attribute @extension SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="not(@extension) or string-length(@extension)&gt;0">(CI-SISAuthor): Attribute @extension SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="string(@root)=('1.2.250.1.71.4.2.2')">(CI-SISAuthor): The value for @root SHALL be '1.2.250.1.71.4.2.2'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.7
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:representedOrganization/hl7:name
Item: (CI-SISAuthor)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:representedOrganization/hl7:name" id="d615545e339-false-d616382e0">
        <extends rule="ON"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ON' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAuthor): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ON", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.12
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant
Item: (CI-SISInformant)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant" id="d616383e29-false-d616423e0">
        <let name="elmcount" value="count(hl7:assignedEntity|hl7:relatedEntity)"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.12" test="$elmcount&gt;=1">(CI-SISInformant): choice (hl7:assignedEntity or hl7:relatedEntity) does not contain enough elements [min 1x]</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.12" test="$elmcount&lt;=1">(CI-SISInformant): choice (hl7:assignedEntity or hl7:relatedEntity) contains too many elements [max 1x]</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.12" test="count(hl7:assignedEntity)&lt;=1">(CI-SISInformant): element hl7:assignedEntity appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.12" test="count(hl7:relatedEntity)&lt;=1">(CI-SISInformant): element hl7:relatedEntity appears too often [max 1x].</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.12
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity
Item: (CI-SISInformant)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.14
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity
Item: (CI-SISAssignedEntity)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="count(hl7:id)&gt;=1">(CI-SISAssignedEntity): element hl7:id is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="count(hl7:id)&lt;=1">(CI-SISAssignedEntity): element hl7:id appears too often [max 1x].</assert>
        <let name="elmcount" value="count(hl7:addr|hl7:addr)"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="count(hl7:assignedPerson)&lt;=1">(CI-SISAssignedEntity): element hl7:assignedPerson appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="count(hl7:representedOrganization)&lt;=1">(CI-SISAssignedEntity): element hl7:representedOrganization appears too often [max 1x].</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.14
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:id
Item: (CI-SISAssignedEntity)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:id">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="@root">(CI-SISAssignedEntity): attribute @root SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="not(@root) or matches(@root,'^[0-2](\.(0|[1-9]\d*))*$') or matches(@root,'^[A-Fa-f\d]{8}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{12}$') or matches(@root,'^[A-Za-z][A-Za-z\d\-]*$')">(CI-SISAssignedEntity): Attribute @root SHALL be of data type 'uid'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="@extension">(CI-SISAssignedEntity): attribute @extension SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="not(@extension) or string-length(@extension)&gt;0">(CI-SISAssignedEntity): Attribute @extension SHALL be of data type 'st'</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity
Item: (CI-SISAddr)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:addr">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@hl7:use) or (string-length(@hl7:use)&gt;0 and not(matches(@hl7:use,'\s')))">(CI-SISAddr): Attribute @hl7:use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:country)&lt;=1">(CI-SISAddr): element hl7:country appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:state)&lt;=1">(CI-SISAddr): element hl7:state appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:city)&lt;=1">(CI-SISAddr): element hl7:city appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postalCode)&lt;=1">(CI-SISAddr): element hl7:postalCode appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumber)&lt;=1">(CI-SISAddr): element hl7:houseNumber appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumberNumeric)&lt;=1">(CI-SISAddr): element hl7:houseNumberNumeric appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetName)&lt;=1">(CI-SISAddr): element hl7:streetName appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetNameType)&lt;=1">(CI-SISAddr): element hl7:streetNameType appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:additionalLocator)&lt;=1">(CI-SISAddr): element hl7:additionalLocator appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:unitID)&lt;=1">(CI-SISAddr): element hl7:unitID appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postBox)&lt;=1">(CI-SISAddr): element hl7:postBox appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:precinct)&lt;=1">(CI-SISAddr): element hl7:precinct appears too often [max 1x].</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:country
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:country">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:state
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:state">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:city
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:city">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:postalCode
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:postalCode">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:houseNumber
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:houseNumber">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:houseNumberNumeric
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:houseNumberNumeric">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:streetName
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:streetName">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:streetNameType
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:streetNameType">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:additionalLocator
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:additionalLocator">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:unitID
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:unitID">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:postBox
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:postBox">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:precinct
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:precinct">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:addr">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISAddr): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.19
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:telecom
Item: (CI-SISTelecom)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:telecom">
        <extends rule="TEL"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TEL' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISTelecom): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TEL", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISTelecom): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@value">(CI-SISTelecom): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@value) or string(@value castable as xs:anyURI)">(CI-SISTelecom): Attribute @value SHALL be of data type 'url'<value-of select="@value"/>
        </assert>
        <let name="prefix" value="substring-before(@value, ':')"/>
        <let name="suffix" value="substring-after(@value, ':')"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(             (count(@*) = 1 and name(@*) = 'nullFlavor' and             (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or             ($suffix and (             $prefix = 'tel' or              $prefix = 'fax' or              $prefix = 'mailto' or              $prefix = 'http' or              $prefix = 'ftp' or              $prefix = 'mllp'))             )">(CI-SISTelecom): Erreur de conformité CI-SIS : <name/> n'est pas conforme à une adresse de télécommunication préfixe:chaîne 
            (avec préfixe = tel, fax, mailto, http, ftp ou mllp) 
            ou est vide et sans nullFlavor, ou contient un nullFlavor non admis.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@use = 'H'                      or @use = 'HP'                      or @use = 'HV'                      or @use = 'WP'                     or @use = 'DIR'                      or @use = 'PUB'                      or @use = 'EC'                      or @use = 'MC'                      or @use = 'PG'                      or not(@use)">(CI-SISTelecom): Erreur de conformité CI-SIS : L'attribut use de l'élément telecom n'est pas conforme. 
            Il est facultatif et les valeurs permises sont 'H','HP', 'HV','WP','DIR','PUB','EC','MC','PG'.</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.14
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson
Item: (CI-SISAssignedEntity)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="count(hl7:name)&gt;=1">(CI-SISAssignedEntity): element hl7:name is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="count(hl7:name)&lt;=1">(CI-SISAssignedEntity): element hl7:name appears too often [max 1x].</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.14
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name
Item: (CI-SISAssignedEntity)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="count(hl7:prefix)&lt;=1">(CI-SISAssignedEntity): element hl7:prefix appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="count(hl7:given)&lt;=1">(CI-SISAssignedEntity): element hl7:given appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="count(hl7:family)&gt;=1">(CI-SISAssignedEntity): element hl7:family is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="count(hl7:family)&lt;=1">(CI-SISAssignedEntity): element hl7:family appears too often [max 1x].</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.14
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name/hl7:prefix
Item: (CI-SISAssignedEntity)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.14
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name/hl7:given
Item: (CI-SISAssignedEntity)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.14
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name/hl7:family
Item: (CI-SISAssignedEntity)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.14
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization
Item: (CI-SISAssignedEntity)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="count(hl7:id[@root='1.2.250.1.71.4.2.2'])&lt;=1">(CI-SISAssignedEntity): element hl7:id[@root='1.2.250.1.71.4.2.2'] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="count(hl7:name)&lt;=1">(CI-SISAssignedEntity): element hl7:name appears too often [max 1x].</assert>
        <let name="elmcount" value="count(hl7:addr|hl7:addr)"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="count(hl7:standardIndustryClassCode[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.4-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem) or @nullFlavor])&lt;=1">(CI-SISAssignedEntity): element hl7:standardIndustryClassCode[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.4-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem) or @nullFlavor] appears too often [max 1x].</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.14
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:id[@root='1.2.250.1.71.4.2.2']
Item: (CI-SISAssignedEntity)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:id[@root='1.2.250.1.71.4.2.2']">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="@extension">(CI-SISAssignedEntity): attribute @extension SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="not(@extension) or string-length(@extension)&gt;0">(CI-SISAssignedEntity): Attribute @extension SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="string(@root)=('1.2.250.1.71.4.2.2')">(CI-SISAssignedEntity): The value for @root SHALL be '1.2.250.1.71.4.2.2'.</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.14
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:name
Item: (CI-SISAssignedEntity)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization
Item: (CI-SISAddr)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@hl7:use) or (string-length(@hl7:use)&gt;0 and not(matches(@hl7:use,'\s')))">(CI-SISAddr): Attribute @hl7:use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:country)&lt;=1">(CI-SISAddr): element hl7:country appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:state)&lt;=1">(CI-SISAddr): element hl7:state appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:city)&lt;=1">(CI-SISAddr): element hl7:city appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postalCode)&lt;=1">(CI-SISAddr): element hl7:postalCode appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumber)&lt;=1">(CI-SISAddr): element hl7:houseNumber appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumberNumeric)&lt;=1">(CI-SISAddr): element hl7:houseNumberNumeric appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetName)&lt;=1">(CI-SISAddr): element hl7:streetName appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetNameType)&lt;=1">(CI-SISAddr): element hl7:streetNameType appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:additionalLocator)&lt;=1">(CI-SISAddr): element hl7:additionalLocator appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:unitID)&lt;=1">(CI-SISAddr): element hl7:unitID appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postBox)&lt;=1">(CI-SISAddr): element hl7:postBox appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:precinct)&lt;=1">(CI-SISAddr): element hl7:precinct appears too often [max 1x].</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:country
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:country">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:state
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:state">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:city
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:city">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:postalCode
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:postalCode">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:houseNumber
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:houseNumber">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:houseNumberNumeric
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:houseNumberNumeric">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetName
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetName">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetNameType
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetNameType">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:additionalLocator
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:additionalLocator">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:unitID
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:unitID">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:postBox
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:postBox">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:precinct
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:precinct">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISAddr): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.19
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:telecom
Item: (CI-SISTelecom)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:telecom">
        <extends rule="TEL"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TEL' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISTelecom): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TEL", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISTelecom): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@value">(CI-SISTelecom): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@value) or string(@value castable as xs:anyURI)">(CI-SISTelecom): Attribute @value SHALL be of data type 'url'<value-of select="@value"/>
        </assert>
        <let name="prefix" value="substring-before(@value, ':')"/>
        <let name="suffix" value="substring-after(@value, ':')"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(             (count(@*) = 1 and name(@*) = 'nullFlavor' and             (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or             ($suffix and (             $prefix = 'tel' or              $prefix = 'fax' or              $prefix = 'mailto' or              $prefix = 'http' or              $prefix = 'ftp' or              $prefix = 'mllp'))             )">(CI-SISTelecom): Erreur de conformité CI-SIS : <name/> n'est pas conforme à une adresse de télécommunication préfixe:chaîne 
            (avec préfixe = tel, fax, mailto, http, ftp ou mllp) 
            ou est vide et sans nullFlavor, ou contient un nullFlavor non admis.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@use = 'H'                      or @use = 'HP'                      or @use = 'HV'                      or @use = 'WP'                     or @use = 'DIR'                      or @use = 'PUB'                      or @use = 'EC'                      or @use = 'MC'                      or @use = 'PG'                      or not(@use)">(CI-SISTelecom): Erreur de conformité CI-SIS : L'attribut use de l'élément telecom n'est pas conforme. 
            Il est facultatif et les valeurs permises sont 'H','HP', 'HV','WP','DIR','PUB','EC','MC','PG'.</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.14
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:standardIndustryClassCode[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.4-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem) or @nullFlavor]
Item: (CI-SISAssignedEntity)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:standardIndustryClassCode[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.4-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem) or @nullFlavor]">
        <let name="theCode" value="@code"/>
        <let name="theCodeSystem" value="@codeSystem"/>
        <let name="theCodeSystemVersion" value="@codeSystemVersion"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="@nullFlavor or exists(doc('include/voc-1.2.250.1.213.1.1.5.4-2018-01-05T000000.xml')//valueSet[1][conceptList/concept[@code = $theCode][@codeSystem = $theCodeSystem][not(@codeSystemVersion) or @codeSystemVersion=$theCodeSystemVersion] or completeCodeSystem[@codeSystem=$theCodeSystem][not(@codeSystemVersion) or @codeSystemVersion=$theCodeSystemVersion]])">(CI-SISAssignedEntity): The element value SHALL be one of '1.2.250.1.213.1.1.5.4 J04-XdsPracticeSettingCode (2018-01-05T00:00:00)'.</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.12
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:relatedEntity
Item: (CI-SISInformant)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.13
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:relatedEntity
Item: (CI-SISRelatedEntity)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:relatedEntity">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="@classCode">(CI-SISRelatedEntity): attribute @classCode SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="not(@classCode) or (string-length(@classCode)&gt;0 and not(matches(@classCode,'\s')))">(CI-SISRelatedEntity): Attribute @classCode SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="@classCode='ECON' or @classCode='GUARD' or @classCode='QUAL' or @classCode='POLHOLD' or @classCode='CON'">(CI-SISRelatedEntity): @classCode doit être renseigné avec une de ces valeurs :
"ECON" pour personne à prévenir en cas d’urgence
"GUARD" pour rôle de tuteur légal
"QUAL" pour personne de confiance
"POLHOLD" pour assuré ouvrant droit
"CON" pour informateur</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="count(hl7:code)&lt;=1">(CI-SISRelatedEntity): element hl7:code appears too often [max 1x].</assert>
        <let name="elmcount" value="count(hl7:addr|hl7:addr)"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="$elmcount&gt;=1">(CI-SISRelatedEntity): choice (hl7:addr or hl7:addr) does not contain enough elements [min 1x]</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="count(hl7:telecom)&gt;=1">(CI-SISRelatedEntity): element hl7:telecom is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="count(hl7:relatedPerson)&gt;=1">(CI-SISRelatedEntity): element hl7:relatedPerson is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="count(hl7:relatedPerson)&lt;=1">(CI-SISRelatedEntity): element hl7:relatedPerson appears too often [max 1x].</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.13
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:relatedEntity/hl7:code
Item: (CI-SISRelatedEntity)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:relatedEntity/hl7:code">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="@displayName">(CI-SISRelatedEntity): attribute @displayName SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="not(@displayName) or string-length(@displayName)&gt;0">(CI-SISRelatedEntity): Attribute @displayName SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="@codeSystem">(CI-SISRelatedEntity): attribute @codeSystem SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="not(@codeSystem) or matches(@codeSystem,'^[0-2](\.(0|[1-9]\d*))*$')">(CI-SISRelatedEntity): Attribute @codeSystem SHALL be of data type 'oid'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="@code">(CI-SISRelatedEntity): attribute @code SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="not(@code) or (string-length(@code)&gt;0 and not(matches(@code,'\s')))">(CI-SISRelatedEntity): Attribute @code SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="count(hl7:originalText)&lt;=1">(CI-SISRelatedEntity): element hl7:originalText appears too often [max 1x].</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.13
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:relatedEntity/hl7:code/hl7:originalText
Item: (CI-SISRelatedEntity)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:relatedEntity
Item: (CI-SISAddr)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:relatedEntity/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:relatedEntity/hl7:addr">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@hl7:use) or (string-length(@hl7:use)&gt;0 and not(matches(@hl7:use,'\s')))">(CI-SISAddr): Attribute @hl7:use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:country)&lt;=1">(CI-SISAddr): element hl7:country appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:state)&lt;=1">(CI-SISAddr): element hl7:state appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:city)&lt;=1">(CI-SISAddr): element hl7:city appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postalCode)&lt;=1">(CI-SISAddr): element hl7:postalCode appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumber)&lt;=1">(CI-SISAddr): element hl7:houseNumber appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumberNumeric)&lt;=1">(CI-SISAddr): element hl7:houseNumberNumeric appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetName)&lt;=1">(CI-SISAddr): element hl7:streetName appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetNameType)&lt;=1">(CI-SISAddr): element hl7:streetNameType appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:additionalLocator)&lt;=1">(CI-SISAddr): element hl7:additionalLocator appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:unitID)&lt;=1">(CI-SISAddr): element hl7:unitID appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postBox)&lt;=1">(CI-SISAddr): element hl7:postBox appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:precinct)&lt;=1">(CI-SISAddr): element hl7:precinct appears too often [max 1x].</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:country
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:country">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:state
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:state">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:city
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:city">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:postalCode
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:postalCode">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:houseNumber
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:houseNumber">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:houseNumberNumeric
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:houseNumberNumeric">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:streetName
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:streetName">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:streetNameType
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:streetNameType">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:additionalLocator
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:additionalLocator">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:unitID
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:unitID">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:postBox
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:postBox">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:precinct
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:precinct">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:relatedEntity/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:relatedEntity/hl7:addr">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISAddr): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:streetAddressLine">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:streetAddressLine">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:streetAddressLine">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:streetAddressLine">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:streetAddressLine">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:streetAddressLine">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.19
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:relatedEntity/hl7:telecom
Item: (CI-SISTelecom)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:relatedEntity/hl7:telecom">
        <extends rule="TEL"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TEL' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISTelecom): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TEL", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISTelecom): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@value">(CI-SISTelecom): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@value) or string(@value castable as xs:anyURI)">(CI-SISTelecom): Attribute @value SHALL be of data type 'url'<value-of select="@value"/>
        </assert>
        <let name="prefix" value="substring-before(@value, ':')"/>
        <let name="suffix" value="substring-after(@value, ':')"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(             (count(@*) = 1 and name(@*) = 'nullFlavor' and             (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or             ($suffix and (             $prefix = 'tel' or              $prefix = 'fax' or              $prefix = 'mailto' or              $prefix = 'http' or              $prefix = 'ftp' or              $prefix = 'mllp'))             )">(CI-SISTelecom): Erreur de conformité CI-SIS : <name/> n'est pas conforme à une adresse de télécommunication préfixe:chaîne 
            (avec préfixe = tel, fax, mailto, http, ftp ou mllp) 
            ou est vide et sans nullFlavor, ou contient un nullFlavor non admis.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@use = 'H'                      or @use = 'HP'                      or @use = 'HV'                      or @use = 'WP'                     or @use = 'DIR'                      or @use = 'PUB'                      or @use = 'EC'                      or @use = 'MC'                      or @use = 'PG'                      or not(@use)">(CI-SISTelecom): Erreur de conformité CI-SIS : L'attribut use de l'élément telecom n'est pas conforme. 
            Il est facultatif et les valeurs permises sont 'H','HP', 'HV','WP','DIR','PUB','EC','MC','PG'.</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.13
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:relatedEntity/hl7:relatedPerson
Item: (CI-SISRelatedEntity)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:relatedEntity/hl7:relatedPerson">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="count(hl7:name)&gt;=1">(CI-SISRelatedEntity): element hl7:name is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="count(hl7:name)&lt;=1">(CI-SISRelatedEntity): element hl7:name appears too often [max 1x].</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.13
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:relatedEntity/hl7:relatedPerson/hl7:name
Item: (CI-SISRelatedEntity)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:relatedEntity/hl7:relatedPerson/hl7:name">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="count(hl7:family)&gt;=1">(CI-SISRelatedEntity): element hl7:family is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="count(hl7:family)&lt;=1">(CI-SISRelatedEntity): element hl7:family appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="count(hl7:given)&lt;=1">(CI-SISRelatedEntity): element hl7:given appears too often [max 1x].</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.13
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:relatedEntity/hl7:relatedPerson/hl7:name/hl7:family
Item: (CI-SISRelatedEntity)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.13
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:informant/hl7:relatedEntity/hl7:relatedPerson/hl7:name/hl7:given
Item: (CI-SISRelatedEntity)
-->
<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.9
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant
Item: (CI-SISParticipant)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant" id="d618078e22-false-d618091e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="@typeCode">(CI-SISParticipant): attribute @typeCode SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="not(@typeCode) or (string-length(@typeCode)&gt;0 and not(matches(@typeCode,'\s')))">(CI-SISParticipant): Attribute @typeCode SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="count(hl7:functionCode[(@code='ADMPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ATTPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='DISPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PRISURG' and @codeSystem='2.16.840.1.113883.5.88') or (@code='FASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='SASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='NASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANEST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANRS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='MDWF' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PCP' and @codeSystem='2.16.840.1.113883.5.88') or (@code='CORRE' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='REMPL' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='GYNEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='OPHTA' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PSYCH' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='CARDT' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PRELV' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='ASPEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='APSIN' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or @nullFlavor])&lt;=1">(CI-SISParticipant): element hl7:functionCode[(@code='ADMPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ATTPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='DISPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PRISURG' and @codeSystem='2.16.840.1.113883.5.88') or (@code='FASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='SASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='NASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANEST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANRS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='MDWF' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PCP' and @codeSystem='2.16.840.1.113883.5.88') or (@code='CORRE' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='REMPL' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='GYNEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='OPHTA' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PSYCH' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='CARDT' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PRELV' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='ASPEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='APSIN' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or @nullFlavor] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="count(hl7:time)&gt;=1">(CI-SISParticipant): element hl7:time is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="count(hl7:time)&lt;=1">(CI-SISParticipant): element hl7:time appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="count(hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']])&gt;=1">(CI-SISParticipant): element hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="count(hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']])&lt;=1">(CI-SISParticipant): element hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.9
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:functionCode[(@code='ADMPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ATTPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='DISPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PRISURG' and @codeSystem='2.16.840.1.113883.5.88') or (@code='FASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='SASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='NASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANEST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANRS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='MDWF' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PCP' and @codeSystem='2.16.840.1.113883.5.88') or (@code='CORRE' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='REMPL' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='GYNEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='OPHTA' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PSYCH' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='CARDT' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PRELV' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='ASPEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='APSIN' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or @nullFlavor]
Item: (CI-SISParticipant)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:functionCode[(@code='ADMPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ATTPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='DISPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PRISURG' and @codeSystem='2.16.840.1.113883.5.88') or (@code='FASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='SASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='NASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANEST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANRS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='MDWF' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PCP' and @codeSystem='2.16.840.1.113883.5.88') or (@code='CORRE' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='REMPL' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='GYNEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='OPHTA' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PSYCH' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='CARDT' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PRELV' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='ASPEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='APSIN' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or @nullFlavor]" id="d618078e43-false-d618139e0">
        <extends rule="CE"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CE' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISParticipant): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CE", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="@nullFlavor or (@code='ADMPHYS' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='médecin ayant admis le patient dans la structure de soins') or (@code='ATTPHYS' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='médecin responsable du patient dans la structure de soins') or (@code='DISPHYS' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='médecin ayant décidé la sortie du patient de la structure de soins') or (@code='PRISURG' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='médecin intervenant principal') or (@code='FASST' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='premier assistant lors de l’intervention') or (@code='SASST' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='second assistant lors de l’intervention') or (@code='NASST' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='infirmier(e)') or (@code='ANEST' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='médecin anesthésiste') or (@code='ANRS' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='infirmier(e) anesthésiste') or (@code='MDWF' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='sage-femme') or (@code='PCP' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='médecin traitant') or (@code='CORRE' and @codeSystem='1.2.250.1.213.1.1.4.2.280' and @displayName='médecin correspondant') or (@code='REMPL' and @codeSystem='1.2.250.1.213.1.1.4.2.280' and @displayName='médecin remplaçant du médecin traitant') or (@code='GYNEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280' and @displayName='gynécologue') or (@code='OPHTA' and @codeSystem='1.2.250.1.213.1.1.4.2.280' and @displayName='ophtalmologue') or (@code='PSYCH' and @codeSystem='1.2.250.1.213.1.1.4.2.280' and @displayName='psychiatre ou neuropsychiatre') or (@code='CARDT' and @codeSystem='1.2.250.1.213.1.1.4.2.280' and @displayName='cardiologue traitant') or (@code='PRELV' and @codeSystem='1.2.250.1.213.1.1.4.2.280' and @displayName='préleveur d’échantillons biologiques pour l’exécution d’une prescription d’examens biologiques') or (@code='ASPEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280' and @displayName='autre spécialiste') or (@code='APSIN' and @codeSystem='1.2.250.1.213.1.1.4.2.280' and @displayName='autre PS informateur')">(CI-SISParticipant): The element value SHALL be one of 'code 'ADMPHYS' codeSystem '2.16.840.1.113883.5.88' displayName='médecin ayant admis le patient dans la structure de soins' or code 'ATTPHYS' codeSystem '2.16.840.1.113883.5.88' displayName='médecin responsable du patient dans la structure de soins' or code 'DISPHYS' codeSystem '2.16.840.1.113883.5.88' displayName='médecin ayant décidé la sortie du patient de la structure de soins' or code 'PRISURG' codeSystem '2.16.840.1.113883.5.88' displayName='médecin intervenant principal' or code 'FASST' codeSystem '2.16.840.1.113883.5.88' displayName='premier assistant lors de l’intervention' or code 'SASST' codeSystem '2.16.840.1.113883.5.88' displayName='second assistant lors de l’intervention' or code 'NASST' codeSystem '2.16.840.1.113883.5.88' displayName='infirmier(e)' or code 'ANEST' codeSystem '2.16.840.1.113883.5.88' displayName='médecin anesthésiste' or code 'ANRS' codeSystem '2.16.840.1.113883.5.88' displayName='infirmier(e) anesthésiste' or code 'MDWF' codeSystem '2.16.840.1.113883.5.88' displayName='sage-femme' or code 'PCP' codeSystem '2.16.840.1.113883.5.88' displayName='médecin traitant' or code 'CORRE' codeSystem '1.2.250.1.213.1.1.4.2.280' displayName='médecin correspondant' or code 'REMPL' codeSystem '1.2.250.1.213.1.1.4.2.280' displayName='médecin remplaçant du médecin traitant' or code 'GYNEC' codeSystem '1.2.250.1.213.1.1.4.2.280' displayName='gynécologue' or code 'OPHTA' codeSystem '1.2.250.1.213.1.1.4.2.280' displayName='ophtalmologue' or code 'PSYCH' codeSystem '1.2.250.1.213.1.1.4.2.280' displayName='psychiatre ou neuropsychiatre' or code 'CARDT' codeSystem '1.2.250.1.213.1.1.4.2.280' displayName='cardiologue traitant' or code 'PRELV' codeSystem '1.2.250.1.213.1.1.4.2.280' displayName='préleveur d’échantillons biologiques pour l’exécution d’une prescription d’examens biologiques' or code 'ASPEC' codeSystem '1.2.250.1.213.1.1.4.2.280' displayName='autre spécialiste' or code 'APSIN' codeSystem '1.2.250.1.213.1.1.4.2.280' displayName='autre PS informateur''.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="count(hl7:originalText)&lt;=1">(CI-SISParticipant): element hl7:originalText appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.9
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:functionCode[(@code='ADMPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ATTPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='DISPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PRISURG' and @codeSystem='2.16.840.1.113883.5.88') or (@code='FASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='SASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='NASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANEST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANRS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='MDWF' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PCP' and @codeSystem='2.16.840.1.113883.5.88') or (@code='CORRE' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='REMPL' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='GYNEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='OPHTA' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PSYCH' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='CARDT' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PRELV' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='ASPEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='APSIN' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or @nullFlavor]/hl7:originalText
Item: (CI-SISParticipant)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:functionCode[(@code='ADMPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ATTPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='DISPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PRISURG' and @codeSystem='2.16.840.1.113883.5.88') or (@code='FASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='SASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='NASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANEST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANRS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='MDWF' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PCP' and @codeSystem='2.16.840.1.113883.5.88') or (@code='CORRE' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='REMPL' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='GYNEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='OPHTA' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PSYCH' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='CARDT' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PRELV' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='ASPEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='APSIN' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or @nullFlavor]/hl7:originalText" id="d618078e119-false-d618219e0">
        <extends rule="ED"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ED' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISParticipant): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ED", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.9
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:time
Item: (CI-SISParticipant)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:time" id="d618078e125-false-d618229e0">
        <extends rule="IVL_TS"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='IVL_TS' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISParticipant): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:IVL_TS", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="count(hl7:low)&lt;=1">(CI-SISParticipant): element hl7:low appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="count(hl7:high)&lt;=1">(CI-SISParticipant): element hl7:high appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.9
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:time/hl7:low
Item: (CI-SISParticipant)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:time/hl7:low" id="d618078e129-false-d618253e0">
        <extends rule="IVXB_TS"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='IVXB_TS' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISParticipant): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:IVXB_TS", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="@value">(CI-SISParticipant): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="not(@value) or matches(string(@value), '^[0-9]{4,14}')">(CI-SISParticipant): Attribute @value SHALL be of data type 'ts'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.9
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:time/hl7:high
Item: (CI-SISParticipant)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:time/hl7:high" id="d618078e137-false-d618270e0">
        <extends rule="IVXB_TS"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='IVXB_TS' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISParticipant): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:IVXB_TS", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="@value">(CI-SISParticipant): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="not(@value) or matches(string(@value), '^[0-9]{4,14}')">(CI-SISParticipant): Attribute @value SHALL be of data type 'ts'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.9
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]
Item: (CI-SISParticipant)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]" id="d618078e147-false-d618294e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="@classCode">(CI-SISParticipant): attribute @classCode SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="not(@classCode) or (string-length(@classCode)&gt;0 and not(matches(@classCode,'\s')))">(CI-SISParticipant): Attribute @classCode SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="count(hl7:id[@root='1.2.250.1.71.4.2.1'])&gt;=1">(CI-SISParticipant): element hl7:id[@root='1.2.250.1.71.4.2.1'] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="count(hl7:id[@root='1.2.250.1.71.4.2.1'])&lt;=1">(CI-SISParticipant): element hl7:id[@root='1.2.250.1.71.4.2.1'] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="count(hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.1-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem) or @nullFlavor])&lt;=1">(CI-SISParticipant): element hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.1-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem) or @nullFlavor] appears too often [max 1x].</assert>
        <let name="elmcount" value="count(hl7:addr|hl7:addr)"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="count(hl7:associatedPerson)&gt;=1">(CI-SISParticipant): element hl7:associatedPerson is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="count(hl7:associatedPerson)&lt;=1">(CI-SISParticipant): element hl7:associatedPerson appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.9
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:id[@root='1.2.250.1.71.4.2.1']
Item: (CI-SISParticipant)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:id[@root='1.2.250.1.71.4.2.1']" id="d618078e157-false-d618367e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISParticipant): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="@extension">(CI-SISParticipant): attribute @extension SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="not(@extension) or string-length(@extension)&gt;0">(CI-SISParticipant): Attribute @extension SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="string(@root)=('1.2.250.1.71.4.2.1')">(CI-SISParticipant): The value for @root SHALL be '1.2.250.1.71.4.2.1'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.9
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.1-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem) or @nullFlavor]
Item: (CI-SISParticipant)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.1-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem) or @nullFlavor]" id="d618078e192-false-d618391e0">
        <extends rule="CE"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CE' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISParticipant): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CE", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <let name="theCode" value="@code"/>
        <let name="theCodeSystem" value="@codeSystem"/>
        <let name="theCodeSystemVersion" value="@codeSystemVersion"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="@nullFlavor or exists(doc('include/voc-1.2.250.1.213.1.1.5.1-2018-01-05T000000.xml')//valueSet[1][conceptList/concept[@code = $theCode][@codeSystem = $theCodeSystem][not(@codeSystemVersion) or @codeSystemVersion=$theCodeSystemVersion] or completeCodeSystem[@codeSystem=$theCodeSystem][not(@codeSystemVersion) or @codeSystemVersion=$theCodeSystemVersion]])">(CI-SISParticipant): The element value SHALL be one of '1.2.250.1.213.1.1.5.1 JDV_J01-XdsAuthorSpecialty-CISIS (2018-01-05T00:00:00)'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]
Item: (CI-SISAddr)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr" id="d618392e75-false-d618411e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@hl7:use) or (string-length(@hl7:use)&gt;0 and not(matches(@hl7:use,'\s')))">(CI-SISAddr): Attribute @hl7:use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:country)&lt;=1">(CI-SISAddr): element hl7:country appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:state)&lt;=1">(CI-SISAddr): element hl7:state appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:city)&lt;=1">(CI-SISAddr): element hl7:city appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postalCode)&lt;=1">(CI-SISAddr): element hl7:postalCode appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumber)&lt;=1">(CI-SISAddr): element hl7:houseNumber appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumberNumeric)&lt;=1">(CI-SISAddr): element hl7:houseNumberNumeric appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetName)&lt;=1">(CI-SISAddr): element hl7:streetName appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetNameType)&lt;=1">(CI-SISAddr): element hl7:streetNameType appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:additionalLocator)&lt;=1">(CI-SISAddr): element hl7:additionalLocator appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:unitID)&lt;=1">(CI-SISAddr): element hl7:unitID appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postBox)&lt;=1">(CI-SISAddr): element hl7:postBox appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:precinct)&lt;=1">(CI-SISAddr): element hl7:precinct appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:country
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:country" id="d618392e93-false-d618509e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:state
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:state" id="d618392e97-false-d618519e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:city
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:city" id="d618392e101-false-d618529e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:postalCode
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:postalCode" id="d618392e109-false-d618539e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:houseNumber
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:houseNumber" id="d618392e117-false-d618549e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:houseNumberNumeric
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:houseNumberNumeric" id="d618392e121-false-d618559e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetName
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetName" id="d618392e125-false-d618569e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetNameType
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetNameType" id="d618392e129-false-d618579e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:additionalLocator
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:additionalLocator" id="d618392e174-false-d618589e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:unitID
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:unitID" id="d618392e189-false-d618599e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:postBox
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:postBox" id="d618392e199-false-d618609e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:precinct
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:precinct" id="d618392e203-false-d618619e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr" id="d618392e207-false-d618629e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISAddr): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine" id="d618392e225-false-d618685e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine" id="d618392e231-false-d618695e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine" id="d618392e235-false-d618705e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine" id="d618392e239-false-d618715e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine" id="d618392e243-false-d618725e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine" id="d618392e247-false-d618735e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.19
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:telecom
Item: (CI-SISTelecom)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:telecom" id="d618736e27-false-d618746e0">
        <extends rule="TEL"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TEL' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISTelecom): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TEL", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISTelecom): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@value">(CI-SISTelecom): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@value) or string(@value castable as xs:anyURI)">(CI-SISTelecom): Attribute @value SHALL be of data type 'url'<value-of select="@value"/>
        </assert>
        <let name="prefix" value="substring-before(@value, ':')"/>
        <let name="suffix" value="substring-after(@value, ':')"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(             (count(@*) = 1 and name(@*) = 'nullFlavor' and             (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or             ($suffix and (             $prefix = 'tel' or              $prefix = 'fax' or              $prefix = 'mailto' or              $prefix = 'http' or              $prefix = 'ftp' or              $prefix = 'mllp'))             )">(CI-SISTelecom): Erreur de conformité CI-SIS : <name/> n'est pas conforme à une adresse de télécommunication préfixe:chaîne 
            (avec préfixe = tel, fax, mailto, http, ftp ou mllp) 
            ou est vide et sans nullFlavor, ou contient un nullFlavor non admis.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@use = 'H'                      or @use = 'HP'                      or @use = 'HV'                      or @use = 'WP'                     or @use = 'DIR'                      or @use = 'PUB'                      or @use = 'EC'                      or @use = 'MC'                      or @use = 'PG'                      or not(@use)">(CI-SISTelecom): Erreur de conformité CI-SIS : L'attribut use de l'élément telecom n'est pas conforme. 
            Il est facultatif et les valeurs permises sont 'H','HP', 'HV','WP','DIR','PUB','EC','MC','PG'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.9
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:associatedPerson
Item: (CI-SISParticipant)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:associatedPerson" id="d618078e218-false-d618767e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="count(hl7:name)&gt;=1">(CI-SISParticipant): element hl7:name is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="count(hl7:name)&lt;=1">(CI-SISParticipant): element hl7:name appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.9
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:associatedPerson/hl7:name
Item: (CI-SISParticipant)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:associatedPerson/hl7:name" id="d618078e323-false-d618783e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="count(hl7:family)&gt;=1">(CI-SISParticipant): element hl7:family is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="count(hl7:family)&lt;=1">(CI-SISParticipant): element hl7:family appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="count(hl7:given)&lt;=1">(CI-SISParticipant): element hl7:given appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="count(hl7:prefix)&lt;=1">(CI-SISParticipant): element hl7:prefix appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.9
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:associatedPerson/hl7:name/hl7:family
Item: (CI-SISParticipant)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.9
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:associatedPerson/hl7:name/hl7:given
Item: (CI-SISParticipant)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.9
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:associatedPerson/hl7:name/hl7:prefix
Item: (CI-SISParticipant)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget
Item: (CI-SISRecordTarget)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget" id="d618829e71-false-d618848e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:patientRole)&gt;=1">(CI-SISRecordTarget): element hl7:patientRole is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:patientRole)&lt;=1">(CI-SISRecordTarget): element hl7:patientRole appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole
Item: (CI-SISRecordTarget)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole" id="d618829e72-false-d618890e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:id[not(@nullFlavor)])&gt;=1">(CI-SISRecordTarget): element hl7:id[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <let name="elmcount" value="count(hl7:addr|hl7:addr)"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="$elmcount&gt;=1">(CI-SISRecordTarget): choice (hl7:addr or hl7:addr) does not contain enough elements [min 1x]</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]])&gt;=1">(CI-SISRecordTarget): element hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]])&lt;=1">(CI-SISRecordTarget): element hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:id[not(@nullFlavor)]
Item: (CI-SISRecordTarget)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:id[not(@nullFlavor)]" id="d618829e78-false-d618954e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISRecordTarget): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="@root">(CI-SISRecordTarget): attribute @root SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="not(@root) or matches(@root,'^[0-2](\.(0|[1-9]\d*))*$') or matches(@root,'^[A-Fa-f\d]{8}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{12}$') or matches(@root,'^[A-Za-z][A-Za-z\d\-]*$')">(CI-SISRecordTarget): Attribute @root SHALL be of data type 'uid'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="@extension">(CI-SISRecordTarget): attribute @extension SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="not(@extension) or string-length(@extension)&gt;0">(CI-SISRecordTarget): Attribute @extension SHALL be of data type 'st'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole
Item: (CI-SISAddr)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:addr" id="d618955e78-false-d618980e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@hl7:use) or (string-length(@hl7:use)&gt;0 and not(matches(@hl7:use,'\s')))">(CI-SISAddr): Attribute @hl7:use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:country)&lt;=1">(CI-SISAddr): element hl7:country appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:state)&lt;=1">(CI-SISAddr): element hl7:state appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:city)&lt;=1">(CI-SISAddr): element hl7:city appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postalCode)&lt;=1">(CI-SISAddr): element hl7:postalCode appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumber)&lt;=1">(CI-SISAddr): element hl7:houseNumber appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumberNumeric)&lt;=1">(CI-SISAddr): element hl7:houseNumberNumeric appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetName)&lt;=1">(CI-SISAddr): element hl7:streetName appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetNameType)&lt;=1">(CI-SISAddr): element hl7:streetNameType appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:additionalLocator)&lt;=1">(CI-SISAddr): element hl7:additionalLocator appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:unitID)&lt;=1">(CI-SISAddr): element hl7:unitID appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postBox)&lt;=1">(CI-SISAddr): element hl7:postBox appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:precinct)&lt;=1">(CI-SISAddr): element hl7:precinct appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:country
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:country" id="d618955e96-false-d619078e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:state
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:state" id="d618955e100-false-d619088e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:city
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:city" id="d618955e104-false-d619098e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:postalCode
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:postalCode" id="d618955e112-false-d619108e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:houseNumber
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:houseNumber" id="d618955e120-false-d619118e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:houseNumberNumeric
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:houseNumberNumeric" id="d618955e124-false-d619128e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:streetName
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:streetName" id="d618955e128-false-d619138e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:streetNameType
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:streetNameType" id="d618955e132-false-d619148e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:additionalLocator
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:additionalLocator" id="d618955e177-false-d619158e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:unitID
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:unitID" id="d618955e192-false-d619168e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:postBox
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:postBox" id="d618955e202-false-d619178e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:precinct
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:precinct" id="d618955e206-false-d619188e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:addr" id="d618955e210-false-d619198e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISAddr): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:streetAddressLine" id="d618955e228-false-d619254e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:streetAddressLine" id="d618955e234-false-d619264e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:streetAddressLine" id="d618955e238-false-d619274e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:streetAddressLine" id="d618955e242-false-d619284e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:streetAddressLine" id="d618955e246-false-d619294e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:streetAddressLine" id="d618955e250-false-d619304e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.19
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:telecom
Item: (CI-SISTelecom)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:telecom" id="d619305e27-false-d619315e0">
        <extends rule="TEL"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TEL' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISTelecom): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TEL", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISTelecom): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@value">(CI-SISTelecom): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@value) or string(@value castable as xs:anyURI)">(CI-SISTelecom): Attribute @value SHALL be of data type 'url'<value-of select="@value"/>
        </assert>
        <let name="prefix" value="substring-before(@value, ':')"/>
        <let name="suffix" value="substring-after(@value, ':')"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(             (count(@*) = 1 and name(@*) = 'nullFlavor' and             (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or             ($suffix and (             $prefix = 'tel' or              $prefix = 'fax' or              $prefix = 'mailto' or              $prefix = 'http' or              $prefix = 'ftp' or              $prefix = 'mllp'))             )">(CI-SISTelecom): Erreur de conformité CI-SIS : <name/> n'est pas conforme à une adresse de télécommunication préfixe:chaîne 
            (avec préfixe = tel, fax, mailto, http, ftp ou mllp) 
            ou est vide et sans nullFlavor, ou contient un nullFlavor non admis.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@use = 'H'                      or @use = 'HP'                      or @use = 'HV'                      or @use = 'WP'                     or @use = 'DIR'                      or @use = 'PUB'                      or @use = 'EC'                      or @use = 'MC'                      or @use = 'PG'                      or not(@use)">(CI-SISTelecom): Erreur de conformité CI-SIS : L'attribut use de l'élément telecom n'est pas conforme. 
            Il est facultatif et les valeurs permises sont 'H','HP', 'HV','WP','DIR','PUB','EC','MC','PG'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]
Item: (CI-SISRecordTarget)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]" id="d618829e91-false-d619345e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:name[not(@nullFlavor)])&gt;=1">(CI-SISRecordTarget): element hl7:name[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:name[not(@nullFlavor)])&lt;=1">(CI-SISRecordTarget): element hl7:name[not(@nullFlavor)] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor])&gt;=1">(CI-SISRecordTarget): element hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor])&lt;=1">(CI-SISRecordTarget): element hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:birthTime)&gt;=1">(CI-SISRecordTarget): element hl7:birthTime is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:birthTime)&lt;=1">(CI-SISRecordTarget): element hl7:birthTime appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:birthplace)&lt;=1">(CI-SISRecordTarget): element hl7:birthplace appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:name[not(@nullFlavor)]
Item: (CI-SISRecordTarget)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:name[not(@nullFlavor)]" id="d618829e100-false-d619401e0">
        <extends rule="PN"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='PN' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISRecordTarget): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:PN", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:given)&gt;=1">(CI-SISRecordTarget): element hl7:given is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:family)&gt;=1">(CI-SISRecordTarget): element hl7:family is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:family)&lt;=3">(CI-SISRecordTarget): element hl7:family appears too often [max 3x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:name[not(@nullFlavor)]/hl7:given
Item: (CI-SISRecordTarget)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:name[not(@nullFlavor)]/hl7:family
Item: (CI-SISRecordTarget)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:name[not(@nullFlavor)]/hl7:family" id="d618829e112-false-d619435e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="@qualifier">(CI-SISRecordTarget): attribute @qualifier SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="not(@qualifier) or (string-length(@qualifier)&gt;0 and not(matches(@qualifier,'\s')))">(CI-SISRecordTarget): Attribute @qualifier SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="//hl7:recordTarget/hl7:patientRole/hl7:patient/hl7:name/hl7:family/@qualifier='BR' or //hl7:recordTarget/hl7:patientRole/hl7:patient/hl7:name/hl7:family/@qualifier='SP' or //hl7:recordTarget/hl7:patientRole/hl7:patient/hl7:name/hl7:family/@qualifier='CL'">(CI-SISRecordTarget): Valeur du nom typé par l’attribut qualifier : 
                "BR" pour le nom de famille
                "SP" pour le nom de d’usage
                "CL" pour le pseudonyme</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]
Item: (CI-SISRecordTarget)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]" id="d618829e121-false-d619449e0">
        <extends rule="CE"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CE' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISRecordTarget): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CE", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="@nullFlavor or (@code='M' and @codeSystem='2.16.840.1.113883.5.1' and @displayName='Masculin') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1' and @displayName='Féminin') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1' and @displayName='Inconnu')">(CI-SISRecordTarget): The element value SHALL be one of 'code 'M' codeSystem '2.16.840.1.113883.5.1' displayName='Masculin' or code 'F' codeSystem '2.16.840.1.113883.5.1' displayName='Féminin' or code 'U' codeSystem '2.16.840.1.113883.5.1' displayName='Inconnu''.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthTime
Item: (CI-SISRecordTarget)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthTime" id="d618829e125-false-d619471e0">
        <extends rule="TS"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TS' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISRecordTarget): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TS", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="not(*)">(CI-SISRecordTarget): <value-of select="local-name()"/> with datatype TS, SHOULD NOT have child elements.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="@value">(CI-SISRecordTarget): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="not(@value) or matches(string(@value), '^[0-9]{4,14}')">(CI-SISRecordTarget): Attribute @value SHALL be of data type 'ts'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian
Item: (CI-SISRecordTarget)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian" id="d618829e141-false-d619497e0">
        <let name="elmcount" value="count(hl7:addr|hl7:addr)"/>
        <let name="elmcount" value="count(hl7:guardianPerson|hl7:guardianOrganization)"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="$elmcount&gt;=1">(CI-SISRecordTarget): choice (hl7:guardianPerson or hl7:guardianOrganization) does not contain enough elements [min 1x]</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="$elmcount&lt;=1">(CI-SISRecordTarget): choice (hl7:guardianPerson or hl7:guardianOrganization) contains too many elements [max 1x]</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:guardianPerson)&lt;=1">(CI-SISRecordTarget): element hl7:guardianPerson appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:guardianOrganization)&lt;=1">(CI-SISRecordTarget): element hl7:guardianOrganization appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian
Item: (CI-SISAddr)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr" id="d619494e126-false-d619562e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@hl7:use) or (string-length(@hl7:use)&gt;0 and not(matches(@hl7:use,'\s')))">(CI-SISAddr): Attribute @hl7:use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:country)&lt;=1">(CI-SISAddr): element hl7:country appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:state)&lt;=1">(CI-SISAddr): element hl7:state appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:city)&lt;=1">(CI-SISAddr): element hl7:city appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postalCode)&lt;=1">(CI-SISAddr): element hl7:postalCode appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumber)&lt;=1">(CI-SISAddr): element hl7:houseNumber appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumberNumeric)&lt;=1">(CI-SISAddr): element hl7:houseNumberNumeric appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetName)&lt;=1">(CI-SISAddr): element hl7:streetName appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetNameType)&lt;=1">(CI-SISAddr): element hl7:streetNameType appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:additionalLocator)&lt;=1">(CI-SISAddr): element hl7:additionalLocator appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:unitID)&lt;=1">(CI-SISAddr): element hl7:unitID appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postBox)&lt;=1">(CI-SISAddr): element hl7:postBox appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:precinct)&lt;=1">(CI-SISAddr): element hl7:precinct appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:country
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:country" id="d619494e144-false-d619660e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:state
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:state" id="d619494e148-false-d619670e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:city
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:city" id="d619494e152-false-d619680e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:postalCode
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:postalCode" id="d619494e160-false-d619690e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:houseNumber
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:houseNumber" id="d619494e168-false-d619700e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:houseNumberNumeric
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:houseNumberNumeric" id="d619494e172-false-d619710e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:streetName
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:streetName" id="d619494e176-false-d619720e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:streetNameType
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:streetNameType" id="d619494e180-false-d619730e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:additionalLocator
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:additionalLocator" id="d619494e225-false-d619740e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:unitID
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:unitID" id="d619494e240-false-d619750e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:postBox
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:postBox" id="d619494e250-false-d619760e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:precinct
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:precinct" id="d619494e254-false-d619770e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr" id="d619494e258-false-d619780e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISAddr): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:streetAddressLine" id="d619494e276-false-d619836e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:streetAddressLine" id="d619494e282-false-d619846e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:streetAddressLine" id="d619494e286-false-d619856e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:streetAddressLine" id="d619494e290-false-d619866e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:streetAddressLine" id="d619494e294-false-d619876e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:streetAddressLine" id="d619494e298-false-d619886e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.19
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:telecom
Item: (CI-SISTelecom)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:telecom" id="d619887e27-false-d619897e0">
        <extends rule="TEL"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TEL' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISTelecom): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TEL", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISTelecom): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@value">(CI-SISTelecom): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@value) or string(@value castable as xs:anyURI)">(CI-SISTelecom): Attribute @value SHALL be of data type 'url'<value-of select="@value"/>
        </assert>
        <let name="prefix" value="substring-before(@value, ':')"/>
        <let name="suffix" value="substring-after(@value, ':')"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(             (count(@*) = 1 and name(@*) = 'nullFlavor' and             (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or             ($suffix and (             $prefix = 'tel' or              $prefix = 'fax' or              $prefix = 'mailto' or              $prefix = 'http' or              $prefix = 'ftp' or              $prefix = 'mllp'))             )">(CI-SISTelecom): Erreur de conformité CI-SIS : <name/> n'est pas conforme à une adresse de télécommunication préfixe:chaîne 
            (avec préfixe = tel, fax, mailto, http, ftp ou mllp) 
            ou est vide et sans nullFlavor, ou contient un nullFlavor non admis.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@use = 'H'                      or @use = 'HP'                      or @use = 'HV'                      or @use = 'WP'                     or @use = 'DIR'                      or @use = 'PUB'                      or @use = 'EC'                      or @use = 'MC'                      or @use = 'PG'                      or not(@use)">(CI-SISTelecom): Erreur de conformité CI-SIS : L'attribut use de l'élément telecom n'est pas conforme. 
            Il est facultatif et les valeurs permises sont 'H','HP', 'HV','WP','DIR','PUB','EC','MC','PG'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:guardianPerson
Item: (CI-SISRecordTarget)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:guardianPerson" id="d618829e151-false-d619918e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:name)&gt;=1">(CI-SISRecordTarget): element hl7:name is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:name)&lt;=1">(CI-SISRecordTarget): element hl7:name appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:guardianPerson/hl7:name
Item: (CI-SISRecordTarget)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:guardianPerson/hl7:name" id="d618829e155-false-d619934e0">
        <extends rule="PN"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='PN' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISRecordTarget): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:PN", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:family)&gt;=1">(CI-SISRecordTarget): element hl7:family is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:family)&lt;=3">(CI-SISRecordTarget): element hl7:family appears too often [max 3x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:guardianPerson/hl7:name/hl7:family
Item: (CI-SISRecordTarget)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:guardianPerson/hl7:name/hl7:family" id="d618829e159-false-d619958e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="@qualifier">(CI-SISRecordTarget): attribute @qualifier SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="not(@qualifier) or (string-length(@qualifier)&gt;0 and not(matches(@qualifier,'\s')))">(CI-SISRecordTarget): Attribute @qualifier SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="//hl7:recordTarget/hl7:patientRole/hl7:patient/hl7:name/hl7:family/@qualifier='BR' or //hl7:recordTarget/hl7:patientRole/hl7:patient/hl7:name/hl7:family/@qualifier='SP' or //hl7:recordTarget/hl7:patientRole/hl7:patient/hl7:name/hl7:family/@qualifier='CL'">(CI-SISRecordTarget): Valeur du nom typé par l’attribut qualifier : 
                      "BR" pour le nom de famille
                      "SP" pour le nom de d’usage
                      "CL" pour le pseudonyme</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:guardianPerson/hl7:name/hl7:given
Item: (CI-SISRecordTarget)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:guardianOrganization
Item: (CI-SISRecordTarget)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:guardianOrganization" id="d618829e177-false-d619978e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:id[@root='1.2.250.1.71.4.2.2'])&lt;=1">(CI-SISRecordTarget): element hl7:id[@root='1.2.250.1.71.4.2.2'] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:name)&lt;=1">(CI-SISRecordTarget): element hl7:name appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:guardianOrganization/hl7:id[@root='1.2.250.1.71.4.2.2']
Item: (CI-SISRecordTarget)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:guardianOrganization/hl7:id[@root='1.2.250.1.71.4.2.2']" id="d618829e181-false-d620000e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISRecordTarget): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="string(@root)=('1.2.250.1.71.4.2.2')">(CI-SISRecordTarget): The value for @root SHALL be '1.2.250.1.71.4.2.2'.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="@extension">(CI-SISRecordTarget): attribute @extension SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="not(@extension) or string-length(@extension)&gt;0">(CI-SISRecordTarget): Attribute @extension SHALL be of data type 'st'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:guardianOrganization/hl7:name
Item: (CI-SISRecordTarget)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:guardianOrganization/hl7:name" id="d618829e194-false-d620021e0">
        <extends rule="ON"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ON' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISRecordTarget): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ON", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace
Item: (CI-SISRecordTarget)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace" id="d618829e202-false-d620034e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:place)&gt;=1">(CI-SISRecordTarget): element hl7:place is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:place)&lt;=1">(CI-SISRecordTarget): element hl7:place appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place
Item: (CI-SISRecordTarget)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place" id="d618829e203-false-d620056e0">
        <let name="elmcount" value="count(hl7:addr|hl7:addr)"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="$elmcount&lt;=1">(CI-SISRecordTarget): choice (hl7:addr or hl7:addr) contains too many elements [max 1x]</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:name)&lt;=1">(CI-SISRecordTarget): element hl7:name appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place
Item: (CI-SISAddr)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr" id="d620076e68-false-d620092e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@hl7:use) or (string-length(@hl7:use)&gt;0 and not(matches(@hl7:use,'\s')))">(CI-SISAddr): Attribute @hl7:use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:country)&lt;=1">(CI-SISAddr): element hl7:country appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:state)&lt;=1">(CI-SISAddr): element hl7:state appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:city)&lt;=1">(CI-SISAddr): element hl7:city appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postalCode)&lt;=1">(CI-SISAddr): element hl7:postalCode appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumber)&lt;=1">(CI-SISAddr): element hl7:houseNumber appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumberNumeric)&lt;=1">(CI-SISAddr): element hl7:houseNumberNumeric appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetName)&lt;=1">(CI-SISAddr): element hl7:streetName appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetNameType)&lt;=1">(CI-SISAddr): element hl7:streetNameType appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:additionalLocator)&lt;=1">(CI-SISAddr): element hl7:additionalLocator appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:unitID)&lt;=1">(CI-SISAddr): element hl7:unitID appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postBox)&lt;=1">(CI-SISAddr): element hl7:postBox appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:precinct)&lt;=1">(CI-SISAddr): element hl7:precinct appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:country
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:country" id="d620076e86-false-d620190e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:state
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:state" id="d620076e90-false-d620200e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:city
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:city" id="d620076e94-false-d620210e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:postalCode
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:postalCode" id="d620076e102-false-d620220e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:houseNumber
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:houseNumber" id="d620076e110-false-d620230e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:houseNumberNumeric
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:houseNumberNumeric" id="d620076e114-false-d620240e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:streetName
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:streetName" id="d620076e118-false-d620250e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:streetNameType
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:streetNameType" id="d620076e122-false-d620260e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:additionalLocator
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:additionalLocator" id="d620076e167-false-d620270e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:unitID
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:unitID" id="d620076e182-false-d620280e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:postBox
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:postBox" id="d620076e192-false-d620290e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:precinct
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:precinct" id="d620076e196-false-d620300e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr" id="d620076e200-false-d620310e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISAddr): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:streetAddressLine" id="d620076e218-false-d620366e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:streetAddressLine" id="d620076e224-false-d620376e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:streetAddressLine" id="d620076e228-false-d620386e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:streetAddressLine" id="d620076e232-false-d620396e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:streetAddressLine" id="d620076e236-false-d620406e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:streetAddressLine" id="d620076e240-false-d620416e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:name
Item: (CI-SISRecordTarget)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.3']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:name" id="d618829e205-false-d620426e0">
        <extends rule="EN"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='EN' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISRecordTarget): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:EN", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
</pattern>