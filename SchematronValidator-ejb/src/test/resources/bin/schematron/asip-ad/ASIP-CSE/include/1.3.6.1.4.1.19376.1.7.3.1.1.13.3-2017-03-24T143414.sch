<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.3.6.1.4.1.19376.1.7.3.1.1.13.3
Name: IHE Child Functional Status Assessment
Description: This section provides a description of the child’s status of normal functioning at the time the document was created. This section includes the psychomotor and the eating and sleeping assessments. This section shall include the Psychomotor Test Observation entry.
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron" id="template-1.3.6.1.4.1.19376.1.7.3.1.1.13.3-2017-03-24T143414">
    <title>IHE Child Functional Status Assessment</title>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.7.3.1.1.13.3
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.3']]]
Item: (IHEChildFunctionalStatusAssessment)
-->

<!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.7.3.1.1.13.3
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.3']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.3']]
Item: (IHEChildFunctionalStatusAssessment)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.3']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.3']]" id="d506897e4947-false-d674271e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.7.3.1.1.13.3" test="count(hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.3'])&gt;=1">(IHEChildFunctionalStatusAssessment): element hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.3'] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.7.3.1.1.13.3" test="count(hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.3'])&lt;=1">(IHEChildFunctionalStatusAssessment): element hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.3'] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.7.3.1.1.13.3" test="count(hl7:id[not(@nullFlavor)])&gt;=1">(IHEChildFunctionalStatusAssessment): element hl7:id[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.7.3.1.1.13.3" test="count(hl7:id[not(@nullFlavor)])&lt;=1">(IHEChildFunctionalStatusAssessment): element hl7:id[not(@nullFlavor)] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.7.3.1.1.13.3" test="count(hl7:code[(@code='47420-5' and @codeSystem='2.16.840.1.113883.6.1')])&gt;=1">(IHEChildFunctionalStatusAssessment): element hl7:code[(@code='47420-5' and @codeSystem='2.16.840.1.113883.6.1')] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.7.3.1.1.13.3" test="count(hl7:code[(@code='47420-5' and @codeSystem='2.16.840.1.113883.6.1')])&lt;=1">(IHEChildFunctionalStatusAssessment): element hl7:code[(@code='47420-5' and @codeSystem='2.16.840.1.113883.6.1')] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.7.3.1.1.13.3" test="count(hl7:text)&gt;=1">(IHEChildFunctionalStatusAssessment): element hl7:text is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.7.3.1.1.13.3" test="count(hl7:text)&lt;=1">(IHEChildFunctionalStatusAssessment): element hl7:text appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.7.3.1.1.13.3
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.3']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.3']]/hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.3']
Item: (IHEChildFunctionalStatusAssessment)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.3']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.3']]/hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.3']" id="d506897e4948-false-d674338e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.7.3.1.1.13.3" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHEChildFunctionalStatusAssessment): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.7.3.1.1.13.3" test="string(@root)=('1.3.6.1.4.1.19376.1.7.3.1.1.13.3')">(IHEChildFunctionalStatusAssessment): The value for @root SHALL be '1.3.6.1.4.1.19376.1.7.3.1.1.13.3'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.7.3.1.1.13.3
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.3']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.3']]/hl7:id[not(@nullFlavor)]
Item: (IHEChildFunctionalStatusAssessment)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.3']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.3']]/hl7:id[not(@nullFlavor)]" id="d506897e4950-false-d674352e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.7.3.1.1.13.3" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHEChildFunctionalStatusAssessment): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.7.3.1.1.13.3
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.3']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.3']]/hl7:code[(@code='47420-5' and @codeSystem='2.16.840.1.113883.6.1')]
Item: (IHEChildFunctionalStatusAssessment)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.3']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.3']]/hl7:code[(@code='47420-5' and @codeSystem='2.16.840.1.113883.6.1')]" id="d506897e4951-false-d674363e0">
        <extends rule="CE"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.7.3.1.1.13.3" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CE' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHEChildFunctionalStatusAssessment): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CE", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.7.3.1.1.13.3" test="@nullFlavor or (@code='47420-5' and @codeSystem='2.16.840.1.113883.6.1' and @displayName='Functional Status Assessment' and @codeSystemName='LOINC')">(IHEChildFunctionalStatusAssessment): The element value SHALL be one of 'code '47420-5' codeSystem '2.16.840.1.113883.6.1' displayName='Functional Status Assessment' codeSystemName='LOINC''.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.7.3.1.1.13.3
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.3']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.3']]/hl7:text
Item: (IHEChildFunctionalStatusAssessment)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.3']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.3']]/hl7:text" id="d506897e4953-false-d674379e0">
        <extends rule="SD.TEXT"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.7.3.1.1.13.3" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='SD.TEXT' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHEChildFunctionalStatusAssessment): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:SD.TEXT", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.7.3.1.1.13.3
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.3']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.3']]/hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.4']]]
Item: (IHEChildFunctionalStatusAssessment)
-->

<!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.7.3.1.1.13.3
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.3']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.3']]/hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.5']]]
Item: (IHEChildFunctionalStatusAssessment)
-->
</pattern>