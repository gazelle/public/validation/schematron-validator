<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.3.6.1.4.1.19376.1.5.3.1.3.28
Name: IHE Coded Results Section
Description: The results section shall contain a narrative description of the relevant diagnostic  procedures the patient received in the past. It shall include entries for procedures and  references to procedure reports when known as described in the Entry Content Modules.
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron" id="template-1.3.6.1.4.1.19376.1.5.3.1.3.28-2015-10-05T175206">
    <title>IHE Coded Results Section</title>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.3.28
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.28'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.27']]]
Item: (IHECodedResultsSection)
-->

<!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.3.28
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.28'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.27']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.28'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.27']]
Item: (IHECodedResultsSection)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.28'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.27']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.28'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.27']]" id="d506897e3590-false-d665007e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.28" test="count(hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.28'])&gt;=1">(IHECodedResultsSection): element hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.28'] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.28" test="count(hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.28'])&lt;=1">(IHECodedResultsSection): element hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.28'] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.28" test="count(hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.27'])&gt;=1">(IHECodedResultsSection): element hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.27'] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.28" test="count(hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.27'])&lt;=1">(IHECodedResultsSection): element hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.27'] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.28" test="count(hl7:id)&gt;=1">(IHECodedResultsSection): element hl7:id is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.28" test="count(hl7:id)&lt;=1">(IHECodedResultsSection): element hl7:id appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.28" test="count(hl7:code[(@code='30954-2' and @codeSystem='2.16.840.1.113883.6.1')])&gt;=1">(IHECodedResultsSection): element hl7:code[(@code='30954-2' and @codeSystem='2.16.840.1.113883.6.1')] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.28" test="count(hl7:code[(@code='30954-2' and @codeSystem='2.16.840.1.113883.6.1')])&lt;=1">(IHECodedResultsSection): element hl7:code[(@code='30954-2' and @codeSystem='2.16.840.1.113883.6.1')] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.28" test="count(hl7:text[not(@nullFlavor)])&gt;=1">(IHECodedResultsSection): element hl7:text[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.28" test="count(hl7:text[not(@nullFlavor)])&lt;=1">(IHECodedResultsSection): element hl7:text[not(@nullFlavor)] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.28" test="count(hl7:entry[hl7:procedure[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.19']]])&gt;=1">(IHECodedResultsSection): element hl7:entry[hl7:procedure[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.19']]] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.28" test="count(hl7:entry[hl7:observation[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13']]])&gt;=1">(IHECodedResultsSection): element hl7:entry[hl7:observation[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13']]] is mandatory [min 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.3.28
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.28'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.27']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.28'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.27']]/hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.28']
Item: (IHECodedResultsSection)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.28'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.27']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.28'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.27']]/hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.28']" id="d506897e3591-false-d665098e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.28" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHECodedResultsSection): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.28" test="string(@root)=('1.3.6.1.4.1.19376.1.5.3.1.3.28')">(IHECodedResultsSection): The value for @root SHALL be '1.3.6.1.4.1.19376.1.5.3.1.3.28'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.3.28
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.28'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.27']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.28'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.27']]/hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.27']
Item: (IHECodedResultsSection)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.28'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.27']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.28'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.27']]/hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.27']" id="d506897e3593-false-d665113e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.28" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHECodedResultsSection): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.28" test="string(@root)=('1.3.6.1.4.1.19376.1.5.3.1.3.27')">(IHECodedResultsSection): The value for @root SHALL be '1.3.6.1.4.1.19376.1.5.3.1.3.27'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.3.28
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.28'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.27']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.28'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.27']]/hl7:id
Item: (IHECodedResultsSection)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.28'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.27']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.28'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.27']]/hl7:id" id="d506897e3595-false-d665127e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.28" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHECodedResultsSection): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.3.28
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.28'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.27']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.28'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.27']]/hl7:code[(@code='30954-2' and @codeSystem='2.16.840.1.113883.6.1')]
Item: (IHECodedResultsSection)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.28'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.27']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.28'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.27']]/hl7:code[(@code='30954-2' and @codeSystem='2.16.840.1.113883.6.1')]" id="d506897e3596-false-d665138e0">
        <extends rule="CE"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.28" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CE' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHECodedResultsSection): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CE", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.28" test="@nullFlavor or (@code='30954-2' and @codeSystem='2.16.840.1.113883.6.1' and @displayName='Relevant diagnostic tests/laboratory data' and @codeSystemName='LOINC')">(IHECodedResultsSection): The element value SHALL be one of 'code '30954-2' codeSystem '2.16.840.1.113883.6.1' displayName='Relevant diagnostic tests/laboratory data' codeSystemName='LOINC''.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.3.28
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.28'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.27']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.28'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.27']]/hl7:text[not(@nullFlavor)]
Item: (IHECodedResultsSection)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.28'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.27']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.28'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.27']]/hl7:text[not(@nullFlavor)]" id="d506897e3598-false-d665154e0">
        <extends rule="SD.TEXT"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.28" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='SD.TEXT' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHECodedResultsSection): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:SD.TEXT", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.3.28
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.28'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.27']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.28'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.27']]/hl7:entry[hl7:procedure[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.19']]]
Item: (IHECodedResultsSection)
-->

<!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.3.28
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.28'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.27']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.28'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.27']]/hl7:entry[hl7:act[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.4']]]
Item: (IHECodedResultsSection)
-->

<!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.3.28
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.28'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.27']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.28'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.27']]/hl7:entry[hl7:observation[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13']]]
Item: (IHECodedResultsSection)
-->
</pattern>