<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.3.6.1.4.1.19376.1.5.3.1.4.20.2
Name: IHE Usual Occupation and Industry Organizer
Description: A Usual Occupation Organizer holds clinical statements about the subject’s usual  occupation and usual industry
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron" id="template-1.3.6.1.4.1.19376.1.5.3.1.4.20.2-2016-10-02T183033">
    <title>IHE Usual Occupation and Industry Organizer</title>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.4.20.2
Context: *[hl7:organizer[@classCode='CLUSTER'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.2']]]
Item: (IHEUsualOccupationAndIndustryOrganizer)
-->

<!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.4.20.2
Context: *[hl7:organizer[@classCode='CLUSTER'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.2']]]/hl7:organizer[@classCode='CLUSTER'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.2']]
Item: (IHEUsualOccupationAndIndustryOrganizer)
-->
    <rule context="*[hl7:organizer[@classCode='CLUSTER'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.2']]]/hl7:organizer[@classCode='CLUSTER'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.2']]" id="d506897e4305-false-d670265e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.2" test="string(@classCode)=('CLUSTER')">(IHEUsualOccupationAndIndustryOrganizer): The value for @classCode SHALL be 'CLUSTER'.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.2" test="string(@moodCode)=('EVN')">(IHEUsualOccupationAndIndustryOrganizer): The value for @moodCode SHALL be 'EVN'.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.2" test="count(hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.2'])&gt;=1">(IHEUsualOccupationAndIndustryOrganizer): element hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.2'] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.2" test="count(hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.2'])&lt;=1">(IHEUsualOccupationAndIndustryOrganizer): element hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.2'] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.2" test="count(hl7:id)&gt;=1">(IHEUsualOccupationAndIndustryOrganizer): element hl7:id is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.2" test="count(hl7:code[(@code='LOINC-3' and @codeSystem='2.16.840.1.113883.6.1')])&gt;=1">(IHEUsualOccupationAndIndustryOrganizer): element hl7:code[(@code='LOINC-3' and @codeSystem='2.16.840.1.113883.6.1')] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.2" test="count(hl7:code[(@code='LOINC-3' and @codeSystem='2.16.840.1.113883.6.1')])&lt;=1">(IHEUsualOccupationAndIndustryOrganizer): element hl7:code[(@code='LOINC-3' and @codeSystem='2.16.840.1.113883.6.1')] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.2" test="count(hl7:statusCode[@code='completed'])&gt;=1">(IHEUsualOccupationAndIndustryOrganizer): element hl7:statusCode[@code='completed'] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.2" test="count(hl7:statusCode[@code='completed'])&lt;=1">(IHEUsualOccupationAndIndustryOrganizer): element hl7:statusCode[@code='completed'] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.2" test="count(hl7:effectiveTime)&gt;=1">(IHEUsualOccupationAndIndustryOrganizer): element hl7:effectiveTime is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.2" test="count(hl7:effectiveTime)&lt;=1">(IHEUsualOccupationAndIndustryOrganizer): element hl7:effectiveTime appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.2" test="count(hl7:component[hl7:observation[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.5']]])&gt;=1">(IHEUsualOccupationAndIndustryOrganizer): element hl7:component[hl7:observation[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.5']]] is mandatory [min 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.4.20.2
Context: *[hl7:organizer[@classCode='CLUSTER'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.2']]]/hl7:organizer[@classCode='CLUSTER'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.2']]/hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.2']
Item: (IHEUsualOccupationAndIndustryOrganizer)
-->
    <rule context="*[hl7:organizer[@classCode='CLUSTER'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.2']]]/hl7:organizer[@classCode='CLUSTER'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.2']]/hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.2']" id="d506897e4308-false-d670344e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHEUsualOccupationAndIndustryOrganizer): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.2" test="string(@root)=('1.3.6.1.4.1.19376.1.5.3.1.4.20.2')">(IHEUsualOccupationAndIndustryOrganizer): The value for @root SHALL be '1.3.6.1.4.1.19376.1.5.3.1.4.20.2'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.4.20.2
Context: *[hl7:organizer[@classCode='CLUSTER'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.2']]]/hl7:organizer[@classCode='CLUSTER'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.2']]/hl7:id
Item: (IHEUsualOccupationAndIndustryOrganizer)
-->
    <rule context="*[hl7:organizer[@classCode='CLUSTER'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.2']]]/hl7:organizer[@classCode='CLUSTER'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.2']]/hl7:id" id="d506897e4310-false-d670358e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHEUsualOccupationAndIndustryOrganizer): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.4.20.2
Context: *[hl7:organizer[@classCode='CLUSTER'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.2']]]/hl7:organizer[@classCode='CLUSTER'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.2']]/hl7:code[(@code='LOINC-3' and @codeSystem='2.16.840.1.113883.6.1')]
Item: (IHEUsualOccupationAndIndustryOrganizer)
-->
    <rule context="*[hl7:organizer[@classCode='CLUSTER'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.2']]]/hl7:organizer[@classCode='CLUSTER'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.2']]/hl7:code[(@code='LOINC-3' and @codeSystem='2.16.840.1.113883.6.1')]" id="d506897e4311-false-d670369e0">
        <extends rule="CD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHEUsualOccupationAndIndustryOrganizer): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.2" test="@nullFlavor or (@code='LOINC-3' and @codeSystem='2.16.840.1.113883.6.1')">(IHEUsualOccupationAndIndustryOrganizer): The element value SHALL be one of 'code 'LOINC-3' codeSystem '2.16.840.1.113883.6.1''.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.4.20.2
Context: *[hl7:organizer[@classCode='CLUSTER'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.2']]]/hl7:organizer[@classCode='CLUSTER'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.2']]/hl7:statusCode[@code='completed']
Item: (IHEUsualOccupationAndIndustryOrganizer)
-->
    <rule context="*[hl7:organizer[@classCode='CLUSTER'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.2']]]/hl7:organizer[@classCode='CLUSTER'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.2']]/hl7:statusCode[@code='completed']" id="d506897e4313-false-d670386e0">
        <extends rule="CS"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CS' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHEUsualOccupationAndIndustryOrganizer): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CS", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.2" test="@nullFlavor or (@code='completed')">(IHEUsualOccupationAndIndustryOrganizer): The element value SHALL be one of 'code 'completed''.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.4.20.2
Context: *[hl7:organizer[@classCode='CLUSTER'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.2']]]/hl7:organizer[@classCode='CLUSTER'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.2']]/hl7:effectiveTime
Item: (IHEUsualOccupationAndIndustryOrganizer)
-->
    <rule context="*[hl7:organizer[@classCode='CLUSTER'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.2']]]/hl7:organizer[@classCode='CLUSTER'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.2']]/hl7:effectiveTime" id="d506897e4315-false-d670402e0">
        <extends rule="IVL_TS"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='IVL_TS' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHEUsualOccupationAndIndustryOrganizer): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:IVL_TS", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.4.20.2
Context: *[hl7:organizer[@classCode='CLUSTER'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.2']]]/hl7:organizer[@classCode='CLUSTER'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.2']]/hl7:component[hl7:observation[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.5']]]
Item: (IHEUsualOccupationAndIndustryOrganizer)
-->
    <rule context="*[hl7:organizer[@classCode='CLUSTER'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.2']]]/hl7:organizer[@classCode='CLUSTER'][@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.2']]/hl7:component[hl7:observation[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.5']]]">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20.2" test="string(@typeCode)=('COMP') or not(@typeCode)">(IHEUsualOccupationAndIndustryOrganizer): The value for @typeCode SHALL be 'COMP'.</assert>
    </rule>
</pattern>