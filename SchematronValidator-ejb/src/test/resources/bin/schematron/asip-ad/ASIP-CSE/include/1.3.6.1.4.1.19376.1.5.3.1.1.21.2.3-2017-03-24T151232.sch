<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3
Name: IHE Labor and Delivery Events Section
Description: The Labor and Delivery Events Section SHALL include a narrative text containing relevant information collected during the labor and delivery process.
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron" id="template-1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3-2017-03-24T151232">
    <title>IHE Labor and Delivery Events Section</title>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]]
Item: (IHELaborandDeliveryEventsSection)
-->

<!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]
Item: (IHELaborandDeliveryEventsSection)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]" id="d506897e2539-false-d655871e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3" test="count(hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3'])&gt;=1">(IHELaborandDeliveryEventsSection): element hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3'] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3" test="count(hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3'])&lt;=1">(IHELaborandDeliveryEventsSection): element hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3'] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3" test="count(hl7:id[not(@nullFlavor)])&gt;=1">(IHELaborandDeliveryEventsSection): element hl7:id[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3" test="count(hl7:id[not(@nullFlavor)])&lt;=1">(IHELaborandDeliveryEventsSection): element hl7:id[not(@nullFlavor)] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3" test="count(hl7:code[(@code='57074-7' and @codeSystem='2.16.840.1.113883.6.1')])&gt;=1">(IHELaborandDeliveryEventsSection): element hl7:code[(@code='57074-7' and @codeSystem='2.16.840.1.113883.6.1')] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3" test="count(hl7:code[(@code='57074-7' and @codeSystem='2.16.840.1.113883.6.1')])&lt;=1">(IHELaborandDeliveryEventsSection): element hl7:code[(@code='57074-7' and @codeSystem='2.16.840.1.113883.6.1')] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3" test="count(hl7:text)&gt;=1">(IHELaborandDeliveryEventsSection): element hl7:text is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3" test="count(hl7:text)&lt;=1">(IHELaborandDeliveryEventsSection): element hl7:text appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]/hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']
Item: (IHELaborandDeliveryEventsSection)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]/hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']" id="d506897e2540-false-d655953e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHELaborandDeliveryEventsSection): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3" test="string(@root)=('1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3')">(IHELaborandDeliveryEventsSection): The value for @root SHALL be '1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]/hl7:id[not(@nullFlavor)]
Item: (IHELaborandDeliveryEventsSection)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]/hl7:id[not(@nullFlavor)]" id="d506897e2542-false-d655967e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHELaborandDeliveryEventsSection): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]/hl7:code[(@code='57074-7' and @codeSystem='2.16.840.1.113883.6.1')]
Item: (IHELaborandDeliveryEventsSection)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]/hl7:code[(@code='57074-7' and @codeSystem='2.16.840.1.113883.6.1')]" id="d506897e2543-false-d655978e0">
        <extends rule="CE"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CE' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHELaborandDeliveryEventsSection): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CE", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3" test="@nullFlavor or (@code='57074-7' and @codeSystem='2.16.840.1.113883.6.1' and @displayName='Labor and delivery process' and @codeSystemName='LOINC')">(IHELaborandDeliveryEventsSection): The element value SHALL be one of 'code '57074-7' codeSystem '2.16.840.1.113883.6.1' displayName='Labor and delivery process' codeSystemName='LOINC''.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]/hl7:text
Item: (IHELaborandDeliveryEventsSection)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]/hl7:text" id="d506897e2545-false-d655994e0">
        <extends rule="SD.TEXT"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='SD.TEXT' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHELaborandDeliveryEventsSection): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:SD.TEXT", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]/hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.13.2.11']]]
Item: (IHELaborandDeliveryEventsSection)
-->

<!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]/hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.7'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.9']]]
Item: (IHELaborandDeliveryEventsSection)
-->
</pattern>