<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.3.6.1.4.1.19376.1.7.3.1.1.13.5
Name: IHE Eating and Sleeping Assessment Section
Description: This section describes a test battery in order to evaluate the psychomotricity of the newborn.
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron" id="template-1.3.6.1.4.1.19376.1.7.3.1.1.13.5-2017-03-24T142046">
    <title>IHE Eating and Sleeping Assessment Section</title>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.7.3.1.1.13.5
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.5']]]
Item: (IHEEatingandSleepingAssessmentSection)
-->

<!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.7.3.1.1.13.5
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.5']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.5']]
Item: (IHEEatingandSleepingAssessmentSection)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.5']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.5']]" id="d506897e5004-false-d674602e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.7.3.1.1.13.5" test="count(hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.5'])&gt;=1">(IHEEatingandSleepingAssessmentSection): element hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.5'] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.7.3.1.1.13.5" test="count(hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.5'])&lt;=1">(IHEEatingandSleepingAssessmentSection): element hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.5'] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.7.3.1.1.13.5" test="count(hl7:id[not(@nullFlavor)])&gt;=1">(IHEEatingandSleepingAssessmentSection): element hl7:id[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.7.3.1.1.13.5" test="count(hl7:id[not(@nullFlavor)])&lt;=1">(IHEEatingandSleepingAssessmentSection): element hl7:id[not(@nullFlavor)] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.7.3.1.1.13.5" test="count(hl7:code[(@code='47420-5' and @codeSystem='2.16.840.1.113883.6.1')])&gt;=1">(IHEEatingandSleepingAssessmentSection): element hl7:code[(@code='47420-5' and @codeSystem='2.16.840.1.113883.6.1')] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.7.3.1.1.13.5" test="count(hl7:code[(@code='47420-5' and @codeSystem='2.16.840.1.113883.6.1')])&lt;=1">(IHEEatingandSleepingAssessmentSection): element hl7:code[(@code='47420-5' and @codeSystem='2.16.840.1.113883.6.1')] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.7.3.1.1.13.5" test="count(hl7:text)&gt;=1">(IHEEatingandSleepingAssessmentSection): element hl7:text is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.7.3.1.1.13.5" test="count(hl7:text)&lt;=1">(IHEEatingandSleepingAssessmentSection): element hl7:text appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.7.3.1.1.13.5" test="count(hl7:entry[hl7:observation[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13']]])&gt;=1">(IHEEatingandSleepingAssessmentSection): element hl7:entry[hl7:observation[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13']]] is mandatory [min 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.7.3.1.1.13.5
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.5']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.5']]/hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.5']
Item: (IHEEatingandSleepingAssessmentSection)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.5']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.5']]/hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.5']" id="d506897e5005-false-d674661e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.7.3.1.1.13.5" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHEEatingandSleepingAssessmentSection): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.7.3.1.1.13.5" test="string(@root)=('1.3.6.1.4.1.19376.1.7.3.1.1.13.5')">(IHEEatingandSleepingAssessmentSection): The value for @root SHALL be '1.3.6.1.4.1.19376.1.7.3.1.1.13.5'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.7.3.1.1.13.5
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.5']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.5']]/hl7:id[not(@nullFlavor)]
Item: (IHEEatingandSleepingAssessmentSection)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.5']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.5']]/hl7:id[not(@nullFlavor)]" id="d506897e5007-false-d674675e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.7.3.1.1.13.5" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHEEatingandSleepingAssessmentSection): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.7.3.1.1.13.5
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.5']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.5']]/hl7:code[(@code='47420-5' and @codeSystem='2.16.840.1.113883.6.1')]
Item: (IHEEatingandSleepingAssessmentSection)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.5']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.5']]/hl7:code[(@code='47420-5' and @codeSystem='2.16.840.1.113883.6.1')]" id="d506897e5008-false-d674686e0">
        <extends rule="CE"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.7.3.1.1.13.5" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CE' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHEEatingandSleepingAssessmentSection): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CE", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.7.3.1.1.13.5" test="@nullFlavor or (@code='47420-5' and @codeSystem='2.16.840.1.113883.6.1' and @displayName='Functional Status Assessment' and @codeSystemName='LOINC')">(IHEEatingandSleepingAssessmentSection): The element value SHALL be one of 'code '47420-5' codeSystem '2.16.840.1.113883.6.1' displayName='Functional Status Assessment' codeSystemName='LOINC''.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.7.3.1.1.13.5
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.5']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.5']]/hl7:text
Item: (IHEEatingandSleepingAssessmentSection)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.5']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.5']]/hl7:text" id="d506897e5010-false-d674702e0">
        <extends rule="SD.TEXT"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.7.3.1.1.13.5" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='SD.TEXT' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHEEatingandSleepingAssessmentSection): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:SD.TEXT", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.7.3.1.1.13.5
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.5']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.5']]/hl7:entry[hl7:observation[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13']]]
Item: (IHEEatingandSleepingAssessmentSection)
-->
</pattern>