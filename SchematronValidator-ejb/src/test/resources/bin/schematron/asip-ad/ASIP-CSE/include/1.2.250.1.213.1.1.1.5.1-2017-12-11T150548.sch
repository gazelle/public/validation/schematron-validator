<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.250.1.213.1.1.1.5.1
Name: Certificat de sante du 8e jour
Description: 
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron" id="template-1.2.250.1.213.1.1.1.5.1-2017-12-11T150548">
    <title>Certificat de sante du 8e jour</title>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.5.1
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]
Item: (CertificatDeSanteDu8eJour)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]" id="d506897e2269-false-d508993e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.1" test="count(hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1'])&gt;=1">(CertificatDeSanteDu8eJour): element hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1'] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.1" test="count(hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1'])&lt;=1">(CertificatDeSanteDu8eJour): element hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1'] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.1" test="count(hl7:id)&gt;=1">(CertificatDeSanteDu8eJour): element hl7:id is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.1" test="count(hl7:id)&lt;=1">(CertificatDeSanteDu8eJour): element hl7:id appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.1" test="count(hl7:code)&gt;=1">(CertificatDeSanteDu8eJour): element hl7:code is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.1" test="count(hl7:code)&lt;=1">(CertificatDeSanteDu8eJour): element hl7:code appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.1" test="count(hl7:title)&lt;=1">(CertificatDeSanteDu8eJour): element hl7:title appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.1" test="count(hl7:effectiveTime)&gt;=1">(CertificatDeSanteDu8eJour): element hl7:effectiveTime is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.1" test="count(hl7:effectiveTime)&lt;=1">(CertificatDeSanteDu8eJour): element hl7:effectiveTime appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.1" test="count(hl7:component)&gt;=1">(CertificatDeSanteDu8eJour): element hl7:component is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.1" test="count(hl7:component)&lt;=1">(CertificatDeSanteDu8eJour): element hl7:component appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.1" test="count(hl7:componentOf[not(@nullFlavor)])&gt;=1">(CertificatDeSanteDu8eJour): element hl7:componentOf[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.1" test="count(hl7:componentOf[not(@nullFlavor)])&lt;=1">(CertificatDeSanteDu8eJour): element hl7:componentOf[not(@nullFlavor)] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.1" test="count(hl7:relatedDocument[@typeCode='RPLC'])&gt;=1">(CertificatDeSanteDu8eJour): element hl7:relatedDocument[@typeCode='RPLC'] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.1" test="count(hl7:relatedDocument[@typeCode='RPLC'])&lt;=1">(CertificatDeSanteDu8eJour): element hl7:relatedDocument[@typeCode='RPLC'] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.1" test="count(hl7:documentationOf[not(@nullFlavor)])&gt;=1">(CertificatDeSanteDu8eJour): element hl7:documentationOf[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.1" test="count(hl7:documentationOf[not(@nullFlavor)])&lt;=1">(CertificatDeSanteDu8eJour): element hl7:documentationOf[not(@nullFlavor)] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.1" test="count(hl7:custodian[not(@nullFlavor)])&gt;=1">(CertificatDeSanteDu8eJour): element hl7:custodian[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.1" test="count(hl7:custodian[not(@nullFlavor)])&lt;=1">(CertificatDeSanteDu8eJour): element hl7:custodian[not(@nullFlavor)] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.1" test="count(hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]])&gt;=1">(CertificatDeSanteDu8eJour): element hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.1" test="count(hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]])&lt;=1">(CertificatDeSanteDu8eJour): element hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.1" test="count(hl7:author[not(@nullFlavor)])&gt;=1">(CertificatDeSanteDu8eJour): element hl7:author[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.1" test="count(hl7:author[not(@nullFlavor)])&lt;=1">(CertificatDeSanteDu8eJour): element hl7:author[not(@nullFlavor)] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.1" test="count(hl7:informant)&gt;=1">(CertificatDeSanteDu8eJour): element hl7:informant is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.1" test="count(hl7:informant)&lt;=1">(CertificatDeSanteDu8eJour): element hl7:informant appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.1" test="count(hl7:participant)&gt;=1">(CertificatDeSanteDu8eJour): element hl7:participant is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.1" test="count(hl7:participant)&lt;=1">(CertificatDeSanteDu8eJour): element hl7:participant appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.1" test="count(hl7:recordTarget)&gt;=1">(CertificatDeSanteDu8eJour): element hl7:recordTarget is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.1" test="count(hl7:recordTarget)&lt;=1">(CertificatDeSanteDu8eJour): element hl7:recordTarget appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.5.1
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']
Item: (CertificatDeSanteDu8eJour)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']" id="d506897e2270-false-d509449e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.1" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CertificatDeSanteDu8eJour): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.1" test="string(@root)=('1.2.250.1.213.1.1.1.5.1')">(CertificatDeSanteDu8eJour): The value for @root SHALL be '1.2.250.1.213.1.1.1.5.1'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.5.1
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:id
Item: (CertificatDeSanteDu8eJour)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:id" id="d506897e2272-false-d509463e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.1" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CertificatDeSanteDu8eJour): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.5.1
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:code
Item: (CertificatDeSanteDu8eJour)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:code" id="d506897e2273-false-d509473e0">
        <extends rule="CE"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.1" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CE' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CertificatDeSanteDu8eJour): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CE", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.5.1
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:title
Item: (CertificatDeSanteDu8eJour)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:title" id="d506897e2274-false-d509483e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.1" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CertificatDeSanteDu8eJour): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.5.1
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:effectiveTime
Item: (CertificatDeSanteDu8eJour)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:effectiveTime" id="d506897e2275-false-d509493e0">
        <extends rule="TS"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.1" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TS' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CertificatDeSanteDu8eJour): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TS", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.1" test="not(*)">(CertificatDeSanteDu8eJour): <value-of select="local-name()"/> with datatype TS, SHOULD NOT have child elements.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.5.1
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:component
Item: (CertificatDeSanteDu8eJour)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:component" id="d506897e2276-false-d509643e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.1" test="count(hl7:structuredBody)&gt;=1">(CertificatDeSanteDu8eJour): element hl7:structuredBody is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.1" test="count(hl7:structuredBody)&lt;=1">(CertificatDeSanteDu8eJour): element hl7:structuredBody appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.5.1
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:component/hl7:structuredBody
Item: (CertificatDeSanteDu8eJour)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:component/hl7:structuredBody" id="d506897e2277-false-d509932e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.1" test="count(hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.15']]])&gt;=1">(CertificatDeSanteDu8eJour): element hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.15']]] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.1" test="count(hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.15']]])&lt;=1">(CertificatDeSanteDu8eJour): element hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.15']]] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.1" test="count(hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.5.3.4']]])&gt;=1">(CertificatDeSanteDu8eJour): element hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.5.3.4']]] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.1" test="count(hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.5.3.4']]])&lt;=1">(CertificatDeSanteDu8eJour): element hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.5.3.4']]] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.1" test="count(hl7:component[hl7:section[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10'] and hl7:templateId[@root='1.2.250.1.213.1.1.2.13'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]])&gt;=1">(CertificatDeSanteDu8eJour): element hl7:component[hl7:section[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10'] and hl7:templateId[@root='1.2.250.1.213.1.1.2.13'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.1" test="count(hl7:component[hl7:section[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10'] and hl7:templateId[@root='1.2.250.1.213.1.1.2.13'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]])&lt;=1">(CertificatDeSanteDu8eJour): element hl7:component[hl7:section[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10'] and hl7:templateId[@root='1.2.250.1.213.1.1.2.13'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.1" test="count(hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.4']]])&gt;=1">(CertificatDeSanteDu8eJour): element hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.4']]] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.1" test="count(hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.4']]])&lt;=1">(CertificatDeSanteDu8eJour): element hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.4']]] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.1" test="count(hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.13.2.5']]])&gt;=1">(CertificatDeSanteDu8eJour): element hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.13.2.5']]] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.1" test="count(hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.13.2.5']]])&lt;=1">(CertificatDeSanteDu8eJour): element hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.13.2.5']]] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.1" test="count(hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.23'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.6']]])&gt;=1">(CertificatDeSanteDu8eJour): element hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.23'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.6']]] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.1" test="count(hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.23'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.6']]])&lt;=1">(CertificatDeSanteDu8eJour): element hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.23'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.6']]] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.1" test="count(hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.31'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.10']]])&gt;=1">(CertificatDeSanteDu8eJour): element hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.31'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.10']]] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.1" test="count(hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.31'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.10']]])&lt;=1">(CertificatDeSanteDu8eJour): element hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.31'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.10']]] appears too often [max 1x].</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.5.1
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:component/hl7:structuredBody/hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.15']]]
Item: (CertificatDeSanteDu8eJour)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.5.1
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:component/hl7:structuredBody/hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.5.3.4']]]
Item: (CertificatDeSanteDu8eJour)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.5.1
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:component/hl7:structuredBody/hl7:component[hl7:section[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10'] and hl7:templateId[@root='1.2.250.1.213.1.1.2.13'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]]
Item: (CertificatDeSanteDu8eJour)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.5.1
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:component/hl7:structuredBody/hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.4']]]
Item: (CertificatDeSanteDu8eJour)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.5.1
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:component/hl7:structuredBody/hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.13.2.5']]]
Item: (CertificatDeSanteDu8eJour)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.5.1
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:component/hl7:structuredBody/hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.23'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.6']]]
Item: (CertificatDeSanteDu8eJour)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.5.1
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:component/hl7:structuredBody/hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.31'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.10']]]
Item: (CertificatDeSanteDu8eJour)
-->
<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]" id="d510396e12-false-d510428e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:encompassingEncounter[not(@nullFlavor)])&gt;=1">(CI-SISComponentOf): element hl7:encompassingEncounter[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:encompassingEncounter[not(@nullFlavor)])&lt;=1">(CI-SISComponentOf): element hl7:encompassingEncounter[not(@nullFlavor)] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]" id="d510396e13-false-d510490e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:code[(@code='IMP' and @codeSystem='2.16.840.1.113883.5.4') or (@code='EMER' and @codeSystem='2.16.840.1.113883.5.4') or (@code='AMB' and @codeSystem='2.16.840.1.113883.5.4') or (@code='FLD' and @codeSystem='2.16.840.1.113883.5.4') or (@code='HH' and @codeSystem='2.16.840.1.113883.5.4') or (@code='VR' and @codeSystem='2.16.840.1.113883.5.4') or (@code='EXTERNE' and @codeSystem='1.2.250.1.213.1.1.4.2.291') or (@code='SEANCE' and @codeSystem='1.2.250.1.213.1.1.4.2.291')])&lt;=1">(CI-SISComponentOf): element hl7:code[(@code='IMP' and @codeSystem='2.16.840.1.113883.5.4') or (@code='EMER' and @codeSystem='2.16.840.1.113883.5.4') or (@code='AMB' and @codeSystem='2.16.840.1.113883.5.4') or (@code='FLD' and @codeSystem='2.16.840.1.113883.5.4') or (@code='HH' and @codeSystem='2.16.840.1.113883.5.4') or (@code='VR' and @codeSystem='2.16.840.1.113883.5.4') or (@code='EXTERNE' and @codeSystem='1.2.250.1.213.1.1.4.2.291') or (@code='SEANCE' and @codeSystem='1.2.250.1.213.1.1.4.2.291')] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:effectiveTime)&gt;=1">(CI-SISComponentOf): element hl7:effectiveTime is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:effectiveTime)&lt;=1">(CI-SISComponentOf): element hl7:effectiveTime appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:dischargeDispositionCode)&lt;=1">(CI-SISComponentOf): element hl7:dischargeDispositionCode appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:responsibleParty)&lt;=1">(CI-SISComponentOf): element hl7:responsibleParty appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]])&gt;=1">(CI-SISComponentOf): element hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]])&lt;=1">(CI-SISComponentOf): element hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:id
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:id" id="d510396e19-false-d510574e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISComponentOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="not(@extension) or string-length(@extension)&gt;0">(CI-SISComponentOf): Attribute @extension SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="@root">(CI-SISComponentOf): attribute @root SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="not(@root) or matches(@root,'^[0-2](\.(0|[1-9]\d*))*$') or matches(@root,'^[A-Fa-f\d]{8}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{12}$') or matches(@root,'^[A-Za-z][A-Za-z\d\-]*$')">(CI-SISComponentOf): Attribute @root SHALL be of data type 'uid'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:code[(@code='IMP' and @codeSystem='2.16.840.1.113883.5.4') or (@code='EMER' and @codeSystem='2.16.840.1.113883.5.4') or (@code='AMB' and @codeSystem='2.16.840.1.113883.5.4') or (@code='FLD' and @codeSystem='2.16.840.1.113883.5.4') or (@code='HH' and @codeSystem='2.16.840.1.113883.5.4') or (@code='VR' and @codeSystem='2.16.840.1.113883.5.4') or (@code='EXTERNE' and @codeSystem='1.2.250.1.213.1.1.4.2.291') or (@code='SEANCE' and @codeSystem='1.2.250.1.213.1.1.4.2.291')]
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:code[(@code='IMP' and @codeSystem='2.16.840.1.113883.5.4') or (@code='EMER' and @codeSystem='2.16.840.1.113883.5.4') or (@code='AMB' and @codeSystem='2.16.840.1.113883.5.4') or (@code='FLD' and @codeSystem='2.16.840.1.113883.5.4') or (@code='HH' and @codeSystem='2.16.840.1.113883.5.4') or (@code='VR' and @codeSystem='2.16.840.1.113883.5.4') or (@code='EXTERNE' and @codeSystem='1.2.250.1.213.1.1.4.2.291') or (@code='SEANCE' and @codeSystem='1.2.250.1.213.1.1.4.2.291')]" id="d510396e27-false-d510596e0">
        <extends rule="CE"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CE' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISComponentOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CE", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="@nullFlavor or (@code='IMP' and @codeSystem='2.16.840.1.113883.5.4') or (@code='EMER' and @codeSystem='2.16.840.1.113883.5.4') or (@code='AMB' and @codeSystem='2.16.840.1.113883.5.4') or (@code='FLD' and @codeSystem='2.16.840.1.113883.5.4') or (@code='HH' and @codeSystem='2.16.840.1.113883.5.4') or (@code='VR' and @codeSystem='2.16.840.1.113883.5.4') or (@code='EXTERNE' and @codeSystem='1.2.250.1.213.1.1.4.2.291') or (@code='SEANCE' and @codeSystem='1.2.250.1.213.1.1.4.2.291')">(CI-SISComponentOf): The element value SHALL be one of 'code 'IMP' codeSystem '2.16.840.1.113883.5.4' or code 'EMER' codeSystem '2.16.840.1.113883.5.4' or code 'AMB' codeSystem '2.16.840.1.113883.5.4' or code 'FLD' codeSystem '2.16.840.1.113883.5.4' or code 'HH' codeSystem '2.16.840.1.113883.5.4' or code 'VR' codeSystem '2.16.840.1.113883.5.4' or code 'EXTERNE' codeSystem '1.2.250.1.213.1.1.4.2.291' or code 'SEANCE' codeSystem '1.2.250.1.213.1.1.4.2.291''.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="@displayName">(CI-SISComponentOf): attribute @displayName SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="not(@displayName) or string-length(@displayName)&gt;0">(CI-SISComponentOf): Attribute @displayName SHALL be of data type 'st'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:effectiveTime
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:effectiveTime" id="d510396e70-false-d510640e0">
        <extends rule="IVL_TS"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='IVL_TS' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISComponentOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:IVL_TS", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:low)&lt;=1">(CI-SISComponentOf): element hl7:low appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:high)&lt;=1">(CI-SISComponentOf): element hl7:high appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:effectiveTime/hl7:low
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:effectiveTime/hl7:low" id="d510396e76-false-d510664e0">
        <extends rule="IVXB_TS"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='IVXB_TS' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISComponentOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:IVXB_TS", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="@value">(CI-SISComponentOf): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="not(@value) or matches(string(@value), '^[0-9]{4,14}')">(CI-SISComponentOf): Attribute @value SHALL be of data type 'ts'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:effectiveTime/hl7:high
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:effectiveTime/hl7:high" id="d510396e84-false-d510681e0">
        <extends rule="IVXB_TS"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='IVXB_TS' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISComponentOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:IVXB_TS", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="@value">(CI-SISComponentOf): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="not(@value) or matches(string(@value), '^[0-9]{4,14}')">(CI-SISComponentOf): Attribute @value SHALL be of data type 'ts'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:dischargeDispositionCode
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:dischargeDispositionCode" id="d510396e92-false-d510698e0">
        <extends rule="CE"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CE' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISComponentOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CE", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="@displayName">(CI-SISComponentOf): attribute @displayName SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="not(@displayName) or string-length(@displayName)&gt;0">(CI-SISComponentOf): Attribute @displayName SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="string(@codeSystem)=('1.2.250.1.213.2.14') or not(@codeSystem)">(CI-SISComponentOf): The value for @codeSystem SHALL be '1.2.250.1.213.2.14'.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="@code">(CI-SISComponentOf): attribute @code SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="not(@code) or (string-length(@code)&gt;0 and not(matches(@code,'\s')))">(CI-SISComponentOf): Attribute @code SHALL be of data type 'cs'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty" id="d510396e107-false-d510737e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']])&gt;=1">(CI-SISComponentOf): element hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']])&lt;=1">(CI-SISComponentOf): element hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]" id="d510396e113-false-d510776e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:id[@root='1.2.250.1.71.4.2.1'])&gt;=1">(CI-SISComponentOf): element hl7:id[@root='1.2.250.1.71.4.2.1'] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:id[@root='1.2.250.1.71.4.2.1'])&lt;=1">(CI-SISComponentOf): element hl7:id[@root='1.2.250.1.71.4.2.1'] appears too often [max 1x].</assert>
        <let name="elmcount" value="count(hl7:addr|hl7:addr)"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:assignedPerson)&lt;=1">(CI-SISComponentOf): element hl7:assignedPerson appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:representedOrganization)&lt;=1">(CI-SISComponentOf): element hl7:representedOrganization appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:id[@root='1.2.250.1.71.4.2.1']
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:id[@root='1.2.250.1.71.4.2.1']" id="d510396e121-false-d510842e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISComponentOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="@extension">(CI-SISComponentOf): attribute @extension SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="not(@extension) or string-length(@extension)&gt;0">(CI-SISComponentOf): Attribute @extension SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="string(@root)=('1.2.250.1.71.4.2.1')">(CI-SISComponentOf): The value for @root SHALL be '1.2.250.1.71.4.2.1'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]
Item: (CI-SISAddr)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr" id="d510843e76-false-d510865e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@hl7:use) or (string-length(@hl7:use)&gt;0 and not(matches(@hl7:use,'\s')))">(CI-SISAddr): Attribute @hl7:use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:country)&lt;=1">(CI-SISAddr): element hl7:country appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:state)&lt;=1">(CI-SISAddr): element hl7:state appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:city)&lt;=1">(CI-SISAddr): element hl7:city appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postalCode)&lt;=1">(CI-SISAddr): element hl7:postalCode appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumber)&lt;=1">(CI-SISAddr): element hl7:houseNumber appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumberNumeric)&lt;=1">(CI-SISAddr): element hl7:houseNumberNumeric appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetName)&lt;=1">(CI-SISAddr): element hl7:streetName appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetNameType)&lt;=1">(CI-SISAddr): element hl7:streetNameType appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:additionalLocator)&lt;=1">(CI-SISAddr): element hl7:additionalLocator appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:unitID)&lt;=1">(CI-SISAddr): element hl7:unitID appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postBox)&lt;=1">(CI-SISAddr): element hl7:postBox appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:precinct)&lt;=1">(CI-SISAddr): element hl7:precinct appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:country
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:country" id="d510843e94-false-d510963e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:state
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:state" id="d510843e98-false-d510973e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:city
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:city" id="d510843e102-false-d510983e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:postalCode
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:postalCode" id="d510843e110-false-d510993e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:houseNumber
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:houseNumber" id="d510843e118-false-d511003e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:houseNumberNumeric
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:houseNumberNumeric" id="d510843e122-false-d511013e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetName
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetName" id="d510843e126-false-d511023e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetNameType
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetNameType" id="d510843e130-false-d511033e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:additionalLocator
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:additionalLocator" id="d510843e175-false-d511043e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:unitID
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:unitID" id="d510843e190-false-d511053e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:postBox
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:postBox" id="d510843e200-false-d511063e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:precinct
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:precinct" id="d510843e204-false-d511073e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr" id="d510843e208-false-d511083e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISAddr): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine" id="d510843e226-false-d511139e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine" id="d510843e232-false-d511149e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine" id="d510843e236-false-d511159e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine" id="d510843e240-false-d511169e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine" id="d510843e244-false-d511179e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine" id="d510843e248-false-d511189e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.19
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:telecom
Item: (CI-SISTelecom)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:telecom" id="d511190e27-false-d511200e0">
        <extends rule="TEL"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TEL' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISTelecom): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TEL", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISTelecom): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@value">(CI-SISTelecom): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@value) or string(@value castable as xs:anyURI)">(CI-SISTelecom): Attribute @value SHALL be of data type 'url'<value-of select="@value"/>
        </assert>
        <let name="prefix" value="substring-before(@value, ':')"/>
        <let name="suffix" value="substring-after(@value, ':')"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(             (count(@*) = 1 and name(@*) = 'nullFlavor' and             (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or             ($suffix and (             $prefix = 'tel' or              $prefix = 'fax' or              $prefix = 'mailto' or              $prefix = 'http' or              $prefix = 'ftp' or              $prefix = 'mllp'))             )">(CI-SISTelecom): Erreur de conformité CI-SIS : <name/> n'est pas conforme à une adresse de télécommunication préfixe:chaîne 
            (avec préfixe = tel, fax, mailto, http, ftp ou mllp) 
            ou est vide et sans nullFlavor, ou contient un nullFlavor non admis.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@use = 'H'                      or @use = 'HP'                      or @use = 'HV'                      or @use = 'WP'                     or @use = 'DIR'                      or @use = 'PUB'                      or @use = 'EC'                      or @use = 'MC'                      or @use = 'PG'                      or not(@use)">(CI-SISTelecom): Erreur de conformité CI-SIS : L'attribut use de l'élément telecom n'est pas conforme. 
            Il est facultatif et les valeurs permises sont 'H','HP', 'HV','WP','DIR','PUB','EC','MC','PG'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:assignedPerson
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:assignedPerson" id="d510396e126-false-d511221e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:name)&gt;=1">(CI-SISComponentOf): element hl7:name is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:name)&lt;=1">(CI-SISComponentOf): element hl7:name appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:assignedPerson/hl7:name
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:assignedPerson/hl7:name" id="d510396e127-false-d511237e0">
        <extends rule="PN"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='PN' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISComponentOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:PN", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:family)&gt;=1">(CI-SISComponentOf): element hl7:family is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:family)&lt;=1">(CI-SISComponentOf): element hl7:family appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:given)&lt;=1">(CI-SISComponentOf): element hl7:given appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:prefix)&lt;=1">(CI-SISComponentOf): element hl7:prefix appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:assignedPerson/hl7:name/hl7:family
Item: (CI-SISComponentOf)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:assignedPerson/hl7:name/hl7:given
Item: (CI-SISComponentOf)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:assignedPerson/hl7:name/hl7:prefix
Item: (CI-SISComponentOf)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization" id="d510396e140-false-d511298e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:id[@root='1.2.250.1.71.4.2.2'])&lt;=1">(CI-SISComponentOf): element hl7:id[@root='1.2.250.1.71.4.2.2'] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:name)&lt;=1">(CI-SISComponentOf): element hl7:name appears too often [max 1x].</assert>
        <let name="elmcount" value="count(hl7:addr|hl7:addr)"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:standardIndustryClassCode)&lt;=1">(CI-SISComponentOf): element hl7:standardIndustryClassCode appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:id[@root='1.2.250.1.71.4.2.2']
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:id[@root='1.2.250.1.71.4.2.2']" id="d510396e141-false-d511355e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISComponentOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="@extension">(CI-SISComponentOf): attribute @extension SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="not(@extension) or string-length(@extension)&gt;0">(CI-SISComponentOf): Attribute @extension SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="string(@root)=('1.2.250.1.71.4.2.2')">(CI-SISComponentOf): The value for @root SHALL be '1.2.250.1.71.4.2.2'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:name
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:name" id="d510396e149-false-d511376e0">
        <extends rule="ON"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ON' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISComponentOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ON", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization
Item: (CI-SISAddr)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr" id="d511377e70-false-d511388e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@hl7:use) or (string-length(@hl7:use)&gt;0 and not(matches(@hl7:use,'\s')))">(CI-SISAddr): Attribute @hl7:use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:country)&lt;=1">(CI-SISAddr): element hl7:country appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:state)&lt;=1">(CI-SISAddr): element hl7:state appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:city)&lt;=1">(CI-SISAddr): element hl7:city appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postalCode)&lt;=1">(CI-SISAddr): element hl7:postalCode appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumber)&lt;=1">(CI-SISAddr): element hl7:houseNumber appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumberNumeric)&lt;=1">(CI-SISAddr): element hl7:houseNumberNumeric appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetName)&lt;=1">(CI-SISAddr): element hl7:streetName appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetNameType)&lt;=1">(CI-SISAddr): element hl7:streetNameType appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:additionalLocator)&lt;=1">(CI-SISAddr): element hl7:additionalLocator appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:unitID)&lt;=1">(CI-SISAddr): element hl7:unitID appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postBox)&lt;=1">(CI-SISAddr): element hl7:postBox appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:precinct)&lt;=1">(CI-SISAddr): element hl7:precinct appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:country
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:country" id="d511377e88-false-d511486e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:state
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:state" id="d511377e92-false-d511496e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:city
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:city" id="d511377e96-false-d511506e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:postalCode
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:postalCode" id="d511377e104-false-d511516e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:houseNumber
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:houseNumber" id="d511377e112-false-d511526e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:houseNumberNumeric
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:houseNumberNumeric" id="d511377e116-false-d511536e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetName
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetName" id="d511377e120-false-d511546e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetNameType
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetNameType" id="d511377e124-false-d511556e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:additionalLocator
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:additionalLocator" id="d511377e169-false-d511566e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:unitID
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:unitID" id="d511377e184-false-d511576e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:postBox
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:postBox" id="d511377e194-false-d511586e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:precinct
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:precinct" id="d511377e198-false-d511596e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr" id="d511377e202-false-d511606e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISAddr): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine" id="d511377e220-false-d511662e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine" id="d511377e226-false-d511672e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine" id="d511377e230-false-d511682e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine" id="d511377e234-false-d511692e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine" id="d511377e238-false-d511702e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine" id="d511377e242-false-d511712e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.19
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:telecom
Item: (CI-SISTelecom)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:telecom" id="d511713e27-false-d511723e0">
        <extends rule="TEL"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TEL' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISTelecom): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TEL", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISTelecom): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@value">(CI-SISTelecom): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@value) or string(@value castable as xs:anyURI)">(CI-SISTelecom): Attribute @value SHALL be of data type 'url'<value-of select="@value"/>
        </assert>
        <let name="prefix" value="substring-before(@value, ':')"/>
        <let name="suffix" value="substring-after(@value, ':')"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(             (count(@*) = 1 and name(@*) = 'nullFlavor' and             (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or             ($suffix and (             $prefix = 'tel' or              $prefix = 'fax' or              $prefix = 'mailto' or              $prefix = 'http' or              $prefix = 'ftp' or              $prefix = 'mllp'))             )">(CI-SISTelecom): Erreur de conformité CI-SIS : <name/> n'est pas conforme à une adresse de télécommunication préfixe:chaîne 
            (avec préfixe = tel, fax, mailto, http, ftp ou mllp) 
            ou est vide et sans nullFlavor, ou contient un nullFlavor non admis.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@use = 'H'                      or @use = 'HP'                      or @use = 'HV'                      or @use = 'WP'                     or @use = 'DIR'                      or @use = 'PUB'                      or @use = 'EC'                      or @use = 'MC'                      or @use = 'PG'                      or not(@use)">(CI-SISTelecom): Erreur de conformité CI-SIS : L'attribut use de l'élément telecom n'est pas conforme. 
            Il est facultatif et les valeurs permises sont 'H','HP', 'HV','WP','DIR','PUB','EC','MC','PG'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:standardIndustryClassCode
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:standardIndustryClassCode" id="d510396e152-false-d511744e0">
        <extends rule="CE"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CE' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISComponentOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CE", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="@displayName">(CI-SISComponentOf): attribute @displayName SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="not(@displayName) or string-length(@displayName)&gt;0">(CI-SISComponentOf): Attribute @displayName SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="@codeSystem">(CI-SISComponentOf): attribute @codeSystem SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="not(@codeSystem) or matches(@codeSystem,'^[0-2](\.(0|[1-9]\d*))*$')">(CI-SISComponentOf): Attribute @codeSystem SHALL be of data type 'oid'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="@code">(CI-SISComponentOf): attribute @code SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="not(@code) or (string-length(@code)&gt;0 and not(matches(@code,'\s')))">(CI-SISComponentOf): Attribute @code SHALL be of data type 'cs'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant" id="d510396e165-false-d511786e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']])&gt;=1">(CI-SISComponentOf): element hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']])&lt;=1">(CI-SISComponentOf): element hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]" id="d510396e171-false-d511825e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:id[@root='1.2.250.1.71.4.2.1'])&gt;=1">(CI-SISComponentOf): element hl7:id[@root='1.2.250.1.71.4.2.1'] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:id[@root='1.2.250.1.71.4.2.1'])&lt;=1">(CI-SISComponentOf): element hl7:id[@root='1.2.250.1.71.4.2.1'] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:assignedPerson)&lt;=1">(CI-SISComponentOf): element hl7:assignedPerson appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:representedOrganization)&lt;=1">(CI-SISComponentOf): element hl7:representedOrganization appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:id[@root='1.2.250.1.71.4.2.1']
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:id[@root='1.2.250.1.71.4.2.1']" id="d510396e179-false-d511881e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISComponentOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="@extension">(CI-SISComponentOf): attribute @extension SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="not(@extension) or string-length(@extension)&gt;0">(CI-SISComponentOf): Attribute @extension SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="string(@root)=('1.2.250.1.71.4.2.1')">(CI-SISComponentOf): The value for @root SHALL be '1.2.250.1.71.4.2.1'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]
Item: (CI-SISAddr)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr" id="d511882e76-false-d511904e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@hl7:use) or (string-length(@hl7:use)&gt;0 and not(matches(@hl7:use,'\s')))">(CI-SISAddr): Attribute @hl7:use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:country)&lt;=1">(CI-SISAddr): element hl7:country appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:state)&lt;=1">(CI-SISAddr): element hl7:state appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:city)&lt;=1">(CI-SISAddr): element hl7:city appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postalCode)&lt;=1">(CI-SISAddr): element hl7:postalCode appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumber)&lt;=1">(CI-SISAddr): element hl7:houseNumber appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumberNumeric)&lt;=1">(CI-SISAddr): element hl7:houseNumberNumeric appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetName)&lt;=1">(CI-SISAddr): element hl7:streetName appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetNameType)&lt;=1">(CI-SISAddr): element hl7:streetNameType appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:additionalLocator)&lt;=1">(CI-SISAddr): element hl7:additionalLocator appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:unitID)&lt;=1">(CI-SISAddr): element hl7:unitID appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postBox)&lt;=1">(CI-SISAddr): element hl7:postBox appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:precinct)&lt;=1">(CI-SISAddr): element hl7:precinct appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:country
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:country" id="d511882e94-false-d512002e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:state
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:state" id="d511882e98-false-d512012e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:city
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:city" id="d511882e102-false-d512022e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:postalCode
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:postalCode" id="d511882e110-false-d512032e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:houseNumber
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:houseNumber" id="d511882e118-false-d512042e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:houseNumberNumeric
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:houseNumberNumeric" id="d511882e122-false-d512052e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetName
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetName" id="d511882e126-false-d512062e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetNameType
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetNameType" id="d511882e130-false-d512072e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:additionalLocator
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:additionalLocator" id="d511882e175-false-d512082e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:unitID
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:unitID" id="d511882e190-false-d512092e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:postBox
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:postBox" id="d511882e200-false-d512102e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:precinct
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:precinct" id="d511882e204-false-d512112e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr" id="d511882e208-false-d512122e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISAddr): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine" id="d511882e226-false-d512178e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine" id="d511882e232-false-d512188e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine" id="d511882e236-false-d512198e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine" id="d511882e240-false-d512208e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine" id="d511882e244-false-d512218e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine" id="d511882e248-false-d512228e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.19
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:telecom
Item: (CI-SISTelecom)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:telecom" id="d512229e27-false-d512239e0">
        <extends rule="TEL"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TEL' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISTelecom): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TEL", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISTelecom): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@value">(CI-SISTelecom): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@value) or string(@value castable as xs:anyURI)">(CI-SISTelecom): Attribute @value SHALL be of data type 'url'<value-of select="@value"/>
        </assert>
        <let name="prefix" value="substring-before(@value, ':')"/>
        <let name="suffix" value="substring-after(@value, ':')"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(             (count(@*) = 1 and name(@*) = 'nullFlavor' and             (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or             ($suffix and (             $prefix = 'tel' or              $prefix = 'fax' or              $prefix = 'mailto' or              $prefix = 'http' or              $prefix = 'ftp' or              $prefix = 'mllp'))             )">(CI-SISTelecom): Erreur de conformité CI-SIS : <name/> n'est pas conforme à une adresse de télécommunication préfixe:chaîne 
            (avec préfixe = tel, fax, mailto, http, ftp ou mllp) 
            ou est vide et sans nullFlavor, ou contient un nullFlavor non admis.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@use = 'H'                      or @use = 'HP'                      or @use = 'HV'                      or @use = 'WP'                     or @use = 'DIR'                      or @use = 'PUB'                      or @use = 'EC'                      or @use = 'MC'                      or @use = 'PG'                      or not(@use)">(CI-SISTelecom): Erreur de conformité CI-SIS : L'attribut use de l'élément telecom n'est pas conforme. 
            Il est facultatif et les valeurs permises sont 'H','HP', 'HV','WP','DIR','PUB','EC','MC','PG'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:assignedPerson
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:assignedPerson" id="d510396e184-false-d512260e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:name)&gt;=1">(CI-SISComponentOf): element hl7:name is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:name)&lt;=1">(CI-SISComponentOf): element hl7:name appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:assignedPerson/hl7:name
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:assignedPerson/hl7:name" id="d510396e185-false-d512276e0">
        <extends rule="PN"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='PN' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISComponentOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:PN", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:family)&gt;=1">(CI-SISComponentOf): element hl7:family is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:family)&lt;=1">(CI-SISComponentOf): element hl7:family appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:given)&lt;=1">(CI-SISComponentOf): element hl7:given appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:prefix)&lt;=1">(CI-SISComponentOf): element hl7:prefix appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:assignedPerson/hl7:name/hl7:family
Item: (CI-SISComponentOf)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:assignedPerson/hl7:name/hl7:given
Item: (CI-SISComponentOf)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:assignedPerson/hl7:name/hl7:prefix
Item: (CI-SISComponentOf)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization" id="d510396e189-false-d512337e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:id[@root='1.2.250.1.71.4.2.2'])&lt;=1">(CI-SISComponentOf): element hl7:id[@root='1.2.250.1.71.4.2.2'] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:name)&lt;=1">(CI-SISComponentOf): element hl7:name appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:standardIndustryClassCode)&lt;=1">(CI-SISComponentOf): element hl7:standardIndustryClassCode appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:id[@root='1.2.250.1.71.4.2.2']
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:id[@root='1.2.250.1.71.4.2.2']" id="d510396e190-false-d512384e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISComponentOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="@extension">(CI-SISComponentOf): attribute @extension SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="not(@extension) or string-length(@extension)&gt;0">(CI-SISComponentOf): Attribute @extension SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="string(@root)=('1.2.250.1.71.4.2.2')">(CI-SISComponentOf): The value for @root SHALL be '1.2.250.1.71.4.2.2'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:name
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:name" id="d510396e198-false-d512405e0">
        <extends rule="ON"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ON' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISComponentOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ON", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization
Item: (CI-SISAddr)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr" id="d512406e70-false-d512417e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@hl7:use) or (string-length(@hl7:use)&gt;0 and not(matches(@hl7:use,'\s')))">(CI-SISAddr): Attribute @hl7:use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:country)&lt;=1">(CI-SISAddr): element hl7:country appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:state)&lt;=1">(CI-SISAddr): element hl7:state appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:city)&lt;=1">(CI-SISAddr): element hl7:city appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postalCode)&lt;=1">(CI-SISAddr): element hl7:postalCode appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumber)&lt;=1">(CI-SISAddr): element hl7:houseNumber appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumberNumeric)&lt;=1">(CI-SISAddr): element hl7:houseNumberNumeric appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetName)&lt;=1">(CI-SISAddr): element hl7:streetName appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetNameType)&lt;=1">(CI-SISAddr): element hl7:streetNameType appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:additionalLocator)&lt;=1">(CI-SISAddr): element hl7:additionalLocator appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:unitID)&lt;=1">(CI-SISAddr): element hl7:unitID appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postBox)&lt;=1">(CI-SISAddr): element hl7:postBox appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:precinct)&lt;=1">(CI-SISAddr): element hl7:precinct appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:country
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:country" id="d512406e88-false-d512515e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:state
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:state" id="d512406e92-false-d512525e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:city
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:city" id="d512406e96-false-d512535e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:postalCode
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:postalCode" id="d512406e104-false-d512545e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:houseNumber
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:houseNumber" id="d512406e112-false-d512555e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:houseNumberNumeric
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:houseNumberNumeric" id="d512406e116-false-d512565e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetName
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetName" id="d512406e120-false-d512575e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetNameType
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetNameType" id="d512406e124-false-d512585e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:additionalLocator
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:additionalLocator" id="d512406e169-false-d512595e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:unitID
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:unitID" id="d512406e184-false-d512605e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:postBox
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:postBox" id="d512406e194-false-d512615e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:precinct
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:precinct" id="d512406e198-false-d512625e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr" id="d512406e202-false-d512635e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISAddr): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine" id="d512406e220-false-d512691e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine" id="d512406e226-false-d512701e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine" id="d512406e230-false-d512711e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine" id="d512406e234-false-d512721e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine" id="d512406e238-false-d512731e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine" id="d512406e242-false-d512741e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.19
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:telecom
Item: (CI-SISTelecom)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:telecom" id="d512742e27-false-d512752e0">
        <extends rule="TEL"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TEL' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISTelecom): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TEL", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISTelecom): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@value">(CI-SISTelecom): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@value) or string(@value castable as xs:anyURI)">(CI-SISTelecom): Attribute @value SHALL be of data type 'url'<value-of select="@value"/>
        </assert>
        <let name="prefix" value="substring-before(@value, ':')"/>
        <let name="suffix" value="substring-after(@value, ':')"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(             (count(@*) = 1 and name(@*) = 'nullFlavor' and             (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or             ($suffix and (             $prefix = 'tel' or              $prefix = 'fax' or              $prefix = 'mailto' or              $prefix = 'http' or              $prefix = 'ftp' or              $prefix = 'mllp'))             )">(CI-SISTelecom): Erreur de conformité CI-SIS : <name/> n'est pas conforme à une adresse de télécommunication préfixe:chaîne 
            (avec préfixe = tel, fax, mailto, http, ftp ou mllp) 
            ou est vide et sans nullFlavor, ou contient un nullFlavor non admis.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@use = 'H'                      or @use = 'HP'                      or @use = 'HV'                      or @use = 'WP'                     or @use = 'DIR'                      or @use = 'PUB'                      or @use = 'EC'                      or @use = 'MC'                      or @use = 'PG'                      or not(@use)">(CI-SISTelecom): Erreur de conformité CI-SIS : L'attribut use de l'élément telecom n'est pas conforme. 
            Il est facultatif et les valeurs permises sont 'H','HP', 'HV','WP','DIR','PUB','EC','MC','PG'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:standardIndustryClassCode
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:standardIndustryClassCode" id="d510396e201-false-d512773e0">
        <extends rule="CE"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CE' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISComponentOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CE", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="@displayName">(CI-SISComponentOf): attribute @displayName SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="not(@displayName) or string-length(@displayName)&gt;0">(CI-SISComponentOf): Attribute @displayName SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="@codeSystem">(CI-SISComponentOf): attribute @codeSystem SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="not(@codeSystem) or matches(@codeSystem,'^[0-2](\.(0|[1-9]\d*))*$')">(CI-SISComponentOf): Attribute @codeSystem SHALL be of data type 'oid'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="@code">(CI-SISComponentOf): attribute @code SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="not(@code) or (string-length(@code)&gt;0 and not(matches(@code,'\s')))">(CI-SISComponentOf): Attribute @code SHALL be of data type 'cs'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]" id="d510396e214-false-d512810e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]])&gt;=1">(CI-SISComponentOf): element hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]])&lt;=1">(CI-SISComponentOf): element hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]" id="d510396e220-false-d512838e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)])&gt;=1">(CI-SISComponentOf): element hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)])&lt;=1">(CI-SISComponentOf): element hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:location)&gt;=1">(CI-SISComponentOf): element hl7:location is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:location)&lt;=1">(CI-SISComponentOf): element hl7:location appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]" id="d510396e226-false-d512873e0">
        <extends rule="CE"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CE' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISComponentOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CE", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <let name="theCode" value="@code"/>
        <let name="theCodeSystem" value="@codeSystem"/>
        <let name="theCodeSystemVersion" value="@codeSystemVersion"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="@nullFlavor or exists(doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1][conceptList/concept[@code = $theCode][@codeSystem = $theCodeSystem][not(@codeSystemVersion) or @codeSystemVersion=$theCodeSystemVersion] or completeCodeSystem[@codeSystem=$theCodeSystem][not(@codeSystemVersion) or @codeSystemVersion=$theCodeSystemVersion]])">(CI-SISComponentOf): The element value SHALL be one of '1.2.250.1.213.1.1.5.3 J02-HealthcareFacilityTypeCode (2018-01-05T00:00:00)'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location" id="d510396e236-false-d512894e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:name)&lt;=1">(CI-SISComponentOf): element hl7:name appears too often [max 1x].</assert>
        <let name="elmcount" value="count(hl7:addr|hl7:addr)"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="$elmcount&lt;=1">(CI-SISComponentOf): choice (hl7:addr or hl7:addr) contains too many elements [max 1x]</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:name
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:name" id="d510396e242-false-d512928e0">
        <extends rule="EN"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='EN' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISComponentOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:EN", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location
Item: (CI-SISAddr)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr" id="d512929e70-false-d512940e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@hl7:use) or (string-length(@hl7:use)&gt;0 and not(matches(@hl7:use,'\s')))">(CI-SISAddr): Attribute @hl7:use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:country)&lt;=1">(CI-SISAddr): element hl7:country appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:state)&lt;=1">(CI-SISAddr): element hl7:state appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:city)&lt;=1">(CI-SISAddr): element hl7:city appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postalCode)&lt;=1">(CI-SISAddr): element hl7:postalCode appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumber)&lt;=1">(CI-SISAddr): element hl7:houseNumber appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumberNumeric)&lt;=1">(CI-SISAddr): element hl7:houseNumberNumeric appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetName)&lt;=1">(CI-SISAddr): element hl7:streetName appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetNameType)&lt;=1">(CI-SISAddr): element hl7:streetNameType appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:additionalLocator)&lt;=1">(CI-SISAddr): element hl7:additionalLocator appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:unitID)&lt;=1">(CI-SISAddr): element hl7:unitID appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postBox)&lt;=1">(CI-SISAddr): element hl7:postBox appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:precinct)&lt;=1">(CI-SISAddr): element hl7:precinct appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:country
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:country" id="d512929e88-false-d513038e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:state
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:state" id="d512929e92-false-d513048e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:city
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:city" id="d512929e96-false-d513058e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:postalCode
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:postalCode" id="d512929e104-false-d513068e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:houseNumber
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:houseNumber" id="d512929e112-false-d513078e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:houseNumberNumeric
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:houseNumberNumeric" id="d512929e116-false-d513088e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:streetName
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:streetName" id="d512929e120-false-d513098e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:streetNameType
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:streetNameType" id="d512929e124-false-d513108e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:additionalLocator
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:additionalLocator" id="d512929e169-false-d513118e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:unitID
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:unitID" id="d512929e184-false-d513128e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:postBox
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:postBox" id="d512929e194-false-d513138e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:precinct
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:precinct" id="d512929e198-false-d513148e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr" id="d512929e202-false-d513158e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISAddr): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:streetAddressLine" id="d512929e220-false-d513214e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:streetAddressLine" id="d512929e226-false-d513224e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:streetAddressLine" id="d512929e230-false-d513234e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:streetAddressLine" id="d512929e234-false-d513244e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:streetAddressLine" id="d512929e238-false-d513254e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:streetAddressLine" id="d512929e242-false-d513264e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.3
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:relatedDocument[@typeCode='RPLC']
Item: (CI-SISRelatedDocument)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:relatedDocument[@typeCode='RPLC']" id="d513265e48-false-d513275e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.3" test="string(@typeCode)=('RPLC')">(CI-SISRelatedDocument): The value for @typeCode SHALL be 'RPLC'.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.3" test="count(hl7:parentDocument[not(@nullFlavor)])&gt;=1">(CI-SISRelatedDocument): element hl7:parentDocument[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.3" test="count(hl7:parentDocument[not(@nullFlavor)])&lt;=1">(CI-SISRelatedDocument): element hl7:parentDocument[not(@nullFlavor)] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.3
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:relatedDocument[@typeCode='RPLC']/hl7:parentDocument[not(@nullFlavor)]
Item: (CI-SISRelatedDocument)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:relatedDocument[@typeCode='RPLC']/hl7:parentDocument[not(@nullFlavor)]" id="d513265e50-false-d513295e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.3" test="count(hl7:id[not(@nullFlavor)])&gt;=1">(CI-SISRelatedDocument): element hl7:id[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.3" test="count(hl7:id[not(@nullFlavor)])&lt;=1">(CI-SISRelatedDocument): element hl7:id[not(@nullFlavor)] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.3
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:relatedDocument[@typeCode='RPLC']/hl7:parentDocument[not(@nullFlavor)]/hl7:id[not(@nullFlavor)]
Item: (CI-SISRelatedDocument)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:relatedDocument[@typeCode='RPLC']/hl7:parentDocument[not(@nullFlavor)]/hl7:id[not(@nullFlavor)]" id="d513265e51-false-d513311e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.3" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISRelatedDocument): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.3" test="not(@extension) or string-length(@extension)&gt;0">(CI-SISRelatedDocument): Attribute @extension SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.3" test="@root">(CI-SISRelatedDocument): attribute @root SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.3" test="not(@root) or matches(@root,'^[0-2](\.(0|[1-9]\d*))*$') or matches(@root,'^[A-Fa-f\d]{8}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{12}$') or matches(@root,'^[A-Za-z][A-Za-z\d\-]*$')">(CI-SISRelatedDocument): Attribute @root SHALL be of data type 'uid'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.4
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]
Item: (CI-SISDocumentationOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]" id="d513312e25-false-d513343e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:serviceEvent[not(@nullFlavor)])&gt;=1">(CI-SISDocumentationOf): element hl7:serviceEvent[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:serviceEvent[not(@nullFlavor)])&lt;=1">(CI-SISDocumentationOf): element hl7:serviceEvent[not(@nullFlavor)] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.4
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]
Item: (CI-SISDocumentationOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]" id="d513312e26-false-d513379e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:code)&lt;=1">(CI-SISDocumentationOf): element hl7:code appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:effectiveTime)&lt;=1">(CI-SISDocumentationOf): element hl7:effectiveTime appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:performer[@typeCode='PRF'][not(@nullFlavor)])&lt;=1">(CI-SISDocumentationOf): element hl7:performer[@typeCode='PRF'][not(@nullFlavor)] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.4
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:code
Item: (CI-SISDocumentationOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:code" id="d513312e34-false-d513416e0">
        <extends rule="CE"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CE' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISDocumentationOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CE", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="not(@displayName) or string-length(@displayName)&gt;0">(CI-SISDocumentationOf): Attribute @displayName SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="@codeSystem">(CI-SISDocumentationOf): attribute @codeSystem SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="not(@codeSystem) or matches(@codeSystem,'^[0-2](\.(0|[1-9]\d*))*$')">(CI-SISDocumentationOf): Attribute @codeSystem SHALL be of data type 'oid'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="@code">(CI-SISDocumentationOf): attribute @code SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="not(@code) or (string-length(@code)&gt;0 and not(matches(@code,'\s')))">(CI-SISDocumentationOf): Attribute @code SHALL be of data type 'cs'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.4
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:effectiveTime
Item: (CI-SISDocumentationOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:effectiveTime" id="d513312e58-false-d513444e0">
        <extends rule="IVL_TS"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='IVL_TS' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISDocumentationOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:IVL_TS", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:low)&gt;=1">(CI-SISDocumentationOf): element hl7:low is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:low)&lt;=1">(CI-SISDocumentationOf): element hl7:low appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:high)&lt;=1">(CI-SISDocumentationOf): element hl7:high appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.4
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:effectiveTime/hl7:low
Item: (CI-SISDocumentationOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:effectiveTime/hl7:low" id="d513312e62-false-d513471e0">
        <extends rule="IVXB_TS"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='IVXB_TS' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISDocumentationOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:IVXB_TS", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="@value">(CI-SISDocumentationOf): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="not(@value) or matches(string(@value), '^[0-9]{4,14}')">(CI-SISDocumentationOf): Attribute @value SHALL be of data type 'ts'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.4
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:effectiveTime/hl7:high
Item: (CI-SISDocumentationOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:effectiveTime/hl7:high" id="d513312e70-false-d513488e0">
        <extends rule="IVXB_TS"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='IVXB_TS' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISDocumentationOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:IVXB_TS", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="@value">(CI-SISDocumentationOf): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="not(@value) or matches(string(@value), '^[0-9]{4,14}')">(CI-SISDocumentationOf): Attribute @value SHALL be of data type 'ts'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.4
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]
Item: (CI-SISDocumentationOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]" id="d513312e78-false-d513515e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="string(@typeCode)=('PRF')">(CI-SISDocumentationOf): The value for @typeCode SHALL be 'PRF'.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:assignedEntity)&gt;=1">(CI-SISDocumentationOf): element hl7:assignedEntity is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:assignedEntity)&lt;=1">(CI-SISDocumentationOf): element hl7:assignedEntity appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.4
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity
Item: (CI-SISDocumentationOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity" id="d513312e117-false-d513555e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:id)&gt;=1">(CI-SISDocumentationOf): element hl7:id is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:id)&lt;=1">(CI-SISDocumentationOf): element hl7:id appears too often [max 1x].</assert>
        <let name="elmcount" value="count(hl7:addr|hl7:addr)"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:assignedPerson)&lt;=1">(CI-SISDocumentationOf): element hl7:assignedPerson appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:representedOrganization)&gt;=1">(CI-SISDocumentationOf): element hl7:representedOrganization is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:representedOrganization)&lt;=1">(CI-SISDocumentationOf): element hl7:representedOrganization appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.4
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:id
Item: (CI-SISDocumentationOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:id" id="d513312e130-false-d513622e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISDocumentationOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="@extension">(CI-SISDocumentationOf): attribute @extension SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="not(@extension) or string-length(@extension)&gt;0">(CI-SISDocumentationOf): Attribute @extension SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="@root">(CI-SISDocumentationOf): attribute @root SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="not(@root) or matches(@root,'^[0-2](\.(0|[1-9]\d*))*$') or matches(@root,'^[A-Fa-f\d]{8}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{12}$') or matches(@root,'^[A-Za-z][A-Za-z\d\-]*$')">(CI-SISDocumentationOf): Attribute @root SHALL be of data type 'uid'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.19
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:telecom
Item: (CI-SISTelecom)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:telecom" id="d513623e35-false-d513647e0">
        <extends rule="TEL"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TEL' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISTelecom): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TEL", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISTelecom): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@value">(CI-SISTelecom): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@value) or string(@value castable as xs:anyURI)">(CI-SISTelecom): Attribute @value SHALL be of data type 'url'<value-of select="@value"/>
        </assert>
        <let name="prefix" value="substring-before(@value, ':')"/>
        <let name="suffix" value="substring-after(@value, ':')"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(             (count(@*) = 1 and name(@*) = 'nullFlavor' and             (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or             ($suffix and (             $prefix = 'tel' or              $prefix = 'fax' or              $prefix = 'mailto' or              $prefix = 'http' or              $prefix = 'ftp' or              $prefix = 'mllp'))             )">(CI-SISTelecom): Erreur de conformité CI-SIS : <name/> n'est pas conforme à une adresse de télécommunication préfixe:chaîne 
            (avec préfixe = tel, fax, mailto, http, ftp ou mllp) 
            ou est vide et sans nullFlavor, ou contient un nullFlavor non admis.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@use = 'H'                      or @use = 'HP'                      or @use = 'HV'                      or @use = 'WP'                     or @use = 'DIR'                      or @use = 'PUB'                      or @use = 'EC'                      or @use = 'MC'                      or @use = 'PG'                      or not(@use)">(CI-SISTelecom): Erreur de conformité CI-SIS : L'attribut use de l'élément telecom n'est pas conforme. 
            Il est facultatif et les valeurs permises sont 'H','HP', 'HV','WP','DIR','PUB','EC','MC','PG'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity
Item: (CI-SISAddr)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr" id="d513648e85-false-d513670e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@hl7:use) or (string-length(@hl7:use)&gt;0 and not(matches(@hl7:use,'\s')))">(CI-SISAddr): Attribute @hl7:use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:country)&lt;=1">(CI-SISAddr): element hl7:country appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:state)&lt;=1">(CI-SISAddr): element hl7:state appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:city)&lt;=1">(CI-SISAddr): element hl7:city appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postalCode)&lt;=1">(CI-SISAddr): element hl7:postalCode appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumber)&lt;=1">(CI-SISAddr): element hl7:houseNumber appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumberNumeric)&lt;=1">(CI-SISAddr): element hl7:houseNumberNumeric appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetName)&lt;=1">(CI-SISAddr): element hl7:streetName appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetNameType)&lt;=1">(CI-SISAddr): element hl7:streetNameType appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:additionalLocator)&lt;=1">(CI-SISAddr): element hl7:additionalLocator appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:unitID)&lt;=1">(CI-SISAddr): element hl7:unitID appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postBox)&lt;=1">(CI-SISAddr): element hl7:postBox appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:precinct)&lt;=1">(CI-SISAddr): element hl7:precinct appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:country
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:country" id="d513648e103-false-d513768e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:state
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:state" id="d513648e107-false-d513778e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:city
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:city" id="d513648e111-false-d513788e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:postalCode
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:postalCode" id="d513648e119-false-d513798e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:houseNumber
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:houseNumber" id="d513648e127-false-d513808e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:houseNumberNumeric
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:houseNumberNumeric" id="d513648e131-false-d513818e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:streetName
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:streetName" id="d513648e135-false-d513828e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:streetNameType
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:streetNameType" id="d513648e139-false-d513838e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:additionalLocator
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:additionalLocator" id="d513648e184-false-d513848e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:unitID
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:unitID" id="d513648e199-false-d513858e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:postBox
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:postBox" id="d513648e209-false-d513868e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:precinct
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:precinct" id="d513648e213-false-d513878e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr" id="d513648e217-false-d513888e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISAddr): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine" id="d513648e235-false-d513944e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine" id="d513648e241-false-d513954e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine" id="d513648e245-false-d513964e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine" id="d513648e249-false-d513974e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine" id="d513648e253-false-d513984e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine" id="d513648e257-false-d513994e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.4
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:assignedPerson
Item: (CI-SISDocumentationOf)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.4
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:assignedPerson/hl7:name
Item: (CI-SISDocumentationOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:assignedPerson/hl7:name" id="d513312e136-false-d514014e0">
        <extends rule="PN"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='PN' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISDocumentationOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:PN", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:family)&gt;=1">(CI-SISDocumentationOf): element hl7:family is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:family)&lt;=1">(CI-SISDocumentationOf): element hl7:family appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:given)&lt;=1">(CI-SISDocumentationOf): element hl7:given appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:prefix)&lt;=1">(CI-SISDocumentationOf): element hl7:prefix appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.4
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:assignedPerson/hl7:name/hl7:family
Item: (CI-SISDocumentationOf)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.4
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:assignedPerson/hl7:name/hl7:given
Item: (CI-SISDocumentationOf)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.4
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:assignedPerson/hl7:name/hl7:prefix
Item: (CI-SISDocumentationOf)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.4
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization
Item: (CI-SISDocumentationOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization" id="d513312e143-false-d514074e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:id)&lt;=1">(CI-SISDocumentationOf): element hl7:id appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:name)&lt;=1">(CI-SISDocumentationOf): element hl7:name appears too often [max 1x].</assert>
        <let name="elmcount" value="count(hl7:addr|hl7:addr)"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:standardIndustryClassCode[not(@nullFlavor)])&gt;=1">(CI-SISDocumentationOf): element hl7:standardIndustryClassCode[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:standardIndustryClassCode[not(@nullFlavor)])&lt;=1">(CI-SISDocumentationOf): element hl7:standardIndustryClassCode[not(@nullFlavor)] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.4
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:id
Item: (CI-SISDocumentationOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:id" id="d513312e147-false-d514133e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISDocumentationOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.4
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:name
Item: (CI-SISDocumentationOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:name" id="d513312e148-false-d514143e0">
        <extends rule="ON"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ON' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISDocumentationOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ON", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.19
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:telecom
Item: (CI-SISTelecom)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:telecom" id="d514144e27-false-d514154e0">
        <extends rule="TEL"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TEL' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISTelecom): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TEL", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISTelecom): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@value">(CI-SISTelecom): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@value) or string(@value castable as xs:anyURI)">(CI-SISTelecom): Attribute @value SHALL be of data type 'url'<value-of select="@value"/>
        </assert>
        <let name="prefix" value="substring-before(@value, ':')"/>
        <let name="suffix" value="substring-after(@value, ':')"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(             (count(@*) = 1 and name(@*) = 'nullFlavor' and             (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or             ($suffix and (             $prefix = 'tel' or              $prefix = 'fax' or              $prefix = 'mailto' or              $prefix = 'http' or              $prefix = 'ftp' or              $prefix = 'mllp'))             )">(CI-SISTelecom): Erreur de conformité CI-SIS : <name/> n'est pas conforme à une adresse de télécommunication préfixe:chaîne 
            (avec préfixe = tel, fax, mailto, http, ftp ou mllp) 
            ou est vide et sans nullFlavor, ou contient un nullFlavor non admis.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@use = 'H'                      or @use = 'HP'                      or @use = 'HV'                      or @use = 'WP'                     or @use = 'DIR'                      or @use = 'PUB'                      or @use = 'EC'                      or @use = 'MC'                      or @use = 'PG'                      or not(@use)">(CI-SISTelecom): Erreur de conformité CI-SIS : L'attribut use de l'élément telecom n'est pas conforme. 
            Il est facultatif et les valeurs permises sont 'H','HP', 'HV','WP','DIR','PUB','EC','MC','PG'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization
Item: (CI-SISAddr)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr" id="d514155e85-false-d514177e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@hl7:use) or (string-length(@hl7:use)&gt;0 and not(matches(@hl7:use,'\s')))">(CI-SISAddr): Attribute @hl7:use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:country)&lt;=1">(CI-SISAddr): element hl7:country appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:state)&lt;=1">(CI-SISAddr): element hl7:state appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:city)&lt;=1">(CI-SISAddr): element hl7:city appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postalCode)&lt;=1">(CI-SISAddr): element hl7:postalCode appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumber)&lt;=1">(CI-SISAddr): element hl7:houseNumber appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumberNumeric)&lt;=1">(CI-SISAddr): element hl7:houseNumberNumeric appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetName)&lt;=1">(CI-SISAddr): element hl7:streetName appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetNameType)&lt;=1">(CI-SISAddr): element hl7:streetNameType appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:additionalLocator)&lt;=1">(CI-SISAddr): element hl7:additionalLocator appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:unitID)&lt;=1">(CI-SISAddr): element hl7:unitID appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postBox)&lt;=1">(CI-SISAddr): element hl7:postBox appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:precinct)&lt;=1">(CI-SISAddr): element hl7:precinct appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:country
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:country" id="d514155e103-false-d514275e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:state
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:state" id="d514155e107-false-d514285e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:city
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:city" id="d514155e111-false-d514295e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:postalCode
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:postalCode" id="d514155e119-false-d514305e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:houseNumber
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:houseNumber" id="d514155e127-false-d514315e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:houseNumberNumeric
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:houseNumberNumeric" id="d514155e131-false-d514325e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetName
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetName" id="d514155e135-false-d514335e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetNameType
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetNameType" id="d514155e139-false-d514345e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:additionalLocator
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:additionalLocator" id="d514155e184-false-d514355e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:unitID
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:unitID" id="d514155e199-false-d514365e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:postBox
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:postBox" id="d514155e209-false-d514375e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:precinct
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:precinct" id="d514155e213-false-d514385e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr" id="d514155e217-false-d514395e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISAddr): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine" id="d514155e235-false-d514451e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine" id="d514155e241-false-d514461e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine" id="d514155e245-false-d514471e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine" id="d514155e249-false-d514481e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine" id="d514155e253-false-d514491e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine" id="d514155e257-false-d514501e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.4
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:standardIndustryClassCode[not(@nullFlavor)]
Item: (CI-SISDocumentationOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:standardIndustryClassCode[not(@nullFlavor)]" id="d513312e151-false-d514511e0">
        <extends rule="CE"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CE' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISDocumentationOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CE", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="@displayName">(CI-SISDocumentationOf): attribute @displayName SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="not(@displayName) or string-length(@displayName)&gt;0">(CI-SISDocumentationOf): Attribute @displayName SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="@codeSystem">(CI-SISDocumentationOf): attribute @codeSystem SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="not(@codeSystem) or matches(@codeSystem,'^[0-2](\.(0|[1-9]\d*))*$')">(CI-SISDocumentationOf): Attribute @codeSystem SHALL be of data type 'oid'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="@code">(CI-SISDocumentationOf): attribute @code SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="not(@code) or (string-length(@code)&gt;0 and not(matches(@code,'\s')))">(CI-SISDocumentationOf): Attribute @code SHALL be of data type 'cs'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.5
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:custodian[not(@nullFlavor)]
Item: (CI-SISCustodian)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:custodian[not(@nullFlavor)]" id="d514512e29-false-d514548e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.5" test="count(hl7:assignedCustodian)&gt;=1">(CI-SISCustodian): element hl7:assignedCustodian is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.5" test="count(hl7:assignedCustodian)&lt;=1">(CI-SISCustodian): element hl7:assignedCustodian appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.5
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian
Item: (CI-SISCustodian)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian" id="d514512e35-false-d514574e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.5" test="count(hl7:representedCustodianOrganization)&gt;=1">(CI-SISCustodian): element hl7:representedCustodianOrganization is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.5" test="count(hl7:representedCustodianOrganization)&lt;=1">(CI-SISCustodian): element hl7:representedCustodianOrganization appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.5
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization
Item: (CI-SISCustodian)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization" id="d514512e41-false-d514600e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.5" test="count(hl7:id[not(@nullFlavor)])&gt;=1">(CI-SISCustodian): element hl7:id[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.5" test="count(hl7:id[not(@nullFlavor)])&lt;=1">(CI-SISCustodian): element hl7:id[not(@nullFlavor)] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.5" test="count(hl7:name)&lt;=1">(CI-SISCustodian): element hl7:name appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.5" test="count(hl7:telecom)&lt;=1">(CI-SISCustodian): element hl7:telecom appears too often [max 1x].</assert>
        <let name="elmcount" value="count(hl7:addr|hl7:addr)"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.5" test="$elmcount&lt;=1">(CI-SISCustodian): choice (hl7:addr or hl7:addr) contains too many elements [max 1x]</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.5
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:id[not(@nullFlavor)]
Item: (CI-SISCustodian)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:id[not(@nullFlavor)]" id="d514512e92-false-d514658e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.5" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISCustodian): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.5" test="not(@extension) or string-length(@extension)&gt;0">(CI-SISCustodian): Attribute @extension SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.5" test="@root">(CI-SISCustodian): attribute @root SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.5" test="not(@root) or matches(@root,'^[0-2](\.(0|[1-9]\d*))*$') or matches(@root,'^[A-Fa-f\d]{8}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{12}$') or matches(@root,'^[A-Za-z][A-Za-z\d\-]*$')">(CI-SISCustodian): Attribute @root SHALL be of data type 'uid'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.5" test="hl7:custodian/hl7:representedCustodianOrganization/hl7:id/@root='1.2.250.1.71.4.2.2' or hl7:custodian/hl7:representedCustodianOrganization/hl7:id/@root='1.2.250.1.213.4.1'">(CI-SISCustodian): L'attribut @root doit avoir la valeur :
- soit "1.2.250.1.71.4.2.2" pour l'OID des structures de santé
- soit "1.2.250.1.213.4.1" pour l'OID de l'organisation hébergeant les documents produits par le patient</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.5
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:name
Item: (CI-SISCustodian)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:name" id="d514512e110-false-d514679e0">
        <extends rule="ON"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.5" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ON' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISCustodian): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ON", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.19
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:telecom
Item: (CI-SISTelecom)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:telecom" id="d514680e27-false-d514690e0">
        <extends rule="TEL"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TEL' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISTelecom): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TEL", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISTelecom): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@value">(CI-SISTelecom): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@value) or string(@value castable as xs:anyURI)">(CI-SISTelecom): Attribute @value SHALL be of data type 'url'<value-of select="@value"/>
        </assert>
        <let name="prefix" value="substring-before(@value, ':')"/>
        <let name="suffix" value="substring-after(@value, ':')"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(             (count(@*) = 1 and name(@*) = 'nullFlavor' and             (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or             ($suffix and (             $prefix = 'tel' or              $prefix = 'fax' or              $prefix = 'mailto' or              $prefix = 'http' or              $prefix = 'ftp' or              $prefix = 'mllp'))             )">(CI-SISTelecom): Erreur de conformité CI-SIS : <name/> n'est pas conforme à une adresse de télécommunication préfixe:chaîne 
            (avec préfixe = tel, fax, mailto, http, ftp ou mllp) 
            ou est vide et sans nullFlavor, ou contient un nullFlavor non admis.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@use = 'H'                      or @use = 'HP'                      or @use = 'HV'                      or @use = 'WP'                     or @use = 'DIR'                      or @use = 'PUB'                      or @use = 'EC'                      or @use = 'MC'                      or @use = 'PG'                      or not(@use)">(CI-SISTelecom): Erreur de conformité CI-SIS : L'attribut use de l'élément telecom n'est pas conforme. 
            Il est facultatif et les valeurs permises sont 'H','HP', 'HV','WP','DIR','PUB','EC','MC','PG'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization
Item: (CI-SISAddr)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr" id="d514691e85-false-d514713e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@hl7:use) or (string-length(@hl7:use)&gt;0 and not(matches(@hl7:use,'\s')))">(CI-SISAddr): Attribute @hl7:use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:country)&lt;=1">(CI-SISAddr): element hl7:country appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:state)&lt;=1">(CI-SISAddr): element hl7:state appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:city)&lt;=1">(CI-SISAddr): element hl7:city appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postalCode)&lt;=1">(CI-SISAddr): element hl7:postalCode appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumber)&lt;=1">(CI-SISAddr): element hl7:houseNumber appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumberNumeric)&lt;=1">(CI-SISAddr): element hl7:houseNumberNumeric appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetName)&lt;=1">(CI-SISAddr): element hl7:streetName appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetNameType)&lt;=1">(CI-SISAddr): element hl7:streetNameType appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:additionalLocator)&lt;=1">(CI-SISAddr): element hl7:additionalLocator appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:unitID)&lt;=1">(CI-SISAddr): element hl7:unitID appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postBox)&lt;=1">(CI-SISAddr): element hl7:postBox appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:precinct)&lt;=1">(CI-SISAddr): element hl7:precinct appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:country
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:country" id="d514691e103-false-d514811e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:state
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:state" id="d514691e107-false-d514821e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:city
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:city" id="d514691e111-false-d514831e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:postalCode
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:postalCode" id="d514691e119-false-d514841e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:houseNumber
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:houseNumber" id="d514691e127-false-d514851e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:houseNumberNumeric
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:houseNumberNumeric" id="d514691e131-false-d514861e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:streetName
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:streetName" id="d514691e135-false-d514871e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:streetNameType
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:streetNameType" id="d514691e139-false-d514881e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:additionalLocator
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:additionalLocator" id="d514691e184-false-d514891e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:unitID
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:unitID" id="d514691e199-false-d514901e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:postBox
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:postBox" id="d514691e209-false-d514911e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:precinct
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:precinct" id="d514691e213-false-d514921e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr" id="d514691e217-false-d514931e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISAddr): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:streetAddressLine" id="d514691e235-false-d514987e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:streetAddressLine" id="d514691e241-false-d514997e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:streetAddressLine" id="d514691e245-false-d515007e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:streetAddressLine" id="d514691e249-false-d515017e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:streetAddressLine" id="d514691e253-false-d515027e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:streetAddressLine" id="d514691e257-false-d515037e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.6
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]
Item: (CI-SISLegalAuthenticator)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]" id="d515038e25-false-d515059e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:time)&gt;=1">(CI-SISLegalAuthenticator): element hl7:time is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:time)&lt;=1">(CI-SISLegalAuthenticator): element hl7:time appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:signatureCode[@code='S' or @nullFlavor])&gt;=1">(CI-SISLegalAuthenticator): element hl7:signatureCode[@code='S' or @nullFlavor] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:signatureCode[@code='S' or @nullFlavor])&lt;=1">(CI-SISLegalAuthenticator): element hl7:signatureCode[@code='S' or @nullFlavor] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:assignedEntity)&gt;=1">(CI-SISLegalAuthenticator): element hl7:assignedEntity is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:assignedEntity)&lt;=1">(CI-SISLegalAuthenticator): element hl7:assignedEntity appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.6
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:time
Item: (CI-SISLegalAuthenticator)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:time" id="d515038e26-false-d515106e0">
        <extends rule="TS"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TS' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISLegalAuthenticator): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TS", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="not(*)">(CI-SISLegalAuthenticator): <value-of select="local-name()"/> with datatype TS, SHOULD NOT have child elements.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="@value">(CI-SISLegalAuthenticator): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="not(@value) or matches(string(@value), '^[0-9]{4,14}')">(CI-SISLegalAuthenticator): Attribute @value SHALL be of data type 'ts'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.6
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:signatureCode[@code='S' or @nullFlavor]
Item: (CI-SISLegalAuthenticator)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:signatureCode[@code='S' or @nullFlavor]" id="d515038e34-false-d515127e0">
        <extends rule="CS"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CS' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISLegalAuthenticator): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CS", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="@nullFlavor or (@code='S')">(CI-SISLegalAuthenticator): The element value SHALL be one of 'code 'S''.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.6
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity
Item: (CI-SISLegalAuthenticator)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity" id="d515038e41-false-d515153e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:id[not(@nullFlavor)])&gt;=1">(CI-SISLegalAuthenticator): element hl7:id[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:id[not(@nullFlavor)])&lt;=1">(CI-SISLegalAuthenticator): element hl7:id[not(@nullFlavor)] appears too often [max 1x].</assert>
        <let name="elmcount" value="count(hl7:addr|hl7:addr)"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:assignedPerson[not(@nullFlavor)])&gt;=1">(CI-SISLegalAuthenticator): element hl7:assignedPerson[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:assignedPerson[not(@nullFlavor)])&lt;=1">(CI-SISLegalAuthenticator): element hl7:assignedPerson[not(@nullFlavor)] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']])&lt;=1">(CI-SISLegalAuthenticator): element hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.6
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:id[not(@nullFlavor)]
Item: (CI-SISLegalAuthenticator)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:id[not(@nullFlavor)]" id="d515038e51-false-d515221e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISLegalAuthenticator): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="@root">(CI-SISLegalAuthenticator): attribute @root SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="not(@root) or matches(@root,'^[0-2](\.(0|[1-9]\d*))*$') or matches(@root,'^[A-Fa-f\d]{8}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{12}$') or matches(@root,'^[A-Za-z][A-Za-z\d\-]*$')">(CI-SISLegalAuthenticator): Attribute @root SHALL be of data type 'uid'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="@extension">(CI-SISLegalAuthenticator): attribute @extension SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="not(@extension) or string-length(@extension)&gt;0">(CI-SISLegalAuthenticator): Attribute @extension SHALL be of data type 'st'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity
Item: (CI-SISAddr)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr" id="d515222e78-false-d515247e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@hl7:use) or (string-length(@hl7:use)&gt;0 and not(matches(@hl7:use,'\s')))">(CI-SISAddr): Attribute @hl7:use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:country)&lt;=1">(CI-SISAddr): element hl7:country appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:state)&lt;=1">(CI-SISAddr): element hl7:state appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:city)&lt;=1">(CI-SISAddr): element hl7:city appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postalCode)&lt;=1">(CI-SISAddr): element hl7:postalCode appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumber)&lt;=1">(CI-SISAddr): element hl7:houseNumber appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumberNumeric)&lt;=1">(CI-SISAddr): element hl7:houseNumberNumeric appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetName)&lt;=1">(CI-SISAddr): element hl7:streetName appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetNameType)&lt;=1">(CI-SISAddr): element hl7:streetNameType appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:additionalLocator)&lt;=1">(CI-SISAddr): element hl7:additionalLocator appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:unitID)&lt;=1">(CI-SISAddr): element hl7:unitID appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postBox)&lt;=1">(CI-SISAddr): element hl7:postBox appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:precinct)&lt;=1">(CI-SISAddr): element hl7:precinct appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:country
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:country" id="d515222e96-false-d515345e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:state
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:state" id="d515222e100-false-d515355e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:city
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:city" id="d515222e104-false-d515365e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:postalCode
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:postalCode" id="d515222e112-false-d515375e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:houseNumber
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:houseNumber" id="d515222e120-false-d515385e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:houseNumberNumeric
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:houseNumberNumeric" id="d515222e124-false-d515395e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:streetName
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:streetName" id="d515222e128-false-d515405e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:streetNameType
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:streetNameType" id="d515222e132-false-d515415e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:additionalLocator
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:additionalLocator" id="d515222e177-false-d515425e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:unitID
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:unitID" id="d515222e192-false-d515435e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:postBox
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:postBox" id="d515222e202-false-d515445e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:precinct
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:precinct" id="d515222e206-false-d515455e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr" id="d515222e210-false-d515465e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISAddr): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine" id="d515222e228-false-d515521e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine" id="d515222e234-false-d515531e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine" id="d515222e238-false-d515541e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine" id="d515222e242-false-d515551e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine" id="d515222e246-false-d515561e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine" id="d515222e250-false-d515571e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.19
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:telecom
Item: (CI-SISTelecom)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:telecom" id="d515572e27-false-d515582e0">
        <extends rule="TEL"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TEL' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISTelecom): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TEL", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISTelecom): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@value">(CI-SISTelecom): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@value) or string(@value castable as xs:anyURI)">(CI-SISTelecom): Attribute @value SHALL be of data type 'url'<value-of select="@value"/>
        </assert>
        <let name="prefix" value="substring-before(@value, ':')"/>
        <let name="suffix" value="substring-after(@value, ':')"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(             (count(@*) = 1 and name(@*) = 'nullFlavor' and             (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or             ($suffix and (             $prefix = 'tel' or              $prefix = 'fax' or              $prefix = 'mailto' or              $prefix = 'http' or              $prefix = 'ftp' or              $prefix = 'mllp'))             )">(CI-SISTelecom): Erreur de conformité CI-SIS : <name/> n'est pas conforme à une adresse de télécommunication préfixe:chaîne 
            (avec préfixe = tel, fax, mailto, http, ftp ou mllp) 
            ou est vide et sans nullFlavor, ou contient un nullFlavor non admis.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@use = 'H'                      or @use = 'HP'                      or @use = 'HV'                      or @use = 'WP'                     or @use = 'DIR'                      or @use = 'PUB'                      or @use = 'EC'                      or @use = 'MC'                      or @use = 'PG'                      or not(@use)">(CI-SISTelecom): Erreur de conformité CI-SIS : L'attribut use de l'élément telecom n'est pas conforme. 
            Il est facultatif et les valeurs permises sont 'H','HP', 'HV','WP','DIR','PUB','EC','MC','PG'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.6
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:assignedPerson[not(@nullFlavor)]
Item: (CI-SISLegalAuthenticator)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:assignedPerson[not(@nullFlavor)]" id="d515038e73-false-d515603e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:name[not(@nullFlavor)])&gt;=1">(CI-SISLegalAuthenticator): element hl7:name[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:name[not(@nullFlavor)])&lt;=1">(CI-SISLegalAuthenticator): element hl7:name[not(@nullFlavor)] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.6
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:assignedPerson[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]
Item: (CI-SISLegalAuthenticator)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:assignedPerson[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]" id="d515038e80-false-d515619e0">
        <extends rule="PN"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='PN' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISLegalAuthenticator): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:PN", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:family)&gt;=1">(CI-SISLegalAuthenticator): element hl7:family is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:family)&lt;=1">(CI-SISLegalAuthenticator): element hl7:family appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:given)&lt;=1">(CI-SISLegalAuthenticator): element hl7:given appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:prefix)&lt;=1">(CI-SISLegalAuthenticator): element hl7:prefix appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.6
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:assignedPerson[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]/hl7:family
Item: (CI-SISLegalAuthenticator)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.6
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:assignedPerson[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]/hl7:given
Item: (CI-SISLegalAuthenticator)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.6
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:assignedPerson[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]/hl7:prefix
Item: (CI-SISLegalAuthenticator)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.6
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]
Item: (CI-SISLegalAuthenticator)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]" id="d515038e90-false-d515680e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:id[@root='1.2.250.1.71.4.2.2'])&gt;=1">(CI-SISLegalAuthenticator): element hl7:id[@root='1.2.250.1.71.4.2.2'] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:id[@root='1.2.250.1.71.4.2.2'])&lt;=1">(CI-SISLegalAuthenticator): element hl7:id[@root='1.2.250.1.71.4.2.2'] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:name)&lt;=1">(CI-SISLegalAuthenticator): element hl7:name appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:standardIndustryClassCode)&lt;=1">(CI-SISLegalAuthenticator): element hl7:standardIndustryClassCode appears too often [max 1x].</assert>
        <let name="elmcount" value="count(hl7:addr|hl7:addr)"/>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.6
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:id[@root='1.2.250.1.71.4.2.2']
Item: (CI-SISLegalAuthenticator)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:id[@root='1.2.250.1.71.4.2.2']" id="d515038e91-false-d515741e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISLegalAuthenticator): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="@extension">(CI-SISLegalAuthenticator): attribute @extension SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="not(@extension) or string-length(@extension)&gt;0">(CI-SISLegalAuthenticator): Attribute @extension SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="string(@root)=('1.2.250.1.71.4.2.2')">(CI-SISLegalAuthenticator): The value for @root SHALL be '1.2.250.1.71.4.2.2'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.6
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:name
Item: (CI-SISLegalAuthenticator)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:name" id="d515038e94-false-d515762e0">
        <extends rule="ON"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ON' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISLegalAuthenticator): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ON", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.6
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:standardIndustryClassCode
Item: (CI-SISLegalAuthenticator)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:standardIndustryClassCode" id="d515038e95-false-d515772e0">
        <extends rule="CE"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CE' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISLegalAuthenticator): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CE", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="@displayName">(CI-SISLegalAuthenticator): attribute @displayName SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="not(@displayName) or string-length(@displayName)&gt;0">(CI-SISLegalAuthenticator): Attribute @displayName SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="@codeSystem">(CI-SISLegalAuthenticator): attribute @codeSystem SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="not(@codeSystem) or matches(@codeSystem,'^[0-2](\.(0|[1-9]\d*))*$')">(CI-SISLegalAuthenticator): Attribute @codeSystem SHALL be of data type 'oid'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="@code">(CI-SISLegalAuthenticator): attribute @code SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="not(@code) or (string-length(@code)&gt;0 and not(matches(@code,'\s')))">(CI-SISLegalAuthenticator): Attribute @code SHALL be of data type 'cs'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.19
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:telecom
Item: (CI-SISTelecom)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:telecom" id="d515773e39-false-d515804e0">
        <extends rule="TEL"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TEL' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISTelecom): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TEL", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISTelecom): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@value">(CI-SISTelecom): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@value) or string(@value castable as xs:anyURI)">(CI-SISTelecom): Attribute @value SHALL be of data type 'url'<value-of select="@value"/>
        </assert>
        <let name="prefix" value="substring-before(@value, ':')"/>
        <let name="suffix" value="substring-after(@value, ':')"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(             (count(@*) = 1 and name(@*) = 'nullFlavor' and             (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or             ($suffix and (             $prefix = 'tel' or              $prefix = 'fax' or              $prefix = 'mailto' or              $prefix = 'http' or              $prefix = 'ftp' or              $prefix = 'mllp'))             )">(CI-SISTelecom): Erreur de conformité CI-SIS : <name/> n'est pas conforme à une adresse de télécommunication préfixe:chaîne 
            (avec préfixe = tel, fax, mailto, http, ftp ou mllp) 
            ou est vide et sans nullFlavor, ou contient un nullFlavor non admis.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@use = 'H'                      or @use = 'HP'                      or @use = 'HV'                      or @use = 'WP'                     or @use = 'DIR'                      or @use = 'PUB'                      or @use = 'EC'                      or @use = 'MC'                      or @use = 'PG'                      or not(@use)">(CI-SISTelecom): Erreur de conformité CI-SIS : L'attribut use de l'élément telecom n'est pas conforme. 
            Il est facultatif et les valeurs permises sont 'H','HP', 'HV','WP','DIR','PUB','EC','MC','PG'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]
Item: (CI-SISAddr)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr" id="d515805e85-false-d515827e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@hl7:use) or (string-length(@hl7:use)&gt;0 and not(matches(@hl7:use,'\s')))">(CI-SISAddr): Attribute @hl7:use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:country)&lt;=1">(CI-SISAddr): element hl7:country appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:state)&lt;=1">(CI-SISAddr): element hl7:state appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:city)&lt;=1">(CI-SISAddr): element hl7:city appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postalCode)&lt;=1">(CI-SISAddr): element hl7:postalCode appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumber)&lt;=1">(CI-SISAddr): element hl7:houseNumber appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumberNumeric)&lt;=1">(CI-SISAddr): element hl7:houseNumberNumeric appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetName)&lt;=1">(CI-SISAddr): element hl7:streetName appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetNameType)&lt;=1">(CI-SISAddr): element hl7:streetNameType appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:additionalLocator)&lt;=1">(CI-SISAddr): element hl7:additionalLocator appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:unitID)&lt;=1">(CI-SISAddr): element hl7:unitID appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postBox)&lt;=1">(CI-SISAddr): element hl7:postBox appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:precinct)&lt;=1">(CI-SISAddr): element hl7:precinct appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:country
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:country" id="d515805e103-false-d515925e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:state
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:state" id="d515805e107-false-d515935e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:city
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:city" id="d515805e111-false-d515945e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:postalCode
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:postalCode" id="d515805e119-false-d515955e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:houseNumber
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:houseNumber" id="d515805e127-false-d515965e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:houseNumberNumeric
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:houseNumberNumeric" id="d515805e131-false-d515975e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:streetName
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:streetName" id="d515805e135-false-d515985e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:streetNameType
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:streetNameType" id="d515805e139-false-d515995e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:additionalLocator
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:additionalLocator" id="d515805e184-false-d516005e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:unitID
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:unitID" id="d515805e199-false-d516015e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:postBox
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:postBox" id="d515805e209-false-d516025e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:precinct
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:precinct" id="d515805e213-false-d516035e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr" id="d515805e217-false-d516045e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISAddr): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:streetAddressLine" id="d515805e235-false-d516101e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:streetAddressLine" id="d515805e241-false-d516111e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:streetAddressLine" id="d515805e245-false-d516121e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:streetAddressLine" id="d515805e249-false-d516131e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:streetAddressLine" id="d515805e253-false-d516141e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:streetAddressLine" id="d515805e257-false-d516151e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.7
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]
Item: (CI-SISAuthor)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]" id="d516152e99-false-d516168e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:functionCode[(@code='ADMPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ATTPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='DISPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PRISURG' and @codeSystem='2.16.840.1.113883.5.88') or (@code='FASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='SASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='NASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANEST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANRS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='MDWF' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PCP' and @codeSystem='2.16.840.1.113883.5.88') or (@code='CORRE' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='REMPL' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='GYNEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='OPHTA' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PSYCH' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='CARDT' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PRELV' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='ASPEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='APSIN' and @codeSystem='1.2.250.1.213.1.1.4.2.280')])&lt;=1">(CI-SISAuthor): element hl7:functionCode[(@code='ADMPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ATTPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='DISPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PRISURG' and @codeSystem='2.16.840.1.113883.5.88') or (@code='FASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='SASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='NASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANEST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANRS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='MDWF' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PCP' and @codeSystem='2.16.840.1.113883.5.88') or (@code='CORRE' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='REMPL' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='GYNEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='OPHTA' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PSYCH' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='CARDT' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PRELV' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='ASPEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='APSIN' and @codeSystem='1.2.250.1.213.1.1.4.2.280')] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:time)&gt;=1">(CI-SISAuthor): element hl7:time is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:time)&lt;=1">(CI-SISAuthor): element hl7:time appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:assignedAuthor[not(@nullFlavor)])&gt;=1">(CI-SISAuthor): element hl7:assignedAuthor[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:assignedAuthor[not(@nullFlavor)])&lt;=1">(CI-SISAuthor): element hl7:assignedAuthor[not(@nullFlavor)] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.7
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:functionCode[(@code='ADMPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ATTPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='DISPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PRISURG' and @codeSystem='2.16.840.1.113883.5.88') or (@code='FASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='SASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='NASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANEST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANRS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='MDWF' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PCP' and @codeSystem='2.16.840.1.113883.5.88') or (@code='CORRE' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='REMPL' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='GYNEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='OPHTA' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PSYCH' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='CARDT' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PRELV' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='ASPEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='APSIN' and @codeSystem='1.2.250.1.213.1.1.4.2.280')]
Item: (CI-SISAuthor)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:functionCode[(@code='ADMPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ATTPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='DISPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PRISURG' and @codeSystem='2.16.840.1.113883.5.88') or (@code='FASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='SASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='NASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANEST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANRS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='MDWF' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PCP' and @codeSystem='2.16.840.1.113883.5.88') or (@code='CORRE' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='REMPL' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='GYNEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='OPHTA' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PSYCH' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='CARDT' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PRELV' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='ASPEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='APSIN' and @codeSystem='1.2.250.1.213.1.1.4.2.280')]" id="d516152e100-false-d516208e0">
        <extends rule="CE"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CE' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAuthor): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CE", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="@nullFlavor or (@code='ADMPHYS' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='médecin ayant admis le patient dans la structure de soins') or (@code='ATTPHYS' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='médecin responsable du patient dans la structure de soins') or (@code='DISPHYS' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='médecin ayant décidé la sortie du patient de la structure de soins') or (@code='PRISURG' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='médecin intervenant principal') or (@code='FASST' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='premier assistant lors de l’intervention') or (@code='SASST' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='second assistant lors de l’intervention') or (@code='NASST' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='infirmier(e)') or (@code='ANEST' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='médecin anesthésiste') or (@code='ANRS' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='infirmier(e) anesthésiste') or (@code='MDWF' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='sage-femme') or (@code='PCP' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='médecin traitant') or (@code='CORRE' and @codeSystem='1.2.250.1.213.1.1.4.2.280' and @displayName='médecin correspondant') or (@code='REMPL' and @codeSystem='1.2.250.1.213.1.1.4.2.280' and @displayName='médecin remplaçant du médecin traitant') or (@code='GYNEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280' and @displayName='gynécologue') or (@code='OPHTA' and @codeSystem='1.2.250.1.213.1.1.4.2.280' and @displayName='ophtalmologue') or (@code='PSYCH' and @codeSystem='1.2.250.1.213.1.1.4.2.280' and @displayName='psychiatre ou neuropsychiatre') or (@code='CARDT' and @codeSystem='1.2.250.1.213.1.1.4.2.280' and @displayName='cardiologue traitant') or (@code='PRELV' and @codeSystem='1.2.250.1.213.1.1.4.2.280' and @displayName='préleveur d’échantillons biologiques pour l’exécution d’une prescription d’examens biologiques') or (@code='ASPEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280' and @displayName='autre spécialiste') or (@code='APSIN' and @codeSystem='1.2.250.1.213.1.1.4.2.280' and @displayName='autre PS informateur')">(CI-SISAuthor): The element value SHALL be one of 'code 'ADMPHYS' codeSystem '2.16.840.1.113883.5.88' displayName='médecin ayant admis le patient dans la structure de soins' or code 'ATTPHYS' codeSystem '2.16.840.1.113883.5.88' displayName='médecin responsable du patient dans la structure de soins' or code 'DISPHYS' codeSystem '2.16.840.1.113883.5.88' displayName='médecin ayant décidé la sortie du patient de la structure de soins' or code 'PRISURG' codeSystem '2.16.840.1.113883.5.88' displayName='médecin intervenant principal' or code 'FASST' codeSystem '2.16.840.1.113883.5.88' displayName='premier assistant lors de l’intervention' or code 'SASST' codeSystem '2.16.840.1.113883.5.88' displayName='second assistant lors de l’intervention' or code 'NASST' codeSystem '2.16.840.1.113883.5.88' displayName='infirmier(e)' or code 'ANEST' codeSystem '2.16.840.1.113883.5.88' displayName='médecin anesthésiste' or code 'ANRS' codeSystem '2.16.840.1.113883.5.88' displayName='infirmier(e) anesthésiste' or code 'MDWF' codeSystem '2.16.840.1.113883.5.88' displayName='sage-femme' or code 'PCP' codeSystem '2.16.840.1.113883.5.88' displayName='médecin traitant' or code 'CORRE' codeSystem '1.2.250.1.213.1.1.4.2.280' displayName='médecin correspondant' or code 'REMPL' codeSystem '1.2.250.1.213.1.1.4.2.280' displayName='médecin remplaçant du médecin traitant' or code 'GYNEC' codeSystem '1.2.250.1.213.1.1.4.2.280' displayName='gynécologue' or code 'OPHTA' codeSystem '1.2.250.1.213.1.1.4.2.280' displayName='ophtalmologue' or code 'PSYCH' codeSystem '1.2.250.1.213.1.1.4.2.280' displayName='psychiatre ou neuropsychiatre' or code 'CARDT' codeSystem '1.2.250.1.213.1.1.4.2.280' displayName='cardiologue traitant' or code 'PRELV' codeSystem '1.2.250.1.213.1.1.4.2.280' displayName='préleveur d’échantillons biologiques pour l’exécution d’une prescription d’examens biologiques' or code 'ASPEC' codeSystem '1.2.250.1.213.1.1.4.2.280' displayName='autre spécialiste' or code 'APSIN' codeSystem '1.2.250.1.213.1.1.4.2.280' displayName='autre PS informateur''.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="not(@displayName) or string-length(@displayName)&gt;0">(CI-SISAuthor): Attribute @displayName SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="@codeSystem">(CI-SISAuthor): attribute @codeSystem SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="not(@codeSystem) or matches(@codeSystem,'^[0-2](\.(0|[1-9]\d*))*$')">(CI-SISAuthor): Attribute @codeSystem SHALL be of data type 'oid'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="@code">(CI-SISAuthor): attribute @code SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="not(@code) or (string-length(@code)&gt;0 and not(matches(@code,'\s')))">(CI-SISAuthor): Attribute @code SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:originalText)&lt;=1">(CI-SISAuthor): element hl7:originalText appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.7
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:functionCode[(@code='ADMPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ATTPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='DISPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PRISURG' and @codeSystem='2.16.840.1.113883.5.88') or (@code='FASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='SASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='NASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANEST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANRS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='MDWF' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PCP' and @codeSystem='2.16.840.1.113883.5.88') or (@code='CORRE' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='REMPL' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='GYNEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='OPHTA' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PSYCH' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='CARDT' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PRELV' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='ASPEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='APSIN' and @codeSystem='1.2.250.1.213.1.1.4.2.280')]/hl7:originalText
Item: (CI-SISAuthor)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:functionCode[(@code='ADMPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ATTPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='DISPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PRISURG' and @codeSystem='2.16.840.1.113883.5.88') or (@code='FASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='SASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='NASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANEST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANRS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='MDWF' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PCP' and @codeSystem='2.16.840.1.113883.5.88') or (@code='CORRE' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='REMPL' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='GYNEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='OPHTA' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PSYCH' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='CARDT' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PRELV' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='ASPEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='APSIN' and @codeSystem='1.2.250.1.213.1.1.4.2.280')]/hl7:originalText" id="d516152e180-false-d516306e0">
        <extends rule="ED"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ED' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAuthor): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ED", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.7
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:time
Item: (CI-SISAuthor)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:time" id="d516152e186-false-d516316e0">
        <extends rule="TS"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TS' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAuthor): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TS", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="not(*)">(CI-SISAuthor): <value-of select="local-name()"/> with datatype TS, SHOULD NOT have child elements.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="@value">(CI-SISAuthor): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="not(@value) or matches(string(@value), '^[0-9]{4,14}')">(CI-SISAuthor): Attribute @value SHALL be of data type 'ts'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.7
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]
Item: (CI-SISAuthor)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]" id="d516152e194-false-d516342e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:id)&gt;=1">(CI-SISAuthor): element hl7:id is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:id)&lt;=1">(CI-SISAuthor): element hl7:id appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.1-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem) or @nullFlavor])&lt;=1">(CI-SISAuthor): element hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.1-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem) or @nullFlavor] appears too often [max 1x].</assert>
        <let name="elmcount" value="count(hl7:addr|hl7:addr)"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:assignedPerson)&lt;=1">(CI-SISAuthor): element hl7:assignedPerson appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:assignedAuthoringDevice)&lt;=1">(CI-SISAuthor): element hl7:assignedAuthoringDevice appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:representedOrganization)&lt;=1">(CI-SISAuthor): element hl7:representedOrganization appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.7
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:id
Item: (CI-SISAuthor)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:id" id="d516152e205-false-d516416e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAuthor): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="@root">(CI-SISAuthor): attribute @root SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="not(@root) or matches(@root,'^[0-2](\.(0|[1-9]\d*))*$') or matches(@root,'^[A-Fa-f\d]{8}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{12}$') or matches(@root,'^[A-Za-z][A-Za-z\d\-]*$')">(CI-SISAuthor): Attribute @root SHALL be of data type 'uid'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="@extension">(CI-SISAuthor): attribute @extension SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="not(@extension) or string-length(@extension)&gt;0">(CI-SISAuthor): Attribute @extension SHALL be of data type 'st'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.7
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.1-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem) or @nullFlavor]
Item: (CI-SISAuthor)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.1-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem) or @nullFlavor]" id="d516152e223-false-d516443e0">
        <extends rule="CE"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CE' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAuthor): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CE", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <let name="theCode" value="@code"/>
        <let name="theCodeSystem" value="@codeSystem"/>
        <let name="theCodeSystemVersion" value="@codeSystemVersion"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="@nullFlavor or exists(doc('include/voc-1.2.250.1.213.1.1.5.1-DYNAMIC.xml')//valueSet[1][conceptList/concept[@code = $theCode][@codeSystem = $theCodeSystem][not(@codeSystemVersion) or @codeSystemVersion=$theCodeSystemVersion] or completeCodeSystem[@codeSystem=$theCodeSystem][not(@codeSystemVersion) or @codeSystemVersion=$theCodeSystemVersion]])">(CI-SISAuthor): The element value SHALL be one of '1.2.250.1.213.1.1.5.1 JDV_J01-XdsAuthorSpecialty-CISIS (DYNAMIC)'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]
Item: (CI-SISAddr)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr" id="d516444e75-false-d516466e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@hl7:use) or (string-length(@hl7:use)&gt;0 and not(matches(@hl7:use,'\s')))">(CI-SISAddr): Attribute @hl7:use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:country)&lt;=1">(CI-SISAddr): element hl7:country appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:state)&lt;=1">(CI-SISAddr): element hl7:state appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:city)&lt;=1">(CI-SISAddr): element hl7:city appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postalCode)&lt;=1">(CI-SISAddr): element hl7:postalCode appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumber)&lt;=1">(CI-SISAddr): element hl7:houseNumber appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumberNumeric)&lt;=1">(CI-SISAddr): element hl7:houseNumberNumeric appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetName)&lt;=1">(CI-SISAddr): element hl7:streetName appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetNameType)&lt;=1">(CI-SISAddr): element hl7:streetNameType appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:additionalLocator)&lt;=1">(CI-SISAddr): element hl7:additionalLocator appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:unitID)&lt;=1">(CI-SISAddr): element hl7:unitID appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postBox)&lt;=1">(CI-SISAddr): element hl7:postBox appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:precinct)&lt;=1">(CI-SISAddr): element hl7:precinct appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:country
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:country" id="d516444e93-false-d516564e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:state
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:state" id="d516444e97-false-d516574e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:city
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:city" id="d516444e101-false-d516584e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:postalCode
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:postalCode" id="d516444e109-false-d516594e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:houseNumber
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:houseNumber" id="d516444e117-false-d516604e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:houseNumberNumeric
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:houseNumberNumeric" id="d516444e121-false-d516614e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:streetName
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:streetName" id="d516444e125-false-d516624e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:streetNameType
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:streetNameType" id="d516444e129-false-d516634e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:additionalLocator
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:additionalLocator" id="d516444e174-false-d516644e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:unitID
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:unitID" id="d516444e189-false-d516654e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:postBox
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:postBox" id="d516444e199-false-d516664e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:precinct
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:precinct" id="d516444e203-false-d516674e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr" id="d516444e207-false-d516684e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISAddr): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:streetAddressLine" id="d516444e225-false-d516740e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:streetAddressLine" id="d516444e231-false-d516750e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:streetAddressLine" id="d516444e235-false-d516760e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:streetAddressLine" id="d516444e239-false-d516770e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:streetAddressLine" id="d516444e243-false-d516780e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:streetAddressLine" id="d516444e247-false-d516790e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.19
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:telecom
Item: (CI-SISTelecom)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:telecom" id="d516791e27-false-d516801e0">
        <extends rule="TEL"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TEL' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISTelecom): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TEL", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISTelecom): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@value">(CI-SISTelecom): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@value) or string(@value castable as xs:anyURI)">(CI-SISTelecom): Attribute @value SHALL be of data type 'url'<value-of select="@value"/>
        </assert>
        <let name="prefix" value="substring-before(@value, ':')"/>
        <let name="suffix" value="substring-after(@value, ':')"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(             (count(@*) = 1 and name(@*) = 'nullFlavor' and             (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or             ($suffix and (             $prefix = 'tel' or              $prefix = 'fax' or              $prefix = 'mailto' or              $prefix = 'http' or              $prefix = 'ftp' or              $prefix = 'mllp'))             )">(CI-SISTelecom): Erreur de conformité CI-SIS : <name/> n'est pas conforme à une adresse de télécommunication préfixe:chaîne 
            (avec préfixe = tel, fax, mailto, http, ftp ou mllp) 
            ou est vide et sans nullFlavor, ou contient un nullFlavor non admis.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@use = 'H'                      or @use = 'HP'                      or @use = 'HV'                      or @use = 'WP'                     or @use = 'DIR'                      or @use = 'PUB'                      or @use = 'EC'                      or @use = 'MC'                      or @use = 'PG'                      or not(@use)">(CI-SISTelecom): Erreur de conformité CI-SIS : L'attribut use de l'élément telecom n'est pas conforme. 
            Il est facultatif et les valeurs permises sont 'H','HP', 'HV','WP','DIR','PUB','EC','MC','PG'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.7
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:assignedPerson
Item: (CI-SISAuthor)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:assignedPerson" id="d516152e259-false-d516822e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:name)&gt;=1">(CI-SISAuthor): element hl7:name is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:name)&lt;=1">(CI-SISAuthor): element hl7:name appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.7
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:assignedPerson/hl7:name
Item: (CI-SISAuthor)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:assignedPerson/hl7:name" id="d516152e265-false-d516838e0">
        <extends rule="PN"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='PN' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAuthor): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:PN", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:family)&gt;=1">(CI-SISAuthor): element hl7:family is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:family)&lt;=1">(CI-SISAuthor): element hl7:family appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:given)&lt;=1">(CI-SISAuthor): element hl7:given appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:prefix[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.24-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem) or @nullFlavor])&lt;=1">(CI-SISAuthor): element hl7:prefix[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.24-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem) or @nullFlavor] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.7
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:assignedPerson/hl7:name/hl7:family
Item: (CI-SISAuthor)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.7
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:assignedPerson/hl7:name/hl7:given
Item: (CI-SISAuthor)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.7
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:assignedPerson/hl7:name/hl7:prefix[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.24-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem) or @nullFlavor]
Item: (CI-SISAuthor)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:assignedPerson/hl7:name/hl7:prefix[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.24-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem) or @nullFlavor]" id="d516152e295-false-d516892e0">
        <let name="theCode" value="@code"/>
        <let name="theCodeSystem" value="@codeSystem"/>
        <let name="theCodeSystemVersion" value="@codeSystemVersion"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="@nullFlavor or exists(doc('include/voc-1.2.250.1.213.1.1.5.24-2018-01-05T000000.xml')//valueSet[1][conceptList/concept[@code = $theCode][@codeSystem = $theCodeSystem][not(@codeSystemVersion) or @codeSystemVersion=$theCodeSystemVersion] or completeCodeSystem[@codeSystem=$theCodeSystem][not(@codeSystemVersion) or @codeSystemVersion=$theCodeSystemVersion]])">(CI-SISAuthor): The element value SHALL be one of '1.2.250.1.213.1.1.5.24 JDV_J12-CiviliteTitre (2018-01-05T00:00:00)'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.7
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:assignedAuthoringDevice
Item: (CI-SISAuthor)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:assignedAuthoringDevice" id="d516152e302-false-d516906e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:manufacturerModelName)&lt;=1">(CI-SISAuthor): element hl7:manufacturerModelName appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:softwareName)&lt;=1">(CI-SISAuthor): element hl7:softwareName appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.7
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:assignedAuthoringDevice/hl7:manufacturerModelName
Item: (CI-SISAuthor)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:assignedAuthoringDevice/hl7:manufacturerModelName" id="d516152e308-false-d516926e0">
        <extends rule="SC"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='SC' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAuthor): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:SC", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.7
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:assignedAuthoringDevice/hl7:softwareName
Item: (CI-SISAuthor)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:assignedAuthoringDevice/hl7:softwareName" id="d516152e314-false-d516936e0">
        <extends rule="SC"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='SC' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAuthor): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:SC", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.7
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:representedOrganization
Item: (CI-SISAuthor)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:representedOrganization" id="d516152e320-false-d516946e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:id[@root='1.2.250.1.71.4.2.2'])&lt;=1">(CI-SISAuthor): element hl7:id[@root='1.2.250.1.71.4.2.2'] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:name)&lt;=1">(CI-SISAuthor): element hl7:name appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.7
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:representedOrganization/hl7:id[@root='1.2.250.1.71.4.2.2']
Item: (CI-SISAuthor)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:representedOrganization/hl7:id[@root='1.2.250.1.71.4.2.2']" id="d516152e326-false-d516968e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAuthor): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="@extension">(CI-SISAuthor): attribute @extension SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="not(@extension) or string-length(@extension)&gt;0">(CI-SISAuthor): Attribute @extension SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="string(@root)=('1.2.250.1.71.4.2.2')">(CI-SISAuthor): The value for @root SHALL be '1.2.250.1.71.4.2.2'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.7
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:representedOrganization/hl7:name
Item: (CI-SISAuthor)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:representedOrganization/hl7:name" id="d516152e339-false-d516989e0">
        <extends rule="ON"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ON' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAuthor): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ON", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.12
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant
Item: (CI-SISInformant)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant" id="d516990e29-false-d517030e0">
        <let name="elmcount" value="count(hl7:assignedEntity|hl7:relatedEntity)"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.12" test="$elmcount&gt;=1">(CI-SISInformant): choice (hl7:assignedEntity or hl7:relatedEntity) does not contain enough elements [min 1x]</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.12" test="$elmcount&lt;=1">(CI-SISInformant): choice (hl7:assignedEntity or hl7:relatedEntity) contains too many elements [max 1x]</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.12" test="count(hl7:assignedEntity)&lt;=1">(CI-SISInformant): element hl7:assignedEntity appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.12" test="count(hl7:relatedEntity)&lt;=1">(CI-SISInformant): element hl7:relatedEntity appears too often [max 1x].</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.12
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity
Item: (CI-SISInformant)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.14
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity
Item: (CI-SISAssignedEntity)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="count(hl7:id)&gt;=1">(CI-SISAssignedEntity): element hl7:id is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="count(hl7:id)&lt;=1">(CI-SISAssignedEntity): element hl7:id appears too often [max 1x].</assert>
        <let name="elmcount" value="count(hl7:addr|hl7:addr)"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="count(hl7:assignedPerson)&lt;=1">(CI-SISAssignedEntity): element hl7:assignedPerson appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="count(hl7:representedOrganization)&lt;=1">(CI-SISAssignedEntity): element hl7:representedOrganization appears too often [max 1x].</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.14
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:id
Item: (CI-SISAssignedEntity)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:id">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="@root">(CI-SISAssignedEntity): attribute @root SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="not(@root) or matches(@root,'^[0-2](\.(0|[1-9]\d*))*$') or matches(@root,'^[A-Fa-f\d]{8}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{12}$') or matches(@root,'^[A-Za-z][A-Za-z\d\-]*$')">(CI-SISAssignedEntity): Attribute @root SHALL be of data type 'uid'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="@extension">(CI-SISAssignedEntity): attribute @extension SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="not(@extension) or string-length(@extension)&gt;0">(CI-SISAssignedEntity): Attribute @extension SHALL be of data type 'st'</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity
Item: (CI-SISAddr)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:addr">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@hl7:use) or (string-length(@hl7:use)&gt;0 and not(matches(@hl7:use,'\s')))">(CI-SISAddr): Attribute @hl7:use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:country)&lt;=1">(CI-SISAddr): element hl7:country appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:state)&lt;=1">(CI-SISAddr): element hl7:state appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:city)&lt;=1">(CI-SISAddr): element hl7:city appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postalCode)&lt;=1">(CI-SISAddr): element hl7:postalCode appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumber)&lt;=1">(CI-SISAddr): element hl7:houseNumber appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumberNumeric)&lt;=1">(CI-SISAddr): element hl7:houseNumberNumeric appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetName)&lt;=1">(CI-SISAddr): element hl7:streetName appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetNameType)&lt;=1">(CI-SISAddr): element hl7:streetNameType appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:additionalLocator)&lt;=1">(CI-SISAddr): element hl7:additionalLocator appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:unitID)&lt;=1">(CI-SISAddr): element hl7:unitID appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postBox)&lt;=1">(CI-SISAddr): element hl7:postBox appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:precinct)&lt;=1">(CI-SISAddr): element hl7:precinct appears too often [max 1x].</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:country
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:country">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:state
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:state">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:city
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:city">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:postalCode
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:postalCode">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:houseNumber
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:houseNumber">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:houseNumberNumeric
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:houseNumberNumeric">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:streetName
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:streetName">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:streetNameType
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:streetNameType">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:additionalLocator
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:additionalLocator">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:unitID
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:unitID">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:postBox
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:postBox">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:precinct
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:precinct">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:addr">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISAddr): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.19
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:telecom
Item: (CI-SISTelecom)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:telecom">
        <extends rule="TEL"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TEL' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISTelecom): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TEL", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISTelecom): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@value">(CI-SISTelecom): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@value) or string(@value castable as xs:anyURI)">(CI-SISTelecom): Attribute @value SHALL be of data type 'url'<value-of select="@value"/>
        </assert>
        <let name="prefix" value="substring-before(@value, ':')"/>
        <let name="suffix" value="substring-after(@value, ':')"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(             (count(@*) = 1 and name(@*) = 'nullFlavor' and             (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or             ($suffix and (             $prefix = 'tel' or              $prefix = 'fax' or              $prefix = 'mailto' or              $prefix = 'http' or              $prefix = 'ftp' or              $prefix = 'mllp'))             )">(CI-SISTelecom): Erreur de conformité CI-SIS : <name/> n'est pas conforme à une adresse de télécommunication préfixe:chaîne 
            (avec préfixe = tel, fax, mailto, http, ftp ou mllp) 
            ou est vide et sans nullFlavor, ou contient un nullFlavor non admis.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@use = 'H'                      or @use = 'HP'                      or @use = 'HV'                      or @use = 'WP'                     or @use = 'DIR'                      or @use = 'PUB'                      or @use = 'EC'                      or @use = 'MC'                      or @use = 'PG'                      or not(@use)">(CI-SISTelecom): Erreur de conformité CI-SIS : L'attribut use de l'élément telecom n'est pas conforme. 
            Il est facultatif et les valeurs permises sont 'H','HP', 'HV','WP','DIR','PUB','EC','MC','PG'.</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.14
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson
Item: (CI-SISAssignedEntity)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="count(hl7:name)&gt;=1">(CI-SISAssignedEntity): element hl7:name is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="count(hl7:name)&lt;=1">(CI-SISAssignedEntity): element hl7:name appears too often [max 1x].</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.14
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name
Item: (CI-SISAssignedEntity)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="count(hl7:prefix)&lt;=1">(CI-SISAssignedEntity): element hl7:prefix appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="count(hl7:given)&lt;=1">(CI-SISAssignedEntity): element hl7:given appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="count(hl7:family)&gt;=1">(CI-SISAssignedEntity): element hl7:family is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="count(hl7:family)&lt;=1">(CI-SISAssignedEntity): element hl7:family appears too often [max 1x].</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.14
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name/hl7:prefix
Item: (CI-SISAssignedEntity)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.14
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name/hl7:given
Item: (CI-SISAssignedEntity)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.14
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name/hl7:family
Item: (CI-SISAssignedEntity)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.14
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization
Item: (CI-SISAssignedEntity)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="count(hl7:id[@root='1.2.250.1.71.4.2.2'])&lt;=1">(CI-SISAssignedEntity): element hl7:id[@root='1.2.250.1.71.4.2.2'] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="count(hl7:name)&lt;=1">(CI-SISAssignedEntity): element hl7:name appears too often [max 1x].</assert>
        <let name="elmcount" value="count(hl7:addr|hl7:addr)"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="count(hl7:standardIndustryClassCode[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.4-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem) or @nullFlavor])&lt;=1">(CI-SISAssignedEntity): element hl7:standardIndustryClassCode[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.4-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem) or @nullFlavor] appears too often [max 1x].</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.14
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:id[@root='1.2.250.1.71.4.2.2']
Item: (CI-SISAssignedEntity)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:id[@root='1.2.250.1.71.4.2.2']">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="@extension">(CI-SISAssignedEntity): attribute @extension SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="not(@extension) or string-length(@extension)&gt;0">(CI-SISAssignedEntity): Attribute @extension SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="string(@root)=('1.2.250.1.71.4.2.2')">(CI-SISAssignedEntity): The value for @root SHALL be '1.2.250.1.71.4.2.2'.</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.14
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:name
Item: (CI-SISAssignedEntity)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization
Item: (CI-SISAddr)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@hl7:use) or (string-length(@hl7:use)&gt;0 and not(matches(@hl7:use,'\s')))">(CI-SISAddr): Attribute @hl7:use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:country)&lt;=1">(CI-SISAddr): element hl7:country appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:state)&lt;=1">(CI-SISAddr): element hl7:state appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:city)&lt;=1">(CI-SISAddr): element hl7:city appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postalCode)&lt;=1">(CI-SISAddr): element hl7:postalCode appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumber)&lt;=1">(CI-SISAddr): element hl7:houseNumber appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumberNumeric)&lt;=1">(CI-SISAddr): element hl7:houseNumberNumeric appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetName)&lt;=1">(CI-SISAddr): element hl7:streetName appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetNameType)&lt;=1">(CI-SISAddr): element hl7:streetNameType appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:additionalLocator)&lt;=1">(CI-SISAddr): element hl7:additionalLocator appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:unitID)&lt;=1">(CI-SISAddr): element hl7:unitID appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postBox)&lt;=1">(CI-SISAddr): element hl7:postBox appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:precinct)&lt;=1">(CI-SISAddr): element hl7:precinct appears too often [max 1x].</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:country
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:country">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:state
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:state">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:city
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:city">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:postalCode
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:postalCode">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:houseNumber
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:houseNumber">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:houseNumberNumeric
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:houseNumberNumeric">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetName
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetName">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetNameType
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetNameType">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:additionalLocator
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:additionalLocator">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:unitID
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:unitID">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:postBox
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:postBox">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:precinct
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:precinct">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISAddr): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.19
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:telecom
Item: (CI-SISTelecom)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:telecom">
        <extends rule="TEL"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TEL' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISTelecom): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TEL", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISTelecom): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@value">(CI-SISTelecom): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@value) or string(@value castable as xs:anyURI)">(CI-SISTelecom): Attribute @value SHALL be of data type 'url'<value-of select="@value"/>
        </assert>
        <let name="prefix" value="substring-before(@value, ':')"/>
        <let name="suffix" value="substring-after(@value, ':')"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(             (count(@*) = 1 and name(@*) = 'nullFlavor' and             (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or             ($suffix and (             $prefix = 'tel' or              $prefix = 'fax' or              $prefix = 'mailto' or              $prefix = 'http' or              $prefix = 'ftp' or              $prefix = 'mllp'))             )">(CI-SISTelecom): Erreur de conformité CI-SIS : <name/> n'est pas conforme à une adresse de télécommunication préfixe:chaîne 
            (avec préfixe = tel, fax, mailto, http, ftp ou mllp) 
            ou est vide et sans nullFlavor, ou contient un nullFlavor non admis.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@use = 'H'                      or @use = 'HP'                      or @use = 'HV'                      or @use = 'WP'                     or @use = 'DIR'                      or @use = 'PUB'                      or @use = 'EC'                      or @use = 'MC'                      or @use = 'PG'                      or not(@use)">(CI-SISTelecom): Erreur de conformité CI-SIS : L'attribut use de l'élément telecom n'est pas conforme. 
            Il est facultatif et les valeurs permises sont 'H','HP', 'HV','WP','DIR','PUB','EC','MC','PG'.</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.14
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:standardIndustryClassCode[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.4-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem) or @nullFlavor]
Item: (CI-SISAssignedEntity)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:standardIndustryClassCode[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.4-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem) or @nullFlavor]">
        <let name="theCode" value="@code"/>
        <let name="theCodeSystem" value="@codeSystem"/>
        <let name="theCodeSystemVersion" value="@codeSystemVersion"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="@nullFlavor or exists(doc('include/voc-1.2.250.1.213.1.1.5.4-2018-01-05T000000.xml')//valueSet[1][conceptList/concept[@code = $theCode][@codeSystem = $theCodeSystem][not(@codeSystemVersion) or @codeSystemVersion=$theCodeSystemVersion] or completeCodeSystem[@codeSystem=$theCodeSystem][not(@codeSystemVersion) or @codeSystemVersion=$theCodeSystemVersion]])">(CI-SISAssignedEntity): The element value SHALL be one of '1.2.250.1.213.1.1.5.4 J04-XdsPracticeSettingCode (2018-01-05T00:00:00)'.</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.12
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:relatedEntity
Item: (CI-SISInformant)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.13
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:relatedEntity
Item: (CI-SISRelatedEntity)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:relatedEntity">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="@classCode">(CI-SISRelatedEntity): attribute @classCode SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="not(@classCode) or (string-length(@classCode)&gt;0 and not(matches(@classCode,'\s')))">(CI-SISRelatedEntity): Attribute @classCode SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="@classCode='ECON' or @classCode='GUARD' or @classCode='QUAL' or @classCode='POLHOLD' or @classCode='CON'">(CI-SISRelatedEntity): @classCode doit être renseigné avec une de ces valeurs :
"ECON" pour personne à prévenir en cas d’urgence
"GUARD" pour rôle de tuteur légal
"QUAL" pour personne de confiance
"POLHOLD" pour assuré ouvrant droit
"CON" pour informateur</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="count(hl7:code)&lt;=1">(CI-SISRelatedEntity): element hl7:code appears too often [max 1x].</assert>
        <let name="elmcount" value="count(hl7:addr|hl7:addr)"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="$elmcount&gt;=1">(CI-SISRelatedEntity): choice (hl7:addr or hl7:addr) does not contain enough elements [min 1x]</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="count(hl7:telecom)&gt;=1">(CI-SISRelatedEntity): element hl7:telecom is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="count(hl7:relatedPerson)&gt;=1">(CI-SISRelatedEntity): element hl7:relatedPerson is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="count(hl7:relatedPerson)&lt;=1">(CI-SISRelatedEntity): element hl7:relatedPerson appears too often [max 1x].</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.13
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:relatedEntity/hl7:code
Item: (CI-SISRelatedEntity)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:relatedEntity/hl7:code">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="@displayName">(CI-SISRelatedEntity): attribute @displayName SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="not(@displayName) or string-length(@displayName)&gt;0">(CI-SISRelatedEntity): Attribute @displayName SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="@codeSystem">(CI-SISRelatedEntity): attribute @codeSystem SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="not(@codeSystem) or matches(@codeSystem,'^[0-2](\.(0|[1-9]\d*))*$')">(CI-SISRelatedEntity): Attribute @codeSystem SHALL be of data type 'oid'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="@code">(CI-SISRelatedEntity): attribute @code SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="not(@code) or (string-length(@code)&gt;0 and not(matches(@code,'\s')))">(CI-SISRelatedEntity): Attribute @code SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="count(hl7:originalText)&lt;=1">(CI-SISRelatedEntity): element hl7:originalText appears too often [max 1x].</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.13
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:relatedEntity/hl7:code/hl7:originalText
Item: (CI-SISRelatedEntity)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:relatedEntity
Item: (CI-SISAddr)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:relatedEntity/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:relatedEntity/hl7:addr">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@hl7:use) or (string-length(@hl7:use)&gt;0 and not(matches(@hl7:use,'\s')))">(CI-SISAddr): Attribute @hl7:use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:country)&lt;=1">(CI-SISAddr): element hl7:country appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:state)&lt;=1">(CI-SISAddr): element hl7:state appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:city)&lt;=1">(CI-SISAddr): element hl7:city appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postalCode)&lt;=1">(CI-SISAddr): element hl7:postalCode appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumber)&lt;=1">(CI-SISAddr): element hl7:houseNumber appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumberNumeric)&lt;=1">(CI-SISAddr): element hl7:houseNumberNumeric appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetName)&lt;=1">(CI-SISAddr): element hl7:streetName appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetNameType)&lt;=1">(CI-SISAddr): element hl7:streetNameType appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:additionalLocator)&lt;=1">(CI-SISAddr): element hl7:additionalLocator appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:unitID)&lt;=1">(CI-SISAddr): element hl7:unitID appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postBox)&lt;=1">(CI-SISAddr): element hl7:postBox appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:precinct)&lt;=1">(CI-SISAddr): element hl7:precinct appears too often [max 1x].</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:country
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:country">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:state
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:state">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:city
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:city">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:postalCode
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:postalCode">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:houseNumber
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:houseNumber">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:houseNumberNumeric
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:houseNumberNumeric">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:streetName
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:streetName">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:streetNameType
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:streetNameType">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:additionalLocator
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:additionalLocator">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:unitID
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:unitID">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:postBox
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:postBox">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:precinct
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:precinct">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:relatedEntity/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:relatedEntity/hl7:addr">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISAddr): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:streetAddressLine">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:streetAddressLine">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:streetAddressLine">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:streetAddressLine">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:streetAddressLine">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:streetAddressLine">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.19
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:relatedEntity/hl7:telecom
Item: (CI-SISTelecom)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:relatedEntity/hl7:telecom">
        <extends rule="TEL"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TEL' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISTelecom): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TEL", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISTelecom): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@value">(CI-SISTelecom): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@value) or string(@value castable as xs:anyURI)">(CI-SISTelecom): Attribute @value SHALL be of data type 'url'<value-of select="@value"/>
        </assert>
        <let name="prefix" value="substring-before(@value, ':')"/>
        <let name="suffix" value="substring-after(@value, ':')"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(             (count(@*) = 1 and name(@*) = 'nullFlavor' and             (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or             ($suffix and (             $prefix = 'tel' or              $prefix = 'fax' or              $prefix = 'mailto' or              $prefix = 'http' or              $prefix = 'ftp' or              $prefix = 'mllp'))             )">(CI-SISTelecom): Erreur de conformité CI-SIS : <name/> n'est pas conforme à une adresse de télécommunication préfixe:chaîne 
            (avec préfixe = tel, fax, mailto, http, ftp ou mllp) 
            ou est vide et sans nullFlavor, ou contient un nullFlavor non admis.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@use = 'H'                      or @use = 'HP'                      or @use = 'HV'                      or @use = 'WP'                     or @use = 'DIR'                      or @use = 'PUB'                      or @use = 'EC'                      or @use = 'MC'                      or @use = 'PG'                      or not(@use)">(CI-SISTelecom): Erreur de conformité CI-SIS : L'attribut use de l'élément telecom n'est pas conforme. 
            Il est facultatif et les valeurs permises sont 'H','HP', 'HV','WP','DIR','PUB','EC','MC','PG'.</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.13
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:relatedEntity/hl7:relatedPerson
Item: (CI-SISRelatedEntity)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:relatedEntity/hl7:relatedPerson">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="count(hl7:name)&gt;=1">(CI-SISRelatedEntity): element hl7:name is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="count(hl7:name)&lt;=1">(CI-SISRelatedEntity): element hl7:name appears too often [max 1x].</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.13
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:relatedEntity/hl7:relatedPerson/hl7:name
Item: (CI-SISRelatedEntity)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:relatedEntity/hl7:relatedPerson/hl7:name">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="count(hl7:family)&gt;=1">(CI-SISRelatedEntity): element hl7:family is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="count(hl7:family)&lt;=1">(CI-SISRelatedEntity): element hl7:family appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="count(hl7:given)&lt;=1">(CI-SISRelatedEntity): element hl7:given appears too often [max 1x].</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.13
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:relatedEntity/hl7:relatedPerson/hl7:name/hl7:family
Item: (CI-SISRelatedEntity)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.13
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:informant/hl7:relatedEntity/hl7:relatedPerson/hl7:name/hl7:given
Item: (CI-SISRelatedEntity)
-->
<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.9
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant
Item: (CI-SISParticipant)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant" id="d518685e22-false-d518698e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="@typeCode">(CI-SISParticipant): attribute @typeCode SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="not(@typeCode) or (string-length(@typeCode)&gt;0 and not(matches(@typeCode,'\s')))">(CI-SISParticipant): Attribute @typeCode SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="count(hl7:functionCode[(@code='ADMPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ATTPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='DISPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PRISURG' and @codeSystem='2.16.840.1.113883.5.88') or (@code='FASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='SASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='NASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANEST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANRS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='MDWF' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PCP' and @codeSystem='2.16.840.1.113883.5.88') or (@code='CORRE' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='REMPL' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='GYNEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='OPHTA' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PSYCH' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='CARDT' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PRELV' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='ASPEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='APSIN' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or @nullFlavor])&lt;=1">(CI-SISParticipant): element hl7:functionCode[(@code='ADMPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ATTPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='DISPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PRISURG' and @codeSystem='2.16.840.1.113883.5.88') or (@code='FASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='SASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='NASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANEST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANRS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='MDWF' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PCP' and @codeSystem='2.16.840.1.113883.5.88') or (@code='CORRE' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='REMPL' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='GYNEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='OPHTA' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PSYCH' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='CARDT' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PRELV' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='ASPEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='APSIN' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or @nullFlavor] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="count(hl7:time)&gt;=1">(CI-SISParticipant): element hl7:time is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="count(hl7:time)&lt;=1">(CI-SISParticipant): element hl7:time appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="count(hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']])&gt;=1">(CI-SISParticipant): element hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="count(hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']])&lt;=1">(CI-SISParticipant): element hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.9
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:functionCode[(@code='ADMPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ATTPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='DISPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PRISURG' and @codeSystem='2.16.840.1.113883.5.88') or (@code='FASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='SASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='NASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANEST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANRS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='MDWF' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PCP' and @codeSystem='2.16.840.1.113883.5.88') or (@code='CORRE' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='REMPL' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='GYNEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='OPHTA' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PSYCH' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='CARDT' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PRELV' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='ASPEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='APSIN' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or @nullFlavor]
Item: (CI-SISParticipant)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:functionCode[(@code='ADMPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ATTPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='DISPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PRISURG' and @codeSystem='2.16.840.1.113883.5.88') or (@code='FASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='SASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='NASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANEST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANRS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='MDWF' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PCP' and @codeSystem='2.16.840.1.113883.5.88') or (@code='CORRE' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='REMPL' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='GYNEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='OPHTA' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PSYCH' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='CARDT' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PRELV' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='ASPEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='APSIN' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or @nullFlavor]" id="d518685e43-false-d518746e0">
        <extends rule="CE"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CE' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISParticipant): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CE", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="@nullFlavor or (@code='ADMPHYS' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='médecin ayant admis le patient dans la structure de soins') or (@code='ATTPHYS' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='médecin responsable du patient dans la structure de soins') or (@code='DISPHYS' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='médecin ayant décidé la sortie du patient de la structure de soins') or (@code='PRISURG' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='médecin intervenant principal') or (@code='FASST' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='premier assistant lors de l’intervention') or (@code='SASST' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='second assistant lors de l’intervention') or (@code='NASST' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='infirmier(e)') or (@code='ANEST' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='médecin anesthésiste') or (@code='ANRS' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='infirmier(e) anesthésiste') or (@code='MDWF' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='sage-femme') or (@code='PCP' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='médecin traitant') or (@code='CORRE' and @codeSystem='1.2.250.1.213.1.1.4.2.280' and @displayName='médecin correspondant') or (@code='REMPL' and @codeSystem='1.2.250.1.213.1.1.4.2.280' and @displayName='médecin remplaçant du médecin traitant') or (@code='GYNEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280' and @displayName='gynécologue') or (@code='OPHTA' and @codeSystem='1.2.250.1.213.1.1.4.2.280' and @displayName='ophtalmologue') or (@code='PSYCH' and @codeSystem='1.2.250.1.213.1.1.4.2.280' and @displayName='psychiatre ou neuropsychiatre') or (@code='CARDT' and @codeSystem='1.2.250.1.213.1.1.4.2.280' and @displayName='cardiologue traitant') or (@code='PRELV' and @codeSystem='1.2.250.1.213.1.1.4.2.280' and @displayName='préleveur d’échantillons biologiques pour l’exécution d’une prescription d’examens biologiques') or (@code='ASPEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280' and @displayName='autre spécialiste') or (@code='APSIN' and @codeSystem='1.2.250.1.213.1.1.4.2.280' and @displayName='autre PS informateur')">(CI-SISParticipant): The element value SHALL be one of 'code 'ADMPHYS' codeSystem '2.16.840.1.113883.5.88' displayName='médecin ayant admis le patient dans la structure de soins' or code 'ATTPHYS' codeSystem '2.16.840.1.113883.5.88' displayName='médecin responsable du patient dans la structure de soins' or code 'DISPHYS' codeSystem '2.16.840.1.113883.5.88' displayName='médecin ayant décidé la sortie du patient de la structure de soins' or code 'PRISURG' codeSystem '2.16.840.1.113883.5.88' displayName='médecin intervenant principal' or code 'FASST' codeSystem '2.16.840.1.113883.5.88' displayName='premier assistant lors de l’intervention' or code 'SASST' codeSystem '2.16.840.1.113883.5.88' displayName='second assistant lors de l’intervention' or code 'NASST' codeSystem '2.16.840.1.113883.5.88' displayName='infirmier(e)' or code 'ANEST' codeSystem '2.16.840.1.113883.5.88' displayName='médecin anesthésiste' or code 'ANRS' codeSystem '2.16.840.1.113883.5.88' displayName='infirmier(e) anesthésiste' or code 'MDWF' codeSystem '2.16.840.1.113883.5.88' displayName='sage-femme' or code 'PCP' codeSystem '2.16.840.1.113883.5.88' displayName='médecin traitant' or code 'CORRE' codeSystem '1.2.250.1.213.1.1.4.2.280' displayName='médecin correspondant' or code 'REMPL' codeSystem '1.2.250.1.213.1.1.4.2.280' displayName='médecin remplaçant du médecin traitant' or code 'GYNEC' codeSystem '1.2.250.1.213.1.1.4.2.280' displayName='gynécologue' or code 'OPHTA' codeSystem '1.2.250.1.213.1.1.4.2.280' displayName='ophtalmologue' or code 'PSYCH' codeSystem '1.2.250.1.213.1.1.4.2.280' displayName='psychiatre ou neuropsychiatre' or code 'CARDT' codeSystem '1.2.250.1.213.1.1.4.2.280' displayName='cardiologue traitant' or code 'PRELV' codeSystem '1.2.250.1.213.1.1.4.2.280' displayName='préleveur d’échantillons biologiques pour l’exécution d’une prescription d’examens biologiques' or code 'ASPEC' codeSystem '1.2.250.1.213.1.1.4.2.280' displayName='autre spécialiste' or code 'APSIN' codeSystem '1.2.250.1.213.1.1.4.2.280' displayName='autre PS informateur''.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="count(hl7:originalText)&lt;=1">(CI-SISParticipant): element hl7:originalText appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.9
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:functionCode[(@code='ADMPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ATTPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='DISPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PRISURG' and @codeSystem='2.16.840.1.113883.5.88') or (@code='FASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='SASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='NASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANEST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANRS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='MDWF' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PCP' and @codeSystem='2.16.840.1.113883.5.88') or (@code='CORRE' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='REMPL' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='GYNEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='OPHTA' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PSYCH' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='CARDT' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PRELV' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='ASPEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='APSIN' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or @nullFlavor]/hl7:originalText
Item: (CI-SISParticipant)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:functionCode[(@code='ADMPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ATTPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='DISPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PRISURG' and @codeSystem='2.16.840.1.113883.5.88') or (@code='FASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='SASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='NASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANEST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANRS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='MDWF' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PCP' and @codeSystem='2.16.840.1.113883.5.88') or (@code='CORRE' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='REMPL' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='GYNEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='OPHTA' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PSYCH' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='CARDT' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PRELV' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='ASPEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='APSIN' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or @nullFlavor]/hl7:originalText" id="d518685e119-false-d518826e0">
        <extends rule="ED"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ED' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISParticipant): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ED", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.9
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:time
Item: (CI-SISParticipant)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:time" id="d518685e125-false-d518836e0">
        <extends rule="IVL_TS"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='IVL_TS' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISParticipant): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:IVL_TS", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="count(hl7:low)&lt;=1">(CI-SISParticipant): element hl7:low appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="count(hl7:high)&lt;=1">(CI-SISParticipant): element hl7:high appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.9
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:time/hl7:low
Item: (CI-SISParticipant)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:time/hl7:low" id="d518685e129-false-d518860e0">
        <extends rule="IVXB_TS"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='IVXB_TS' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISParticipant): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:IVXB_TS", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="@value">(CI-SISParticipant): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="not(@value) or matches(string(@value), '^[0-9]{4,14}')">(CI-SISParticipant): Attribute @value SHALL be of data type 'ts'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.9
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:time/hl7:high
Item: (CI-SISParticipant)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:time/hl7:high" id="d518685e137-false-d518877e0">
        <extends rule="IVXB_TS"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='IVXB_TS' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISParticipant): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:IVXB_TS", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="@value">(CI-SISParticipant): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="not(@value) or matches(string(@value), '^[0-9]{4,14}')">(CI-SISParticipant): Attribute @value SHALL be of data type 'ts'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.9
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]
Item: (CI-SISParticipant)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]" id="d518685e147-false-d518901e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="@classCode">(CI-SISParticipant): attribute @classCode SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="not(@classCode) or (string-length(@classCode)&gt;0 and not(matches(@classCode,'\s')))">(CI-SISParticipant): Attribute @classCode SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="count(hl7:id[@root='1.2.250.1.71.4.2.1'])&gt;=1">(CI-SISParticipant): element hl7:id[@root='1.2.250.1.71.4.2.1'] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="count(hl7:id[@root='1.2.250.1.71.4.2.1'])&lt;=1">(CI-SISParticipant): element hl7:id[@root='1.2.250.1.71.4.2.1'] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="count(hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.1-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem) or @nullFlavor])&lt;=1">(CI-SISParticipant): element hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.1-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem) or @nullFlavor] appears too often [max 1x].</assert>
        <let name="elmcount" value="count(hl7:addr|hl7:addr)"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="count(hl7:associatedPerson)&gt;=1">(CI-SISParticipant): element hl7:associatedPerson is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="count(hl7:associatedPerson)&lt;=1">(CI-SISParticipant): element hl7:associatedPerson appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.9
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:id[@root='1.2.250.1.71.4.2.1']
Item: (CI-SISParticipant)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:id[@root='1.2.250.1.71.4.2.1']" id="d518685e157-false-d518974e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISParticipant): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="@extension">(CI-SISParticipant): attribute @extension SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="not(@extension) or string-length(@extension)&gt;0">(CI-SISParticipant): Attribute @extension SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="string(@root)=('1.2.250.1.71.4.2.1')">(CI-SISParticipant): The value for @root SHALL be '1.2.250.1.71.4.2.1'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.9
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.1-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem) or @nullFlavor]
Item: (CI-SISParticipant)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.1-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem) or @nullFlavor]" id="d518685e192-false-d518998e0">
        <extends rule="CE"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CE' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISParticipant): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CE", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <let name="theCode" value="@code"/>
        <let name="theCodeSystem" value="@codeSystem"/>
        <let name="theCodeSystemVersion" value="@codeSystemVersion"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="@nullFlavor or exists(doc('include/voc-1.2.250.1.213.1.1.5.1-2018-01-05T000000.xml')//valueSet[1][conceptList/concept[@code = $theCode][@codeSystem = $theCodeSystem][not(@codeSystemVersion) or @codeSystemVersion=$theCodeSystemVersion] or completeCodeSystem[@codeSystem=$theCodeSystem][not(@codeSystemVersion) or @codeSystemVersion=$theCodeSystemVersion]])">(CI-SISParticipant): The element value SHALL be one of '1.2.250.1.213.1.1.5.1 JDV_J01-XdsAuthorSpecialty-CISIS (2018-01-05T00:00:00)'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]
Item: (CI-SISAddr)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr" id="d518999e75-false-d519018e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@hl7:use) or (string-length(@hl7:use)&gt;0 and not(matches(@hl7:use,'\s')))">(CI-SISAddr): Attribute @hl7:use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:country)&lt;=1">(CI-SISAddr): element hl7:country appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:state)&lt;=1">(CI-SISAddr): element hl7:state appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:city)&lt;=1">(CI-SISAddr): element hl7:city appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postalCode)&lt;=1">(CI-SISAddr): element hl7:postalCode appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumber)&lt;=1">(CI-SISAddr): element hl7:houseNumber appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumberNumeric)&lt;=1">(CI-SISAddr): element hl7:houseNumberNumeric appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetName)&lt;=1">(CI-SISAddr): element hl7:streetName appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetNameType)&lt;=1">(CI-SISAddr): element hl7:streetNameType appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:additionalLocator)&lt;=1">(CI-SISAddr): element hl7:additionalLocator appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:unitID)&lt;=1">(CI-SISAddr): element hl7:unitID appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postBox)&lt;=1">(CI-SISAddr): element hl7:postBox appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:precinct)&lt;=1">(CI-SISAddr): element hl7:precinct appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:country
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:country" id="d518999e93-false-d519116e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:state
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:state" id="d518999e97-false-d519126e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:city
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:city" id="d518999e101-false-d519136e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:postalCode
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:postalCode" id="d518999e109-false-d519146e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:houseNumber
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:houseNumber" id="d518999e117-false-d519156e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:houseNumberNumeric
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:houseNumberNumeric" id="d518999e121-false-d519166e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetName
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetName" id="d518999e125-false-d519176e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetNameType
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetNameType" id="d518999e129-false-d519186e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:additionalLocator
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:additionalLocator" id="d518999e174-false-d519196e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:unitID
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:unitID" id="d518999e189-false-d519206e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:postBox
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:postBox" id="d518999e199-false-d519216e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:precinct
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:precinct" id="d518999e203-false-d519226e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr" id="d518999e207-false-d519236e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISAddr): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine" id="d518999e225-false-d519292e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine" id="d518999e231-false-d519302e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine" id="d518999e235-false-d519312e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine" id="d518999e239-false-d519322e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine" id="d518999e243-false-d519332e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine" id="d518999e247-false-d519342e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.19
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:telecom
Item: (CI-SISTelecom)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:telecom" id="d519343e27-false-d519353e0">
        <extends rule="TEL"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TEL' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISTelecom): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TEL", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISTelecom): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@value">(CI-SISTelecom): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@value) or string(@value castable as xs:anyURI)">(CI-SISTelecom): Attribute @value SHALL be of data type 'url'<value-of select="@value"/>
        </assert>
        <let name="prefix" value="substring-before(@value, ':')"/>
        <let name="suffix" value="substring-after(@value, ':')"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(             (count(@*) = 1 and name(@*) = 'nullFlavor' and             (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or             ($suffix and (             $prefix = 'tel' or              $prefix = 'fax' or              $prefix = 'mailto' or              $prefix = 'http' or              $prefix = 'ftp' or              $prefix = 'mllp'))             )">(CI-SISTelecom): Erreur de conformité CI-SIS : <name/> n'est pas conforme à une adresse de télécommunication préfixe:chaîne 
            (avec préfixe = tel, fax, mailto, http, ftp ou mllp) 
            ou est vide et sans nullFlavor, ou contient un nullFlavor non admis.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@use = 'H'                      or @use = 'HP'                      or @use = 'HV'                      or @use = 'WP'                     or @use = 'DIR'                      or @use = 'PUB'                      or @use = 'EC'                      or @use = 'MC'                      or @use = 'PG'                      or not(@use)">(CI-SISTelecom): Erreur de conformité CI-SIS : L'attribut use de l'élément telecom n'est pas conforme. 
            Il est facultatif et les valeurs permises sont 'H','HP', 'HV','WP','DIR','PUB','EC','MC','PG'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.9
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:associatedPerson
Item: (CI-SISParticipant)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:associatedPerson" id="d518685e218-false-d519374e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="count(hl7:name)&gt;=1">(CI-SISParticipant): element hl7:name is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="count(hl7:name)&lt;=1">(CI-SISParticipant): element hl7:name appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.9
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:associatedPerson/hl7:name
Item: (CI-SISParticipant)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:associatedPerson/hl7:name" id="d518685e323-false-d519390e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="count(hl7:family)&gt;=1">(CI-SISParticipant): element hl7:family is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="count(hl7:family)&lt;=1">(CI-SISParticipant): element hl7:family appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="count(hl7:given)&lt;=1">(CI-SISParticipant): element hl7:given appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="count(hl7:prefix)&lt;=1">(CI-SISParticipant): element hl7:prefix appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.9
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:associatedPerson/hl7:name/hl7:family
Item: (CI-SISParticipant)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.9
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:associatedPerson/hl7:name/hl7:given
Item: (CI-SISParticipant)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.9
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:associatedPerson/hl7:name/hl7:prefix
Item: (CI-SISParticipant)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget
Item: (CI-SISRecordTarget)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget" id="d519436e71-false-d519455e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:patientRole)&gt;=1">(CI-SISRecordTarget): element hl7:patientRole is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:patientRole)&lt;=1">(CI-SISRecordTarget): element hl7:patientRole appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole
Item: (CI-SISRecordTarget)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole" id="d519436e72-false-d519497e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:id[not(@nullFlavor)])&gt;=1">(CI-SISRecordTarget): element hl7:id[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <let name="elmcount" value="count(hl7:addr|hl7:addr)"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="$elmcount&gt;=1">(CI-SISRecordTarget): choice (hl7:addr or hl7:addr) does not contain enough elements [min 1x]</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]])&gt;=1">(CI-SISRecordTarget): element hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]])&lt;=1">(CI-SISRecordTarget): element hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:id[not(@nullFlavor)]
Item: (CI-SISRecordTarget)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:id[not(@nullFlavor)]" id="d519436e78-false-d519561e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISRecordTarget): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="@root">(CI-SISRecordTarget): attribute @root SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="not(@root) or matches(@root,'^[0-2](\.(0|[1-9]\d*))*$') or matches(@root,'^[A-Fa-f\d]{8}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{12}$') or matches(@root,'^[A-Za-z][A-Za-z\d\-]*$')">(CI-SISRecordTarget): Attribute @root SHALL be of data type 'uid'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="@extension">(CI-SISRecordTarget): attribute @extension SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="not(@extension) or string-length(@extension)&gt;0">(CI-SISRecordTarget): Attribute @extension SHALL be of data type 'st'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole
Item: (CI-SISAddr)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:addr" id="d519562e78-false-d519587e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@hl7:use) or (string-length(@hl7:use)&gt;0 and not(matches(@hl7:use,'\s')))">(CI-SISAddr): Attribute @hl7:use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:country)&lt;=1">(CI-SISAddr): element hl7:country appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:state)&lt;=1">(CI-SISAddr): element hl7:state appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:city)&lt;=1">(CI-SISAddr): element hl7:city appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postalCode)&lt;=1">(CI-SISAddr): element hl7:postalCode appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumber)&lt;=1">(CI-SISAddr): element hl7:houseNumber appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumberNumeric)&lt;=1">(CI-SISAddr): element hl7:houseNumberNumeric appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetName)&lt;=1">(CI-SISAddr): element hl7:streetName appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetNameType)&lt;=1">(CI-SISAddr): element hl7:streetNameType appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:additionalLocator)&lt;=1">(CI-SISAddr): element hl7:additionalLocator appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:unitID)&lt;=1">(CI-SISAddr): element hl7:unitID appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postBox)&lt;=1">(CI-SISAddr): element hl7:postBox appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:precinct)&lt;=1">(CI-SISAddr): element hl7:precinct appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:country
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:country" id="d519562e96-false-d519685e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:state
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:state" id="d519562e100-false-d519695e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:city
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:city" id="d519562e104-false-d519705e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:postalCode
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:postalCode" id="d519562e112-false-d519715e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:houseNumber
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:houseNumber" id="d519562e120-false-d519725e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:houseNumberNumeric
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:houseNumberNumeric" id="d519562e124-false-d519735e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:streetName
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:streetName" id="d519562e128-false-d519745e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:streetNameType
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:streetNameType" id="d519562e132-false-d519755e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:additionalLocator
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:additionalLocator" id="d519562e177-false-d519765e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:unitID
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:unitID" id="d519562e192-false-d519775e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:postBox
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:postBox" id="d519562e202-false-d519785e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:precinct
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:precinct" id="d519562e206-false-d519795e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:addr" id="d519562e210-false-d519805e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISAddr): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:streetAddressLine" id="d519562e228-false-d519861e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:streetAddressLine" id="d519562e234-false-d519871e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:streetAddressLine" id="d519562e238-false-d519881e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:streetAddressLine" id="d519562e242-false-d519891e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:streetAddressLine" id="d519562e246-false-d519901e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:streetAddressLine" id="d519562e250-false-d519911e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.19
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:telecom
Item: (CI-SISTelecom)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:telecom" id="d519912e27-false-d519922e0">
        <extends rule="TEL"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TEL' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISTelecom): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TEL", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISTelecom): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@value">(CI-SISTelecom): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@value) or string(@value castable as xs:anyURI)">(CI-SISTelecom): Attribute @value SHALL be of data type 'url'<value-of select="@value"/>
        </assert>
        <let name="prefix" value="substring-before(@value, ':')"/>
        <let name="suffix" value="substring-after(@value, ':')"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(             (count(@*) = 1 and name(@*) = 'nullFlavor' and             (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or             ($suffix and (             $prefix = 'tel' or              $prefix = 'fax' or              $prefix = 'mailto' or              $prefix = 'http' or              $prefix = 'ftp' or              $prefix = 'mllp'))             )">(CI-SISTelecom): Erreur de conformité CI-SIS : <name/> n'est pas conforme à une adresse de télécommunication préfixe:chaîne 
            (avec préfixe = tel, fax, mailto, http, ftp ou mllp) 
            ou est vide et sans nullFlavor, ou contient un nullFlavor non admis.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@use = 'H'                      or @use = 'HP'                      or @use = 'HV'                      or @use = 'WP'                     or @use = 'DIR'                      or @use = 'PUB'                      or @use = 'EC'                      or @use = 'MC'                      or @use = 'PG'                      or not(@use)">(CI-SISTelecom): Erreur de conformité CI-SIS : L'attribut use de l'élément telecom n'est pas conforme. 
            Il est facultatif et les valeurs permises sont 'H','HP', 'HV','WP','DIR','PUB','EC','MC','PG'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]
Item: (CI-SISRecordTarget)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]" id="d519436e91-false-d519952e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:name[not(@nullFlavor)])&gt;=1">(CI-SISRecordTarget): element hl7:name[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:name[not(@nullFlavor)])&lt;=1">(CI-SISRecordTarget): element hl7:name[not(@nullFlavor)] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor])&gt;=1">(CI-SISRecordTarget): element hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor])&lt;=1">(CI-SISRecordTarget): element hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:birthTime)&gt;=1">(CI-SISRecordTarget): element hl7:birthTime is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:birthTime)&lt;=1">(CI-SISRecordTarget): element hl7:birthTime appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:birthplace)&lt;=1">(CI-SISRecordTarget): element hl7:birthplace appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:name[not(@nullFlavor)]
Item: (CI-SISRecordTarget)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:name[not(@nullFlavor)]" id="d519436e100-false-d520008e0">
        <extends rule="PN"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='PN' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISRecordTarget): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:PN", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:given)&gt;=1">(CI-SISRecordTarget): element hl7:given is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:family)&gt;=1">(CI-SISRecordTarget): element hl7:family is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:family)&lt;=3">(CI-SISRecordTarget): element hl7:family appears too often [max 3x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:name[not(@nullFlavor)]/hl7:given
Item: (CI-SISRecordTarget)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:name[not(@nullFlavor)]/hl7:family
Item: (CI-SISRecordTarget)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:name[not(@nullFlavor)]/hl7:family" id="d519436e112-false-d520042e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="@qualifier">(CI-SISRecordTarget): attribute @qualifier SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="not(@qualifier) or (string-length(@qualifier)&gt;0 and not(matches(@qualifier,'\s')))">(CI-SISRecordTarget): Attribute @qualifier SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="//hl7:recordTarget/hl7:patientRole/hl7:patient/hl7:name/hl7:family/@qualifier='BR' or //hl7:recordTarget/hl7:patientRole/hl7:patient/hl7:name/hl7:family/@qualifier='SP' or //hl7:recordTarget/hl7:patientRole/hl7:patient/hl7:name/hl7:family/@qualifier='CL'">(CI-SISRecordTarget): Valeur du nom typé par l’attribut qualifier : 
                "BR" pour le nom de famille
                "SP" pour le nom de d’usage
                "CL" pour le pseudonyme</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]
Item: (CI-SISRecordTarget)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]" id="d519436e121-false-d520056e0">
        <extends rule="CE"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CE' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISRecordTarget): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CE", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="@nullFlavor or (@code='M' and @codeSystem='2.16.840.1.113883.5.1' and @displayName='Masculin') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1' and @displayName='Féminin') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1' and @displayName='Inconnu')">(CI-SISRecordTarget): The element value SHALL be one of 'code 'M' codeSystem '2.16.840.1.113883.5.1' displayName='Masculin' or code 'F' codeSystem '2.16.840.1.113883.5.1' displayName='Féminin' or code 'U' codeSystem '2.16.840.1.113883.5.1' displayName='Inconnu''.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthTime
Item: (CI-SISRecordTarget)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthTime" id="d519436e125-false-d520078e0">
        <extends rule="TS"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TS' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISRecordTarget): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TS", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="not(*)">(CI-SISRecordTarget): <value-of select="local-name()"/> with datatype TS, SHOULD NOT have child elements.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="@value">(CI-SISRecordTarget): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="not(@value) or matches(string(@value), '^[0-9]{4,14}')">(CI-SISRecordTarget): Attribute @value SHALL be of data type 'ts'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian
Item: (CI-SISRecordTarget)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian" id="d519436e141-false-d520104e0">
        <let name="elmcount" value="count(hl7:addr|hl7:addr)"/>
        <let name="elmcount" value="count(hl7:guardianPerson|hl7:guardianOrganization)"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="$elmcount&gt;=1">(CI-SISRecordTarget): choice (hl7:guardianPerson or hl7:guardianOrganization) does not contain enough elements [min 1x]</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="$elmcount&lt;=1">(CI-SISRecordTarget): choice (hl7:guardianPerson or hl7:guardianOrganization) contains too many elements [max 1x]</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:guardianPerson)&lt;=1">(CI-SISRecordTarget): element hl7:guardianPerson appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:guardianOrganization)&lt;=1">(CI-SISRecordTarget): element hl7:guardianOrganization appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian
Item: (CI-SISAddr)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr" id="d520101e126-false-d520169e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@hl7:use) or (string-length(@hl7:use)&gt;0 and not(matches(@hl7:use,'\s')))">(CI-SISAddr): Attribute @hl7:use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:country)&lt;=1">(CI-SISAddr): element hl7:country appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:state)&lt;=1">(CI-SISAddr): element hl7:state appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:city)&lt;=1">(CI-SISAddr): element hl7:city appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postalCode)&lt;=1">(CI-SISAddr): element hl7:postalCode appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumber)&lt;=1">(CI-SISAddr): element hl7:houseNumber appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumberNumeric)&lt;=1">(CI-SISAddr): element hl7:houseNumberNumeric appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetName)&lt;=1">(CI-SISAddr): element hl7:streetName appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetNameType)&lt;=1">(CI-SISAddr): element hl7:streetNameType appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:additionalLocator)&lt;=1">(CI-SISAddr): element hl7:additionalLocator appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:unitID)&lt;=1">(CI-SISAddr): element hl7:unitID appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postBox)&lt;=1">(CI-SISAddr): element hl7:postBox appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:precinct)&lt;=1">(CI-SISAddr): element hl7:precinct appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:country
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:country" id="d520101e144-false-d520267e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:state
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:state" id="d520101e148-false-d520277e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:city
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:city" id="d520101e152-false-d520287e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:postalCode
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:postalCode" id="d520101e160-false-d520297e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:houseNumber
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:houseNumber" id="d520101e168-false-d520307e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:houseNumberNumeric
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:houseNumberNumeric" id="d520101e172-false-d520317e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:streetName
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:streetName" id="d520101e176-false-d520327e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:streetNameType
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:streetNameType" id="d520101e180-false-d520337e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:additionalLocator
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:additionalLocator" id="d520101e225-false-d520347e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:unitID
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:unitID" id="d520101e240-false-d520357e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:postBox
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:postBox" id="d520101e250-false-d520367e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:precinct
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:precinct" id="d520101e254-false-d520377e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr" id="d520101e258-false-d520387e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISAddr): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:streetAddressLine" id="d520101e276-false-d520443e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:streetAddressLine" id="d520101e282-false-d520453e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:streetAddressLine" id="d520101e286-false-d520463e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:streetAddressLine" id="d520101e290-false-d520473e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:streetAddressLine" id="d520101e294-false-d520483e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:streetAddressLine" id="d520101e298-false-d520493e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.19
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:telecom
Item: (CI-SISTelecom)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:telecom" id="d520494e27-false-d520504e0">
        <extends rule="TEL"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TEL' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISTelecom): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TEL", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISTelecom): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@value">(CI-SISTelecom): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@value) or string(@value castable as xs:anyURI)">(CI-SISTelecom): Attribute @value SHALL be of data type 'url'<value-of select="@value"/>
        </assert>
        <let name="prefix" value="substring-before(@value, ':')"/>
        <let name="suffix" value="substring-after(@value, ':')"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(             (count(@*) = 1 and name(@*) = 'nullFlavor' and             (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or             ($suffix and (             $prefix = 'tel' or              $prefix = 'fax' or              $prefix = 'mailto' or              $prefix = 'http' or              $prefix = 'ftp' or              $prefix = 'mllp'))             )">(CI-SISTelecom): Erreur de conformité CI-SIS : <name/> n'est pas conforme à une adresse de télécommunication préfixe:chaîne 
            (avec préfixe = tel, fax, mailto, http, ftp ou mllp) 
            ou est vide et sans nullFlavor, ou contient un nullFlavor non admis.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@use = 'H'                      or @use = 'HP'                      or @use = 'HV'                      or @use = 'WP'                     or @use = 'DIR'                      or @use = 'PUB'                      or @use = 'EC'                      or @use = 'MC'                      or @use = 'PG'                      or not(@use)">(CI-SISTelecom): Erreur de conformité CI-SIS : L'attribut use de l'élément telecom n'est pas conforme. 
            Il est facultatif et les valeurs permises sont 'H','HP', 'HV','WP','DIR','PUB','EC','MC','PG'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:guardianPerson
Item: (CI-SISRecordTarget)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:guardianPerson" id="d519436e151-false-d520525e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:name)&gt;=1">(CI-SISRecordTarget): element hl7:name is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:name)&lt;=1">(CI-SISRecordTarget): element hl7:name appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:guardianPerson/hl7:name
Item: (CI-SISRecordTarget)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:guardianPerson/hl7:name" id="d519436e155-false-d520541e0">
        <extends rule="PN"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='PN' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISRecordTarget): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:PN", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:family)&gt;=1">(CI-SISRecordTarget): element hl7:family is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:family)&lt;=3">(CI-SISRecordTarget): element hl7:family appears too often [max 3x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:guardianPerson/hl7:name/hl7:family
Item: (CI-SISRecordTarget)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:guardianPerson/hl7:name/hl7:family" id="d519436e159-false-d520565e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="@qualifier">(CI-SISRecordTarget): attribute @qualifier SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="not(@qualifier) or (string-length(@qualifier)&gt;0 and not(matches(@qualifier,'\s')))">(CI-SISRecordTarget): Attribute @qualifier SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="//hl7:recordTarget/hl7:patientRole/hl7:patient/hl7:name/hl7:family/@qualifier='BR' or //hl7:recordTarget/hl7:patientRole/hl7:patient/hl7:name/hl7:family/@qualifier='SP' or //hl7:recordTarget/hl7:patientRole/hl7:patient/hl7:name/hl7:family/@qualifier='CL'">(CI-SISRecordTarget): Valeur du nom typé par l’attribut qualifier : 
                      "BR" pour le nom de famille
                      "SP" pour le nom de d’usage
                      "CL" pour le pseudonyme</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:guardianPerson/hl7:name/hl7:given
Item: (CI-SISRecordTarget)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:guardianOrganization
Item: (CI-SISRecordTarget)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:guardianOrganization" id="d519436e177-false-d520585e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:id[@root='1.2.250.1.71.4.2.2'])&lt;=1">(CI-SISRecordTarget): element hl7:id[@root='1.2.250.1.71.4.2.2'] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:name)&lt;=1">(CI-SISRecordTarget): element hl7:name appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:guardianOrganization/hl7:id[@root='1.2.250.1.71.4.2.2']
Item: (CI-SISRecordTarget)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:guardianOrganization/hl7:id[@root='1.2.250.1.71.4.2.2']" id="d519436e181-false-d520607e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISRecordTarget): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="string(@root)=('1.2.250.1.71.4.2.2')">(CI-SISRecordTarget): The value for @root SHALL be '1.2.250.1.71.4.2.2'.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="@extension">(CI-SISRecordTarget): attribute @extension SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="not(@extension) or string-length(@extension)&gt;0">(CI-SISRecordTarget): Attribute @extension SHALL be of data type 'st'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:guardianOrganization/hl7:name
Item: (CI-SISRecordTarget)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:guardianOrganization/hl7:name" id="d519436e194-false-d520628e0">
        <extends rule="ON"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ON' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISRecordTarget): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ON", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace
Item: (CI-SISRecordTarget)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace" id="d519436e202-false-d520641e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:place)&gt;=1">(CI-SISRecordTarget): element hl7:place is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:place)&lt;=1">(CI-SISRecordTarget): element hl7:place appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place
Item: (CI-SISRecordTarget)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place" id="d519436e203-false-d520663e0">
        <let name="elmcount" value="count(hl7:addr|hl7:addr)"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="$elmcount&lt;=1">(CI-SISRecordTarget): choice (hl7:addr or hl7:addr) contains too many elements [max 1x]</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:name)&lt;=1">(CI-SISRecordTarget): element hl7:name appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place
Item: (CI-SISAddr)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr" id="d520683e68-false-d520699e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@hl7:use) or (string-length(@hl7:use)&gt;0 and not(matches(@hl7:use,'\s')))">(CI-SISAddr): Attribute @hl7:use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:country)&lt;=1">(CI-SISAddr): element hl7:country appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:state)&lt;=1">(CI-SISAddr): element hl7:state appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:city)&lt;=1">(CI-SISAddr): element hl7:city appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postalCode)&lt;=1">(CI-SISAddr): element hl7:postalCode appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumber)&lt;=1">(CI-SISAddr): element hl7:houseNumber appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumberNumeric)&lt;=1">(CI-SISAddr): element hl7:houseNumberNumeric appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetName)&lt;=1">(CI-SISAddr): element hl7:streetName appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetNameType)&lt;=1">(CI-SISAddr): element hl7:streetNameType appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:additionalLocator)&lt;=1">(CI-SISAddr): element hl7:additionalLocator appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:unitID)&lt;=1">(CI-SISAddr): element hl7:unitID appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postBox)&lt;=1">(CI-SISAddr): element hl7:postBox appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:precinct)&lt;=1">(CI-SISAddr): element hl7:precinct appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:country
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:country" id="d520683e86-false-d520797e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:state
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:state" id="d520683e90-false-d520807e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:city
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:city" id="d520683e94-false-d520817e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:postalCode
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:postalCode" id="d520683e102-false-d520827e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:houseNumber
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:houseNumber" id="d520683e110-false-d520837e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:houseNumberNumeric
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:houseNumberNumeric" id="d520683e114-false-d520847e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:streetName
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:streetName" id="d520683e118-false-d520857e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:streetNameType
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:streetNameType" id="d520683e122-false-d520867e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:additionalLocator
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:additionalLocator" id="d520683e167-false-d520877e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:unitID
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:unitID" id="d520683e182-false-d520887e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:postBox
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:postBox" id="d520683e192-false-d520897e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:precinct
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:precinct" id="d520683e196-false-d520907e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr" id="d520683e200-false-d520917e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISAddr): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:streetAddressLine" id="d520683e218-false-d520973e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:streetAddressLine" id="d520683e224-false-d520983e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:streetAddressLine" id="d520683e228-false-d520993e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:streetAddressLine" id="d520683e232-false-d521003e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:streetAddressLine" id="d520683e236-false-d521013e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:streetAddressLine" id="d520683e240-false-d521023e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:name
Item: (CI-SISRecordTarget)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.1']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:name" id="d519436e205-false-d521033e0">
        <extends rule="EN"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='EN' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISRecordTarget): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:EN", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
</pattern>