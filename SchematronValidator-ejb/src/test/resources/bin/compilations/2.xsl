<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:saxon="http://saxon.sf.net/" xmlns:schold="http://www.ascc.net/xml/schematron" xmlns:iso="http://purl.oclc.org/dsdl/schematron" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:xdw="urn:ihe:iti:xdw:2011" xmlns:cda="urn:hl7-org:v3" xmlns:hl7="urn:hl7-org:v3" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ws-ht="http://docs.oasis-open.org/ns/bpel4people/ws-humantask/types/200803" xmlns:ext2018="urn:rve:2018:xdw:extension" xmlns:ext2017="urn:rve:2017:xdw:extension" xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:ihe="urn:ihe:iti:xds-b:2007" xmlns:rim="urn:oasis:names:tc:ebxml-regrep:xsd:rim:3.0" xmlns:rs="urn:oasis:names:tc:ebxml-regrep:xsd:rs:3.0" xmlns:lcm="urn:oasis:names:tc:ebxml-regrep:xsd:lcm:3.0" xmlns:query="urn:oasis:names:tc:ebxml-regrep:xsd:query:3.0" xmlns:env="http://www.w3.org/2003/05/soap-envelope" xmlns:adt="urn:hl7-org:v2xml" xmlns:soapAssertion="http://schemas.xmlsoap.org/soap/envelope/" version="2.0"><!--Implementers: please note that overriding process-prolog or process-root is 
    the preferred method for meta-stylesheets to use where possible. -->
<xsl:param name="archiveDirParameter" /><xsl:param name="archiveNameParameter" /><xsl:param name="fileNameParameter" /><xsl:param name="fileDirParameter" /><xsl:variable name="document-uri"><xsl:value-of select="document-uri(/)" /></xsl:variable>

<!--PHASES-->


<!--PROLOG-->
<xsl:output xmlns:svrl="http://purl.oclc.org/dsdl/svrl" method="xml" omit-xml-declaration="no" standalone="yes" indent="yes" />

<!--XSD TYPES FOR XSLT2-->


<!--KEYS AND FUNCTIONS-->


<!--DEFAULT RULES-->


<!--MODE: SCHEMATRON-SELECT-FULL-PATH-->
<!--This mode can be used to generate an ugly though full XPath for locators-->
<xsl:template match="*" mode="schematron-select-full-path"><xsl:apply-templates select="." mode="schematron-get-full-path" /></xsl:template>

<!--MODE: SCHEMATRON-FULL-PATH-->
<!--This mode can be used to generate an ugly though full XPath for locators-->
<xsl:template match="*" mode="schematron-get-full-path"><xsl:apply-templates select="parent::*" mode="schematron-get-full-path" /><xsl:text>/</xsl:text><xsl:choose><xsl:when test="namespace-uri()=''"><xsl:value-of select="name()" /></xsl:when><xsl:otherwise><xsl:text>*:</xsl:text><xsl:value-of select="local-name()" /><xsl:text>[namespace-uri()='</xsl:text><xsl:value-of select="namespace-uri()" /><xsl:text>']</xsl:text></xsl:otherwise></xsl:choose><xsl:variable name="preceding" select="count(preceding-sibling::*[local-name()=local-name(current())                                   and namespace-uri() = namespace-uri(current())])" /><xsl:text>[</xsl:text><xsl:value-of select="1+ $preceding" /><xsl:text>]</xsl:text></xsl:template><xsl:template match="@*" mode="schematron-get-full-path"><xsl:apply-templates select="parent::*" mode="schematron-get-full-path" /><xsl:text>/</xsl:text><xsl:choose><xsl:when test="namespace-uri()=''">@<xsl:value-of select="name()" /></xsl:when><xsl:otherwise><xsl:text>@*[local-name()='</xsl:text><xsl:value-of select="local-name()" /><xsl:text>' and namespace-uri()='</xsl:text><xsl:value-of select="namespace-uri()" /><xsl:text>']</xsl:text></xsl:otherwise></xsl:choose></xsl:template>

<!--MODE: SCHEMATRON-FULL-PATH-2-->
<!--This mode can be used to generate prefixed XPath for humans-->
<xsl:template match="node() | @*" mode="schematron-get-full-path-2"><xsl:for-each select="ancestor-or-self::*"><xsl:text>/</xsl:text><xsl:value-of select="name(.)" /><xsl:if test="preceding-sibling::*[name(.)=name(current())]"><xsl:text>[</xsl:text><xsl:value-of select="count(preceding-sibling::*[name(.)=name(current())])+1" /><xsl:text>]</xsl:text></xsl:if></xsl:for-each><xsl:if test="not(self::*)"><xsl:text />/@<xsl:value-of select="name(.)" /></xsl:if></xsl:template><!--MODE: SCHEMATRON-FULL-PATH-3-->
<!--This mode can be used to generate prefixed XPath for humans 
	(Top-level element has index)-->
<xsl:template match="node() | @*" mode="schematron-get-full-path-3"><xsl:for-each select="ancestor-or-self::*"><xsl:text>/</xsl:text><xsl:value-of select="name(.)" /><xsl:if test="parent::*"><xsl:text>[</xsl:text><xsl:value-of select="count(preceding-sibling::*[name(.)=name(current())])+1" /><xsl:text>]</xsl:text></xsl:if></xsl:for-each><xsl:if test="not(self::*)"><xsl:text />/@<xsl:value-of select="name(.)" /></xsl:if></xsl:template>

<!--MODE: GENERATE-ID-FROM-PATH -->
<xsl:template match="/" mode="generate-id-from-path" /><xsl:template match="text()" mode="generate-id-from-path"><xsl:apply-templates select="parent::*" mode="generate-id-from-path" /><xsl:value-of select="concat('.text-', 1+count(preceding-sibling::text()), '-')" /></xsl:template><xsl:template match="comment()" mode="generate-id-from-path"><xsl:apply-templates select="parent::*" mode="generate-id-from-path" /><xsl:value-of select="concat('.comment-', 1+count(preceding-sibling::comment()), '-')" /></xsl:template><xsl:template match="processing-instruction()" mode="generate-id-from-path"><xsl:apply-templates select="parent::*" mode="generate-id-from-path" /><xsl:value-of select="concat('.processing-instruction-', 1+count(preceding-sibling::processing-instruction()), '-')" /></xsl:template><xsl:template match="@*" mode="generate-id-from-path"><xsl:apply-templates select="parent::*" mode="generate-id-from-path" /><xsl:value-of select="concat('.@', name())" /></xsl:template><xsl:template match="*" mode="generate-id-from-path" priority="-0.5"><xsl:apply-templates select="parent::*" mode="generate-id-from-path" /><xsl:text>.</xsl:text><xsl:value-of select="concat('.',name(),'-',1+count(preceding-sibling::*[name()=name(current())]),'-')" /></xsl:template>

<!--MODE: GENERATE-ID-2 -->
<xsl:template match="/" mode="generate-id-2">U</xsl:template><xsl:template match="*" mode="generate-id-2" priority="2"><xsl:text>U</xsl:text><xsl:number level="multiple" count="*" /></xsl:template><xsl:template match="node()" mode="generate-id-2"><xsl:text>U.</xsl:text><xsl:number level="multiple" count="*" /><xsl:text>n</xsl:text><xsl:number count="node()" /></xsl:template><xsl:template match="@*" mode="generate-id-2"><xsl:text>U.</xsl:text><xsl:number level="multiple" count="*" /><xsl:text>_</xsl:text><xsl:value-of select="string-length(local-name(.))" /><xsl:text>_</xsl:text><xsl:value-of select="translate(name(),':','.')" /></xsl:template><!--Strip characters--><xsl:template match="text()" priority="-1" />

<!--SCHEMA SETUP-->
<xsl:template match="/"><svrl:schematron-output xmlns:svrl="http://purl.oclc.org/dsdl/svrl" title="" schemaVersion=""><xsl:comment><xsl:value-of select="$archiveDirParameter" />   
				<xsl:value-of select="$archiveNameParameter" />  
				<xsl:value-of select="$fileNameParameter" />  
				<xsl:value-of select="$fileDirParameter" /></xsl:comment><svrl:ns-prefix-in-attribute-values uri="urn:ihe:iti:xdw:2011" prefix="xdw" /><svrl:ns-prefix-in-attribute-values uri="urn:hl7-org:v3" prefix="hl7" /><svrl:ns-prefix-in-attribute-values uri="http://www.w3.org/2001/XMLSchema-instance" prefix="xsi" /><svrl:ns-prefix-in-attribute-values uri="http://docs.oasis-open.org/ns/bpel4people/ws-humantask/types/200803" prefix="ws-ht" /><svrl:ns-prefix-in-attribute-values uri="urn:rve:2018:xdw:extension" prefix="ext2018" /><svrl:ns-prefix-in-attribute-values uri="urn:rve:2017:xdw:extension" prefix="ext2017" /><svrl:ns-prefix-in-attribute-values uri="http://www.w3.org/2003/05/soap-envelope" prefix="soap" /><svrl:ns-prefix-in-attribute-values uri="urn:oasis:names:tc:SAML:2.0:assertion" prefix="saml" /><svrl:ns-prefix-in-attribute-values uri="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" prefix="wsse" /><svrl:ns-prefix-in-attribute-values uri="urn:ihe:iti:xds-b:2007" prefix="ihe" /><svrl:ns-prefix-in-attribute-values uri="urn:oasis:names:tc:ebxml-regrep:xsd:rim:3.0" prefix="rim" /><svrl:ns-prefix-in-attribute-values uri="urn:oasis:names:tc:ebxml-regrep:xsd:rs:3.0" prefix="rs" /><svrl:ns-prefix-in-attribute-values uri="urn:oasis:names:tc:ebxml-regrep:xsd:lcm:3.0" prefix="lcm" /><svrl:ns-prefix-in-attribute-values uri="urn:oasis:names:tc:ebxml-regrep:xsd:query:3.0" prefix="query" /><svrl:ns-prefix-in-attribute-values uri="urn:ihe:iti:xdw:2011" prefix="xdw" /><svrl:ns-prefix-in-attribute-values uri="urn:hl7-org:v3" prefix="hl7" /><svrl:ns-prefix-in-attribute-values uri="http://www.w3.org/2001/XMLSchema-instance" prefix="xsi" /><svrl:ns-prefix-in-attribute-values uri="http://docs.oasis-open.org/ns/bpel4people/ws-humantask/types/200803" prefix="ws-ht" /><svrl:ns-prefix-in-attribute-values uri="urn:rve:2018:xdw:extension" prefix="ext2018" /><svrl:ns-prefix-in-attribute-values uri="urn:rve:2017:xdw:extension" prefix="ext2017" /><svrl:ns-prefix-in-attribute-values uri="http://www.w3.org/2003/05/soap-envelope" prefix="env" /><svrl:ns-prefix-in-attribute-values uri="urn:hl7-org:v2xml" prefix="adt" /><svrl:active-pattern><xsl:attribute name="document"><xsl:value-of select="document-uri(/)" /></xsl:attribute><xsl:apply-templates /></svrl:active-pattern><xsl:apply-templates select="/" mode="M22" /><svrl:ns-prefix-in-attribute-values uri="urn:ihe:iti:xdw:2011" prefix="xdw" /><svrl:ns-prefix-in-attribute-values uri="urn:hl7-org:v3" prefix="hl7" /><svrl:ns-prefix-in-attribute-values uri="http://www.w3.org/2001/XMLSchema-instance" prefix="xsi" /><svrl:ns-prefix-in-attribute-values uri="http://docs.oasis-open.org/ns/bpel4people/ws-humantask/types/200803" prefix="ws-ht" /><svrl:ns-prefix-in-attribute-values uri="urn:rve:2018:xdw:extension" prefix="ext2018" /><svrl:ns-prefix-in-attribute-values uri="urn:rve:2017:xdw:extension" prefix="ext2017" /><svrl:ns-prefix-in-attribute-values uri="http://www.w3.org/2003/05/soap-envelope" prefix="soap" /><svrl:ns-prefix-in-attribute-values uri="http://schemas.xmlsoap.org/soap/envelope/" prefix="soapAssertion" /><svrl:ns-prefix-in-attribute-values uri="urn:oasis:names:tc:SAML:2.0:assertion" prefix="saml" /><svrl:ns-prefix-in-attribute-values uri="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" prefix="wsse" /><svrl:ns-prefix-in-attribute-values uri="urn:ihe:iti:xds-b:2007" prefix="ihe" /><svrl:ns-prefix-in-attribute-values uri="urn:oasis:names:tc:ebxml-regrep:xsd:rim:3.0" prefix="rim" /><svrl:ns-prefix-in-attribute-values uri="urn:oasis:names:tc:ebxml-regrep:xsd:rs:3.0" prefix="rs" /><svrl:ns-prefix-in-attribute-values uri="urn:oasis:names:tc:ebxml-regrep:xsd:lcm:3.0" prefix="lcm" /><svrl:ns-prefix-in-attribute-values uri="urn:oasis:names:tc:ebxml-regrep:xsd:query:3.0" prefix="query" /><svrl:active-pattern><xsl:attribute name="document"><xsl:value-of select="document-uri(/)" /></xsl:attribute><xsl:apply-templates /></svrl:active-pattern><xsl:apply-templates select="/" mode="M38" /><svrl:active-pattern><xsl:attribute name="document"><xsl:value-of select="document-uri(/)" /></xsl:attribute><xsl:apply-templates /></svrl:active-pattern><xsl:apply-templates select="/" mode="M39" /></svrl:schematron-output></xsl:template>

<!--SCHEMATRON PATTERNS-->


<!--PATTERN -->
<xsl:variable name="MPI" select="2857784" /><xsl:variable name="SAR_request" select="document(concat('src/test/resources/bin/schematron/', 'SAR_request.xml'))" />

	<!--RULE -->
<xsl:template match="xdw:XDW.WorkflowDocument" priority="1001" mode="M22"><svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl" context="xdw:XDW.WorkflowDocument" />

		<!--ASSERT -->
<xsl:choose><xsl:when test="count(xdw:id)=1" /><xsl:otherwise><svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count(xdw:id)=1"><xsl:attribute name="location"><xsl:apply-templates select="." mode="schematron-select-full-path" /></xsl:attribute><svrl:text>
                Error1
            </svrl:text></svrl:failed-assert></xsl:otherwise></xsl:choose><xsl:apply-templates select="*|comment()|processing-instruction()" mode="M22" /></xsl:template>

	<!--RULE -->
<xsl:template match="xdw:XDW.WorkflowDocument/xdw:id" priority="1000" mode="M22"><svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl" context="xdw:XDW.WorkflowDocument/xdw:id" />

		<!--ASSERT -->
<xsl:choose><xsl:when test="count(@root)=1" /><xsl:otherwise><svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count(@root)=1"><xsl:attribute name="location"><xsl:apply-templates select="." mode="schematron-select-full-path" /></xsl:attribute><svrl:text>
                Error2
            </svrl:text></svrl:failed-assert></xsl:otherwise></xsl:choose><xsl:apply-templates select="*|comment()|processing-instruction()" mode="M22" /></xsl:template><xsl:template match="text()" priority="-1" mode="M22" /><xsl:template match="@*|node()" priority="-2" mode="M22"><xsl:apply-templates select="*|comment()|processing-instruction()" mode="M22" /></xsl:template>

<!--PATTERN -->


	<!--RULE -->
<xsl:template match="union/xdw:XDW.WorkflowDocument/xdw:author/xdw:assignedAuthor/hl7:id" priority="1001" mode="M38"><svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl" context="union/xdw:XDW.WorkflowDocument/xdw:author/xdw:assignedAuthor/hl7:id" /><xsl:variable name="author_tot" select="//union/soap:Envelope/soap:Body/query:AdhocQueryResponse/rim:RegistryObjectList/rim:ExtrinsicObject/rim:Classification[@classificationScheme = 'urn:uuid:93606bcf-9494-43ec-9b4e-a7748d1a838d']/rim:Slot[@name = 'authorPerson']/rim:ValueList/rim:Value" /><xsl:variable name="author" select="tokenize($author_tot,'\^')[1]" /><xsl:apply-templates select="*|comment()|processing-instruction()" mode="M38" /></xsl:template>

	<!--RULE -->
<xsl:template match="union/xdw:XDW.WorkflowDocument/xdw:workflowStatusHistory/xdw:documentEvent[xdw:actualStatus='OPEN']/xdw:eventTime" priority="1000" mode="M38"><svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl" context="union/xdw:XDW.WorkflowDocument/xdw:workflowStatusHistory/xdw:documentEvent[xdw:actualStatus='OPEN']/xdw:eventTime" /><xsl:variable name="serviceStartTime" select="//union/soap:Envelope/soap:Body/query:AdhocQueryResponse/rim:RegistryObjectList/rim:ExtrinsicObject/rim:Slot[@name = 'serviceStartTime']/rim:ValueList/rim:Value" /><xsl:variable name="eventTime_transl" select="translate(translate(translate(translate(text(),':',''),'T',''),'-',''),'Z','')" />

		<!--ASSERT -->
<xsl:choose><xsl:when test="(string-length($serviceStartTime)&gt;0 and $serviceStartTime=$eventTime_transl) or (string-length($serviceStartTime)=0 and string-length($eventTime_transl)&gt;0)" /><xsl:otherwise><svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="(string-length($serviceStartTime)&gt;0 and $serviceStartTime=$eventTime_transl) or (string-length($serviceStartTime)=0 and string-length($eventTime_transl)&gt;0)"><xsl:attribute name="location"><xsl:apply-templates select="." mode="schematron-select-full-path" /></xsl:attribute><svrl:text>
                Error3
            </svrl:text></svrl:failed-assert></xsl:otherwise></xsl:choose><xsl:apply-templates select="*|comment()|processing-instruction()" mode="M38" /></xsl:template><xsl:template match="text()" priority="-1" mode="M38" /><xsl:template match="@*|node()" priority="-2" mode="M38"><xsl:apply-templates select="*|comment()|processing-instruction()" mode="M38" /></xsl:template>

<!--PATTERN -->
<xsl:template match="text()" priority="-1" mode="M39" /><xsl:template match="@*|node()" priority="-2" mode="M39"><xsl:apply-templates select="*|comment()|processing-instruction()" mode="M39" /></xsl:template></xsl:stylesheet>

