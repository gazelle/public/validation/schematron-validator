package net.ihe.gazelle.schematron.converter;

import net.ihe.gazelle.schematron.model.Schematron;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.faces.Converter;
import org.jboss.seam.annotations.intercept.BypassInterceptors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import java.io.Serializable;

@BypassInterceptors
@Name("schematronConverter")
@Converter(forClass = Schematron.class)
public class SchematronConverter implements javax.faces.convert.Converter, Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5967636883630432606L;

	private static final Logger LOGGER = LoggerFactory.getLogger(SchematronConverter.class);

	public Object getAsObject(final FacesContext facesContext, final UIComponent uiComponent, final String value) {
		if(value != null)
		{
			try{
				final Integer id = Integer.parseInt(value);
				if (id != null)
				{
					final EntityManager em = (EntityManager) Component.getInstance("entityManager");
					final Schematron schematron = em.find(Schematron.class, id);
					return schematron;
				}
			}catch(final NullPointerException e){
				LOGGER.error(e.getMessage());
			}
		}
		return null;
	}

	public String getAsString(final FacesContext facesContext, final UIComponent uiComponent, final Object value) {
		if (value instanceof Schematron) {
			final Schematron schematron = (Schematron) value;
			return schematron.getId().toString();
		} else {
			return null;
		}
	}
	
	

}
