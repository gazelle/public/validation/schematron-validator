package net.ihe.gazelle.schematron.preferences;

import net.ihe.gazelle.common.Preferences.PreferencesKey;
import net.ihe.gazelle.common.servletfilter.CSPHeaderFilter;
import net.ihe.gazelle.common.servletfilter.CSPPoliciesPreferences;
import net.ihe.gazelle.preferences.ApplicationConfiguration;
import net.ihe.gazelle.preferences.ApplicationConfigurationDAO;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.preferences.exception.EntityConstraintException;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Synchronized;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.kohsuke.MetaInfServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FilenameFilter;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Name("applicationManager")
@Scope(ScopeType.PAGE)
@Synchronized(timeout = 60000)
@MetaInfServices(CSPPoliciesPreferences.class)
public class ApplicationManager implements Serializable, CSPPoliciesPreferences {


    private static final long serialVersionUID = 1L;
    private static final String DOT_EXTENSION = ".xml";
    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationManager.class);

    @In
    private ApplicationConfigurationDAO applicationConfigurationDAO;
    private ApplicationConfiguration preference = null;

    public List<ApplicationConfiguration> getAllPreferences() {
        return applicationConfigurationDAO.getAll();
    }

    public void savePreference(final ApplicationConfiguration inPreference) {
        try {
            applicationConfigurationDAO.save(inPreference);
            FacesMessages.instance().add(StatusMessage.Severity.INFO, "Preference saved");
        } catch (EntityConstraintException e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Cannot save preference: {0}", e.getMessage());
        }
    }

    public void createNewPreference() {
        this.setPreference(new ApplicationConfiguration());
    }

    private static FilenameFilter filter = new FilenameFilter() {
        @Override
        public boolean accept(final File dir, final String name) {
            return name.endsWith(DOT_EXTENSION);
        }
    };


    public void setPreference(final ApplicationConfiguration preference) {
        this.preference = preference;
    }

    public ApplicationConfiguration getPreference() {
        return this.preference;
    }

    public void resetHttpHeaders() {
        LOGGER.info("Reset http headers to default values");

        for (PreferencesKey preferencesKey : PreferencesKey.values()) {

            ApplicationConfiguration securityApplicationConfiguration = applicationConfigurationDAO.getByVariable(preferencesKey.getFriendlyName());
            if (securityApplicationConfiguration == null) {
                securityApplicationConfiguration = new ApplicationConfiguration();
                securityApplicationConfiguration.setVariable(preferencesKey.getFriendlyName());
            }
            securityApplicationConfiguration.setValue(preferencesKey.getDefaultValue());
            try {
                applicationConfigurationDAO.save(securityApplicationConfiguration);
            } catch (EntityConstraintException e) {
                FacesMessages.instance()
                        .add(StatusMessage.Severity.ERROR, "Cannot save preference '{0}': {1}", securityApplicationConfiguration.getVariable(),
                                e.getMessage());
            }
        }
        updateHttpHeaders();
    }

    public void updateHttpHeaders() {
        CSPHeaderFilter.clearCache();
    }


    public String getDocumentationUrl(){
        return PreferenceService.getString("application_documentation_url");
    }

    @Override
    public boolean isContentPolicyActivated() {
        return false;
    }

    @Override
    public Map<String, String> getHttpSecurityPolicies() {
        return null;
    }

    @Override
    public boolean getSqlInjectionFilterSwitch() {
        return false;
    }

}
