package net.ihe.gazelle.schematron.converter;

import net.ihe.gazelle.schematron.model.ObjectType;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.faces.Converter;
import org.jboss.seam.annotations.intercept.BypassInterceptors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import java.io.Serializable;

@BypassInterceptors
@Name("objectTypeConverter")
@Converter(forClass=ObjectType.class)
public class ObjectTypeConverter implements javax.faces.convert.Converter, Serializable{

	/**
	 *
	 */
	private static final long serialVersionUID = 6167087068631260012L;

	private static final Logger LOGGER = LoggerFactory.getLogger(ObjectTypeConverter.class);


	public Object getAsObject(final FacesContext facesContext, final UIComponent uiComponent, final String value) {
		if (value != null) {
			try{
				final Integer id = Integer.parseInt(value);
				if (id != null)
				{
					final EntityManager em = (EntityManager) Component.getInstance("entityManager");
					final ObjectType ot = em.find(ObjectType.class, id);
					return ot;
				}
			}catch(final NumberFormatException e){
				LOGGER.error(e.getMessage());
			}
		}
		return null;
	}

	public String getAsString(final FacesContext facesContext, final UIComponent uiComponent, final Object value) {
		if (value instanceof ObjectType) {
			final ObjectType ot = (ObjectType) value;
			return ot.getId().toString();
		}
		else {
			return null;
		}
	}

}
