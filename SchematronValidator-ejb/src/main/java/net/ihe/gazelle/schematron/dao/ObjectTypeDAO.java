package net.ihe.gazelle.schematron.dao;

import net.ihe.gazelle.schematron.model.ObjectType;

import javax.ejb.Local;
import java.util.List;

@Local
public interface ObjectTypeDAO extends FilteredDAO<ObjectType> {

    ObjectType getById(Integer id);

    ObjectType getByKeyword(String inKeyword);

    List<ObjectType> getAllObjectTypes();

    ObjectType saveObjectType(ObjectType ObjectType);

    void deleteObjectType(ObjectType objectType);

    boolean isObjectTypeDeletable(final ObjectType objectType);
}
