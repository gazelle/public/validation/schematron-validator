package net.ihe.gazelle.schematron.transformation;

import net.ihe.gazelle.schematron.model.Schematron;
import net.ihe.gazelle.transformation.service.CompilationException;
import net.ihe.gazelle.transformation.service.ObjectNotFoundException;
import net.ihe.gazelle.transformation.service.TransformationException;

import java.net.MalformedURLException;

public interface DfdlTransformation {

    public String doDfdlTransformation(Schematron schematron, byte[] objToValidate) throws MalformedURLException, CompilationException, TransformationException, ObjectNotFoundException;
}
