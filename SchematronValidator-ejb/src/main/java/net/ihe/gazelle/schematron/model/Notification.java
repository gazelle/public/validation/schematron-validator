package net.ihe.gazelle.schematron.model;

/**
 * 
 * @author abderrazek boufahja
 *
 */
public class Notification {
	
	private String test;
	
	private String context;
	
	private String severity;
	
	private String message;
	
	public Notification(){
	}

	public Notification(String test, String context, String severity, String message) {
		this.test = test;
		this.context = context;
		this.severity = severity;
		this.message = message;
	}
	
	public String getTest() {
		return this.test;
	}

	public void setTest(String test) {
		this.test = test;
	}

	public String getContext() {
		return this.context;
	}

	public void setContext(String context) {
		this.context = context;
	}

	public String getSeverity() {
		return this.severity;
	}

	public void setSeverity(String severity) {
		this.severity = severity;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	
}
