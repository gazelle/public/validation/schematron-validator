package net.ihe.gazelle.schematron.util;

import net.ihe.gazelle.common.marshaller.ObjectMarshaller;
import net.ihe.gazelle.common.marshaller.ObjectMarshallerImpl;
import net.ihe.gazelle.validation.DetailedResult;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBException;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;

@AutoCreate
@Name("detailedResultMarshaller")
public class DetailedResultMarshallerImpl implements DetailedResultMarshaller {

    private static final Logger LOGGER = LoggerFactory.getLogger(DetailedResultMarshallerImpl.class);

    void marshall(DetailedResult dr, OutputStream os) throws JAXBException {
        ObjectMarshaller marshaller = new ObjectMarshallerImpl(DetailedResult.class);
        marshaller.marshall(dr, os);
    }

    public String getDetailedResultAsString(DetailedResult dr) {
        if (dr != null) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            try {
                marshall(dr, baos);
            } catch (JAXBException e) {
                LOGGER.error(e.getMessage(), e);
            }
            String res = baos.toString();
            if (res.contains("?>")) {
                res = res.substring(res.indexOf("?>") + 2);
            }
            return res;
        }
        return null;
    }

}
