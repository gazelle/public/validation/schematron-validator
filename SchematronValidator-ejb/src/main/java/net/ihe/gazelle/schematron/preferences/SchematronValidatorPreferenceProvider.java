package net.ihe.gazelle.schematron.preferences;

import net.ihe.gazelle.preferences.ApplicationConfiguration;
import net.ihe.gazelle.preferences.ApplicationConfigurationQuery;
import net.ihe.gazelle.preferences.PreferenceProvider;
import org.jboss.seam.contexts.Contexts;
import org.kohsuke.MetaInfServices;

import java.util.Date;

@MetaInfServices(PreferenceProvider.class)
public class SchematronValidatorPreferenceProvider implements PreferenceProvider {

    @Override
    public int compareTo(PreferenceProvider o) {
        return this.getWeight().compareTo(o.getWeight());
    }

    @Override
    public Boolean getBoolean(String key) {
        String prefAsString = this.getString(key);
        if ((prefAsString != null) && !prefAsString.isEmpty()) {
            return Boolean.valueOf(prefAsString);
        } else {
            return false;
        }
    }

    @Override
    public Date getDate(String arg0) {
        return null;
    }

    @Override
    public Integer getInteger(String key) {
        String prefAsString = this.getString(key);
        if ((prefAsString != null) && !prefAsString.isEmpty()) {
            try {
                return Integer.decode(prefAsString);
            } catch (NumberFormatException e) {
                return null;
            }
        } else {
            return null;
        }
    }

    @Override
    public Object getObject(Object arg0) {
        return null;
    }

    @Override
    public String getString(String key) {
        ApplicationConfiguration appConfig = getApplicationConfiguration(key);
        return appConfig != null ? appConfig.getValue() : null;
    }

    @Override
    public Integer getWeight() {
        if (Contexts.isApplicationContextActive()) {
            return -100;
        } else {
            return 100;
        }
    }

    @Override
    public void setBoolean(String arg0, Boolean arg1) {

    }

    @Override
    public void setDate(String arg0, Date arg1) {

    }

    @Override
    public void setInteger(String arg0, Integer arg1) {

    }

    @Override
    public void setObject(Object arg0, Object arg1) {

    }

    @Override
    public void setString(String arg0, String arg1) {
    }

    public SchematronValidatorPreferenceProvider() {
        super();
    }

    @Override
    public boolean equals(Object o){
        if (o == null){
            return false;
        }
        if (o instanceof PreferenceProvider){
            PreferenceProvider pp = (PreferenceProvider)o;
            return this.getWeight().equals(pp.getWeight());
        }else{
            return false;
        }
    }

    @Override public int hashCode() {
        final int prime = 78;
        int result = super.hashCode();
        result = prime * result + ((this.getWeight() == null) ? 0 : this.getWeight());
        return result;
    }

    private ApplicationConfiguration getApplicationConfiguration(String variable) {
        // TODO [ceoche] mutualise cache used in net.ihe.gazelle.preferences.ApplicationConfigurationDAOImpl to optimize db
        // access, then add key-value in cache here
        ApplicationConfigurationQuery query = new ApplicationConfigurationQuery();
        query.variable().eq(variable);
        return query.getUniqueResult();
    }
}
