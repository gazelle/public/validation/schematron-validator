/*
 * Copyright 2013 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.schematron.model;

import org.jboss.seam.annotations.Name;

import javax.faces.model.SelectItem;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.List;

/**
 * Class Description :  Schematron
 * This class describes the Schematron object against the one the files can be validated (CDA file, HL7v3 messages)
 * Schematron possesses the following attributes :
 * <p>
 * id : id of the message in database
 * name : name of the schematron (must be clear its the name which is display for users)
 * version : version of the schematron
 * author : author of the schematron
 * label : is made of the name and the version of the schematron
 * description : short description of the schematron usage
 * path : path from the application bin/schematrons directory
 * xsdpath : path to the XSD used for checking the document to validate is valid
 * xsdVersion : XSD Version used for checking the document to validate (XSD 1.0 or XSD 1.1)
 * available : whether the schematron can be used or not
 * objectType : the type of object (CDA, HL7v3) which can be validate by this schematron
 *
 * @author Anne-Gaelle Berge / INRIA Rennes IHE development Project
 * @version 1.0 - 2010, July 8th
 * @class Schematron.java
 * @package net.ihe.gazelle.sch.validator.model
 * @see anne-gaelle.berge@inria.fr -  http://www.ihe-europe.org
 */

@Entity
@Name("schematron")
@Table(name = "sch_validator_schematron", schema = "public", uniqueConstraints = {
        @UniqueConstraint(columnNames = "keyword", name = "uk_sch_validator_schematron_keyword")})
@SequenceGenerator(name = "sch_validator_schematron_sequence", sequenceName = "sch_validator_schematron_id_seq", allocationSize = 1)
public class Schematron extends AuditModule implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -6619466590020344345L;
    public static final String XSD_VERSION_10 = "1.0";
    public static final String XSD_VERSION_11 = "1.1";
    public static final String TYPE_STANDARD = "Standard";
    public static final String TYPE_ART_DECOR = "ART-DECOR";
    private static List<SelectItem> xsdVersions;
    private static List<SelectItem> schematronTypes;

    @Id
    @NotNull
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue(generator = "sch_validator_schematron_sequence", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name = "keyword", nullable = false, unique = true)
    private String keyword;

    @Column(name = "version", nullable = false)
    private String version;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "path")
    private String path;

    @Column(name = "available")
    private boolean available;

    @Column(name = "author", nullable = false)
    private String author;

    // Schematron type : standard or ART-DECOR
    @Column(name = "type")
    private String type;

    @Column(name = "xsd_path")
    private String xsdPath;

    // Version of XSD to be used for the XSD validation (1.1 or 1.0) Default value is 1.0
    @Column(name = "xsd_version", nullable = false)
    private String xsdVersion;

    @Column(name = "provider")
    private String provider;

    @Column(name = "need_report_generation")
    private Boolean needReportGeneration;

    @Column(name = "transform_unknowns")
    private Boolean transformUnknowns;

    @OneToOne
    @JoinColumn(name = "object_type_id")
    private ObjectType objectType;

    @Column(name = "dfdl_transformation_needed")
    private Boolean dfdlTransformationNeeded = false;

    @Column(name = "dfdl_schema_keyword")
    private String dfdlSchemaKeyword;

    @Column(name = "use_relative_links")
    private Boolean useRelativeLinks;

    /**
     * Constructors
     */
    public Schematron() {
    }

    public Schematron(final Schematron inSchematron) {
        if (inSchematron != null) {
            this.keyword = inSchematron.getKeyword().concat("_COPY");
            this.version = inSchematron.getVersion().concat(".copy");
            this.name = inSchematron.getName().concat("_COPY");
            this.description = inSchematron.getDescription();
            this.path = inSchematron.getPath();
            this.xsdPath = inSchematron.getXsdPath();
            this.xsdVersion = inSchematron.getXsdVersion();
            this.objectType = inSchematron.getObjectType();
            this.provider = inSchematron.getProvider();
            this.needReportGeneration = inSchematron.getNeedReportGeneration();
            this.transformUnknowns = inSchematron.getTransformUnknowns();
            this.author = inSchematron.getAuthor();
            this.type = inSchematron.getType();
            this.dfdlTransformationNeeded = inSchematron.getDfdlTransformationNeeded();
            this.dfdlSchemaKeyword = inSchematron.getDfdlSchemaKeyword();
            this.useRelativeLinks = inSchematron.getUseRelativeLinks();
        }
    }

    public boolean transformUnknownsValue() {
        if (this.transformUnknowns == null) {
            return false;
        }
        return this.transformUnknowns;
    }

    public Boolean getTransformUnknowns() {
        return this.transformUnknowns;
    }

    public void setTransformUnknowns(final Boolean transformUnknowns) {
        this.transformUnknowns = transformUnknowns;
    }

    public Boolean getNeedReportGeneration() {
        return this.needReportGeneration;
    }

    public void setNeedReportGeneration(final Boolean needReportGeneration) {
        this.needReportGeneration = needReportGeneration;
    }

    public String getProvider() {
        return this.provider;
    }

    public void setProvider(final String provider) {
        this.provider = provider;
    }

    public Integer getId() {
        return this.id;
    }


    public void setId(final Integer id) {
        this.id = id;
    }


    public String getKeyword() {
        return this.keyword;
    }


    public void setKeyword(final String keyword) {
        this.keyword = keyword;
    }

    public String getVersion() {
        return this.version;
    }


    public void setVersion(final String version) {
        this.version = version;
    }

    public String getDescription() {
        return this.description;
    }


    public void setDescription(final String description) {
        this.description = description;
    }


    public void setName(final String name) {
        this.name = name;
    }

    @XmlTransient
    public String getType() {
        return this.type;
    }

    public void setType(final String paramType) {
        this.type = paramType;
    }


    public String getName() {
        return this.name;
    }


    public String getPath() {
        return this.path;
    }


    public void setPath(final String path) {
        this.path = path;
    }


    public void setAvailable(final boolean available) {
        this.available = available;
    }


    public boolean isAvailable() {
        return this.available;
    }


    public void setXsdPath(final String xsdPath) {
        this.xsdPath = xsdPath;
    }

    public String getXsdPath() {
        return this.xsdPath;
    }

    public void setXsdVersion(final String version) {
        this.xsdVersion = version;
    }

    public String getXsdVersion() {
        return this.xsdVersion;
    }

    public void setAuthor(final String paramAuthor) {
        this.author = paramAuthor;
    }

    public String getAuthor() {
        return this.author;
    }

    public ObjectType getObjectType() {
        return this.objectType;
    }

    public void setObjectType(final ObjectType objectType) {
        this.objectType = objectType;
    }

    public Boolean getDfdlTransformationNeeded() {
        return dfdlTransformationNeeded;
    }

    public void setDfdlTransformationNeeded(Boolean dfdlTransformationNeeded) {
        this.dfdlTransformationNeeded = dfdlTransformationNeeded;
    }

    public String getDfdlSchemaKeyword() {
        return dfdlSchemaKeyword;
    }

    public void setDfdlSchemaKeyword(String dfdlSchemaKeyword) {

        this.dfdlSchemaKeyword = dfdlSchemaKeyword;
    }

    public Boolean getUseRelativeLinks() {
        return useRelativeLinks;
    }

    public void setUseRelativeLinks(Boolean useRelativeLinks) {
        this.useRelativeLinks = useRelativeLinks;
    }


    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (this.available ? 1231 : 1237);
        result = prime * result
                + ((this.description == null) ? 0 : this.description.hashCode());
        result = prime * result + ((this.name == null) ? 0 : this.name.hashCode());
        result = prime * result + ((this.keyword == null) ? 0 : this.keyword.hashCode());
        result = prime * result + ((this.path == null) ? 0 : this.path.hashCode());
        result = prime * result + ((this.version == null) ? 0 : this.version.hashCode());
        result = prime * result + ((this.xsdPath == null) ? 0 : this.xsdPath.hashCode());
        result = prime * result + ((this.type == null) ? 0 : this.type.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final Schematron other = (Schematron) obj;
        if (this.available != other.available) {
            return false;
        }
        if (this.description == null) {
            if (other.description != null) {
                return false;
            }
        } else if (!this.description.equals(other.description)) {
            return false;
        }
        if (this.name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!this.name.equals(other.name)) {
            return false;
        }
        if (this.keyword == null) {
            if (other.keyword != null) {
                return false;
            }
        } else if (!this.keyword.equals(other.keyword)) {
            return false;
        }
        if (this.path == null) {
            if (other.path != null) {
                return false;
            }
        } else if (!this.path.equals(other.path)) {
            return false;
        }
        if (this.version == null) {
            if (other.version != null) {
                return false;
            }
        } else if (!this.version.equals(other.version)) {
            return false;
        }
        if (this.author == null) {
            if (other.author != null) {
                return false;
            }
        } else if (!this.author.equals(other.author)) {
            return false;
        }
        if (this.type == null) {
            if (other.type != null) {
                return false;
            }
        } else if (!this.type.equals(other.type)) {
            return false;
        }
        if (this.xsdPath == null) {
            if (other.xsdPath != null) {
                return false;
            }
        } else if (!this.xsdPath.equals(other.xsdPath)) {
            return false;
        }
        if (this.xsdVersion == null) {
            if (other.xsdVersion != null) {
                return false;
            }
        } else if (!this.xsdVersion.equals(other.xsdVersion)) {
            return false;
        }
        return true;
    }

}
